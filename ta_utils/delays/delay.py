"""
Delay Module
"""
from datetime import datetime


class Delay(object):
    """
    The Delay Object represents a delay in a signal.
    """
    def __init__(self, reading_date, processed_date):
        """
        Initialize the Delay with the reading date and processed date.

        :param reading_date: The reading datetime where there is a delay
        :type reading_date: datetime
        :param processed_date: The processed datetime where there is a delay
        :type processed_date: datetime
        """
        self.reading_date = reading_date
        self.processed_date = processed_date

    @property
    def age(self):
        """
        Get the age of this delay in seconds.

        :return: Age of this delay in seconds.
        :rtype: int
        """
        return int((self.processed_date - self.reading_date).total_seconds())

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Delay):
            return False

        processed_eq = self.processed_date == other.processed_date
        reading_eq = self.reading_date == other.reading_date

        return processed_eq and reading_eq
