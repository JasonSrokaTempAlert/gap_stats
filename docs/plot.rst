.. plot:

Plot
====

TempAlert Utilities contains a set of plotting functions for visualizing data.

Below are examples of different plot functions for various data types:

***********
Plot Signal
***********

.. automodule:: ta_utils.plot.signal
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__

********
Examples
********

Here is an example of plotting a sine wave signal::

    from datetime import datetime, timedelta

    import pandas as pd
    import numpy as np

    from ta_utils import Signal
    from ta_utils.plot import plot_signal

    minutes = np.arange(0, 60 * 24 * 6, 15)
    dates = [datetime.now() + timedelta(minutes=(i)) for i in minutes]

    sine = np.sin(2. * np.pi * minutes / (60. * 24.)) + 2
    trivial_signal = Signal(pd.Series(data=sine, index=dates))

    plot_signal(trivial_signal, filename="test_plot.png", degrees='C', title='Signal Title',
                range_min=1.4, range_max=2.6)


.. image:: test_plot.png
    :alt: Sine Wave Signal Plot

And here is an example of plotting that same sine wave histogram::

    from ta_utils.plot import plot_signal_histogram

    plot_signal_histogram(trivial_signal, filename="test_histogram.png", degrees='C', title='Signal Histogram Title',
                      range_min=1.4, range_max=2.6)


.. image:: test_histogram.png
    :alt: Sine Wave Signal Histogram
