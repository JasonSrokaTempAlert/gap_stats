.. incremental_signal:

Incremental Signal
==================

.. autoclass:: ta_utils.IncrementalSignal
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__
