.. update_pipeline:

Update Pipeline
===============

.. autoclass:: ta_utils.UpdatePipeline
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__
