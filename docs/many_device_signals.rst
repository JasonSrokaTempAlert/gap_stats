.. many_device_signals:

ManyDeviceSignals
=================

.. autoclass:: ta_utils.ManyDeviceSignals
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__
