"""
Update Pipeline Module
"""
import pandas as pd

from ta_utils import ManyDeviceSignals


class UpdatePipeline(object):
    def __init__(self, filenames, device_id_col, date_col, signal_cols):
        """
        Create an update pipeline for a list of file names and various settings.

        :param filenames: A list of file paths as strings.
        :type filenames: list[str]
        :param device_id_col: The column name that contains the device IDs.
        :type device_id_col: str
        :param date_col: The column name that contains the signal dates.
        :type date_col: str
        :param signal_cols: A list of column names of the signals we want to update.
        :type signal_cols: list[str]
        """
        self.filenames = filenames
        self.device_id_col = device_id_col
        self.date_col = date_col
        self.signal_cols = signal_cols

    def run(self):
        """
        Update the persisted signals from the list of file names.

        :return: None
        """
        for filename in self.filenames:
            data = pd.read_csv(filename)
            many_device_signals = ManyDeviceSignals(data,
                                                    self.device_id_col,
                                                    self.date_col)
            for signal_col in self.signal_cols:
                many_device_signals.update_all_signals(signal_col)
