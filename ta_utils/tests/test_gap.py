"""
Test Gap Module
"""

import unittest
from datetime import datetime, timedelta

from ta_utils import Gap

SECONDS_IN_DAY = 60 * 60 * 24

now = datetime.now()
trivial_gap = Gap(now, now + timedelta(days=1), now + timedelta(days=2))
other_trivial_gap = Gap(now + timedelta(days=1), now + timedelta(days=2), now + timedelta(days=3))
same_trivial_gap = Gap(now, now + timedelta(days=1), now + timedelta(days=2))


class GapTestCase(unittest.TestCase):
    def test_gap_length(self):
        self.assertEqual(SECONDS_IN_DAY, trivial_gap.length)

    def test_gap_equals(self):
        self.assertFalse(trivial_gap == None)
        self.assertFalse(trivial_gap == trivial_gap.length)
        self.assertFalse (other_trivial_gap == trivial_gap)
        self.assertEqual(same_trivial_gap, trivial_gap)
