"""
Windows Module
"""
import numpy as np
import pandas as pd

from ta_utils.signals.signal import Signal

class Windows(object):
    """
    The Windows object represents a series of Signal windows generated from a single Signal.
    """
    def __init__(self, signals):
        """
        Initializes a Windows object.

        :param signals: A list of Signals.
        :type signals: list[Signal]
        """
        self.signals = signals

    @classmethod
    def sliding_windows_by_index(cls, signal, window_size, step_size=1):
        """
        Generates sliding windows by index (rather than time).

        :param signal: The signal to generate the windows from.
        :type signal: Signal
        :param window_size: The size of each window (in terms of indexes).
        :type window_size: int
        :param step_size: The step size between each window.
        :type step_size: int
        :return: A Windows object representing these sliding windows.
        """
        windows = []

        for i in range(0, len(signal) - window_size + 1, step_size):
            window = signal[i:(i + window_size)]
            windows.append(window)

        return cls(windows)

    @classmethod
    def sliding_windows_by_time(cls, signal, window_size,
                                step_size=pd.Timedelta(minutes=15)):
        """
        Generates sliding windows by time (rather than index).

        :param signal: The signal to generate the windows from.
        :type signal: Signal
        :param window_size: The size of each window (in terms of indexes).
        :type window_size: pd.Timedelta
        :param step_size: The step size between each window.
        :type step_size: pd.Timedelta
        :return: A Windows object representing these sliding windows.
        """
        min_time = signal.min_time
        max_time = signal.max_time

        windows = []

        window_start = min_time
        while window_start + window_size <= max_time:
            window = signal.between(window_start, window_start + window_size)
            windows.append(window)
            window_start += step_size

        return cls(windows)

    def mean(self, func):
        """
        The mean value of the function applied to each signal.

        :param func: A function that returns some number for each Signal.
        :type func: (Signal) -> int|float
        :return: Mean of function applied to each signal
        :rtype: float
        """
        return np.mean([func(signal) for signal in self.signals])

    def std(self, func):
        """
        The standard deviation of the function applied to each signal.

        :param func: A function that returns some number for each Signal.
        :type func: (Signal) -> int|float
        :return: Standard deviation of function applied to each signal
        :rtype: float
        """
        return np.std([func(signal) for signal in self.signals])

    def var(self, func):
        """
        The variance of the function applied to each signal.

        :param func: A function that returns some number for each Signal.
        :type func: (Signal) -> int|float
        :return: Variance of function applied to each signal
        :rtype: float
        """
        return np.var([func(signal) for signal in self.signals])

    def min(self, func):
        """
        The minimum value of the function applied to each signal.

        :param func: A function that returns some number for each Signal.
        :type func: (Signal) -> int|float
        :return: Minimum of function applied to each signal
        :rtype: float
        """
        return min([func(signal) for signal in self.signals])

    def max(self, func):
        """
        The maximum value of the function applied to each signal.

        :param func: A function that returns some number for each Signal.
        :type func: (Signal) -> int|float
        :return: Maximum of function applied to each signal
        :rtype: float
        """
        return max([func(signal) for signal in self.signals])

    def mid(self, func):
        """
        The average of the min and max values of this signal.

        :return: Mid value of signal
        :rtype: float
        """
        return float(self.min(func) + self.max(func)) / 2.

    def __len__(self):
        return len(self.signals)
