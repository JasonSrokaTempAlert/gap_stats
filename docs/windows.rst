.. windows:

Windows
=======

.. autoclass:: ta_utils.Windows
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__
