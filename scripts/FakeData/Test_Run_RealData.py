deviceid= 11666000000133535681
strdeviceid="'{}'".format(deviceid)
q1 = """Select * from sensor_temperature_history where device_id = 11666000000133535681 order by reading_date desc;"""
q2 = """Select * from signals where device_id=%s ;"""%(strdeviceid)
q3 = """Select c.* from signal_segments c join signals b on b.id=c.signal_id where device_id=%s order by start_time desc;"""%(strdeviceid)
query1 = """delete from signal_segments c using signals b where b.id=c.signal_id and b.device_id = '11666000000133535681';"""
query2 = """delete from signals b where b.device_id = '11666000000133535681';"""
query = """delete from sensor_temperature_history where device_id = 11666000000133535681;"""



import os,sys,inspect
import EnvironmentTesting

current_dir= os.getcwd()
values = EnvironmentTesting.dirfind(current_dir)
env_tag = EnvironmentTesting.gen_env_tag(current_dir)
testing_dir = values[0]
scripts_dir = values[1]

import numpy as np
import pandas as pd
import psycopg2
import pandas.io.sql as psql
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 1000)
import time

database = EnvironmentTesting.lookup_dbname_forenv(env_tag)
user = 'jason'
password = 'qvz2LFRzMMVPku7'
host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com'
port = '5432'
m= '                                '
conn_signalsdb = psycopg2.connect(database = database, user = user, password = password,host = host, port = port,sslmode = 'require')
conn_warehouse = psycopg2.connect(database = 'warehouse', user = user, password = password,host = host, port = port,sslmode = 'require')

cursor_signalsdb = conn_signalsdb.cursor()
cursor_signalsdb.execute(query1)
conn_signalsdb.commit()      
cursor_signalsdb.execute(query2)
conn_signalsdb.commit()      
time.sleep(1)

cursor_warehouse = conn_warehouse.cursor()
cursor_warehouse.execute(query)
conn_warehouse.commit()

time.sleep(1)
execfile(scripts_dir+'/update_tempandchan_history_tables.py')
print("Device in the sensor temp history table first run")
q1 = psql.read_sql(q1, conn_warehouse)
print(q1.head())

print "\n\n****\nRunning Gaps_Gen for first time\n****\n\n"
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsbefore = psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsbefore = psql.read_sql(q3, conn_warehouse)



print "\n\n****\nRunning Gaps_Gen for second time\n****\n\n"
print 'works'
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsafter = psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsafter = psql.read_sql(q3, conn_warehouse)


print "\n\n****\nRunning Gaps_Gen for third time\n****\n\n"
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsafter2nd= psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsafter2nd = psql.read_sql(q3, conn_warehouse)


writer = pd.ExcelWriter((testing_dir+'/Test_Run_RealData{tag}.xlsx'.format(tag = env_tag)))
signals_resultsbefore.to_excel(writer,'signals_results_before',index=False)
signal_segmentsresultsbefore.to_excel(writer,'signal_segments_resultsbefore',index=False)
signals_resultsafter.to_excel(writer,'signals_resultsafter',index=False)
signal_segmentsresultsafter.to_excel(writer,'signal_segments_resultsafter',index=False)
signals_resultsafter2nd.to_excel(writer,'s_resultsafter2nd',index=False)
signal_segmentsresultsafter2nd.to_excel(writer,'ss_resultsafter2nd',index=False)
writer.save()

conn_signalsdb.close()
conn_warehouse.close()
cursor_signalsdb.close()
cursor_warehouse.close()
