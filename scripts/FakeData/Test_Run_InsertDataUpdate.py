deviceid= 11666000000133535681
strdeviceid="'{}'".format(deviceid)
q1 = """Select * from sensor_temperature_history where device_id = 11666000000133535681 and port=0 order by reading_date desc;"""
q2 = """Select * from signals where device_id=%s and port=0 ;"""%(strdeviceid)
q3 = """Select c.* from signal_segments c join signals b on b.id=c.signal_id where device_id=%s and b.port=0 order by start_time desc;"""%(strdeviceid)
query1 = """delete from signal_segments c using signals b where b.id=c.signal_id and b.device_id = '11666000000133535681' and b.port=0;"""
query2 = """delete from signals b where b.device_id = '11666000000133535681' and b.port=0;"""
query = """delete from sensor_temperature_history where str_device_id= 'Device:11666000000133535681:0';"""
query3 = """delete from trundle_access_history where deviceid = '11666000000133535681';"""


import os,sys,inspect
import EnvironmentTesting
current_dir= os.getcwd()
values = EnvironmentTesting.dirfind(current_dir)
env_tag = EnvironmentTesting.gen_env_tag(current_dir)
testing_dir = values[0]
scripts_dir = values[1]
import numpy as np
import pandas as pd
import psycopg2
import pandas.io.sql as psql
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 1000)
import time
from datetime import datetime



database = EnvironmentTesting.lookup_dbname_forenv(env_tag)
user = 'jason'
password = 'qvz2LFRzMMVPku7'
host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com'
port = '5432'
m= '                                '
conn_testrun = psycopg2.connect(database = database, user = user, password = password,host = host, port = port,sslmode = 'require')
conn_warehouse = psycopg2.connect(database = 'warehouse', user = user, password = password,host = host, port = port,sslmode = 'require')

x = conn_warehouse.cursor()
x.execute(query1)
print("Deleted signal segments")
conn_warehouse.commit()      
x.execute(query2)
conn_warehouse.commit()   
print("Deleted signals")
time.sleep(1)
x.execute(query)
conn_warehouse.commit()
print("Deleted Temperature")
x.execute(query3)
conn_warehouse.commit()
print("Deleted trundle")
x.close()

df = pd.read_excel('Temp.xlsx')
df_before= df.loc[(df['reading_date'] <= '2017-09-30 16:40:54.917')]
df_before.reset_index(drop=True)
print df_before.head()
print df_before.shape
df_after= df.loc[(df['reading_date'] > '2017-09-30 16:40:54.917')]
df_after.reset_index(drop=True)
print df_after.head()
print df_after.shape

print "\n\n****\nInsert Sensor Temp Data for first time \n****\n\n"
x = conn_warehouse.cursor()
appended_data = []
for index, row in df_before.iterrows():  
    if index % 100 == 0 : print index, 'done'
    a= row['device_id']
    b= row['reading_date']
    c= row['processed_date']
    d= row['temperature']
    e= row['age']
    f= row['port']
    g= row['str_device_id']   
    InsertQuery = """INSERT INTO sensor_temperature_history
            values (%s,'%s','%s',%s,%s,%s,'%s')"""%(a,b,c,d,e,f,g)
    x.execute(InsertQuery)
x.close()
conn_warehouse.commit()
print("Device in the sensor temp history table first run")
q1 = psql.read_sql(q1, conn_warehouse)
print(q1.head())

print "\n\n****\nUpdating Trundle History\n****\n\n"
# time=datetime.now()
#time= '2017-09-30 16:40:54.917' # Reading Time
time= '2017-09-30 16:41:00.349' # Processed Time
# time = '2017-09-30 16:46:54.917' # 6 mins ahead of Reading Time
# time = '2017-09-30 23:46:54.917' # 7 hours ahead of Reading Time
print time
x = conn_warehouse.cursor()
trundlehistory_update = """Insert into  trundle_access_history values ('11666000000133535681', 0, '%s', 'temperature')"""%(time)
x.execute(trundlehistory_update)
conn_warehouse.commit()

print "\n\n****\nRunning Gaps_Gen for first time\n****\n\n"
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsbefore = psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsbefore = psql.read_sql(q3, conn_warehouse)

print "\n\n****\nInsert Sensor Temp Data for second time \n****\n\n"
x = conn_warehouse.cursor()
appended_data = []
for index, row in df_after.iterrows():  
    if index % 100 == 0 : print index, 'done'
    a= row['device_id']
    b= row['reading_date']
    c= row['processed_date']
    d= row['temperature']
    e= row['age']
    f= row['port']
    g= row['str_device_id']   
    InsertQuery = """INSERT INTO sensor_temperature_history
            values (%s,'%s','%s',%s,%s,%s,'%s')"""%(a,b,c,d,e,f,g)
    x.execute(InsertQuery)
x.close()
conn_warehouse.commit()


print "\n\n****\nUpdating Trundle History for second time\n****\n\n"
time=datetime.now()
#time= '2017-10-18 14:09:04.430' # Reading Time
#time= '2017-10-18 14:09:07.001' # Processed Time
#time = '2017-10-18 14:15:04.430' # 6 mins ahead of Reading Time
# time = '2017-10-18 21:09:04.430' # 7 hours ahead of Reading Time
print time
x = conn_warehouse.cursor()
trundlehistory_update = "update trundle_access_history set update_time = '%s'   where deviceid ='11666000000133535681';"%(time)
x.execute(trundlehistory_update)
conn_warehouse.commit()

print "\n\n****\nRunning Gaps_Gen for second time\n****\n\n"
print 'works'
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsafter = psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsafter = psql.read_sql(q3, conn_warehouse)

print "\n\n****\nRunning Gaps_Gen for third time\n****\n\n"
print 'works'
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsafter2 = psql.read_sql(q2, conn_warehouse)
signal_segmentsresultsafter2 = psql.read_sql(q3, conn_warehouse)

writer = pd.ExcelWriter((testing_dir+'/Test_Run_InsertDataUpdate{tag}.xlsx'.format(tag = env_tag)))
signals_resultsbefore.to_excel(writer,'signals_results_before',index=False)
signal_segmentsresultsbefore.to_excel(writer,'signal_segments_resultsbefore',index=False)
signals_resultsafter.to_excel(writer,'signals_resultsafter',index=False)
signal_segmentsresultsafter.to_excel(writer,'signal_segments_resultsafter',index=False)
signals_resultsafter.to_excel(writer,'signals_resultsafter2',index=False)
signal_segmentsresultsafter.to_excel(writer,'signal_segments_resultsafter2',index=False)
writer.save()




x = conn_warehouse.cursor()
x.execute(query1)
print("Deleted signal segments")
conn_warehouse.commit()      
x.execute(query2)
conn_warehouse.commit()   
print("Deleted signals")
time.sleep(1)
x.execute(query)
conn_warehouse.commit()
print("Deleted Temperature")
x.execute(query3)
conn_warehouse.commit()
print("Deleted trundle")
x.close()

conn_testrun.close()
conn_warehouse.close()