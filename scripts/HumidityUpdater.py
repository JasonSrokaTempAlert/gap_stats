####  Controls
FLAG_pull_temp_data = True
FLAG_pull_power_data = True
AnalysisWindow_StartDate_str = '2017-11-09 00:00:00'
ANTOINECONSTANT_A = 8.07131
ANTOINECONSTANT_B = 1730.63
ANTOINECONSTANT_C = 233.426



def calculate_vapor_pressure(temperature_celsius, relative_humidity, flag_verbose = False):
    if flag_verbose:
        print("calculate_vapor_pressure():: called with temperature: {temp}  and relative humidity: {rh}".format(
            temp = temperature_celsius, rh = relative_humidity))
    if (temperature_celsius > 99):
        error("temperature ({temp}) above valid range (99 degC max)".format(temp = temperature_celsius))
    if (temperature_celsius < 1):
        return 0
    #  Calculate the vapor pressure at 100% humidity
    vapor_pressure_100 = 10**(ANTOINECONSTANT_A - (ANTOINECONSTANT_B/(ANTOINECONSTANT_C + temperature_celsius)))
    #  Calculate vapor pressure at provided relative humidity
    vapor_pressure = round(vapor_pressure_100 * relative_humidity/100,1)
    if flag_verbose:
        print("calculate_vapor_pressure():: full vapor pressure: {vp100}  actual vapor pressure: {vp}".format(
            vp100 = vapor_pressure_100, vp = vapor_pressure))
    return vapor_pressure


####  Environment setup
import os
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import psycopg2
import json
import re

AnalysisWindow_StartDate = pd.to_datetime(AnalysisWindow_StartDate_str)

# ####  These worked
# # database.init('test_ta_utils',)
# # database.init('Local_DataWarehouse',)
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)
cursor = DATABASE_CONN.cursor()

def FormatDatetime(RawDatetime):
    if isinstance(RawDatetime,basestring):
        result = RawDatetime.replace('T', ' ')
        ReturnDatetime = result.split('.')[0]
        #     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
        return ReturnDatetime
    return RawDatetime

##TODO:  add unit tests for FormatDatetime function



##TODO:  How do we create a generic config file starting point to make the code portable across local/EC2 setups?
if os.path.isdir('/Users/Jason/'):
    Environment = 'local'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
else:
    Environment = 'ubuntu'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'universal.json'))

##  Set up account ids
enterprise_acct_ids = [5250]
accts_string = ','.join(map(str,enterprise_acct_ids))

####  Device Metadata - used for filtering to a good set of Device IDs
# DevicesInfoFile=os.path.join(data_dir,'vu_DeviceAndStoreInfo.csv')
# print "Reading devices info from file: {file}".format(file = DevicesInfoFile)
# DevicesInfo = pd.read_csv(DevicesInfoFile)
print "Reading devices info from vu_deviceandstoreinfo"
DevicesInfo = pd.read_sql("""select * from vu_deviceandstoreinfo v where v.deviceid in
 (select distinct(d.deviceid) from sensor s
join device d on s.deviceid = d.deviceid
where sensortypeid = 3
and d.accountid in ({accts}));""".format(accts = accts_string), DATABASE_CONN)
DevicesInfo['deviceinstalldate']=DevicesInfo['deviceinstalldate'].apply(FormatDatetime)
print "done with devices info select, number of devices: {count}".format(count=len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(
    DevicesInfo[DevicesInfo['datedeleted'] >= AnalysisWindow_StartDate])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(
    start=AnalysisWindow_StartDate, count=len(DevicesInfo))


# DevicesInfo = DevicesInfo[DevicesInfo['state'].isin(['LA','AL','MS'])]
# print "after filtering for TX and LA, {rows} devices left".format(rows = len(DevicesInfo))

print "DevicesInfo.head(): {}".format(DevicesInfo.head())

Monitored_DeviceIDs = DevicesInfo['deviceid']
NumMonitoredDevices = len(Monitored_DeviceIDs)

numrows_per_insert = 100
StartTime = datetime.now()
print 'StartTime: {}'.format(StartTime)

RunTimestamp = datetime.now().date()
NumMonitoredDevices = len(Monitored_DeviceIDs)

run_date = datetime.utcnow()

# for DeviceIter in range(0, 5):
for DeviceIter in range(0, NumMonitoredDevices):

    thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
    # if thisDeviceID =='11666000000127965600':
    #     print "debug opp"
    print"humidity Data Pull:  Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(
        DeviceID=thisDeviceID,
        DeviceIter=DeviceIter,
        TotalDevices=NumMonitoredDevices)

    thisDeviceDataDF = pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
    if (len(thisDeviceDataDF) == 0):
        print'No data found for device, skipping to next'
        continue
    if ('sensors' in thisDeviceDataDF.columns):
        print'No sensors for this device, skipping to next'
        continue
    thisNumSensors = len(thisDeviceDataDF.port)

    thisdevice_temperaturedf = pd.DataFrame()
    thisdevice_humiditydf = pd.DataFrame()

    for SensorIndex in range(0, thisNumSensors):
        thisPortID = thisDeviceDataDF.port[SensorIndex]
        if thisPortID==0:
            "skipping port 0"
            continue
        print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID, index=SensorIndex,
                                                                          total=thisNumSensors)

        max_time_query = "select max(processed_date) MaxProcDate from device_humidity_history where deviceid = '{DevID}'".format(
            DevID=thisDeviceID)
        # print "querying for max_proc_time: {}".format(max_time_query)
        sensor_temp_data = pd.read_sql(max_time_query, DATABASE_CONN)
        # print "max_proc_time response head: {}".format(sensor_temp_data.head())
        max_processed_date = sensor_temp_data['maxprocdate'].values[0]
        # print "max_processed_date: {}".format(max_processed_date)
        if max_processed_date is not None:
            print "Humidity Data Pull:  Found prior data, using {time} as MaxLookbackTime".format(
                time=max_processed_date)
            max_proc_str = str(max_processed_date)
            print "max_proc_str: {}".format(max_proc_str)
            max_proc_dt = datetime.strptime(max_proc_str.split('.', 1)[0], '%Y-%m-%dT%H:%M:%S')
            thisSensorDataDF = pd.DataFrame(
                thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True, #command_ids=['104'],
                                                          reading_start_dt=AnalysisWindow_StartDate,
                                                          processed_after_dt=max_proc_dt,
                                                          reading_end_dt= run_date))
        else:
            thisSensorDataDF = pd.DataFrame(
                thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True, #command_ids=['104'],
                                                          reading_start_dt=AnalysisWindow_StartDate,
                                                          reading_end_dt = run_date))

        print "thisSensorDataDF.head(): \n{}".format(thisSensorDataDF.head(20))
        if len(thisSensorDataDF)==0:
            print "no data returned, skipping"
            continue
        thisSensorDataDF_temperature = thisSensorDataDF[thisSensorDataDF['readingType']=='Temperature']
        # thisSensorDataDF_temperature = thisSensorDataDF_temperature[['deviceid']]
        temperature_df = thisSensorDataDF_temperature[['readingDate','readingValue']]
        temperature_df = temperature_df.rename(columns = {'readingValue':'temperature'})
        temperature_df['readingDate'] = temperature_df['readingDate'].apply(lambda x: ':'.join(x.split(':')[0:-1]))
        temperature_df['deviceid'] = thisDeviceID
        thisdevice_temperaturedf = thisdevice_temperaturedf.append(temperature_df)

        thisSensorDataDF_humidity = thisSensorDataDF[thisSensorDataDF['readingType']=='Humidity']
        humidity_df = thisSensorDataDF_humidity[['readingDate','readingValue','processedDate']]
        humidity_df = humidity_df.rename(columns = {'readingValue':'humidity'})
        humidity_df['readingDate'] = humidity_df['readingDate'].apply(lambda x: ':'.join(x.split(':')[0:-1]))
        humidity_df['deviceid'] = thisDeviceID
        thisdevice_humiditydf = thisdevice_humiditydf.append(humidity_df)

        if ('processedDate' not in thisSensorDataDF.columns):
            print "no processed date in data returned, skipping"
            continue
        if len(thisSensorDataDF) == 0:
            print "no humidity found, skipping"
            continue
        # thisSensorDataDF['processed_date'] = thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
        # thisSensorDataDF['reading_date'] = thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))


    if len(thisdevice_humiditydf) == 0:
        print "no humidity found for device, skipping"
        continue
    combined_datadf = thisdevice_temperaturedf.merge(thisdevice_humiditydf, on=['deviceid','readingDate'])
    vap_pressure_vec = combined_datadf.apply(lambda x: calculate_vapor_pressure(x['temperature'],x['humidity']), axis=1)
    # print "combined_datadf.head(): {}".format(combined_datadf.head())
    combined_datadf['water_vapor_pressure'] = vap_pressure_vec
    # print "len(combined_datadf): {l1}  len(vap_pressure_vec): {l2}".format(l1=len(combined_datadf), l2 = len(vap_pressure_vec))
    num_data_rows = len(combined_datadf)
    while (num_data_rows > 0):
        thisupload_rowcount = min(num_data_rows, numrows_per_insert)
        thisuploaddata = combined_datadf[0:thisupload_rowcount]
        combined_datadf = combined_datadf[thisupload_rowcount:]
        num_data_rows = len(combined_datadf)

        ##  Build the insert statement
        insert_statement = 'insert into device_humidity_history(deviceid,reading_date,processed_date,humidity,temperature,water_vapor_pressure) values'
        for iter in range(0, (thisupload_rowcount - 1)):
            insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcDate}',{Hum},{Temp},{wp}),".format(
                DevID=str(thisDeviceID),
                ReadingDate=thisuploaddata['readingDate'].values[iter],
                ProcDate=thisuploaddata['processedDate'].values[iter],
                Hum=thisuploaddata['humidity'].values[iter],
                Temp=thisuploaddata['temperature'].values[iter],
                wp=thisuploaddata['water_vapor_pressure'].values[iter]
            )
        insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcDate}',{Hum},{Temp},{wp})".format(
            DevID=str(thisDeviceID),
            ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
            ProcDate=thisuploaddata['processedDate'].values[(thisupload_rowcount - 1)],
            Hum=thisuploaddata['humidity'].values[(thisupload_rowcount - 1)],
                Temp=thisuploaddata['temperature'].values[(thisupload_rowcount - 1)],
                wp=thisuploaddata['water_vapor_pressure'].values[(thisupload_rowcount - 1)])
        insert_statement = insert_statement + " on conflict do nothing"
        # print 'insert_statement:'
        # print insert_statement
        #                 pd.read_sql(insert_statement, DATABASE_CONN)
        ret = cursor.execute(insert_statement)
        ret = DATABASE_CONN.commit()

        print "after commit, {} rows remaining".format(num_data_rows)



EndTime = datetime.now()

TimeDuration = EndTime - StartTime
print "Data Pull Time: {TD}".format(TD=TimeDuration)



cursor.close()
DATABASE_CONN.close()

