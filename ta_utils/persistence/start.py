"""
Start Module
"""
from ta_utils import database

from ta_utils import PersistableSignal, PersistableSignalSegment,\
    PersistableGap, PersistableOfflinePeriod, PersistableDelay


def drop_tables(safe=True, cascade=True):
    """
    Drop the tables used in ta_utils.

    :param safe: Whether or not to "safely" drop the tables.
    :type safe: bool
    :param cascade: Whether or not to cascade the drop.
    :type cascade: bool
    :return: None
    :rtype: None
    """
    database.drop_tables([PersistableGap,
                          PersistableOfflinePeriod,
                          PersistableDelay,
                          PersistableSignal,
                          PersistableSignalSegment],
                         safe=safe,
                         cascade=cascade)


def create_tables(safe=True):
    """
    Create the tables used in ta_utils.

    :param safe: Whether or not to "safely" drop the tables.
    :type safe: bool
    :return: None
    :rtype: None
    """
    database.create_tables([PersistableGap,
                            PersistableOfflinePeriod,
                            PersistableDelay,
                            PersistableSignal,
                            PersistableSignalSegment],
                           safe=safe)
