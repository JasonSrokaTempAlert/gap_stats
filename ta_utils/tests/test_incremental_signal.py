"""
Test Incremental Signal Module
"""

import unittest
import logging
from datetime import datetime, timedelta
from subprocess import call

import pandas as pd

from ta_utils import create_tables, drop_tables
from ta_utils import database
from ta_utils import Signal
from ta_utils import PersistableGap
from ta_utils import IncrementalSignal

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

now = datetime.now()

REPORTING_INTERVAL_OVERRIDE = 60 * 2

null_signal = Signal(None)

dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]
processed_dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 26]]
signal = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                processed_dates=pd.Series(data=processed_dates, index=dates),
                device_id='test_signal')

null_signal_with_overrides = Signal(None,
                                    max_time_override=(now + timedelta(minutes=23)),
                                    max_processed_time_override=(now + timedelta(minutes=30)))

empty_test_signal_init = Signal(None,
                                min_time_override=(now - timedelta(days=7)),
                                max_time_override=now,
                                max_processed_time_override=now)

empty_test_signal_empty6hrupdate = Signal(None,
                                          max_time_override=now + timedelta(minutes=360),
                                          max_processed_time_override=now + timedelta(minutes=360))
empty_test_signal_empty1dayupdate = Signal(None,
                                          max_time_override=now,
                                          max_processed_time_override=now)

anchor_date = (now - timedelta(days=7))
data_length = 4 * 24 * 7 + 1
seven_day_dates = pd.date_range(anchor_date, freq='15min', periods=data_length)

simple_7day_signal_init = Signal(pd.Series(data=[0] * data_length,
                                           index=seven_day_dates),
                                 processed_dates=pd.Series(data=seven_day_dates, index=seven_day_dates))


small_anchor_date = (now - timedelta(days=7))
small_data_length = 4 * 24 * 3 + 1
three_day_dates = pd.date_range(small_anchor_date, freq='15min', periods=small_data_length)
                                 

semi_empty_3day_signal_init = Signal(pd.Series(data=[0]*small_data_length,
                                           index=three_day_dates),
                                 processed_dates=pd.Series(data=three_day_dates, index=three_day_dates))
                                 
small_empty_test_signal_init = Signal(None,
                                min_time_override=(now - timedelta(days=4)),
                                max_time_override=now,
                                max_processed_time_override=now)



empty_test_signal_notime_init = Signal(None,
                                min_time_override=(now),
                                max_time_override=now,
                                max_processed_time_override=now)

august_dates = pd.date_range(pd.datetime.strptime('2017-08-15', '%Y-%m-%d'),
                             freq='15min', periods=data_length)
simple_7daysignal_longleadgap = Signal(pd.Series(data=[0] * data_length,
                                                 index=august_dates),
                                       processed_dates=pd.Series(data=august_dates, index=august_dates),
                                       min_time_override=pd.datetime.strptime('2017-05-01', '%Y-%m-%d'),
                                       min_processed_time_override=pd.datetime.strptime('2017-05-01', '%Y-%m-%d'))
empty_august_update = Signal(None,
                             max_time_override=pd.datetime.strptime('2017-08-28', '%Y-%m-%d'),
                             max_processed_time_override=pd.datetime.strptime('2017-08-28', '%Y-%m-%d'))

signal_with_override = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                              processed_dates=pd.Series(data=processed_dates, index=dates),
                              reporting_interval_override=REPORTING_INTERVAL_OVERRIDE)


anchor_date = (now - timedelta(days=7))
data_length = 4 * 24 * 6 + 1
seven_day_dates = pd.date_range(anchor_date, freq='15min', periods=data_length)

simple_6day_signal_init = Signal(pd.Series(data=[0] * data_length,
                                           index=seven_day_dates),
                                 processed_dates=pd.Series(data=seven_day_dates, index=seven_day_dates), max_time_override=now,
                                max_processed_time_override=now)
                                
                                
first_signal_with_override = signal_with_override[:5]
second_signal_with_override = signal_with_override[5:]

first_signal = signal[:5]
second_signal = signal[5:]
second_signal_with_repeats = signal[3:]

new_date = now + timedelta(minutes=6)
old_start_gap_date = now + timedelta(minutes=5)
end_gap_date = now + timedelta(minutes=10)
new_signal = Signal(pd.Series(data=[1.], index=[new_date]),
                    processed_dates=pd.Series(data=[new_date], index=[new_date]),
                    device_id='test_signal')

signal_with_overrides_two = Signal(None,
                                   max_time_override=(now + timedelta(minutes=24)),
                                   max_processed_time_override=(now + timedelta(minutes=31)),
                                   reporting_interval_override=REPORTING_INTERVAL_OVERRIDE)

call(['createdb', 'test_ta_utils'])
database.init('test_ta_utils')


class IncrementalSignalTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_null_signal_save(self):
        first_persisted_signal = null_signal.save()
        incremental_signal = IncrementalSignal(first_persisted_signal, signal_with_override)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(null_signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(null_signal.port, persisted_incremental_signal.port)
        self.assertEqual(null_signal.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal_with_override.count, persisted_incremental_signal.count)
        self.assertEqual(signal_with_override.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal_with_override.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal_with_override.var, persisted_incremental_signal.var)
        self.assertEqual(signal_with_override.min, persisted_incremental_signal.min)
        self.assertEqual(signal_with_override.max, persisted_incremental_signal.max)
        self.assertEqual(signal_with_override.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal_with_override.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal_with_override.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal_with_override.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(signal_with_override.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal_with_override.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal_with_override.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal_with_override.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal_with_override.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal_with_override.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal_with_override.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal_with_override.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal_with_override.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal_with_override.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal_with_override.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal_with_override.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal_with_override.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal_with_override.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal_with_override.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

    def test_empty_signal_save(self):
        empty_signal = Signal(pd.Series())
        first_persisted_signal = empty_signal.save()
        incremental_signal = IncrementalSignal(first_persisted_signal, signal_with_override)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(empty_signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(empty_signal.port, persisted_incremental_signal.port)
        self.assertEqual(empty_signal.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal_with_override.count, persisted_incremental_signal.count)
        self.assertEqual(signal_with_override.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal_with_override.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal_with_override.var, persisted_incremental_signal.var)
        self.assertEqual(signal_with_override.min, persisted_incremental_signal.min)
        self.assertEqual(signal_with_override.max, persisted_incremental_signal.max)
        self.assertEqual(signal_with_override.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal_with_override.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal_with_override.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal_with_override.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(first_persisted_signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal_with_override.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal_with_override.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal_with_override.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal_with_override.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal_with_override.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal_with_override.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal_with_override.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal_with_override.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal_with_override.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal_with_override.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal_with_override.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal_with_override.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal_with_override.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal_with_override.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

    def test_signal_save(self):
        first_persisted_signal = first_signal.save()
        incremental_signal = IncrementalSignal(first_persisted_signal, second_signal)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(signal.port, persisted_incremental_signal.port)
        self.assertEqual(signal.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal.count, persisted_incremental_signal.count)
        self.assertEqual(signal.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal.var, persisted_incremental_signal.var)
        self.assertEqual(signal.min, persisted_incremental_signal.min)
        self.assertEqual(signal.max, persisted_incremental_signal.max)
        self.assertEqual(signal.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

    def test_signal_with_repeats_save(self):
        first_persisted_signal = first_signal.save()
        incremental_signal = IncrementalSignal(first_persisted_signal, second_signal_with_repeats)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(signal.port, persisted_incremental_signal.port)
        self.assertEqual(signal.data_type, persisted_incremental_signal.data_type)
        # The following commented out lines will no longer be true because of the extra data
        # self.assertEqual(signal.count + 1, persisted_incremental_signal.count)
        # self.assertEqual(signal.mean, persisted_incremental_signal.mean)
        # self.assertAlmostEqual(signal.std, persisted_incremental_signal.std)
        # self.assertAlmostEqual(signal.var, persisted_incremental_signal.var)
        self.assertEqual(signal.min, persisted_incremental_signal.min)
        self.assertEqual(signal.max, persisted_incremental_signal.max)
        self.assertEqual(signal.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal.min_gap_time, persisted_incremental_signal.min_gap_time)

        self.assertEqual(signal.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)


    def test_incremental_null_signal_with_time_overrides(self):
        persisted_signal = signal.save()
        incremental_signal = IncrementalSignal(persisted_signal, null_signal_with_overrides)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(null_signal_with_overrides.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(null_signal_with_overrides.max_processed_time, persisted_incremental_signal.max_processed_time)

    def test_signal_with_override_save(self):
        first_persisted_signal = first_signal_with_override.save()
        incremental_signal = IncrementalSignal(first_persisted_signal, second_signal_with_override)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal_with_override.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(signal_with_override.port, persisted_incremental_signal.port)
        self.assertEqual(signal_with_override.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal_with_override.count, persisted_incremental_signal.count)
        self.assertEqual(signal_with_override.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal_with_override.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal_with_override.var, persisted_incremental_signal.var)
        self.assertEqual(signal_with_override.min, persisted_incremental_signal.min)
        self.assertEqual(signal_with_override.max, persisted_incremental_signal.max)
        self.assertEqual(signal_with_override.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal_with_override.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal_with_override.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal_with_override.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(first_persisted_signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal_with_override.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal_with_override.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal_with_override.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal_with_override.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal_with_override.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal_with_override.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal_with_override.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal_with_override.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal_with_override.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal_with_override.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal_with_override.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal_with_override.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal_with_override.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal_with_override.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

    def test_signal_incremental_no_new_data(self):
        persisted_signal = signal.save()
        incremental_signal = IncrementalSignal(persisted_signal, new_signal)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(signal.port, persisted_incremental_signal.port)
        self.assertEqual(signal.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal.count, persisted_incremental_signal.count)
        self.assertEqual(signal.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal.var, persisted_incremental_signal.var)
        self.assertEqual(signal.min, persisted_incremental_signal.min)
        self.assertEqual(signal.max, persisted_incremental_signal.max)
        self.assertEqual(signal.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

    def test_signal_incremental_offline_periods(self):
        pass

    def test_signal_incremental_time_overrides(self):
        persisted_signal = null_signal_with_overrides.save()
        incremental_signal = IncrementalSignal(persisted_signal, signal_with_overrides_two)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal_with_overrides_two.max_time_override, persisted_incremental_signal.max_time_override)
        self.assertEqual(signal_with_overrides_two.max_processed_time_override,
                         persisted_incremental_signal.max_processed_time_override)
        self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       
                         

    def empty_test_signal_init(self):
        persisted_signal = empty_test_signal_init.save()

        self.assertEqual(persisted_signal.max_time, now)
        self.assertEqual(persisted_signal.max_processed_time_override, now)
        self.assertEqual(persisted_signal.max_time_override, now)
        self.assertEqual(persisted_signal.min_time, now - timedelta(days=7))
        self.assertEqual(persisted_signal.min_time_override, now - timedelta(days=7))
        self.assertEqual(persisted_signal.online, 0.0)
        self.assertEqual(persisted_signal.coverage, 0.0)

    def test_run_emptysignalupdatedwithempty(self):
        persisted_signal = empty_test_signal_init.save()
        incremental_signal = IncrementalSignal(persisted_signal, empty_test_signal_empty6hrupdate)
        persisted_incremental_signal = incremental_signal.save()
        self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       
        self.assertEqual(persisted_incremental_signal.max_time_override, now + timedelta(minutes=360))
        self.assertEqual(persisted_incremental_signal.max_processed_time_override, now + timedelta(minutes=360))
        self.assertEqual(persisted_incremental_signal.max_time, now + timedelta(minutes=360))
        self.assertEqual(persisted_signal.min_time, now - timedelta(days=7))
        self.assertEqual(persisted_incremental_signal.min_time_override, now - timedelta(days=7))
        self.assertEqual(persisted_incremental_signal.online, 0)
        self.assertEqual(persisted_incremental_signal.coverage, 0)

    def test_run_simplesignalaugmentedwithempty(self):
        persisted_signal = simple_7day_signal_init.save()

        self.assertEqual(persisted_signal.max_time, now)
        self.assertEqual(persisted_signal.online, 1)
        self.assertEqual(persisted_signal.coverage, 1)

        incremental_signal = IncrementalSignal(persisted_signal, empty_test_signal_empty6hrupdate)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(persisted_incremental_signal.max_time, now + timedelta(minutes=360))

        self.assertEqual(persisted_incremental_signal.max_time_override, now+timedelta(hours=6))
        self.assertEqual(persisted_incremental_signal.max_processed_time_override, now + timedelta(hours=6))

        self.assertAlmostEqual(timedelta(days=7).total_seconds() / timedelta(days=7, hours=6).total_seconds(),
                               persisted_incremental_signal.online)
        self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       
        self.assertAlmostEqual(timedelta(days=7).total_seconds() / timedelta(days=7, hours=6).total_seconds(),
                               persisted_incremental_signal.coverage)

        self.assertEqual(persisted_incremental_signal.total_gap_time, timedelta(hours=6).total_seconds())
        self.assertEqual(persisted_incremental_signal.total_offline_period_time, timedelta(hours=6).total_seconds())

    def test_run_longleadgapsignal_augmentedwithempty(self):
        persisted_signal = simple_7daysignal_longleadgap.save()

        # First there are 7 days of actual data with 106 days of gap
        self.assertAlmostEqual(persisted_signal.coverage, 7. / 113.)
        self.assertAlmostEqual(persisted_signal.online, 7. / 113.)

        incremental_signal = IncrementalSignal(persisted_signal, empty_august_update)
        persisted_incremental_signal = incremental_signal.save()
        self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       
        # Then we add 6 days of additional "empty" time, changing the denominator to 119
        self.assertAlmostEqual(persisted_incremental_signal.coverage, 7. / 119.)
        self.assertAlmostEqual(persisted_incremental_signal.online, 7. / 119.)


    def test_run_simplesignalNotIncremental(self):
        persisted_signal = simple_6day_signal_init.save()
        self.assertEqual(persisted_signal.max_time, now)
        self.assertAlmostEqual(0.858630952381,persisted_signal.online)
        self.assertAlmostEqual(0.858630952381,persisted_signal.coverage)



    def test_run_simplesignalaugmentedwithemptynotime(self):
        persisted_signal = simple_7day_signal_init.save()

        self.assertEqual(persisted_signal.max_time, now)
        self.assertEqual(persisted_signal.online, 1)
        self.assertEqual(persisted_signal.coverage, 1)

        incremental_signal = IncrementalSignal(persisted_signal, empty_test_signal_notime_init)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(persisted_incremental_signal.max_time, now)

        self.assertEqual(persisted_incremental_signal.max_time_override, now)
        self.assertEqual(persisted_incremental_signal.max_processed_time_override, now)
        self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       

        self.assertAlmostEqual(1,persisted_incremental_signal.online)
        self.assertAlmostEqual(1,persisted_incremental_signal.coverage)
        self.assertEqual(persisted_incremental_signal.total_gap_time, 0)
        self.assertEqual(persisted_incremental_signal.total_offline_period_time,0)
        self.assertEqual(persisted_incremental_signal.min_time, anchor_date) 
        self.assertEqual(persisted_incremental_signal.min_processed_time,anchor_date)        
        self.assertEqual(persisted_incremental_signal.max_processed_time, now)
 

    def test_run_dataandnodata(self):
        persisted_signal = semi_empty_3day_signal_init.save()

        self.assertEqual(persisted_signal.max_time, now - timedelta(days=4))
        self.assertEqual(persisted_signal.online, 1)
        self.assertEqual(persisted_signal.coverage, 1)
        incremental_signal = IncrementalSignal(persisted_signal, small_empty_test_signal_init)
        persisted_incremental_signal = incremental_signal.save()
        self.assertEqual(persisted_incremental_signal.max_time, now)
        self.assertLess(persisted_incremental_signal.online, 1)
        self.assertLess(persisted_incremental_signal.coverage, 1)

        incremental_signal_real = IncrementalSignal(persisted_incremental_signal, empty_test_signal_empty6hrupdate)
        persisted_incremental_signal_real = incremental_signal_real.save()
        self.assertEqual(persisted_incremental_signal_real.max_time_override, now + timedelta(minutes=360))
        self.assertEqual(persisted_incremental_signal_real.max_processed_time_override, now + timedelta(minutes=360))
        self.assertEqual(persisted_incremental_signal_real.max_time, now + timedelta(minutes=360))
        self.assertEqual(persisted_incremental_signal_real.min_time, now - timedelta(days=7))
#         self.assertEqual(persisted_incremental_signal_real.min_time_override, now - timedelta(days=7))
#         self.assert
        self.assertGreater(0.428571428571,persisted_incremental_signal_real.online)
        self.assertGreater(0.428571428571,persisted_incremental_signal_real.coverage)


    def test_run_dataandnotime(self):
        persisted_signal = semi_empty_3day_signal_init.save()

        self.assertEqual(persisted_signal.max_time, now - timedelta(days=4))
        self.assertEqual(persisted_signal.online, 1)
        self.assertEqual(persisted_signal.coverage, 1)
        incremental_signal = IncrementalSignal(persisted_signal, small_empty_test_signal_init)
        persisted_incremental_signal = incremental_signal.save()
        self.assertEqual(persisted_incremental_signal.max_time, now)
        self.assertLess(persisted_incremental_signal.online, 1)
        self.assertLess(persisted_incremental_signal.coverage, 1)

        incremental_signal_real = IncrementalSignal(persisted_incremental_signal,empty_test_signal_notime_init )
        persisted_incremental_signal_real = incremental_signal_real.save()
        self.assertEqual(persisted_incremental_signal_real.max_time_override, now )
        self.assertEqual(persisted_incremental_signal_real.max_processed_time_override, now )
        self.assertEqual(persisted_incremental_signal_real.max_time, now)
        self.assertEqual(persisted_incremental_signal_real.min_time, now - timedelta(days=7))
#         self.assertEqual(persisted_incremental_signal_real.min_time_override, now - timedelta(days=7))
#         self.assert
        self.assertEqual(0.4285714285714286,persisted_incremental_signal_real.online)
        self.assertEqual(0.4285714285714286,persisted_incremental_signal_real.coverage)
        

#         incremental_signal = IncrementalSignal(persisted_signal, empty_test_signal_empty6hrupdate)
#         persisted_incremental_signal = incremental_signal.save()
# 
#         self.assertEqual(persisted_incremental_signal.max_time, now + timedelta(minutes=360))
# 
#         self.assertEqual(persisted_incremental_signal.max_time_override, now+timedelta(hours=6))
#         self.assertEqual(persisted_incremental_signal.max_processed_time_override, now + timedelta(hours=6))
# 
#         self.assertAlmostEqual(timedelta(days=7).total_seconds() / timedelta(days=7, hours=6).total_seconds(),
#                                persisted_incremental_signal.online)
#         self.assertGreaterEqual(persisted_signal.online,persisted_incremental_signal.online)                       
#         self.assertAlmostEqual(timedelta(days=7).total_seconds() / timedelta(days=7, hours=6).total_seconds(),
#                                persisted_incremental_signal.coverage)
# 
#         self.assertEqual(persisted_incremental_signal.total_gap_time, timedelta(hours=6).total_seconds())
#         self.assertEqual(persisted_incremental_signal.total_offline_period_time, timedelta(hours=6).total_seconds())
