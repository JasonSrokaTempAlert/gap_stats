"""
Gap Module
"""
from datetime import datetime

class Gap(object):
    """
    The Gap Object represents a gap in a time-series.
    """
    def __init__(self, previous_reading, start, end):
        """
        Initialize the Gap with the previous reading, start, and stop.

        :param previous_reading: The reading datetime preceding the gap
        :type previous_reading: datetime
        :param start: The start datetime of the gap
        :type start: datetime
        :param end: The end datetime of the gap
        :type end: datetime
        """
        self.previous_reading = previous_reading
        self.start = start
        self.end = end

    @property
    def length(self):
        """
        Get the time length of this gap in seconds.

        :return: Time length of this gap in seconds.
        :rtype: int
        """
        return int((self.end - self.start).total_seconds())

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Gap):
            return False

        previous_eq = self.previous_reading == other.previous_reading
        start_eq = self.start == other.start
        end_eq = self.end == other.end

        return previous_eq and start_eq and end_eq
