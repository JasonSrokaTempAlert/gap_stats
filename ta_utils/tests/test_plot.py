from datetime import datetime, timedelta

import pandas as pd
import numpy as np
PLOT = False
#To avoid plotting errors
if PLOT == True :
	from ta_utils import Signal
	from ta_utils.plot import plot_signal, plot_signal_histogram

	minutes = np.arange(0, 60 * 24 * 6, 15)
	dates = [datetime.now() + timedelta(minutes=(i)) for i in minutes]

	sine = np.sin(2. * np.pi * minutes / (60. * 24.)) + 2
	trivial_signal = Signal(pd.Series(data=sine, index=dates))

	plot_signal(trivial_signal, filename="test_plot.png", degrees='C', title='Signal Title',
            range_min=1.4, range_max=2.6)

	plot_signal_histogram(trivial_signal, filename="test_histogram.png", degrees='C', title='Signal Histogram Title',
                      range_min=1.4, range_max=2.6)