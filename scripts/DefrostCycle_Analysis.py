import os
import pandas as pd
import numpy as np
import psycopg2
from datetime import datetime, timedelta


##  PyCharm/File
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


##  Noetbook:
#from matplotlib import pyplot as plt
#% matplotlib inline

#  DB Access
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)

#  Configuration Params
ANALYSIS_WINDOWS_IN_HOURS = [4, 5, 6, 8, 10, 15, 20, 30, 40]
MAX_ANALYSIS_WINDOW_IN_HOURS = max(ANALYSIS_WINDOWS_IN_HOURS)
MIN_READINGS_FOR_STATS = 10
MAX_DEFROSTCYCLE_DURATION_MINUTES = 120
MIN_DEFROSTCYCLE_INTERCYCLE_STARTTOSTART_HOURS = 4
MAX_DEFROSTCYCLE_INTERCYCLE_STARTTOSTART_HOURS = 48
MIN_PERIODICANOMALIES_FOR_ANALYSIS = 7
MAX_PERIODICANOMALIES_FOR_ANALYSIS = 20

#  I/O
DATA_DIR = '/Users/Jason/Desktop/Analytics/Projects/PROD_745_CumbieAnalytics/Data/'
RESULTS_DIR = '/Users/Jason/Desktop/Analytics/Projects/PROD_745_CumbieAnalytics/Results/'
CF_FILENAME = 'CF_TemperatureDataPull.csv'

def generate_periodic_anomaly_stable_stats(anomaly_events_df, flag_verbose = True):
    num_anomalies = len(anomaly_events_df)
    if flag_verbose:
        print "generate_periodic_anomaly_prediction()::  call with {num} anomaly events".format(num = num_anomalies)

    #  make sure we have enough events to process
    if num_anomalies < MIN_PERIODICANOMALIES_FOR_ANALYSIS:
        if flag_verbose:
            print "generate_periodic_anomaly_prediction()::  not enough anomalies to characterize, returning NULL result"
        return None

    #  trim to only the most recent if there are more than the max_for_analysis
    if num_anomalies > MAX_PERIODICANOMALIES_FOR_ANALYSIS:
        anomaly_events_df = anomaly_events_df[-MAX_PERIODICANOMALIES_FOR_ANALYSIS:-1]

    periodic_anomalies_df = pd.DataFrame()
    periodic_anomalies_df['anomalies_maxtemp_mean'] = np.mean(anomaly_events_df['TemperatureMax_C'])
    periodic_anomalies_df['anomalies_maxtemp_standev'] = np.std(anomaly_events_df['TemperatureMax_C'])

    periodic_anomalies_df['anomalies_duration_minutes_mean'] = np.mean(anomaly_events_df['Duration_Minutes'])
    periodic_anomalies_df['anomalies_duration_minutes_standev'] = np.std(anomaly_events_df['Duration_Minutes'])

    start_to_start_minutes = (anomaly_events_df['StartTime'].values[1:-1] - anomaly_events_df['StartTime'].values[0:-2]).total_seconds()/60
    periodic_anomalies_df['anonalies_starttostart_minutes_mean'] = np.mean(start_to_start_minutes)
    periodic_anomalies_df['anonalies_starttostart_minutes_standev'] = np.std(start_to_start_minutes)

    end_to_start_minutes = (anomaly_events_df['StartTime'].values[1:-1] - anomaly_events_df['EndTime'].values[0:-2]).total_seconds()/60
    periodic_anomalies_df['anonalies_endtostart_minutes_mean'] = np.mean(end_to_start_minutes)
    periodic_anomalies_df['anonalies_endtostart_minutes_standev'] = np.std(end_to_start_minutes)

    if flag_verbose:
        print "generate_periodic_anomaly_prediction()::  periodic_anomalies_df:\n{}".format(periodic_anomalies_df)


def generate_windowed_stats(timeseries, windows_in_hours=MAX_ANALYSIS_WINDOW_IN_HOURS, flag_plot=False):
    window_end_time = timeseries.index[-1]

    print "timeseries.index[0]: {}".format(timeseries.index[0])
    print 'type(timeseries.index[0]): {}'.format(type(timeseries.index[0]))
    print "timeseries.index[-1]: {}".format(timeseries.index[-1])
    print "(timeseries.index[-1] -  timeseries.index[0]).total_seconds: {}".format(
        (timeseries.index[-1] - timeseries.index[0]).total_seconds())
    print "(timeseries.index[-1] -  timeseries.index[0]): {}".format((timeseries.index[-1] - timeseries.index[0]))
    total_timeseries_hours = round((timeseries.index[-1] - timeseries.index[0]).total_seconds() / 3600, 1)
    print "generate_windowed_stats()::  called with timeseries having {len} values from {first} to {last} over {hrs} hours ".format(
        len=len(timeseries),
        first=timeseries.index[0],
        hrs=total_timeseries_hours,
        last=window_end_time)
    print type(timeseries.index[0])
    timestamps = timeseries.index
    if flag_plot:
        plt.plot(timestamps, timeseries.values)
        plt.show()
    if total_timeseries_hours >= max(windows_in_hours):
        lowest_window_above_actual = max(windows_in_hours)
    else:
        lowest_window_above_actual = min([hours for hours in windows_in_hours if hours > total_timeseries_hours])
    valid_window_hours = [hours for hours in windows_in_hours if hours <= lowest_window_above_actual]

    #  create reverse-sorted list for sequential reduction in
    working_readings = timeseries
    reversed_windows = sorted(valid_window_hours, reverse=True)
    reports_df = pd.DataFrame()
    print "generate_windowed_stats()::  reversed_windows: {}".format(reversed_windows)
    for index, window_hours in enumerate(reversed_windows):
        if index < len(reversed_windows) - 1:
            next_window = reversed_windows[index + 1]
        else:
            next_window = -1
        max_lookback_time = window_end_time - timedelta(hours=window_hours)
        # print "generate_windowed_stats()::  looking at window of {hrs} hours, from {then} to {now}".format(hrs = window_hours,
        #                                                                                                    then = max_lookback_time,
        #                                                                                                    now = window_end_time)

        working_readings = working_readings.loc[(working_readings.index >= max_lookback_time)]
        num_working_readings = len(working_readings)
        # print "working_readings found with {len} rows, head():".format(len = num_working_readings)
        # print working_readings.head()
        if num_working_readings < MIN_READINGS_FOR_STATS:
            this_report = pd.DataFrame({'WindowInHours': window_hours,
                                        'NumReadings': num_working_readings,
                                        'ActualWindow_Hours': None,
                                        'Mean': None,
                                        'StanDev': None,
                                        'Max': None,
                                        'Min': None}, index=[1])
        else:
            # print "generate_windowed_stats()::  ActualWindow_Hours terms:  from {then} to {now}".format(then = working_readings.index[0], now = working_readings.index[-1])
            # print "generate_windowed_stats()::  working_readings.index[-1] -  working_readings.index[0]: {}".format((working_readings.index[-1] -  working_readings.index[0]).seconds)
            this_report = pd.DataFrame({'WindowInHours': window_hours,
                                        'NumReadings': num_working_readings,
                                        'ActualWindow_Hours': round(
                                            (working_readings.index[-1] - working_readings.index[0]).seconds / 3600, 1),
                                        'Mean': np.mean(working_readings),
                                        'StanDev': np.std(working_readings),
                                        'Max': max(working_readings),
                                        'Min': min(working_readings)}, index=[1])
            # print "generate_windowed_stats()::  this_report:\n{}".format(this_report)
        reports_df = reports_df.append(this_report)
    # print "generate_windowed_stats()::  reports_df:"
    # print reports_df
    return reports_df


def translate_windowed_stats(windowed_stats, max_windowed_standev_threshold=2, flag_plot=False, FLAG_Verbose=False):
    windowed_stats = windowed_stats.reset_index(drop=True)
    print "translate_windowed_stats()::  called using max_windowed_standev_threshold param: {thresh}".format(thresh = max_windowed_standev_threshold)
    print "translate_windowed_stats()::  windowed_stats: {}".format(windowed_stats)

    ##  For now, a very simple approach:  take the minimum (no adjustment for window size yet) and use a maximum stasndev threshold
    min_std_idx = windowed_stats['StanDev'].idxmin()
    print "translate_windowed_stats(): min_std_idx:{}".format(min_std_idx)
    min_std = windowed_stats['StanDev'].values[min_std_idx]
    print "translate_windowed_stats(): min_std:{}".format(min_std)
    max_std_idx = windowed_stats['StanDev'].idxmax()
    print "translate_windowed_stats(): max_st_idxd:{}".format(max_std_idx)


    valid_stats = windowed_stats[-pd.isnull(windowed_stats['StanDev'])]

    if len(valid_stats) == 0:
        print 'translate_windowed_stats():  no valid stats, returning defaults'
        return pd.DataFrame({'DFCycle_RecentStable_State': 'NoStableState'})
    else:
        if flag_plot:
            plt.plot(windowed_stats['WindowInHours'], windowed_stats['StanDev'])
            plt.show()
        return pd.DataFrame({'DFCycle_RecentStable_State': 'StableState',
                             'DFCycle_AnalysisWindowSize_Hours': windowed_stats['WindowInHours'].values[min_std_idx],
                             'DFCycle_RecentStable_ActualWindow_Hours': windowed_stats['ActualWindow_Hours'].values[
                                 min_std_idx],
                             'DFCycle_RecentStable_MaxTemp': windowed_stats['Max'].values[min_std_idx],
                             'DFCycle_RecentStable_MeanTemp': windowed_stats['Mean'].values[min_std_idx],
                             'DFCycle_RecentStable_MinTemp': windowed_stats['Min'].values[min_std_idx],
                             'DFCycle_RecentStable_StanDev': windowed_stats['StanDev'].values[min_std_idx],
                             'DFCycle_RecentStable_NumReadings': windowed_stats['NumReadings'].values[min_std_idx], },
                            index=[1])


def analyze_timeseries_for_defrost(timeseries, FLAG_Verbose=True, max_numsamples_toprocess = None, index_offset = 0):
    """ taking an ordered time series of temperature measurements (indexed by reading date)
    and iterates through the data making calls to the incremental defrost cycle analysis routine,
    returning a data frame of state /results with one entry per reading in original timeseries"""

    num_readings = len(timeseries)

    if FLAG_Verbose:
        print "analyze_timeseries_for_defrost()::  called with timeseries having {num} entries".format(num=num_readings)
        print "analyze_timeseries_for_defrost()::  timeseries.head(): {}".format(timeseries.head())

    # Make sure we have data, otherwise return an empty data frame
    if num_readings < 2:
        if FLAG_Verbose:
            print "analyze_timeseries_for_defrost()::  timeseries has zero or one length, returning empty results"
            return pd.DataFrame()

    # debug tool
    if max_numsamples_toprocess is None:
        max_iteration = len(timeseries)
    else:
        max_iteration = min(index_offset + max_numsamples_toprocess, len(timeseries))

    reports_df = pd.DataFrame()
    defrost_event_candidates_df = pd.DataFrame()
    FLAG_FIRSTITERATION = True
    for func_index in range(index_offset,max_iteration):
        #  Special processing for first reading
        current_index = timeseries.index[func_index]
        this_reading = timeseries.values[func_index]
        if FLAG_Verbose:
            print "analyze_timeseries_for_defrost()::  processing reading # {func_index} for reading time: {time} with reading: {val}".format(
                func_index=func_index, time=current_index, val=this_reading)
        this_report_df = pd.DataFrame({'reading_index' : func_index,
                                       'reading': timeseries.values[func_index],
                                       'reading_date': timeseries.index[func_index],
                                       'DFCycle_RecentStable_state_label': 'Initialized'}, index=[timeseries.index[func_index]])
        if FLAG_Verbose:
            print "analyze_timeseries_for_defrost()::  initialized this_report_df to {}".format(this_report_df)
        if FLAG_FIRSTITERATION:
            this_report_df['DFCycle_RecentStable_state_label'] = 'NotEnoughData'
            this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'NotEnoughData'
            this_report_df['DFCycle_State'] = 'NotEnoughData'
            this_report_df['DFCycle_DefrostEvent_Threshold'] = -101
            this_report_df['DFCycle_Duration_Minutes'] = -101
            this_report_df['DFCycle_Peak'] = -101
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost()::  initialized this_report_df:"
                print this_report_df
            reports_df = reports_df.append(this_report_df)
            FLAG_FIRSTITERATION = False
        else:

            max_lookback_index = timeseries.index[func_index - 1] - timedelta(hours=MAX_ANALYSIS_WINDOW_IN_HOURS)
            end_of_window_index = timeseries.index[func_index - 1]
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost()::  max_lookback_index (calculated): {max}  to end_of_window_index: {cur}".format(
                    max=max_lookback_index,
                    cur=end_of_window_index)
            recent_history = timeseries.loc[
                (timeseries.index >= max_lookback_index) & (timeseries.index < current_index)]
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost()::  first index in recent history: {first}, to last index: {last}".format(
                    first=recent_history.index[0],
                    last=recent_history.index[-1])

            if len(recent_history) < MIN_READINGS_FOR_STATS:
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost():: not enough readings"
                this_report_df['DFCycle_RecentStable_state_label'] = 'NotEnoughData'
            else:

                ##  Preliminary/Basic recent windowing and anomaly detection
                windowed_stats = generate_windowed_stats(recent_history, ANALYSIS_WINDOWS_IN_HOURS)
                # print "windowed_stats: {}".format(windowed_stats)
                translated_stats_df = translate_windowed_stats(windowed_stats)
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost()::  translated_stats_df: {}".format(translated_stats_df)

                if translated_stats_df['DFCycle_RecentStable_State'].values[0] == 'NoStableState':
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost():: no stable recent state"
                    this_report_df['DFCycle_RecentStable_state_label'] = 'NoRecentStableState'
                else:
                    recent_mean = translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                    recent_standev = translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                    thistemp_threshold = recent_mean + 5 * recent_standev
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost()::  recent_mean: {mean}  recent_standev: {standev}  thistemp_threshold: {thres}".format(
                            mean=recent_mean,
                            standev=recent_standev,
                            thres=thistemp_threshold)
                    if ((this_report_df['reading'].values[0] <= recent_mean + 3 * recent_standev) &
                            (this_report_df['reading'].values[0] >= recent_mean - 3 * recent_standev)):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading consistent with recent stable state"
                            print "analyze_timeseries_for_defrost():: translated_stats_df: {}".format(translated_stats_df)
                        this_report_df['DFCycle_RecentStable_state_label'] = 'DFCycle_InRange'
                        this_report_df['DFCycle_RecentStable_AnalysisWindowSize_Hours'] = translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_ActualWindow_Hours'] = translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_MaxTemp'] = translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MeanTemp'] = translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MinTemp'] = translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_StanDev'] = translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentStable_NumReadings'] = translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: this_report_df: {}".format(this_report_df)

                    if (this_report_df['reading'].values[0] < recent_mean - 3 * recent_standev):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomolously low with recent stable state"
                        this_report_df['DFCycle_RecentStable_state_label'] = 'DFCycle_AnomalousLow'
                        this_report_df['DFCycle_RecentStable_AnalysisWindowSize_Hours'] = translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_ActualWindow_Hours'] = translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_MaxTemp'] = translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MeanTemp'] = translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MinTemp'] = translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_StanDev'] = translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentStable_NumReadings'] = translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]

                    if ( (this_report_df['reading'].values[0] > (recent_mean + 3 * recent_standev)) & (this_report_df['reading'].values[0] <= thistemp_threshold)):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomolously low with recent stable state"
                        this_report_df['DFCycle_RecentStable_state_label'] = 'DFCycle_AnomalousHigh'
                        this_report_df['DFCycle_RecentStable_AnalysisWindowSize_Hours'] = translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_ActualWindow_Hours'] = translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_MaxTemp'] = translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MeanTemp'] = translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MinTemp'] = translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_StanDev'] = translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentStable_NumReadings'] = translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]

                    if this_report_df['reading'].values[0] > thistemp_threshold:
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomalous with recent stable state"
                        this_report_df['DFCycle_RecentStable_state_label'] = 'DFCycle_PotentialDefrost'
                        this_report_df['DFCycle_RecentStable_AnalysisWindowSize_Hours'] = translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_ActualWindow_Hours'] = translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentStable_MaxTemp'] = translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MeanTemp'] = translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_MinTemp'] = translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentStable_StanDev'] = translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentStable_NumReadings'] = translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]


            ##  Parallel evaluation of anomaly-filtered history
            print "analyze_timeseries_for_defrost():: reports_df: {}".format(reports_df)
            recent_nonanomaly_reports = reports_df.loc[ ( (reports_df.index >= max_lookback_index) & (reports_df['DFCycle_RecentStable_state_label'].isin(['NotEnoughData',
                                                                                                                                    'NoRecentStableState',
                                                                                                                                    'DFCycle_InRange']) ) )]
            recent_nonanomaly_history = pd.Series(recent_nonanomaly_reports['reading'].values,
                       index=recent_nonanomaly_reports['reading_date'])
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost()::  recent_nonanomaly_timeseries has length {len}".format(len = len(recent_nonanomaly_history))

            if len(recent_nonanomaly_history) < MIN_READINGS_FOR_STATS:
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost():: not enough readings for nonanomaly data"
                this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'NotEnoughData'
            else:
                recent_nonanomaly_windowed_stats = generate_windowed_stats(recent_nonanomaly_history, ANALYSIS_WINDOWS_IN_HOURS)
                # print "recent_nonanomaly_windowed_stats: {}".format(recent_nonanomaly_windowed_stats)
                recent_nonanomaly_translated_stats_df = translate_windowed_stats(recent_nonanomaly_windowed_stats)
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost()::  recent_nonanomaly_translated_stats_df: {}".format(recent_nonanomaly_translated_stats_df)

                if recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_State'].values[0] == 'NoStableState':
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost():: no stable recent state"
                    this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'NoRecentStableState'
                else:
                    recent_mean = recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                    recent_standev = recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                    thistemp_nonanomaly_threshold = recent_mean + 5 * recent_standev
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost()::  recent_mean: {mean}  recent_standev: {standev}  thistemp_threshold: {thres}".format(
                            mean=recent_mean,
                            standev=recent_standev,
                            thres=thistemp_threshold)
                    if ((this_report_df['reading'].values[0] <= recent_mean + 3 * recent_standev) &
                            (this_report_df['reading'].values[0] >= recent_mean - 3 * recent_standev)):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading consistent with recent stable state"
                            print "analyze_timeseries_for_defrost():: recent_nonanomaly_translated_stats_df: {}".format(recent_nonanomaly_translated_stats_df)
                        this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'DFCycle_InRange'
                        this_report_df['DFCycle_RecentNonAnomalyStable_AnalysisWindowSize_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_ActualWindow_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MaxTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MeanTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MinTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_StanDev'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_NumReadings'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: this_report_df: {}".format(this_report_df)

                    if (this_report_df['reading'].values[0] < recent_mean - 3 * recent_standev):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomolously low with recent stable state"
                        this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'DFCycle_AnomalousLow'
                        this_report_df['DFCycle_RecentNonAnomalyStable_AnalysisWindowSize_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_ActualWindow_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MaxTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MeanTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MinTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_StanDev'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_NumReadings'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]

                    if ((this_report_df['reading'].values[0] > (recent_mean + 3 * recent_standev)) & (
                        this_report_df['reading'].values[0] <= thistemp_nonanomaly_threshold)):
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomolously low with recent stable state"
                        this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'DFCycle_AnomalousHigh'
                        this_report_df['DFCycle_RecentNonAnomalyStable_AnalysisWindowSize_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_ActualWindow_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MaxTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MeanTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MinTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_StanDev'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_NumReadings'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_PreDefrostThreshold_High'] = thistemp_threshold

                    if this_report_df['reading'].values[0] > thistemp_nonanomaly_threshold:
                        if FLAG_Verbose:
                            print "analyze_timeseries_for_defrost():: new reading anomalous with recent stable state"
                        this_report_df['DFCycle_RecentNonAnomalyStable_state_label'] = 'DFCycle_PotentialDefrost'
                        this_report_df['DFCycle_RecentNonAnomalyStable_AnalysisWindowSize_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_AnalysisWindowSize_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_ActualWindow_Hours'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_ActualWindow_Hours'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MaxTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MaxTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MeanTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MeanTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_MinTemp'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_MinTemp'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_StanDev'] = \
                        recent_nonanomaly_translated_stats_df['DFCycle_RecentStable_StanDev'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_NumReadings'] = recent_nonanomaly_translated_stats_df[
                            'DFCycle_RecentStable_NumReadings'].values[0]
                        this_report_df['DFCycle_RecentNonAnomalyStable_PreDefrostThreshold_High'] = thistemp_threshold

            ##  Deal with overall state
            last_state = reports_df['DFCycle_State'].values[-1]
            this_report_df['last_state'] = last_state
            defrostevent_tempthreshold = reports_df['DFCycle_DefrostEvent_Threshold'].values[-1]
            if last_state == 'DFCycle_PotentialDefrost':
                print "analyze_timeseries_for_defrost()::   pause opportunity for DFCycle_PotentialDefrost"
            this_state = this_report_df['DFCycle_RecentNonAnomalyStable_state_label'].values[0]
            this_report_df['this_state'] = this_state
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost():: last state: {last},  this state: {current}".format(last = last_state, current = this_state)

            ##  Start of a defrost event
            if ((last_state != 'DFCycle_PotentialDefrost') & (this_state == 'DFCycle_PotentialDefrost')):
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost():: start of a potential defrost".format(last_state)
                this_report_df['DFCycle_State'] = 'DFCycle_PotentialDefrost'
                this_report_df['DFCycle_DefrostEvent_Threshold'] = thistemp_nonanomaly_threshold
                this_report_df['DFCycle_DefrostEvent_StartTime'] = this_report_df.index[0]
                this_report_df['DFCycle_DefrostEvent_RunningTime_Minutes'] = 0

            ##  Continuation of a defrost event
            elif ((last_state == 'DFCycle_PotentialDefrost') & (this_report_df['reading'].values[0] >= defrostevent_tempthreshold)):
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost():: continuing a potential defrost".format(last_state)
                ##  Check if we've exceeded the threshold, if so then 'undo' the labeling
                defrost_start_time = reports_df['DFCycle_DefrostEvent_StartTime'].values[-1]
                total_defrost_event_time_minutes = (this_report_df.index[0] - defrost_start_time).total_seconds()/60

                if total_defrost_event_time_minutes > MAX_DEFROSTCYCLE_DURATION_MINUTES:
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost():: defrost event exceeded max threshold ({max_min} minutes)".format(max_min = MAX_DEFROSTCYCLE_DURATION_MINUTES)
                    reports_df.loc[reports_df.index >= defrost_start_time,'DFCycle_State'] = 'DFCycle_NonDefrostAnomaly'
                    this_report_df['DFCycle_State'] = 'DFCycle_NonDefrostAnomaly'
                else :
                    this_report_df['DFCycle_State'] = 'DFCycle_PotentialDefrost'
                    this_report_df['DFCycle_DefrostEvent_Threshold'] = defrostevent_tempthreshold
                    this_report_df['DFCycle_DefrostEvent_StartTime'] = defrost_start_time
                    this_report_df['DFCycle_DefrostEvent_RunningTime_Minutes'] = total_defrost_event_time_minutes
                    this_report_df['DFCycle_RecentNonAnomalyStable_PreDefrostThreshold_High'] = reports_df['DFCycle_RecentNonAnomalyStable_PreDefrostThreshold_High'].values[-1]

            ##  Termination of a defrost event
            elif ((last_state == 'DFCycle_PotentialDefrost') & (
                this_report_df['reading'].values[0] < defrostevent_tempthreshold)):
                defrost_start_time = reports_df['DFCycle_DefrostEvent_StartTime'].values[-1]
                total_defrost_event_time_minutes = (reports_df.index[-1] - defrost_start_time).total_seconds()/60
                if FLAG_Verbose:
                    print "analyze_timeseries_for_defrost():: ending a potential defrost".format(last_state)
                reports_df[reports_df.index >= defrost_start_time]['DFCycle_State'] = 'DFCycle_DefrostEvent'
                reports_df['DFCycle_Duration_Minutes'].values[-1] = total_defrost_event_time_minutes
                reports_df['DFCycle_Peak'].values[-1] = max(reports_df[reports_df.index >= defrost_start_time]['reading'].values)
                this_report_df['DFCycle_State'] = 'DFCycle_InRange'

                this_defrost_event_df = pd.DataFrame({ 'StartTime' : reports_df['DFCycle_DefrostEvent_StartTime'].values[-1],
                                                       'EndTime' : reports_df.index.values[-1],
                                                       'Duration_Minutes' : reports_df['DFCycle_DefrostEvent_RunningTime_Minutes'].values[-1],
                                                       'TemperatureMax_C' : max(reports_df.loc[reports_df.index >= reports_df['DFCycle_DefrostEvent_StartTime'].values[-1], 'reading'])}, index = [len(defrost_event_candidates_df)])
                defrost_event_candidates_df = defrost_event_candidates_df.append(this_defrost_event_df)
                
                periodic_anomalies_df = generate_periodic_anomaly_stable_stats(defrost_event_candidates_df)

                if periodic_anomalies_df is not None:
                    if FLAG_Verbose:
                        print "analyze_timeseries_for_defrost()::  analyzing candidate defrost events"

            else:
                this_report_df['DFCycle_State'] = this_report_df['DFCycle_RecentNonAnomalyStable_state_label']

            #  Append to complete set
            if FLAG_Verbose:
                print "analyze_timeseries_for_defrost()::  appending to reports_df this_report_df: {}".format(this_report_df)
            reports_df = reports_df.append(this_report_df)

    return reports_df, defrost_event_candidates_df


##
##  Main

#  Get a list of Cumberland Farms data

#  Get a list of CF devices
devices_query = "select deviceid from vu_deviceandstoreinfo where accountid = 5767"
devices_list = pd.read_sql(devices_query, DATABASE_CONN)
print "found {num} devices".format(num=len(devices_list))

#  Pull device data
devices_list = pd.DataFrame({'deviceid' : '11666000000122703371'}, index = [1])
for device_index, deviceid in enumerate(devices_list['deviceid']):
    print "iteration: {index}  deviceid: {id}".format(index=device_index, id=deviceid)

    temp_data_query = "select * from temperature where deviceid = '{id}' and portid = 0 and readingdate > '2017-07-04' order by readingdate".format(
        id=deviceid)
    print "temp_data_query: {}".format(temp_data_query)

    temp_data = pd.read_sql(temp_data_query, DATABASE_CONN)
    # print "temp_data.head():"
    # print temp_data.head()

    timeseries = pd.Series(temp_data['originaltemperature'].values,
                           index=temp_data['readingdate'].apply(lambda x: x.replace(microsecond=0)))
    devices_reports_df, defrost_event_candidates_df = analyze_timeseries_for_defrost(timeseries, max_numsamples_toprocess = 1000)
    devices_reports_df['device_id'] = str(deviceid)
    # print "main()::  devices_reports_df: {}".format(devices_reports_df)
    devices_reports_df.to_csv(os.path.join(RESULTS_DIR, "Dev_{}_DefCycleAnalysis.csv".format(str(deviceid))))
    defrost_event_candidates_df['device_id'] = str(deviceid)
    defrost_event_candidates_df.to_csv(os.path.join(RESULTS_DIR,"Dev_{}_DefrostEvents.csv".format(str(deviceid))))