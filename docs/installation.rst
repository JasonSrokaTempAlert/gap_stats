.. installation:

Installation
============

To install this package, you need to clone the repo and run the setup script::

    git clone git@bitbucket.org:JasonSrokaTempAlert/temperature-analysis-utilities.git
    cd temperature-analysis-utilities/
    python setup.py install

If you are developing on this application, you should replace the
last command with the following::

    pip install -r dev-requirements.txt
    python setup.py develop

To run the tests located in *ta_utils/tests*, run the following::

    python setup.py test

In order to generate documentation HTML, you should simply run the following::

    cd docs/
    make html

Then simply view the HTML located in *docs/_build/html*.
