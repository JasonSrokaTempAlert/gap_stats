"""
offlineperiod Module
"""
from datetime import datetime


class OfflinePeriod(object):
    """
    The OfflinePeriod Object represents an offline period in a time-series.
    """
    def __init__(self, previous_transmission, start, end):
        """
        Initialize the offline period with the previous transmission, start, and stop.

        :param previous_transmission: The transmission datetime preceding the offline period
        :type previous_transmission: datetime
        :param start: The start datetime of the offlineperiod
        :type start: datetime
        :param end: The end datetime of the offlineperiod
        :type end: datetime
        """
        self.previous_transmission = previous_transmission
        self.start = start
        self.end = end

    @property
    def length(self):
        """
        Get the time length of this offline period in seconds.

        :return: Time length of this offline period in seconds.
        :rtype: int
        """
        return int((self.end - self.start).total_seconds())

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, OfflinePeriod):
            return False

        previous_eq = self.previous_transmission == other.previous_transmission
        start_eq = self.start == other.start
        end_eq = self.end == other.end

        return previous_eq and start_eq and end_eq
