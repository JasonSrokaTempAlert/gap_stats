"""
Temperature Analytics Utils Module
"""

from ta_utils.log import logger

from ta_utils.persistence.db import database, query_to_dataframe
from ta_utils.persistence.persistable import Persistable
from ta_utils.persistence.models import PersistableSignal, PersistableSignalSegment,\
    PersistableGap, PersistableOfflinePeriod, PersistableDelay, PersistenceException
from ta_utils.persistence.start import create_tables, drop_tables

from ta_utils.gaps.gap import Gap
from ta_utils.offline_periods.offline_period import OfflinePeriod
from ta_utils.delays.delay import Delay

from ta_utils.signals.signal import Signal, SignalError, SignalSegmentError, SignalSegment
from ta_utils.signals.windows import Windows
from ta_utils.signals.incremental_signal_segment import IncrementalSignalSegment
from ta_utils.signals.incremental_signal import IncrementalSignal
from ta_utils.signals.many_device_signals import ManyDeviceSignals, ManyDeviceSignalsException

from ta_utils.analysis.defrost import has_defrost_cycle_naive
from ta_utils.analysis.compliance import find_compliance, YES_COMPLIANCE, NO_COMPLIANCE, CLOSE_COMPLIANCE
from ta_utils.analysis.device_label import get_device_metadata

from ta_utils.pipelines.update_pipeline import UpdatePipeline
