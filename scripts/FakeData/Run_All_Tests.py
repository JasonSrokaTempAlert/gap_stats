import os
import glob


test_files = glob.glob('Test_Run_*.py')

print "test_files: \n{}".format(test_files)

for test_file in test_files:
    print "executing {}".format(test_file)
    execfile(test_file)

