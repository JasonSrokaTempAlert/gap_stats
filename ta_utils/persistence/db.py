"""
Database Module

Initialize the database using the following:

>>> from ta_utils.persistence.db import database
>>> database.init('name', user='username', password='password')
"""
import pandas as pd

import psycopg2
import peewee
from playhouse.postgres_ext import PostgresqlDatabase

DATE_FORMAT = '%Y-%m-%d-%H%H%S'

database = PostgresqlDatabase(None)


def query_to_dataframe(query):
    """
    Converts a peewee Query to a DataFrame.

    :param query: A peewee SelectQuery.
    :type query: peewee.SelectQuery
    :return: DataFrame of the query result
    :rtype: pd.DataFrame
    """
    db = query.database
    connection = psycopg2.connect(database=db.database,
                                  user=db.connect_kwargs.get('user'),
                                  password=db.connect_kwargs.get('password'))

    raw_sql = db.get_cursor().mogrify(*query.sql())

    return pd.read_sql(raw_sql, con=connection)
