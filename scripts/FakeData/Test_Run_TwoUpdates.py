deviceid= 11666000000132476924
strdeviceid="'{}'".format(deviceid)
q1 = """Select * from sensor_temperature_history where device_id = 11666000000132476924 order by reading_date desc limit 5;"""
q2 = """Select * from signals where device_id=%s ;"""%(strdeviceid)
q3 = """Select c.* from signal_segments c join signals b on b.id=c.signal_id where device_id=%s order by start_time desc;"""%(strdeviceid)
q4 = """Select * from trundle_access_history where deviceid ='11666000000132476924'; """


import os,sys,inspect
import EnvironmentTesting

current_dir= os.getcwd()
values = EnvironmentTesting.dirfind(current_dir)
env_tag = EnvironmentTesting.gen_env_tag(current_dir)
testing_dir = values[0]
scripts_dir = values[1]

import numpy as np
import pandas as pd
import psycopg2
import pandas.io.sql as psql
import time
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 1000)

database = EnvironmentTesting.lookup_dbname_forenv(env_tag)
print "database: {}".format(database)
user = 'jason'
password = 'qvz2LFRzMMVPku7'
host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com'
port = '5432'
m= '                                '
conn_testrun = psycopg2.connect(database = database, user = user, password = password,host = host, port = port,sslmode = 'require')
conn_warehouse = psycopg2.connect(database = 'warehouse', user = user, password = password,host = host, port = port,sslmode = 'require')

execfile(testing_dir+'/delete_init.py')

execfile(testing_dir+'/datasim_dev6924_insert_base.py')
time.sleep(1)
print("Device in the sensor temp history table first run")
q1 = psql.read_sql(q1, conn_warehouse)
print(q1.head())

trundle_atStart = psql.read_sql(q4, conn_warehouse)


print "\n\n****\nRunning Gaps_Gen for first time\n****\n\n"
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsbefore = psql.read_sql(q2, conn_testrun)
print "signals_resultsbefore.head(): {}".format(signals_resultsbefore.head())
signal_segments_resultsbefore = psql.read_sql(q3, conn_testrun)
print "signal_segments_resultsbefore.head(): {}".format(signal_segments_resultsbefore.head())

trundle_atFirstRun = psql.read_sql(q4, conn_warehouse)

print "\n\n****\nRunning Sensor_Temperature_History 2nd time injection\n****\n\n"
execfile(testing_dir+'/datasim_dev6924_insert_weeklyUpdate.py')


print "\n\n****\nRunning Gaps_Gen for second time\n****\n\n"
execfile(scripts_dir+'/gaps_gen.py')
signals_resultsafter = psql.read_sql(q2, conn_testrun)
signal_segments_resultsafter = psql.read_sql(q3, conn_testrun)

trundle_atSecondRun = psql.read_sql(q4, conn_warehouse)


outfile = (testing_dir+'/Test_Run_TwoUpdates{tag}.xlsx'.format(tag = env_tag))
print "outfile: {}".format(outfile)
writer = pd.ExcelWriter(outfile)
signals_resultsbefore.to_excel(writer,'signals_results_before',index=False)
signal_segments_resultsbefore.to_excel(writer,'signal_segments_resultsbefore',index=False)
signals_resultsafter.to_excel(writer,'signals_resultsafter',index=False)
signal_segments_resultsafter.to_excel(writer,'ss_resultsafter',index=False)
trundle_atStart.to_excel(writer,'trundle_atStart',index=False)
trundle_atFirstRun.to_excel(writer,'trundle_atFirstRun',index=False)
trundle_atSecondRun.to_excel(writer,'trundle_atSecondRun',index=False)
writer.save()


conn_testrun.close()
conn_warehouse.close()
