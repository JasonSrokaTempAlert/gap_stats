#Testt
import os
from datetime import datetime
import time
from name_tags import name_tags


import numpy as np
import pandas as pd
import psycopg2

from trundle.trundle_client import TrundleClient

from ta_utils import ManyDeviceSignals
from ta_utils import database
from ta_utils import create_tables, drop_tables
import snowflake.connector as sc

import sys
#sys.path.insert(0,os.path.dirname(__file__)+'/FakeData')
from FakeData import EnvironmentTesting
#import EnvironmentTesting

FLAG_SAVE_SIGNALS = True
FLAG_DONT_RUN = False

REPORTING_INTERVAL_MINUTES = 60 * 15

RESET = False
#ENVIRONMENT = 'local'
#ENVIRONMENT = 'ubuntu'
#ENVIRONMENT = 'local'
# ENVIRONMENT = 'ubuntu'

# DATATAG = 'MC_and_NodeNanny'
# DATATAGS = ['GroupTest']
DATATAGS= ['DeviceTest']
# DATATAGS=['Agne']
# DATATAGS = ['NewEngland']
# DATATAG = 'AllCVS'
#DATATAGS = ['RaceTrac']
#DATATAGS = ['Racetrac','TacoBell','GoLiveFS']
#DATATAGS = ['99']
# DATATAGS=['Dominos']
#DATATAGS = ['GoLive']
#DATATAGS = ['Google']
#DATATAGS = ['GoLiveFS']

#DATATAGS = ['Walgreens']
#DATATAGS=['SaveALot']
#DATATAGS=['iPic']
#DATATAGS=['RachaelsFoods','JennyCraig']
#DATATAGS=['SheridanWindermere']
#DATATAGS=['Vallee']

#DATATAGS = ['VA']
#DATATAGS = ['Walgreens']


#ONLY CHANGED TO STATE TAGS FOR GO LIVE
DEVICE_ID_COL = 'device_id'
DATE_COL = 'reading_date'
READING_COL_NODES = 'temperature'
READING_COL_GATEWAYS = 'channel'
PROCESSED_COL = 'processed_date'
PORT_COL = 'port'


ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2017-11-01 00:00:00')



# Environment
if os.path.isdir('/Users/Jason/'):
    Environment = 'jason_local'
    DATA_DIR = '/Users/Jason/Desktop/Local_Data_Store'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
elif os.path.isdir('/Users/jheel/'):
    Environment = 'jheel_local'
    DATA_DIR = '/Users/jheel/gap_stats/Data'
    ACCESS_CREDS_DIR = '/Users/jheel/gap_stats/prod-trundle-analyticsconfig'
elif os.path.isdir('/home/jason/'):
    Environment = 'ubuntu'
    DATA_DIR = '/home/jason/Data'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
else:
    error("Don't recognize environment")

current_dir= os.getcwd()
env_tag = EnvironmentTesting.gen_env_tag(current_dir)
db_name = EnvironmentTesting.lookup_dbname_forenv(env_tag)

conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='{db}' user='jason' password='qvz2LFRzMMVPku7' ".format(db = db_name)
# conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='gapstats_sandbox' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)
warehouse_conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
WAREHOUSE_DATABASE_CONN = psycopg2.connect(warehouse_conn_string)
SNOWFLAKE_DATABASE_CONN= sc.connect(user='jheel', password='Paperfold40!', account='fo68387.us-east-1')


print "Environment: {env}  \n DATA_DIR: {dd} \n ACCESS_CRED_DIR: {ad} \n conn_string: {cs}".format(
    env = Environment, dd = DATA_DIR, ad = ACCESS_CREDS_DIR, cs = conn_string
)
    
database.init(db_name, user='jason', password='qvz2LFRzMMVPku7',
                   host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
                   port='5432')


SECONDS_DELAY_BEFORE_TABLE_DROP = 15 if db_name == 'warehouse' else 5
if RESET:
    print "\n\nWARNING!\nWARNING!\nprepared to drop tables in {tag}- you have {secs} seconds to quite before it happens...".format(
        tag = db_name, secs = SECONDS_DELAY_BEFORE_TABLE_DROP)
    time.sleep(SECONDS_DELAY_BEFORE_TABLE_DROP)
    print "time's up, dropping tables"
    drop_tables()
    print 'dropped tables'
    create_tables()
    print 'created tables'




# TODO:  add unit tests for FormatDatetime function
# def format_datetime(raw_datetime):
#     # print "format_datetime()::  called with raw_datetime: {}".format(raw_datetime)
#     result = raw_datetime.replace('T', ' ')
#     if '.' in result:
#         index =int(result.index('.'))
#         final = (result[0:index]) + (result[index:index+6])
#     else:
#         final = result + '.00'
# #    return_datetime = result.split('.')[0]
#     # print "format_datetime()::  generated result: {}".format(return_datetime)
#     return final


def format_datetime(raw_datetime):
    # print "format_datetime()::  called with raw_datetime: {}".format(raw_datetime)
    result = raw_datetime.replace('T', ' ')
    if '.' in result:
        index =int(result.index('.'))
        return_datetime = result.split('.')[0]
        final = return_datetime
    else:
        final = result 
#    return_datetime = result.split('.')[0]
    # print "format_datetime()::  generated result: {}".format(return_datetime)
    return final


def read_query_snowflake(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute( query )
        names = [ x[0] for x in cursor.description]
        rows = cursor.fetchall()
        return pd.DataFrame( rows, columns=names)
    finally:
        if cursor is not None:
            cursor.close()  

    
print "Reading devices info from vu_deviceandstoreinfo"
device_query= "select datedeleted::timestamp(0),deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0),state from vu_deviceandstoreinfo "
DevicesInfo = pd.read_sql(device_query,WAREHOUSE_DATABASE_CONN)
#Snowflake same query
#device_query= "select * from ds.main.vu_deviceandstoreinfo"
#DevicesInfo  =read_query_snowflake(SNOWFLAKE_DATABASE_CONN,device_query)
print "done with devices info  read, raw number of devices: {count}".format(count = len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(DevicesInfo[DevicesInfo['datedeleted']>=ANALYSIS_WINDOW_START_DATE])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(start = ANALYSIS_WINDOW_START_DATE, count = len(DevicesInfo))

print "Reading sensors info from sensor table"
#VERY VERY IMPORTANT CHANGE HIDDEN== FALSE 
sensor_query= "select s.sensorid, s.deviceid, s.portid from sensor s join trundle_access_history t on (t.deviceid=s.deviceid and t.portid=s.portid) where s.hidden = False and s.datedeleted is NULL"
sensor_info = pd.read_sql(sensor_query, WAREHOUSE_DATABASE_CONN)
#Snowflake same query
#sensor_query= "select s.sensorid, s.deviceid, s.portid from staging.accounts.sensor s join ds.main.trundle_access_history t on (t.deviceid=s.deviceid and t.portid=s.portid) where s.hidden = False and s.datedeleted is NULL"
#sensor_info  =read_query_snowflake(SNOWFLAKE_DATABASE_CONN,sensor_query)


print "done with sensor read, number of non-hidden sensors = {numsens}".format(numsens = len(sensor_info))

def device_mintime_override_lookup(deviceid, reading_col, port):
    print "device_mintime_override_lookup():: called with deviceid = '{devid}' and port: {port}".format(devid = deviceid, port = port)

    install_date_query = "select deviceinstalldate::timestamp(0) from vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)

    install_results = pd.read_sql(install_date_query, WAREHOUSE_DATABASE_CONN)
    print install_results, 'x'
    if len(install_results) >= 1 :
    	print "Entering the loop"
    	port_string_1 = '' if port is None else "and portid = {}".format(port)
    	print port_string_1
    	if port_string_1 == '' :
    		print("going in gateway loop")
    		install_date_query_null = "select MIN(readingdate)::timestamp(0) as deviceinstalldate from gateway_power where deviceid = '{devid}' ".format(devid = deviceid)
    	else :
    		#print("SHOULD GO IN THIS LOOP")
    		#print("going in node loop")
        	install_date_query_null = "select first_readingdate::timestamp(0)  as deviceinstalldate from first_reading where deviceid = '{devid}' {port}".format(devid = deviceid, port = port_string_1)
    	install_results_null = pd.read_sql(install_date_query_null, WAREHOUSE_DATABASE_CONN)
    	print install_results_null
    	# if pd.isnull(install_results_null.values[0]) :
        	# install_results_null.values[0]  = ANALYSIS_WINDOW_START_DATE
    	if install_results.values[0] != ANALYSIS_WINDOW_START_DATE and pd.notnull(install_results.values[0]) :

            install_results = min(install_results['deviceinstalldate'].values[0],
                                  install_results_null['deviceinstalldate'].values[0]) \
                if pd.notnull(install_results_null['deviceinstalldate'].values[0]) else \
                install_results['deviceinstalldate'].values[0]
			#print "IF"
    	else :
	    	install_results = install_results_null['deviceinstalldate'].values[0]
	    	#print "ELSE"
    	install_date_dt64 = install_results
    	# print install_results
    	# print type(install_results)
    	# print install_date_dt64
    	# print type(install_date_dt64)
    	install_date_str = format_datetime(str(install_date_dt64))
    	# print install_date_str
    	install_date = datetime.strptime(install_date_str,'%Y-%m-%d %H:%M:%S.%f')
    	#print "Stupid"
        result = max(ANALYSIS_WINDOW_START_DATE, install_date)
        print "device_mintime_override_lookup()::  returning {p1} as type {p2}".format(p1 = result, p2 = type(result))
    	return result
    else :
        return ANALYSIS_WINDOW_START_DATE

def device_maxtime_override_lookup(deviceid, reading_col, port):
    print "device_maxtime_override_lookup():: called with deviceid = '{devid}' and port: {port}".format(devid = deviceid, port = port)

    port_string = '' if port is None else "and portid = {}".format(port)
    ##  order of priority:  datedeleted (from Monitored_DeviceIDs), channel, temperature
    trundle_query = """select update_time::timestamp(0) from trundle_access_history where deviceid = '{devid}' {port_string} and data_type = '{type}'""".format(devid = deviceid,
                                                                                                                                        type = reading_col,
                                                                                                                                        port_string = port_string)
    print "trundle_query: {}".format(trundle_query)
    trundle_results = pd.read_sql(trundle_query, WAREHOUSE_DATABASE_CONN)
    print "trundle_results: {}".format(trundle_results)
    if len(trundle_results)>1:
        print "multiple results from trundle_access_history table - which should be used? - using min for now"
        trundle_max_time = datetime.strptime(format_datetime(str(min(trundle_results['update_time'].values))),'%Y-%m-%d %H:%M:%S')
        print "len > 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
    elif len(trundle_results)==1:
        trundle_max_time = datetime.strptime(format_datetime(str(trundle_results['update_time'].values[0])),
                                             '%Y-%m-%d %H:%M:%S')
        print "len = 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
    else:
        print "no trundle access in history, using signals date"
        signals_query = "select max_processed_time::timestamp(0),max_time_override::timestamp(0) from signals where device_id = '{devid}' and data_type = '{type}';".format(devid = device_id, type = reading_col)
        print 'submitting query: {query}'.format(query = signals_query)
        signals_entry = pd.read_sql(signals_query, DATABASE_CONN)
        print "signals query return: \n{}".format(signals_entry)
        if len(signals_entry) == 1:
            max_time = signals_entry['max_time_override'].values[0] if signals_entry['max_time_override'].values[0] is not None else signals_entry['max_processed_time'].values[0]
            print "signals entry max_time: {}".format(max_time)
            if max_time is None:
                trundle_max_time = None
            else:
                trundle_max_time = datetime.strptime(format_datetime(str(max_time)),'%Y-%m-%d %H:%M:%S')
            print "trundle_max_time: {}".format(trundle_max_time)
        else:
            trundle_max_time = None

    datedeleted = Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid']==deviceid].datedeleted.values[0]
    print "datedeleted: {}".format(datedeleted)
    if ( (datedeleted is not None) & ~(pd.isnull(datedeleted))):
        datedeleted = datetime.strptime(format_datetime(str(datedeleted)),'%Y-%m-%d %H:%M:%S')
        if trundle_max_time is None:
            max_time = datedeleted
        else:
            max_time = min(trundle_max_time, datedeleted)
    else:
        max_time = trundle_max_time
    print "device_maxtime_override_lookup()::  max_time: {}".format(max_time)
    return max_time



print "setting up devicesinfo with DATATAGS: {tag}".format(tag=DATATAGS)


def TranslateGroupIDsToDeviceInfo(GroupIDs):
    ##  First get the devices that are in the store's group
    print DevicesInfo.head()
    DeviceIndices = np.where(DevicesInfo['groupid'].isin(GroupIDs))[0]
    DeviceInfo = DevicesInfo.iloc[DeviceIndices]
    #     DeviceInfo['StoreGroupID'] = DeviceInfo['GroupID']
    ##  Next get the devices that are in the children of a store's group
    DeviceIndices_ChildrenGroups = np.where(DevicesInfo['parentgroupid'].isin(GroupIDs))[0]
    DeviceInfo_ChildrenGroups = DevicesInfo.iloc[DeviceIndices_ChildrenGroups]
    #     DeviceInfo_ChildrenGroups['StoreGroupID'] = DeviceInfo_ChildrenGroups['ParentGroupID']
    ##  Put them together
    DeviceInfo = DeviceInfo.append(DeviceInfo_ChildrenGroups)
    return DeviceInfo


def AugmentDevicesInfoWithStoreGroupID(DevicesInfo):
    num_devices = len(DevicesInfo)
    print "AugmentDevicesInfoWithStoreGroupID():: called with {numdevs} devices".format(numdevs=num_devices)
    AugDevicesInfo = DevicesInfo
#    AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid'].where(AugDevicesInfo['accountid'] != 4226 | AugDevicesInfo['groupname'].str.contains('Store #'), AugDevicesInfo['parentgroupid'])
    AugDevicesInfo['store_groupid'] = AugDevicesInfo['groupid']
    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    # print "beginning storegroupid lookup at time {now}".format(now=datetime.now())
    # for row_index in range(0, num_devices):
    #     this_parent_group_id = AugDevicesInfo['parentgroupid'].values[row_index]
    #     num_devices_in_parent_group = len(DevicesInfo[DevicesInfo['groupid'] == this_parent_group_id])
    #     #         print "processing row {row}, with parent groupid {gid}, found {numdev} devices in parent".format(row = row_index, gid = this_parent_group_id, numdev = num_devices_in_parent_group)
    #     if num_devices_in_parent_group > 0:
    #         #             print "found parent group with devices, resetting store group id"
    #         AugDevicesInfo['storegroupid'].iloc[row_index] = this_parent_group_id
    # print "completed storegroupid lookup at time {now}".format(now=datetime.now())
	    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    return AugDevicesInfo


for this_data_tag in DATATAGS:
    
    Monitored_DevicesInfo = name_tags(DevicesInfo, this_data_tag)
    Monitored_DeviceIDs = Monitored_DevicesInfo['deviceid']
    NumMonitoredDevices = len(Monitored_DeviceIDs)
    print "Monitored_DevicesInfo.head(): {}".format(Monitored_DevicesInfo.head())
    Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['devicetype']=='Gateway']
   
    print """Done with group selection.
             DATATAG: {tag}
             NumDevices: {devcnt}
             NumGateways: {grpcnt}
             """.format(tag=this_data_tag,
                       devcnt=NumMonitoredDevices,
                       grpcnt=len(Monitored_Gateway_DevicesInfo))
    
    

    
    if FLAG_SAVE_SIGNALS is True:
        StartTime = datetime.now()
        print "Beginning combined analysis using Postgres Temperature at time {now}".format(now=StartTime)
    
        ###   Get the set of groups being monitored
    
        all_group_ids = Monitored_DevicesInfo['groupid'].unique()
        num_group_ids = len(all_group_ids)
        group_id_index = 1
        for group_id in all_group_ids:
            group_starttime = datetime.now()
            group_index_string = 'processing group: {id},  #{index} of {total}'.format(id = group_id,index = group_id_index, total = num_group_ids)
            print group_index_string
    
            ##
            this_group_devicesinfo = Monitored_DevicesInfo[Monitored_DevicesInfo['groupid'] == group_id]
            this_group_nodesinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='ZPoint Node']
    
            if np.isnan(group_id):
                print "special processing for 'nan' groupid"
                all_device_ids = this_group_devicesinfo[np.isnan(this_group_devicesinfo['groupid'])].deviceid
            else:
                all_device_ids = this_group_devicesinfo[this_group_devicesinfo['groupid'] == group_id]['deviceid'].unique()
    
            num_devices = len(all_device_ids)
    
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, port, max_processed_time::timestamp(0), max_time_override::timestamp(0) from signals
            where data_type = 'temperature' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_temperature_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            # print "Found {numrows} existing signals".format(numrows = len(existing_temperature_signals))
            print "existing_temperature_signals: {}".format(existing_temperature_signals)
            # print "Found {numrows} existing signals".format(numrows = len(existing_temperature_signals))
            if len(existing_temperature_signals)>0:
                existing_temperature_signals['max_time'] = ''
                for row_index in range(0,len(existing_temperature_signals)):
                    print "evaluating row {index} in existing_temperature_signals".format(index = row_index)
                    existing_temperature_signals['max_time'].iloc[row_index] = existing_temperature_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_temperature_signals['max_time_override'].iloc[row_index]) else existing_temperature_signals['max_time_override'].iloc[row_index]
                        
                print "After Initial max_time set:    existing_temperature_signals: {}".format(existing_temperature_signals)
            	print "existing_temperature_signals: {}".format(existing_temperature_signals)

            else:
                existing_temperature_signals=pd.DataFrame({'device_id' : None, 'max_time' : None}, index = range(0,0))
            ##
            ##   First, process all devices using temperature data
            ##
			    
            empty_device_ids = []
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for dev_index, device_id in enumerate(all_device_ids):
                print group_index_string + " Jheel Look here   deviceid: {devid} #{dev_index} of {num_devids}".format(devid = device_id,
                                                                                                   dev_index = dev_index,
                                                                                                   num_devids = len(all_device_ids))

                #  for devices with one or more non-hidden sensor, process for temperature
                if device_id in sensor_info.deviceid.values:
                    # Determine the processed_after date either from prior signal or using window start
                    if device_id in existing_temperature_signals['device_id'].values:
                        print "existing_temperature_signals[existing_temperature_signals['device_id']==device_id]: {}".format(existing_temperature_signals[existing_temperature_signals['device_id']==device_id])
                        raw_max_time = existing_temperature_signals[existing_temperature_signals['device_id']==device_id].max_time.values[0]
                        if raw_max_time is None:
                              max_time = ANALYSIS_WINDOW_START_DATE
                        else:
                              print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                              print "Temp time",raw_max_time
                              m= datetime.strptime(format_datetime(str(raw_max_time)),'%Y-%m-%d %H:%M:%S')
                              print "CHECKING",m
                              this_group_trundle_query = """select deviceid, portid, update_time::timestamp(0), data_type from trundle_access_history where deviceid = '{devid_list}'; """.format(devid_list = device_id)
                              existing_trundle = pd.read_sql(this_group_trundle_query, DATABASE_CONN)
                              a = datetime.strptime(format_datetime(str(existing_trundle['update_time'].values[0])),'%Y-%m-%d %H:%M:%S')
                              print "Trundle Time",a
                              diff = (a-m).seconds
                              print diff
                              if diff == 0 or a <= m :
                                    print "CONTINUE"
                                    FLAG_DONT_RUN = True
                                    continue
                              else:
                                  max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
                                  print max_time,'THIS DAMN LOOP'
                                  FLAG_DONT_RUN = False
                        time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                        print "found device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
                        flag_existing_signal = True                   #print "JJJJJ",flag_existing_signal,max_time   
                    else :
					    max_time = ANALYSIS_WINDOW_START_DATE
					    time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
					    print "did not find device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
					    flag_existing_signal = False
					    
					                            
                    #print "Pulling temperature history for {devid}".format(devid=device_id)
                #for loop should go
                #compare without milliseconds maxtime and trundle access
                    newdevices_data_query = """SELECT
                                            device_id::text device_id, reading_date::timestamp(0), processed_date::timestamp(0), temperature, age, port, str_device_id
                                          FROM public.sensor_temperature_history
                                          WHERE {time_clause}
                                             AND device_id = '{devid}' """.format(time_clause = time_clause,
                                                                                  devid = device_id)
                    print "submitting new devices query: {query}".format(query = newdevices_data_query)
                    results_df = pd.read_sql(newdevices_data_query, WAREHOUSE_DATABASE_CONN)
#                    newdevices_data_query = """SELECT
#                                            device_id::text device_id, reading_date, processed_date, temperature, age, port, str_device_id
#                                          FROM ds.main.sensor_temperature_history
#                                          WHERE {time_clause}
 #                                            AND device_id = '{devid}' """.format(time_clause = time_clause,
  #                                                                                devid = device_id)
                    #print "submitting new devices query: {query}".format(query = newdevices_data_query)
                    #results_df = pd.read_sql(newdevices_data_query, SNOWFLAKE_DATABASE_CONN)
                    
                    if len(results_df) > 0:
                        print "results_df.head(): {}".format(results_df.head())
                        all_results_df = all_results_df.append(results_df)
                    else:
                        print "no data found"
                        # if max_time is not None:
                        empty_device_ids.append(device_id)
    
            if len(all_results_df) > 0:
                print "temperature call to ManyDeviceSignals with {rows} rows".format(rows = len(all_results_df))
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL,
                                        port_col=PORT_COL)
    
                print "temperature call to update_all_signals"
                updated_signals = mds.update_all_signals(READING_COL_NODES,
                                                         segment_daily=True,
                                                         # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                         #min_processed_time_override=device_mintime_override_lookup,
                                                         #min_time_override=device_mintime_override_lookup,
                                                         max_processed_time_override=device_maxtime_override_lookup,
                                                         max_time_override=device_maxtime_override_lookup
                                                         )
                print "Number of Updated Temperature Signals: {0}".format(len(updated_signals))

            print "Before handling empty temperature signals, time is: {}".format(datetime.now())
            print "empty_device_ids: {}".format(empty_device_ids)
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    print "temperature empty device data update"
                    if empty_device_id not in existing_temperature_signals['device_id'].values:
                        ports = sensor_info[sensor_info['deviceid']==empty_device_id].portid.values
                        print ports,'x- Port loop'
                        for port in ports:
                            if pd.isnull(port):
                            	print 'new signal, port is null'
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=True,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup(empty_device_id,READING_COL_NODES,None),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None)
                                                                        )
                            else:
                            	print "new signal, empty_device_id: {did}, port: {port}".format(
                                    port = port, did = empty_device_id
                                )
                                print "gaps_gen calling update_empty_device_signal with max_processed_time_override = {mto}".format(
                                    mto = device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,port)
                                )
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=True,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup(empty_device_id,READING_COL_NODES,int(port)),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,int(port)),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,int(port)))

                    else:

                        ports = existing_temperature_signals[existing_temperature_signals['device_id']==empty_device_id].port.values
                        print "ports for empty device: {ports}".format(ports = ports)
                        empty_mds = ManyDeviceSignals('Empty_MDS','Empty_MDS','Empty_MDS')
                        for port in ports:
                            if pd.isnull(port):
                                print "pre-existing signal, empty_device_id: {did}, null port".format(
                                    	port=port, did=empty_device_id
                                	)
                                empty_mds.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=True,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=max_time,
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id, READING_COL_NODES, None),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None))
                            else:
                                    print "pre-existing signal, empty_device_id: {did}, port: {port}".format(
                                    	port=int(port), did=empty_device_id
                                	)
                                    empty_mds.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=True,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=max_time,
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id, READING_COL_NODES, int(port)),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,int(port)))

                print "Number of empty Temperature Signals: {0}".format(len(empty_device_ids))
    

            ##
            ##   Second, process gateways using channel data
            ##
            this_group_gatewaysinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='Gateway']
            all_gateway_ids = this_group_gatewaysinfo['deviceid'].unique()
            num_gateways = len(all_gateway_ids)
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, max_processed_time::timestamp(0), max_time_override::timestamp(0) from signals
            where data_type = 'channel' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_channel_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            if len(existing_channel_signals)>0:
                existing_channel_signals['max_time'] = ''
                for row_index in range(0,len(existing_channel_signals)):
                    print "evaluating row {index} in existing_channel_signals".format(index = row_index)
                    existing_channel_signals['max_time'].iloc[row_index] = existing_channel_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_channel_signals['max_time_override'].iloc[row_index]) else existing_channel_signals['max_time_override'].iloc[row_index]

            else:
                existing_channel_signals=pd.DataFrame({'device_id' : None, 'max_time' : None},index = range(0,0))
            print "existing_channel_signals: {}".format(existing_channel_signals)
    
            empty_device_ids = []
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for device_id in all_gateway_ids:
                # Determine the processed_after date either from prior signal or using window start
                print "examining gateway deviceid: {devid}".format(devid = device_id)
                if device_id in existing_channel_signals['device_id'].values:
                    raw_max_time = existing_channel_signals[existing_channel_signals['device_id']==device_id].max_time.values[0]
                    if raw_max_time is None:
                        max_time = ANALYSIS_WINDOW_START_DATE
                    else:
                        max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
                    time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                    flag_existing_signal = True
                else:
                    max_time = ANALYSIS_WINDOW_START_DATE
                    time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
                    flag_existing_signal = False
                print "Pulling channel history for {devid}".format(devid=device_id)
                newdevices_data_query = """SELECT
                                        device_id::text device_id, reading_date::timestamp(0), processed_date::timestamp(0), channel, str_device_id
                                      FROM public.device_channel_history
                                      WHERE
                                        {time_clause}
                                         AND device_id = '{devid}' """.format(time_clause = time_clause,
                                                                              devid = device_id)
                print "submitting query: {query}".format(query = newdevices_data_query)
                results_df = pd.read_sql(newdevices_data_query, WAREHOUSE_DATABASE_CONN)
                if len(results_df) > 0:
                    print "results_df.head(): {}".format(results_df.head())
                    all_results_df = all_results_df.append(results_df)
                else:
                    print "no data found"
                    # if max_time is not None:
                    empty_device_ids.append(device_id)
    
            print "pre-ManyDeviceSignals all_results_df.head(): {}".format(all_results_df.head())
    
            if len(all_results_df) > 0:
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL)
    
                updated_signals = mds.update_all_signals(READING_COL_GATEWAYS,
                                                         segment_daily=True,
                                                         # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                        # min_time_override=max_time,
                                                         #max_time_override=device_maxtime_override_lookup
                                                         max_processed_time_override=device_maxtime_override_lookup,
                                                         max_time_override=device_maxtime_override_lookup
                                                         )
                print "Number of Updated Channel Signals: {0}".format(len(updated_signals))
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    print "empty device data update for channel info"
                    if empty_device_id not in existing_channel_signals['device_id'].values:
                        print "New channel signal, no data"
                        ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_GATEWAYS,
                                                                 segment_daily=True,
                                                                 reporting_interval_override=REPORTING_INTERVAL_MINUTES,
    																 min_time_override=device_mintime_override_lookup(empty_device_id,READING_COL_GATEWAYS,None),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_GATEWAYS,-1),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_GATEWAYS,-1)  
                                                                 )
                    else:
                        empty_mds = ManyDeviceSignals('Empty_MDS','Empty_MDS','Empty_MDS')
                        empty_mds.update_empty_device_signal(empty_device_id, READING_COL_GATEWAYS,
                                                             segment_daily=True,
                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                             min_time_override=max_time,
                                                             max_time_override=device_maxtime_override_lookup(
                                                                 empty_device_id, READING_COL_NODES, -1),
                                                             max_processed_time_override=device_maxtime_override_lookup(
                                                                 empty_device_id, READING_COL_NODES, -1))

                print "updated {num} empty signals".format(num = len(empty_device_ids))
            # print "updated_signals: {}".format(updated_signals)

	    for device_id in all_gateway_ids:
	    	print device_id,'This test round'
		update_gateway_query = """update signals set coverage= null where device_id = '{devid}' and data_type='channel' """.format(devid = device_id)
	        print update_gateway_query
		update_segments_gateway_query = """update signal_segments c set coverage= null from signals b where b.id = c.signal_id and b.device_id = '{devid}' """.format(devid = device_id)

		cs = WAREHOUSE_DATABASE_CONN.cursor() 
		print 'Running first query'
		cs.execute(update_gateway_query) 
		print 'Running second query'
		cs.execute(update_segments_gateway_query)       
 		WAREHOUSE_DATABASE_CONN.commit()
		cs.close()   
            print "group runtime: {}".format(datetime.now() - group_starttime)
            group_id_index += 1
    
    
        EndTime = datetime.now()
        print "Combined Analysis Time: {0}".format(EndTime - StartTime)
