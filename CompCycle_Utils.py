import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.dates as mdates
import re
from os import walk
from scipy.spatial.distance import euclidean
from scipy.stats import mode
from fastdtw import fastdtw
import peakutils
import subprocess
from time import time

pd.set_option('display.float_format', lambda x: '%.3f' % x)

one_minute = np.timedelta64(1, 'm')
one_hour = np.timedelta64(1, 'h')
one_day = 24 * one_hour


def get_sensor_tempdata(data, sensor, SortField='Time') :
    print 'get_sensor_tempdata() called with SortField: {}'.format(SortField)
    sensor_data = data[data['SensorID'] == sensor]
    sensor_data = sensor_data.sort_values(by = SortField).reset_index()

    return sensor_data


def find_start_stop(sensor_data, start_time, end_time) :
    print 'find_start_stop() called with start_time: {} and end_time: {}'.format(start_time,end_time)
#    print 'ReadingTimes:'
#    print sensor_data['ReadingDate']
    start_ind = len(sensor_data[sensor_data['ReadingDate'] < pd.to_datetime(start_time)])
    end_ind = len(sensor_data[sensor_data['ReadingDate'] < pd.to_datetime(end_time)]) - 1
    
    return start_ind, end_ind

def temps_times(sensor_data, start_ind, end_ind) :
    temps = sensor_data['OriginalReading'][start_ind:end_ind].values
    times = sensor_data['ReadingDate'][start_ind:end_ind].values

    return temps, times

    

def plot_intervals(times) :    
    print 'times[1:] - times[:-1]:'
    print times[1:] - times[:-1]
    intervals = (times[1:] - times[:-1]) / one_minute
    print 'intervals:'
    print intervals

    fig = plt.figure(figsize=(15,2))
    ax = fig.add_subplot(1, 1, 1)
#    ax.plot(intervals[:480])
    ax.plot(intervals)
    ax.set_ylim(-1,1+max(intervals))
    ax.set_xlabel('Index')
    ax.set_ylabel('Interval (min)')

def plot_temps(temps, times) : 
    print 'plot_temps() call with times vector:'
#    print times   
    fig = plt.figure(figsize=(15,2))
    ax = fig.add_subplot(1, 1, 1)
#    ax.plot(times[:480], temps[:480])
    ax.plot(times, temps)
#    ax.plot(temps)
#     ax.set_ylim(0,20)
    ax.set_xlabel('Time')
    ax.set_ylabel('Temp')
#    ax.set_title('Data from {} to {}'.format(pd.to_datetime(times[0]).date(),pd.to_datetime(times[-1]).date()))

def plot_sensor_tempdata_recenthistory(RawTempData,Days=-1,ax=False,CompCycleInMinutes=False) :
#	If Days argument is 0 or negative, will show full history, otherwise will show most recent Days
#    ax is an optional axis on which to put the plot - if nothing is passed, one will be created
    print 'plot_sensor_tempdata_recenthistory() called with Days: {}'.format(Days)
    if(ax==False) :
        fig = plt.figure(figsize=(15,2))
        ax = fig.add_subplot(1, 1, 1)
        
    TitleString = 'SensorID: {}'.format(RawTempData['SensorID'].values[0])
    
    #  Extract the subset of data reflecting the days of interest (if necessary)
    LastReadingDate=RawTempData['ReadingDate'].iloc[-1]
    if (Days > 0) :
        FirstReadingDate=LastReadingDate - Days*one_day
        print 'DateMath: Last Date: {}  Days: {}  Start Date: {}'.format(LastReadingDate,Days,FirstReadingDate)
        TempData=RawTempData[RawTempData['ReadingDate'] >= FirstReadingDate]
        TitleString = TitleString + '  Last {} days'.format(Days)
    else :
        TempData=RawTempData
        FirstReadingDate=RawTempData['ReadingDate'].iloc[0]
        
    
#    ax.plot(times[:480], temps[:480])
    ax.plot(TempData['ReadingDate'], TempData['OriginalReading'])
#    ax.plot(temps)
#     ax.set_ylim(0,20)
    ax.set_xlabel('Time')
    ax.set_ylabel('Temp')
#    ax.set_title('Data from {} to {}'.format(pd.to_datetime(times[0]).date(),pd.to_datetime(times[-1]).date()))
    if(CompCycleInMinutes!=False) :
        TitleString = TitleString + '  CycleTime: {} minutes'.format(CompCycleInMinutes)
        CompCycleTimes=pd.date_range(FirstReadingDate,LastReadingDate,freq='{}T'.format(CompCycleInMinutes))
        for thisTime in CompCycleTimes :
            ax.axvline(thisTime,color='r',linewidth=0.1)
    ax.set_title(TitleString)
    
def segment_data(temps, n_r) :

    min_temp = np.min(temps)
    max_temp = np.max(temps)
    half_temp = (max_temp + min_temp) / 2.

    pct_above_half = float(len(temps[temps > half_temp])) / len(temps)


    if pct_above_half > 0.2 :
        peak_locs = [0, len(temps) - 1]
    else :
        peak_locs = peakutils.indexes(temps, thres =  0.5, min_dist = 20)
        peak_locs = list(peak_locs)
#         peak_locs.insert(0,0)
        peak_locs.append(len(temps) - 1)

    seg_locs = [0]

    nn = n_r
    for loc in peak_locs :
        nn = seg_locs[-1] + n_r
        if loc - seg_locs[-1] > n_r :
            while nn < loc :
                seg_locs.append(nn)
                nn += n_r
#         if loc != 0 : 
        seg_locs.append(loc)
    
    return seg_locs, peak_locs


def plot_n_days(temps, seg_locs, peak_locs) :

    f_trans = np.fft.fft(temps)
    freq = np.fft.fftfreq(len(temps))
    n_f = len(f_trans)
    hours = 60. / standard_interval
    ft_peak_locs = peakutils.indexes(abs(f_trans[1:n_f/2]), thres =  0.25, min_dist = 20)
#     if len(ft_peak_locs) > 0 : print 60. / (freq[1:n_f/2][ft_peak_locs] * hours)

    fig = plt.figure(figsize=(15,5))
    ax = fig.add_subplot(1, 2, 1)
    ax.plot(freq[1:n_f/2] * hours, abs(f_trans[1:n_f/2]))
    ax.plot(freq[1:n_f/2][ft_peak_locs] * hours, abs(f_trans[1:n_f/2])[ft_peak_locs], marker = 'o', ls = 'None')
    ax.set_xlabel(r'cycles / hour')
    ax = fig.add_subplot(1, 2, 2)
    ax.plot(temps)
    ax.plot(peak_locs, temps[peak_locs], marker = 'o', ls = 'None')
    ax.set_xlabel(r'Readings')

    for loc in seg_locs :
        ax.axvline(x = loc, c = 'k')
    
    return
    
def plot_segments(freq, f_trans, n_f, fft_peak_locs, temps, start_ind, end_ind) :
    fig = plt.figure(figsize=(10,2))
    ax = fig.add_subplot(1, 2, 1)
    ax.plot(freq[1:n_f/2] * 20., abs(f_trans[1:n_f/2]))
    ax.plot(freq[1:n_f/2][fft_peak_locs] * 20., abs(f_trans[1:n_f/2][fft_peak_locs]), marker = 'o', ls = 'None')
    ax.set_xlabel(r'cycles / hour')
    ax = fig.add_subplot(1, 2, 2)
    ax.plot(temps[start_ind:end_ind])

def find_fft_peaks(temps, start_ind, end_ind) :
    n_inds = end_ind - start_ind
    f_trans = np.fft.fft(temps[start_ind:end_ind])# - tbase)
    freq = np.fft.fftfreq(end_ind - start_ind)
    n_f = len(f_trans)
    fft_peak_locs = peakutils.indexes(abs(f_trans[1:n_f/2]), thres =  0.7, min_dist = n_inds / 20)
    return f_trans, freq, n_f, fft_peak_locs
    


def mean_std(comp_cyc, weights) :
    if len(weights) > 0 :
        mean_comp_cyc = np.average(comp_cyc, weights = weights)
        std_comp_cyc = np.sqrt(np.average((comp_cyc - mean_comp_cyc)**2, weights=weights))
    else :
        mean_comp_cyc = n_r * standard_interval
        std_comp_cyc = n_r * standard_interval
    return mean_comp_cyc, std_comp_cyc


def alt_comp_cyc(comp_cyc, weights, alt_cyc_ind) :
    alt_comp_cyc = [cyc for ii, cyc in enumerate(comp_cyc) if ii != alt_cyc_ind]
    alt_weights = [weight for ii, weight in enumerate(weights) if ii != alt_cyc_ind]
    alt_mean_comp_cyc, alt_std_comp_cyc = mean_std(alt_comp_cyc, alt_weights)

    return alt_mean_comp_cyc, alt_std_comp_cyc
    
def find_mean_std_comp_cyc(comp_cyc, weights, len_cycs, window) :
    mean_comp_cyc, std_comp_cyc = mean_std(comp_cyc, weights) 
#     print 'Compressor Cycle = %.2f +/- %.2f minutes' % (mean_comp_cyc, std_comp_cyc)
    lost_ind = -1
    
#     if len_cycs > window / 2. + 1 and 
    if len(comp_cyc) > 2:
#     if std_comp_cyc < mean_comp_cyc and len_cycs > min_len_measured + n_r / 2 and len(comp_cyc) > 2:
        max_cyc_ind = comp_cyc.index(max(comp_cyc))
        low_mean_comp_cyc, low_std_comp_cyc = alt_comp_cyc(comp_cyc, weights, max_cyc_ind)
#         print 'Max-dropped Compressor Cycle = %.2f +/- %.2f minutes, %d' % (low_mean_comp_cyc, low_std_comp_cyc, max_cyc_ind)
        min_cyc_ind = comp_cyc.index(min(comp_cyc))
        hi_mean_comp_cyc, hi_std_comp_cyc = alt_comp_cyc(comp_cyc, weights, min_cyc_ind)
#         print 'Min-dropped Compressor Cycle = %.2f +/- %.2f minutes, %d' % (hi_mean_comp_cyc, hi_std_comp_cyc, min_cyc_ind)
        if low_std_comp_cyc < std_comp_cyc and low_std_comp_cyc < hi_std_comp_cyc :
            mean_comp_cyc = low_mean_comp_cyc
            std_comp_cyc = low_std_comp_cyc
            lost_ind = min_cyc_ind
        elif hi_std_comp_cyc < std_comp_cyc and hi_std_comp_cyc < low_std_comp_cyc :
            mean_comp_cyc = hi_mean_comp_cyc
            std_comp_cyc = hi_std_comp_cyc
            lost_ind = max_cyc_ind

    return mean_comp_cyc, std_comp_cyc, lost_ind     


def whole_compressor_cycle(temps, standard_interval) :
    whole_comp_cyc = -1
    f_trans = np.fft.fft(temps)
    n_f = len(f_trans)
    abs_f_trans = abs(f_trans[1:n_f/2])
    freq = np.fft.fftfreq(len(temps))
    freq = freq[1:n_f/2]
    hours = 60. / standard_interval
    ft_peak_locs = peakutils.indexes(abs_f_trans, thres =  0.25, min_dist = 20)
    if len(ft_peak_locs) == 1 : 
        whole_comp_cyc = 60. / (freq[ft_peak_locs] * hours)[0]
    elif len(ft_peak_locs) == 0 :
        if len(abs_f_trans[abs_f_trans < max(abs_f_trans) * 0.25]) > len(abs_f_trans) * 0.9 :
            whole_comp_cyc = 60. / (freq[abs_f_trans == np.max(abs_f_trans)][0] * hours)
    
#     print whole_comp_cyc, freq[abs_f_trans == np.max(abs_f_trans)]
    return whole_comp_cyc

def find_comp_cyc_fft(temps, seg_locs, standard_interval=15) :
    comp_cyc = []
    read_ranges = []
    len_cycs = 0
    seg_pairs = np.transpose((seg_locs[:-1], seg_locs[1:]))
    seg_lens = []
    n_included = 0
    all_inds = 0
    for seg_pair in seg_pairs :
        start_ind = seg_pair[0]
        end_ind = seg_pair[1] 
        n_inds = end_ind - start_ind
        all_inds += n_inds
        if n_inds > 20 :

            f_trans, freq, n_f, fft_peak_locs = find_fft_peaks(temps, start_ind, end_ind)
#             plot_segments(freq, f_trans, n_f, fft_peak_locs, temps, start_ind, end_ind) 

            if len(fft_peak_locs) > 0 :
                comp_cyc.append(float(standard_interval) / freq[1:n_f/2][fft_peak_locs][-1])
                len_cycs += n_inds
                n_included += 1
                read_ranges.append(max(temps[start_ind:end_ind]) - min(temps[start_ind:end_ind]))
                seg_lens.append(n_inds)

    read_ranges = np.array(read_ranges)
    
    comp_cyc = np.array(comp_cyc)
    seg_lens = np.array(seg_lens)
    weights = (np.sum(read_ranges) / read_ranges + 0.01) * (seg_lens * 1. / np.sum(seg_lens))
    ww = np.where(weights < 100000000.)[0]

#     print len(ww), 'weighted segments'
    weights = weights[ww]
    comp_cyc = comp_cyc[ww]
    comp_cyc = list(comp_cyc)
    
    frac = sum(seg_lens) / float(all_inds)
#     print frac
    
#     print n_included, len(seg_pairs)
    
    return comp_cyc, weights, seg_lens, frac

def find_win_peaks(temps, start_ind, end_ind) :
    win_size = 5
    ii = start_ind + win_size
    win_peak_locs = []
    while ii <= end_ind :
        temps_win = temps[ii - win_size : ii]
        if temps_win[2] == np.max(temps_win) and temps_win[2] > temps_win[0] and temps_win[2] > temps_win[4] :
            win_peak_locs.append(ii - win_size / 2 - 1)
        ii += 1

    return win_peak_locs


def find_comp_cyc_win(temps, times, n_r, standard_interval) :
    seg_locs, peak_locs = segment_data(temps, n_r)
    seg_pairs = np.transpose((seg_locs[:-1], seg_locs[1:]))
    comp_cyc = []
    var = []
    all_inds = 0
    incl_inds = 0
    
    for seg_pair in seg_pairs :
        start_ind = seg_pair[0]
        end_ind = seg_pair[1] 
        n_inds = end_ind - start_ind
        all_inds += n_inds

        win_peak_locs = find_win_peaks(temps, start_ind, end_ind)

        if len(win_peak_locs) > 1 :

            peak_times = times[win_peak_locs]
            peak_gaps = (peak_times[1:] - peak_times[:-1]) / one_minute
            comp_cyc.append(np.mean(peak_gaps))
            var.append(np.std(peak_gaps))
            incl_inds += n_inds

    comp_cyc = np.array(comp_cyc)
    var = np.array(var)
    weights = comp_cyc / var
    if all_inds>0:
        frac = float(incl_inds) / float(all_inds)
    else:
		frac=0
    return comp_cyc, weights, frac

def calculate_compressor_cycles(temps, n_r, min_ratio, min_ratio_n_r, standard_interval) :

    seg_locs, peak_locs = segment_data(temps, n_r)
    window = n_r * standard_interval
    
    
    comp_cyc, weights, seg_lens, frac_comp_cyc = find_comp_cyc_fft(temps, seg_locs, standard_interval)
    len_cycs = np.sum(seg_lens)

    if len_cycs > window / 2 :
        mean_comp_cyc, std_comp_cyc, lost_ind  = find_mean_std_comp_cyc(comp_cyc, weights, len_cycs, window)
        
        if lost_ind >= 0 :
            den = len_cycs / frac_comp_cyc
            num = float(len_cycs - seg_lens[lost_ind])
            frac_comp_cyc = num / den
#         sensor_info[sensor]['CompCyc'] = mean_comp_cyc
#         sensor_info[sensor]['CompCycVar'] = std_comp_cyc
#         sensor_info[sensor]['Fraction'] = frac_comp_cyc

        mean_std_ratio = std_comp_cyc / mean_comp_cyc
        if mean_std_ratio < min_ratio and mean_comp_cyc / window < 0.49 : 
            min_ratio = mean_std_ratio
            min_ratio_n_r = n_r
        
    else :
        mean_comp_cyc = -1
        std_comp_cyc = -1
#         sensor_info[sensor]['CompCyc'] = -1
#         sensor_info[sensor]['CompCycVar'] = -1
#         sensor_info[sensor]['Fraction'] = frac_comp_cyc

#     sensor_info[sensor]['Window'] = window

    return mean_comp_cyc, std_comp_cyc, frac_comp_cyc, min_ratio, min_ratio_n_r, window
    
