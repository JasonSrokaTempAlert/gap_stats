"""
Device Type Module
"""
import re

DEVICE_TYPE_RENAME = {
    'ambient': 'Ambient',
    'repeater': 'Ambient',
    'open Air': 'Cooler',
    'cooler': 'Cooler',
    'walk': 'Cooler',
    'frz': 'Freezer',
    'freezer': 'Freezer'
}


def get_device_metadata(device_label):
    """
    Attempts to extract the device name, type, and location from a raw device label string.

    :param device_label: The raw device label string.
    :type device_label: str
    :return: A tuple of device name, type, and location.
    :rtype: (str, str, str)
    """
    device_name = device_label
    device_type = 'Unknown'
    device_loc = 'FS'

    if re.search('RX', device_label, re.I):
        device_loc = 'RX'

        if device_label[0] != 'R':
            dum_ = re.match("(.*) (RX)(.*)", device_label, re.I)
            device_label = dum_.group(2) + dum_.group(3)
            device_name = device_label

    if re.search('MC', device_label, re.I):
        device_loc = 'MC'

    elif re.search('HC', device_label, re.I):
        device_loc = 'HC'

    elif re.search('Clinic', device_label, re.I):
        device_loc = 'RX'
        dum_ = re.match("(.*) (Clinic)(.*)", device_label, re.I)
        device_label = dum_.group(2) + dum_.group(3)
        device_name = device_label

    elif re.search('Gateway', device_label, re.I):
        device_loc = 'RX'
        dum_ = re.match("(.*) (Gateway)(.*)", device_label, re.I)
        device_label = dum_.group(2) + dum_.group(3)
        device_name = device_label
        device_type = 'Ambient'

    elif re.search('Ambient', device_label, re.I):
        device_loc = 'RX'

    # unit_type = device_name.split(device_loc)[1]
    # device_name = device_loc + unit_type

    for key in DEVICE_TYPE_RENAME:
        if re.search(key, device_label, re.I):
            device_type = DEVICE_TYPE_RENAME[key]

    return device_name, device_type, device_loc
