"""
Test Many Device Signals Module
"""

import unittest
import logging
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal, PersistableSignalSegment
from ta_utils import ManyDeviceSignals, ManyDeviceSignalsException

from ta_utils import database
from ta_utils import create_tables, drop_tables

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

DEVICE_ID = '4E.94.20.00.00.6F.0D.00'

FILE_NAME = 'Deviceid_FaultyUnitTest.csv'
DEVICE_ID_COL = 'DEVICEID'
DATE_COL = 'READINGDATE'
SIGNAL_COL = 'TEMPERATURE'
PORT_COL = 'NODEUID'
PROCESSED_DATE_COL = 'PROCESSEDDATE'

MAX_TIME_OVERRIDE = datetime(year=2018, month=2, day=1)
READING_DATE_ADJUSTMENT = timedelta(hours=5)

database.init('test_ta_utils')


class DigiFunctionalityTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_update_all_signals(self):
        many_device_signals = ManyDeviceSignals(FILE_NAME, DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)

        many_device_signals.data[DATE_COL] = many_device_signals.data[DATE_COL].apply(lambda local: local + READING_DATE_ADJUSTMENT)

        many_device_signals.update_all_signals(SIGNAL_COL, segment_daily=True)

        last_signal_segment = (PersistableSignalSegment
            .select()
            .order_by(PersistableSignalSegment.end_time.desc())
            .limit(1)
            .get())

        self.assertGreater(last_signal_segment.max_time, last_signal_segment.min_time)
        self.assertEqual(1, last_signal_segment.coverage)
        self.assertEqual(1, last_signal_segment.on_time)
