"""
Test Many Device Signals Module
"""

import unittest
import logging
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal, PersistableSignal
from ta_utils import ManyDeviceSignals, ManyDeviceSignalsException

from ta_utils import database
from ta_utils import create_tables, drop_tables

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

DEVICE_ID = 'test'

DEVICE_ID_COL = 'device_id'
DATE_COL = 'date'
SIGNAL_COL = 'signal'
PORT_COL = 'port'
PROCESSED_DATE_COL = 'processed_date'

now = datetime.now()
many_device_signals_data = [{DEVICE_ID_COL: DEVICE_ID,
                             DATE_COL: now + timedelta(days=i),
                             SIGNAL_COL: float(i)} for i in range(0, 3)]

df = pd.DataFrame(many_device_signals_data)

device_signal_values = [float(i) for i in range(0, 3)]
device_signal_dates = [now + timedelta(days=i) for i in range(0, 3)]
device_signal_series = pd.Series(data=device_signal_values,
                                 index=pd.to_datetime(device_signal_dates))
expected_device_signal = Signal(device_signal_series,
                                device_id=DEVICE_ID,
                                data_type=SIGNAL_COL)

many_device_signals_data_port_0 = [{DEVICE_ID_COL: DEVICE_ID,
                                    PORT_COL: 0,
                                    DATE_COL: now + timedelta(days=i),
                                    SIGNAL_COL: float(i)} for i in range(0, 3)]
many_device_signals_data_port_1 = [{DEVICE_ID_COL: DEVICE_ID,
                                    PORT_COL: 1,
                                    DATE_COL: now + timedelta(days=i),
                                    SIGNAL_COL: float(i)} for i in range(0, 3)]
many_device_signals_data_ports = many_device_signals_data_port_0 + many_device_signals_data_port_1

df_ports = pd.DataFrame(many_device_signals_data_ports)

device_signal_values = [float(i) for i in range(0, 3)]
device_signal_dates = [now + timedelta(days=i) for i in range(0, 3)]
device_signal_series = pd.Series(data=device_signal_values,
                                 index=pd.to_datetime(device_signal_dates))

expected_device_signal_port_0 = Signal(device_signal_series,
                                       port=0,
                                       device_id=DEVICE_ID,
                                       data_type=SIGNAL_COL)

expected_device_signal_port_1 = Signal(device_signal_series,
                                       port=1,
                                       device_id=DEVICE_ID,
                                       data_type=SIGNAL_COL)
database.init('test_ta_utils')


class ManyDeviceSignalsTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_invalid_data(self):
        with self.assertRaises(ManyDeviceSignalsException):
            print(ManyDeviceSignals(12345, DEVICE_ID_COL, DATE_COL))

    def test_invalid_device_column(self):
        with self.assertRaises(ManyDeviceSignalsException):
            print(ManyDeviceSignals(df, 'FAKE', DATE_COL))

    def test_invalid_date_column(self):
        with self.assertRaises(ManyDeviceSignalsException):
            print(ManyDeviceSignals(df, DEVICE_ID_COL, 'FAKE'))

    def test_invalid_signal_column(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        with self.assertRaises(ManyDeviceSignalsException):
            print(many_device_signals.get_device_signal(DEVICE_ID, 'FAKE'))

    def test_invalid_device_id(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        with self.assertRaises(ManyDeviceSignalsException):
            print(many_device_signals.get_device_signal('FAKE', SIGNAL_COL))

    def test_build_many_device_signals(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        self.assertIsNotNone(many_device_signals)

    def test_devices_property(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        self.assertListEqual(many_device_signals.devices, [DEVICE_ID])

    def test_get_device_signal(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        device_signal = many_device_signals.get_device_signal(DEVICE_ID, SIGNAL_COL)
        self.assertEqual(device_signal, expected_device_signal)

    def test_get_all_device_signal(self):
        many_device_signals = ManyDeviceSignals(df, DEVICE_ID_COL, DATE_COL)
        device_signals = many_device_signals.get_all_signals(SIGNAL_COL)
        self.assertEqual(1, len(device_signals))
        self.assertEqual(device_signals[0], expected_device_signal)

    def test_build_many_device_signals_ports(self):
        many_device_signals = ManyDeviceSignals(df_ports, DEVICE_ID_COL, DATE_COL, port_col=PORT_COL)
        self.assertIsNotNone(many_device_signals)

    def test_devices_property_ports(self):
        many_device_signals = ManyDeviceSignals(df_ports, DEVICE_ID_COL, DATE_COL, port_col=PORT_COL)
        self.assertListEqual(many_device_signals.devices, [DEVICE_ID])

    def test_devices_port_pairs_property_ports(self):
        many_device_signals = ManyDeviceSignals(df_ports, DEVICE_ID_COL, DATE_COL, port_col=PORT_COL)
        self.assertListEqual(many_device_signals.device_port_pairs, [(DEVICE_ID, 0), (DEVICE_ID, 1)])

    def test_get_device_signal_ports(self):
        many_device_signals = ManyDeviceSignals(df_ports, DEVICE_ID_COL, DATE_COL, port_col=PORT_COL)
        device_signal = many_device_signals.get_device_signal(DEVICE_ID, SIGNAL_COL, port=0)
        self.assertEqual(device_signal, expected_device_signal_port_0)

        device_signal = many_device_signals.get_device_signal(DEVICE_ID, SIGNAL_COL, port=1)
        self.assertEqual(device_signal, expected_device_signal_port_1)

    def test_get_all_device_signal_ports(self):
        many_device_signals = ManyDeviceSignals(df_ports, DEVICE_ID_COL, DATE_COL, port_col=PORT_COL)
        device_signals = many_device_signals.get_all_signals(SIGNAL_COL)
        self.assertEqual(2, len(device_signals))
        self.assertEqual(device_signals[0], expected_device_signal_port_0)
        self.assertEqual(device_signals[1], expected_device_signal_port_1)

    def test_build_many_device_signals_from_csv(self):
        many_device_signals = ManyDeviceSignals('test_data.csv', DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)
        self.assertIsNotNone(many_device_signals)
        device_signal = many_device_signals.get_device_signal(DEVICE_ID, SIGNAL_COL)
        self.assertIsNotNone(device_signal)
        self.assertEqual(3, device_signal.count)

    def test_update_all_signals(self):
        many_device_signals = ManyDeviceSignals('test_data.csv', DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)
        many_device_signals.update_all_signals(SIGNAL_COL)
        self.assertIsNotNone((PersistableSignal
                              .select()
                              .where((PersistableSignal.device_id == DEVICE_ID) &
                                     (PersistableSignal.data_type == SIGNAL_COL))
                              .get()))

    def test_update_all_signals_second_time(self):
        many_device_signals = ManyDeviceSignals('test_data.csv', DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)

        many_device_signals.update_all_signals(SIGNAL_COL)
        persistable_signal = (PersistableSignal
                              .select()
                              .where((PersistableSignal.device_id == DEVICE_ID) &
                                     (PersistableSignal.data_type == SIGNAL_COL))
                              .get())
        self.assertIsNotNone(persistable_signal)
        self.assertEqual(3, persistable_signal.count)

        many_device_signals.update_all_signals(SIGNAL_COL)
        persistable_signal = (PersistableSignal
                              .select()
                              .where((PersistableSignal.device_id == DEVICE_ID) &
                                     (PersistableSignal.data_type == SIGNAL_COL))
                              .get())
        self.assertIsNotNone(persistable_signal)
        self.assertEqual(3, persistable_signal.count)
