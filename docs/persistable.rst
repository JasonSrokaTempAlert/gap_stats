.. persistable:

Persistable
===========

.. autoclass:: ta_utils.persistence.persistable.Persistable
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__