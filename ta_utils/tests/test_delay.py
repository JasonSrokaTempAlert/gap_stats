"""
Test Delay Module
"""

import unittest
from datetime import datetime, timedelta

from ta_utils import Delay

SECONDS_IN_DAY = 60 * 60 * 24

now = datetime.now()
trivial_delay = Delay(now, now + timedelta(days=1))
other_trivial_delay = Delay(now + timedelta(days=1), now + timedelta(days=2))
same_trivial_delay = Delay(now, now + timedelta(days=1))


class DelayTestCase(unittest.TestCase):
    def test_delay_length(self):
        self.assertEqual(SECONDS_IN_DAY, trivial_delay.age)

    def test_delay_equals(self):
        self.assertFalse(trivial_delay == None)
        self.assertFalse(trivial_delay == trivial_delay.age)
        self.assertFalse (other_trivial_delay == trivial_delay)
        self.assertEqual(same_trivial_delay, trivial_delay)
