.. data:

Data
====

TempAlert Utilities handles different types of data you might encounter from a sensor.
Currently, this package supports *Signal* data that can be extracted from CSVs using *ManyDeviceSignals*.
You can compute and analyze windows of signals easily by using the *Windows* class.

.. toctree::
    :maxdepth: 2

    many_device_signals
    signal
    signal_segment
    gap
    offline_period
    delay
    incremental_signal
    windows
