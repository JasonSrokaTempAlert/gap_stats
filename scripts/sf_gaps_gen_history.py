#Testt
import os
from datetime import datetime
import time
from name_tags import name_tags

import math
import numpy as np
import pandas as pd
import psycopg2

from trundle.trundle_client import TrundleClient

from ta_utils import ManyDeviceSignals
from ta_utils import database
from ta_utils import create_tables, drop_tables
import snowflake.connector as sc

import sys
#sys.path.insert(0,os.path.dirname(__file__)+'/FakeData')
from FakeData import EnvironmentTesting
#import EnvironmentTesting

FLAG_SAVE_SIGNALS = True
FLAG_DONT_RUN = False


SAFE= False
SMART=False
#If both are false then we are running on tempalert flags. Dont think flag is required for that
#To run safe and fresh make both safe and fresh AS TRUE,  Fresh set as true alone doesn't do anything
FRESH= False

POSTGRESDB= False
#Code for postgres is not up to date at all

VERBOSE=True
#Helpful while testing not otherwise


RESET = False
#ENVIRONMENT = 'local'
#ENVIRONMENT = 'ubuntu'
#ENVIRONMENT = 'local'
# ENVIRONMENT = 'ubuntu'

# DATATAG = 'MC_and_NodeNanny'
# DATATAGS = ['GroupTest']
#DATATAGS= ['DEVICETEST']
# DATATAGS=['Agne']
# DATATAGS = ['NewEngland']
# DATATAG = 'AllCVS'
#DATATAGS = ['TacoBell']
#DATATAGS = ['Racetrac','TacoBell','GoLiveFS']
#DATATAGS = ['Google']
#DATATAGS=['Dominos']
#DATATAGS = ['GoLive']
#DATATAGS = ['Google']
#DATATAGS = ['GoLiveFS']
DATATAGS = ['WALGREENS']
#DATATAGS=['SaveALot']
#DATATAGS=['iPic']
#DATATAGS=['SaveALot']
#DATATAGS=['RachaelsFoods','JennyCraig']
#DATATAGS=['SheridanWindermere']
#DATATAGS=['Vallee']
#DATATAGS = ['VA']
#DATATAGS = ['Walgreens']
#DATATAGS=['SMART']
#DATATAGS=['SAFE']
#DATATAGS=['SAFEWC']
#DATATAGS=['Cumbies']
#DATATAGS=['Albertsons']
#DATATAGS=['SAFEFRESHRENN']

#ONLY CHANGED TO STATE TAGS FOR GO LIVEs
if POSTGRESDB==False:
	DEVICE_ID_COL = 'deviceid'
else:
	DEVICE_ID_COL = 'device_id'

DATE_COL = 'reading_date'
READING_COL_NODES = 'temperature'
READING_COL_GATEWAYS = 'channel'
PROCESSED_COL = 'processed_date'
PORT_COL = 'port'

if (SMART== True or SAFE== True):
	ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2018-01-01 00:00:00')
else:
	ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2018-02-15 00:00:00')


# Environment
if os.path.isdir('/Users/Jason/'):
    Environment = 'jason_local'
    DATA_DIR = '/Users/Jason/Desktop/Local_Data_Store'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
elif os.path.isdir('/Users/jheel/'):
    Environment = 'jheel_local'
    DATA_DIR = '/Users/jheel/gap_stats/Data'
    ACCESS_CREDS_DIR = '/Users/jheel/gap_stats/prod-trundle-analyticsconfig'
elif os.path.isdir('/home/jason/'):
    Environment = 'ubuntu'
    DATA_DIR = '/home/jason/Data'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
else:
    error("Don't recognize environment")

current_dir= os.getcwd()
env_tag = EnvironmentTesting.gen_env_tag(current_dir)
db_name = EnvironmentTesting.lookup_dbname_forenv(env_tag)
print "Database",db_name
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='{db}' user='jason' password='qvz2LFRzMMVPku7' ".format(db = db_name)
# conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='gapstats_sandbox' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)
warehouse_conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
WAREHOUSE_DATABASE_CONN = psycopg2.connect(warehouse_conn_string)
database.init(db_name, user='jason', password='qvz2LFRzMMVPku7',
                   host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
                   port='5432')
import snowflake.connector

database = 'warehouse' 
account = 'fo68387.us-east-1'
warehouse = 'DATASCIENCE'


SNOWFLAKE_DATABASE_CONN = snowflake.connector.connect(user= 'jheel', 
                                    password='Paperfold40!', 
                                    account=account,
                                    warehouse=warehouse)
def read_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        names = [ x[0].lower() for x in cursor.description]
        rows = cursor.fetchall()
        return pd.DataFrame( rows, columns=names, dtype=object)
    finally:
        if cursor is not None:
            cursor.close()


print "Environment: {env}  \n DATA_DIR: {dd} \n ACCESS_CRED_DIR: {ad} \n conn_string: {cs}".format(
    env = Environment, dd = DATA_DIR, ad = ACCESS_CREDS_DIR, cs = conn_string
)
    


SECONDS_DELAY_BEFORE_TABLE_DROP = 15 if db_name == 'warehouse' else 5
if RESET:
    print "\n\nWARNING!\nWARNING!\nprepared to drop tables in {tag}- you have {secs} seconds to quite before it happens...".format(
        tag = db_name, secs = SECONDS_DELAY_BEFORE_TABLE_DROP)
    time.sleep(SECONDS_DELAY_BEFORE_TABLE_DROP)
    print "time's up, dropping tables"
    drop_tables()
    print 'dropped tables'
    create_tables()
    print 'created tables'




def format_datetime(raw_datetime):
    result = raw_datetime.replace('T', ' ')
    if '.' in result:
        index =int(result.index('.'))
        return_datetime = result.split('.')[0]
        final = return_datetime
    else:
        final = result 
    return final

start_time_main= time.time()
if POSTGRESDB== False:
	if SMART== True:
		trundle_query = """select MAX(processeddate)::timestamp(0) as update_time from ds.main.smart_temp_data; """	
	elif SAFE == True:
		if FRESH==True:
			trundle_query = """select MAX(processeddate)::timestamp(0) as update_time from ds.main.SAFEWHOLECART_ACCOUNT_DATA; """	
		else:
			trundle_query = """select MAX(timestamp)::timestamp(0) as update_time from ds.main.safetemps_data; """			
	else:
		trundle_query = """select MAX(createdate)::timestamp(0) as update_time from adw.dw.F_TEMPERATURE_device; """
	if VERBOSE==True:print "trundle_query: {}".format(trundle_query)
	trundle_results = read_query(SNOWFLAKE_DATABASE_CONN,trundle_query)
# trundle_results['update_time'] = trundle_results['update_time'] .astype('timestamp') 
	if VERBOSE==True:print "trundle_results: {}".format(trundle_results)
	trundle_results['update_time'] = pd.to_datetime(trundle_results['update_time'], format='%Y-%m-%d %H:%M:%S')
else:
	print "Add Trundle lines for postgres before you run"
	exit()

print "Reading devices info from vu_deviceandstoreinfo"

if POSTGRESDB== False:
	if SMART== True:
		device_query= "select datedeleted as datedeleted,deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state,tz_offset,transmitrate from ds.main.smart_vu_deviceandstoreinfo  order by deviceid limit 500"
	elif SAFE == True:
		if FRESH== False:
			device_query= "select datedeleted::timestamp(0) as datedeleted,CAST(deviceid as text) as deviceid,groupid,store_groupid,parentgroupid,accountid,accountname as groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state from ds.main.safe_vu_deviceandstoreinfo  "
		else:
			device_query= "select datedeleted::timestamp(0) as datedeleted,CAST(deviceid as text) as deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state,account_id from ds.main.safewholecart_vu_deviceandstoreinfo order by deviceid desc "
	else:
		device_query= "select datedeleted::timestamp(0) as datedeleted,deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state from ds.main.vu_deviceandstoreinfo where lastactivity is not null and groupid is not null and ignore is null order by deviceid asc"


#limit 2000 offset 3504
        # device_query= "select datedeleted::timestamp(0) as datedeleted,deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state from ds.main.digi_vu_deviceandstoreinfo where deviceid = 'FF.F4.04.00.00.A3.22.00'"
	DevicesInfo = read_query(SNOWFLAKE_DATABASE_CONN,device_query)
	DevicesInfo['deviceid'] = DevicesInfo['deviceid'] .astype('str')
else:
	if DIGI== False:
		device_query= "select datedeleted::timestamp(0) as datedeleted,deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state from vu_deviceandstoreinfo  "

	else :
		device_query= "select datedeleted::timestamp(0) as datedeleted,deviceid,groupid,store_groupid,parentgroupid,accountid,groupname,devicetype,deviceinstalldate::timestamp(0) as deviceinstalldate,state from vu_deviceandstoreinfo_digi order by deviceid desc limit 2000 offset 3504"
	DevicesInfo = pd.read_sql(device_query,WAREHOUSE_DATABASE_CONN)	



print "done with devices info  read, raw number of devices: {count}".format(count = len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(DevicesInfo[DevicesInfo['datedeleted']>=ANALYSIS_WINDOW_START_DATE])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(start = ANALYSIS_WINDOW_START_DATE, count = len(DevicesInfo))

print "Reading sensors info from sensor table"

if (SMART== False and SAFE== False):
	if POSTGRESDB== False:
		sensor_query= "select s.sensorid, s.deviceid, s.portid from staging.accounts.Sensor s join ds.main.first_reading a on (s.deviceid=a.deviceid and s.portid=a.portid) where s.hidden = False and s.datedeleted is NULL"
		sensor_info = read_query(SNOWFLAKE_DATABASE_CONN,sensor_query)
		sensor_info['deviceid'] = sensor_info['deviceid'] .astype('str') 
	else:
		sensor_query= "select s.sensorid, s.deviceid, s.portid from sensor s join trundle_access_history t on (t.deviceid=s.deviceid and t.portid=s.portid) where s.hidden = False and s.datedeleted is NULL"
		sensor_info = pd.read_sql(sensor_query, WAREHOUSE_DATABASE_CONN)		
else:
	print "Should go here for digi companies"
	sensor_info=DevicesInfo[['deviceid']]
	sensor_info['portid']=10
	sensor_info['sensorid']=DevicesInfo[['deviceid']]



if (POSTGRESDB == True and SMART== False and SAFE== False):
	def REPORTING_INTERVAL_MINUTES(deviceid):
		return 60 * 15
elif SMART== True:
	def REPORTING_INTERVAL_MINUTES(deviceid):
		x=DevicesInfo[DevicesInfo['deviceid']==deviceid].transmitrate.values[0]		
		return 60 * x 
else:
	def REPORTING_INTERVAL_MINUTES(deviceid):
		return 60 * 15
	


if VERBOSE==True:print "SENSOR_TABLE"
#Snowflake same query
#sensor_query= "select s.sensorid, s.deviceid, s.portid from staging.accounts.sensor s join ds.main.trundle_access_history t on (t.deviceid=s.deviceid and t.portid=s.portid) where s.hidden = False and s.datedeleted is NULL"
#sensor_info  =read_query_snowflake(SNOWFLAKE_DATABASE_CONN,sensor_query)


print "done with sensor read, number of non-hidden sensors = {numsens}".format(numsens = len(sensor_info))

def device_mintime_override_lookup(deviceid, reading_col, port):
    if POSTGRESDB==False:
        if SMART==True:
            install_date_query = "select deviceinstalldate::timestamp(0) as deviceinstalldate from ds.main.smart_vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)
        elif SAFE== True:
			if FRESH == False:
				install_date_query= "select deviceinstalldate::timestamp(0) as deviceinstalldate from ds.main.safe_vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)
			else:
				install_date_query= "select deviceinstalldate::timestamp(0) as deviceinstalldate from ds.main.safewholecart_vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)
        else:
            install_date_query = "select deviceinstalldate::timestamp(0) as deviceinstalldate from ds.main.vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)
        install_results=read_query(SNOWFLAKE_DATABASE_CONN,install_date_query)
    else:
        install_date_query = "select deviceinstalldate::timestamp(0) as deviceinstalldate from ds.main.vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)
        install_results = pd.read_sql(install_date_query, WAREHOUSE_DATABASE_CONN)
    if VERBOSE==True:print install_results,"MMMMMMMMSMSMMSMSMSMSMSMMSMSM"
    if len(install_results) >= 1 :
        if VERBOSE==True:print "Entering the loop"
        port_string_1 = '' if port is None else "and portid = {}".format(port)
        if VERBOSE==True:print port_string_1,"THIS IS THE PORT STRING"
        if port_string_1 == '' :
            if VERBOSE==True:print("going in gateway loop")
            install_date_query_null = "select MIN(readingdate)::timestamp(0) as deviceinstalldate from adw.dw.f_gateway_power where deviceid = '{devid}' ".format(devid = deviceid)
            install_results_null = read_query(SNOWFLAKE_DATABASE_CONN,install_date_query_null)
            print install_results_null
            print "XXXXXXXXXXXXXXX"
        else :
            if (SMART== False and SAFE== False and POSTGRESDB==False):
            	print "ENTERING THIS LOOP for first reading"
                install_date_query_null = "select first_readingdate::timestamp(0)  as deviceinstalldate from ds.main.first_reading where deviceid = '{devid}' {port}".format(devid = deviceid, port = port_string_1)
                install_results_null = read_query(SNOWFLAKE_DATABASE_CONN,install_date_query_null)
            elif POSTGRESDB==False:
                install_results_null = install_results
            else:
                install_date_query_null = "select first_readingdate::timestamp(0)  as deviceinstalldate from first_reading where deviceid = '{devid}' {port}".format(devid = deviceid, port = port_string_1)
                install_results_null = pd.read_sql(install_date_query_null, WAREHOUSE_DATABASE_CONN)
		print "YYAYYAYAYAYYA",install_results_null,install_results
        if pd.isnull(install_results_null.values[0]) :
            install_results_null.values[0]  = ANALYSIS_WINDOW_START_DATE
        else:
            install_results_null.values[0]=install_results_null.values[0]
        if install_results.values[0] != ANALYSIS_WINDOW_START_DATE and pd.notnull(install_results.values[0]) :
            if VERBOSE==True:print "GOING HERE"
            install_results = min(install_results['deviceinstalldate'].values[0],install_results_null['deviceinstalldate'].values[0])
        else :
            if VERBOSE==True:print "HERE"
            install_results = install_results_null['deviceinstalldate'].values[0]
        if pd.notnull(install_results):
            if VERBOSE==True:print "REALLY"
            install_results=install_results
        else:
            if VERBOSE==True:print "NO REALLLLY"
            install_results= ANALYSIS_WINDOW_START_DATE
#       install_date_dt64 = install_results
#       install_date_str = format_datetime(str(install_date_dt64))
#       print install_date_str
#       print install_results
#       print install_date_dt64
#       # print install_date_str
#       install_date = datetime.strptime(install_date_str,'%Y-%m-%d %H:%M:%S')
        #print "Stupid"
        result = max(ANALYSIS_WINDOW_START_DATE, install_results)
        if VERBOSE==True:print "device_mintime_override_lookup()::  returning {p1} as type {p2}".format(p1 = result, p2 = type(result))
        return result
    else :
        return ANALYSIS_WINDOW_START_DATE




def device_maxtime_override_lookup(deviceid, reading_col, port):

	if (SMART== False and SAFE== False and POSTGRESDB==False):
		if len(trundle_results)>=2:
			if VERBOSE==True:print "multiple results from trundle_access_history table - which should be used? - using min for now"
			trundle_max_time =  pd.to_datetime(trundle_results['update_time'].values[0])
			if VERBOSE==True:print trundle_max_time
			if VERBOSE==True:print "len > 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
			if VERBOSE==True:print("--- %s seconds for this iteration" % (time.time() - start_time_main))
		elif len(trundle_results)==1:
			trundle_max_time = pd.to_datetime(trundle_results['update_time'].values[0])  
			if VERBOSE==True:print "len = 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
			if VERBOSE==True:print("--- %s seconds for this iteration" % (time.time() - start_time_main))
		else:
			trundle_max_time = None
	elif SMART == True:
		if VERBOSE==True:print "SMART"
		deviceid_query = """select MAX(processeddate)::timestamp(0) as update_time from ds.main.smart_temp_data where deviceid='{devid}'; """.format(devid=deviceid)
		device_trundle_results = read_query(SNOWFLAKE_DATABASE_CONN,deviceid_query)
		if VERBOSE==True:print "trundle_results: {}".format(device_trundle_results)
		device_trundle_results['update_time'] = pd.to_datetime(device_trundle_results['update_time'], format='%Y-%m-%d %H:%M:%S')
		trundle_max_time =  pd.to_datetime(device_trundle_results['update_time'].values[0])
		if VERBOSE==True:print 'MMMMMMMMMMMMMM',trundle_max_time
	elif SAFE == True:
		if VERBOSE==True:print "SAFE"
		if VERBOSE==True:print deviceid,type(deviceid)
		if FRESH==False:
			deviceid_query = """select MAX(timestamp)::timestamp(0) as update_time from ds.main.safetemps_data where adminspointpkey={devid}; """.format(devid=deviceid)
		else:
			deviceid_query = """select MAX(readingdate)::timestamp(0) as update_time from ds.main.SAFEWHOLECART_ACCOUNT_DATA where deviceid={devid}; """.format(devid=deviceid)
		device_trundle_results = read_query(SNOWFLAKE_DATABASE_CONN,deviceid_query)
		if VERBOSE==True:print "trundle_results: {}".format(device_trundle_results)
		device_trundle_results['update_time'] = pd.to_datetime(device_trundle_results['update_time'], format='%Y-%m-%d %H:%M:%S')
		trundle_max_time =  pd.to_datetime(device_trundle_results['update_time'].values[0])
	else :
		print "ADD LINES"
		exit()
	print "device_maxtime_override_lookup()::  max_time: {}".format(trundle_max_time)
	return trundle_max_time

#device_maxtime_override_lookup=  format_datetime(str(pd.to_datetime('2018-02-09 08:10:48')))

print "setting up devicesinfo with DATATAGS: {tag}".format(tag=DATATAGS)


def TranslateGroupIDsToDeviceInfo(GroupIDs):
    ##  First get the devices that are in the store's group
    if VERBOSE==True:print DevicesInfo
    DeviceIndices = np.where(DevicesInfo['groupid'].isin(GroupIDs))[0]
    DeviceInfo = DevicesInfo.iloc[DeviceIndices]
    #     DeviceInfo['StoreGroupID'] = DeviceInfo['GroupID']
    ##  Next get the devices that are in the children of a store's group
    DeviceIndices_ChildrenGroups = np.where(DevicesInfo['parentgroupid'].isin(GroupIDs))[0]
    DeviceInfo_ChildrenGroups = DevicesInfo.iloc[DeviceIndices_ChildrenGroups]
    #     DeviceInfo_ChildrenGroups['StoreGroupID'] = DeviceInfo_ChildrenGroups['ParentGroupID']
    ##  Put them together
    DeviceInfo = DeviceInfo.append(DeviceInfo_ChildrenGroups)
    return DeviceInfo


def AugmentDevicesInfoWithStoreGroupID(DevicesInfo):
    num_devices = len(DevicesInfo)
    print "AugmentDevicesInfoWithStoreGroupID():: called with {numdevs} devices".format(numdevs=num_devices)
    AugDevicesInfo = DevicesInfo
#    AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid'].where(AugDevicesInfo['accountid'] != 4226 | AugDevicesInfo['groupname'].str.contains('Store #'), AugDevicesInfo['parentgroupid'])
    AugDevicesInfo['store_groupid'] = AugDevicesInfo['groupid']
    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    # print "beginning storegroupid lookup at time {now}".format(now=datetime.now())
    # for row_index in range(0, num_devices):
    #     this_parent_group_id = AugDevicesInfo['parentgroupid'].values[row_index]
    #     num_devices_in_parent_group = len(DevicesInfo[DevicesInfo['groupid'] == this_parent_group_id])
    #     #         print "processing row {row}, with parent groupid {gid}, found {numdev} devices in parent".format(row = row_index, gid = this_parent_group_id, numdev = num_devices_in_parent_group)
    #     if num_devices_in_parent_group > 0:
    #         #             print "found parent group with devices, resetting store group id"
    #         AugDevicesInfo['storegroupid'].iloc[row_index] = this_parent_group_id
    # print "completed storegroupid lookup at time {now}".format(now=datetime.now())
	    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    return AugDevicesInfo


for this_data_tag in DATATAGS:
    
    if VERBOSE==True:print "Part of First Loop"
    if VERBOSE==True:print DevicesInfo.head()
    Monitored_DevicesInfo = name_tags(DevicesInfo, this_data_tag)
    if VERBOSE==True:print Monitored_DevicesInfo
    Monitored_DeviceIDs = Monitored_DevicesInfo['deviceid']
    NumMonitoredDevices = len(Monitored_DeviceIDs)
#    print "Monitored_DevicesInfo.head(): {}".format(Monitored_DevicesInfo.head())
    Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['devicetype']=='Gateway']
    if VERBOSE==True:print "Monitored",Monitored_DeviceIDs
   
    if VERBOSE==True:print """Done with group selection.
             DATATAG: {tag}
             NumDevices: {devcnt}
             NumGateways: {grpcnt}
             """.format(tag=this_data_tag,
                       devcnt=NumMonitoredDevices,
                       grpcnt=len(Monitored_Gateway_DevicesInfo))
    
    

    
    if FLAG_SAVE_SIGNALS is True:
        StartTime = datetime.now()
        print "Beginning combined analysis using Postgres Temperature at time {now}".format(now=StartTime)
    
        ###   Get the set of groups being monitored
    
        all_group_ids = Monitored_DevicesInfo['groupid'].unique()
        num_group_ids = len(all_group_ids)
        group_id_index = 1
        for group_id in all_group_ids:
            group_starttime = datetime.now()
            group_index_string = 'processing group: {id},  #{index} of {total}'.format(id = group_id,index = group_id_index, total = num_group_ids)
            print group_index_string
    
            ##
            this_group_devicesinfo = Monitored_DevicesInfo[Monitored_DevicesInfo['groupid'] == group_id]
            this_group_nodesinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='ZPoint Node']
    
            if np.isnan(group_id):
                if VERBOSE==True:print "special processing for 'nan' groupid"
                all_device_ids = this_group_devicesinfo[np.isnan(this_group_devicesinfo['groupid'])].deviceid
            else:
                all_device_ids = this_group_devicesinfo[this_group_devicesinfo['groupid'] == group_id]['deviceid'].unique()
    
            num_devices = len(all_device_ids)
    
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, port, max_processed_time::timestamp(0), max_time_override::timestamp(0) from signals
            where data_type = 'temperature' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_temperature_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            # print "Found {numrows} existing signals".format(numrows = len(existing_temperature_signals))
            if VERBOSE==True:print "existing_temperature_signals: {}".format(existing_temperature_signals)
            print "Length:" , len(existing_temperature_signals)
            # print "Found {numrows} existing signals".format(numrows = len(existing_temperature_signals))
            if len(existing_temperature_signals)>0:
                existing_temperature_signals['max_time'] = ''
                for row_index in range(0,len(existing_temperature_signals)):
                    if VERBOSE==True:print "evaluating row {index} in existing_temperature_signals".format(index = row_index)
                    existing_temperature_signals['max_time'].iloc[row_index] = existing_temperature_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_temperature_signals['max_time_override'].iloc[row_index]) else existing_temperature_signals['max_time_override'].iloc[row_index]
                        
                if VERBOSE==True:print "After Initial max_time set:    existing_temperature_signals: {}".format(existing_temperature_signals)
            	if VERBOSE==True:print "existing_temperature_signals: {}".format(existing_temperature_signals)

            else:
            	print "EMPTY SIGNAL"
                existing_temperature_signals=pd.DataFrame({'device_id' : None, 'max_time' : None}, index = range(0,0))
            ##
            ##   First, process all devices using temperature data
            ##
			    
            empty_device_ids = []
            if VERBOSE==True:print "XXXXXXXXXXXX",all_device_ids
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for dev_index, device_id in enumerate(all_device_ids):
            	if VERBOSE==True:print "Entering this first  loop",dev_index,device_id
                if VERBOSE==True:print group_index_string + "   deviceid: {devid} #{dev_index} of {num_devids}".format(devid = device_id,
                                                                                                   dev_index = dev_index,
                                                                                                   num_devids = len(all_device_ids))

                #  for devices with one or more non-hidden sensor, process for temperature
                if device_id in sensor_info['deviceid'].values:
                    if VERBOSE==True:print "Jheel look here ",device_id
                    # Determine the processed_after date either from prior signal or using window start
                    if device_id in existing_temperature_signals['device_id'].values:
                    	if VERBOSE==True:print existing_temperature_signals['device_id'].values
                        if VERBOSE==True:print "existing_temperature_signals[existing_temperature_signals['device_id']==device_id]: {}".format(existing_temperature_signals[existing_temperature_signals['device_id']==device_id])
                        raw_max_time = existing_temperature_signals[existing_temperature_signals['device_id']==device_id].max_time.values[0]
                        if raw_max_time is None:
                              max_time = ANALYSIS_WINDOW_START_DATE
                        else:
                        	if VERBOSE==True:print "Go check it"
                        	max_time= raw_max_time
#                         	print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
#                         	print "Temp time",raw_max_time
#                         	m= datetime.strptime(format_datetime(str(raw_max_time)),'%Y-%m-%d %H:%M:%S')
#                         	print "CHECKING",m
#                         	this_group_trundle_query = """select deviceid, portid, update_time::timestamp(0), data_type from trundle_access_history where deviceid = '{devid_list}'; """.format(devid_list = device_id)
#                         	existing_trundle = pd.read_sql(this_group_trundle_query, DATABASE_CONN)
#                         	a = datetime.strptime(format_datetime(str(existing_trundle['update_time'].values[0])),'%Y-%m-%d %H:%M:%S')
#                         	diff = (a-m).seconds
#                         	if diff == 0 or a <= m :
#                         		print "CONTINUE"
#                         		FLAG_DONT_RUN = True
#                         		continue
#                         	else:
#                         		max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
#                         		FLAG_DONT_RUN = False
                        time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                        if VERBOSE==True:print "found device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
                        flag_existing_signal = True                   #print "JJJJJ",flag_existing_signal,max_time   
                    else :
					    max_time = ANALYSIS_WINDOW_START_DATE
					    time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
					    print "did not find device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
					    flag_existing_signal = False
					    
					                            
                    #print "Pulling temperature history for {devid}".format(devid=device_id)
                #for loop should go
                #compare without milliseconds maxtime and trundle access
                    if (SMART== False and SAFE== False and POSTGRESDB==False):
                    	print "TEMPALERT"
                    	newdevices_data_query = """SELECT
                                            CAST(deviceid as varchar) as deviceid, readingdate::timestamp(0) as reading_date, createdate::timestamp(0) as processed_date, ORIGINALTEMPERATURE as temperature, age, portid as port,CONCAT('DEVICEID: ',deviceid) as str_device_id
                                          FROM adw.dw.F_TEMPERATURE_Device
                                          WHERE {time_clause}
                                             AND deviceid = '{devid}' """.format(time_clause = time_clause,
					                                                                                  devid = device_id)
                    	results_df = read_query(SNOWFLAKE_DATABASE_CONN,newdevices_data_query)
                    elif SMART== True:
						if VERBOSE==True:print "Testing for Timezone here"
						print "Smart Temps"
						newdevices_data_query = """SELECT a.deviceid ,readingdate_utc::timestamp(0) AS reading_date, processeddate::timestamp(0) as processed_date,temperature,CONCAT('DEVICEID: ',deviceid) as str_device_id
                                          FROM ds.main.smart_temp_data a 
                                          WHERE {time_clause} AND deviceid = '{devid}' and temperature is not null order by reading_date"""
						newdevices_data_query=newdevices_data_query.format(time_clause = time_clause,devid = device_id)
						results_df = read_query(SNOWFLAKE_DATABASE_CONN,newdevices_data_query)
						results_df['age']=0
						results_df['port']=10
						if VERBOSE==True:print results_df.head()
                    elif SAFE== True:
						print "SAFE Temps"
						if FRESH==True:
							if VERBOSE==True:print "FRESH"
							newdevices_data_query = """SELECT deviceid,
                    						readingdate::timestamp(0) as reading_date,processeddate::timestamp(0) as processed_date,CONCAT('DEVICEID: ',deviceid) as str_device_id,temperature
                                          FROM ds.main.SAFEWHOLECART_ACCOUNT_DATA
                                          WHERE {time_clause} AND deviceid = '{devid}' """.format(time_clause = time_clause,devid = device_id)
						else:
							newdevices_data_query = """SELECT CAST(adminspointpkey as text) as deviceid,
                    						timestamp::timestamp(0) as reading_date,temperature,CONCAT('DEVICEID: ',adminspointpkey) as str_device_id
                                          FROM ds.main.safetemps_data
                                          WHERE {time_clause} AND deviceid = '{devid}' """.format(time_clause = time_clause,devid = device_id)
						results_df = read_query(SNOWFLAKE_DATABASE_CONN,newdevices_data_query)
						results_df['age']=0
						results_df['port']=10
#                    	results_df['temperature']=0.0
						results_df['processed_date']=results_df['reading_date']

                    else :
                    	exit()
                    	print "You need to add this code jheel"	
                    if VERBOSE==True:print "submitting new devices query: {query}".format(query = newdevices_data_query)
                    print("--- %s seconds for this iteration" % (time.time() - start_time_main))
                    if VERBOSE==True:print results_df.head()
                    results_df['deviceid'] = results_df['deviceid'].astype('str')
                    results_df['temperature'] = results_df['temperature'].astype('float64')
                    results_df['reading_date'] = pd.to_datetime(results_df['reading_date'], format='%Y-%m-%d %H:%M:%S')
                    results_df['processed_date'] = pd.to_datetime(results_df['processed_date'], format='%Y-%m-%d %H:%M:%S')
#                     print "DATATYPES",results_df.dtypes
#                     print results_df.head()
                    				
                    if len(results_df) > 0:
#                         print "results_df.head(): {}".format(results_df.head())
                    	all_results_df = all_results_df.append(results_df)
                    else:
                        if VERBOSE==True:print "XXXXXXXXXXXXXXXXX","no data found"
                        # if max_time is not None:
                        empty_device_ids.append(device_id)
    
            if len(all_results_df) > 0:
                if VERBOSE==True:print "temperature call to ManyDeviceSignals with {rows} rows".format(rows = len(all_results_df))
#                 print "ALL_RESULTS_DF:",all_results_df.head()
                if VERBOSE==True:print "Device Col",DEVICE_ID_COL, "DATE Col",DATE_COL,"Process:" ,PROCESSED_COL,"READING COLS",READING_COL_NODES,"PORT_COL",PORT_COL
                if VERBOSE==True:print "MAXTIME",device_maxtime_override_lookup
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL,
                                        port_col=PORT_COL)
    
                if VERBOSE==True:print "temperature call to update_all_signals"
                x= device_maxtime_override_lookup
                if VERBOSE==True:print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",device_maxtime_override_lookup, type(device_maxtime_override_lookup)
                if VERBOSE==True:print "YYYYYY", x, type(x)
                updated_signals = mds.update_all_signals(READING_COL_NODES,
                                                         segment_daily=True,
                                                         #min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES(device_id),
                                                         min_processed_time_override=device_mintime_override_lookup,
                                                         min_time_override=device_mintime_override_lookup,
                                                         max_processed_time_override=device_maxtime_override_lookup(device_id,None,PORT_COL),
                                                         max_time_override=device_maxtime_override_lookup(device_id,None,PORT_COL)
                                                         )
                                                         
                if VERBOSE==True:print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",device_maxtime_override_lookup
                if VERBOSE==True:print "XXXXXXXXXXXXXXXXXXXXXXXXYYYYYYYYYYYYYYYYYY",device_maxtime_override_lookup(DEVICE_ID_COL,READING_COL_NODES,PORT_COL), type(device_maxtime_override_lookup(DEVICE_ID_COL,READING_COL_NODES,PORT_COL))
                print "Number of Updated Temperature Signals: {0}".format(len(updated_signals))
                print("--- %s seconds ---" % (time.time() - start_time_main))

            print "Before handling empty temperature signals, time is: {}".format(datetime.now())
            print "empty_device_ids: {}".format(empty_device_ids)
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    print "temperature empty device data update"
                    if empty_device_id not in existing_temperature_signals['device_id'].values:
                        ports = sensor_info[sensor_info['deviceid']==empty_device_id].portid.values
                        ports=ports.tolist()
                        if VERBOSE==True:print ports,type(ports),'x- Port loop'
                        for port in ports:
                            if VERBOSE==True:print port,type(port)
                            if pd.isnull(port):
                                print 'new signal, port is null'
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=True,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                                             min_time_override=device_mintime_override_lookup(empty_device_id,READING_COL_NODES,None),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None)
                                                                        )
                            else:
                            	if VERBOSE==True:print "new signal, empty_device_id: {did}, port: {port}".format(
                                    port = port, did = empty_device_id
                                )
                                if VERBOSE==True:print "gaps_gen calling update_empty_device_signal with max_processed_time_override = {mto}".format(
                                    mto = device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,port)
                                )
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=True,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                                             min_time_override=device_mintime_override_lookup(empty_device_id,None,port),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,None,port),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,None,port))
                    else:

                        ports = existing_temperature_signals[existing_temperature_signals['device_id']==empty_device_id].port.values
                        if VERBOSE==True:print "ports for empty device: {ports}".format(ports = ports)
                        empty_mds = ManyDeviceSignals('Empty_MDS','Empty_MDS','Empty_MDS')
                        for port in ports:
                            if pd.isnull(port):
                                if VERBOSE==True:print "pre-existing signal, empty_device_id: {did}, null port".format(
                                    	port=port, did=empty_device_id
                                	)
                                empty_mds.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=True,
                                                                            reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                                             min_time_override=max_time,
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id, READING_COL_NODES, None),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,None))
                            else:
                                    if VERBOSE==True:print "pre-existing signal, empty_device_id: {did}, port: {port}".format(
                                    	port=int(port), did=empty_device_id
                                	)
                                    empty_mds.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=True,
                                                                            reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                                             min_time_override=max_time,
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id, READING_COL_NODES, int(port)),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,READING_COL_NODES,int(port)))

                print "Number of empty Temperature Signals: {0}".format(len(empty_device_ids))
    

            ##
            ##   Second, process gateways using channel data
            ##
            this_group_gatewaysinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='Gateway']
            all_gateway_ids = this_group_gatewaysinfo['deviceid'].unique()
            num_gateways = len(all_gateway_ids)
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, max_processed_time::timestamp(0), max_time_override::timestamp(0) from signals
            where data_type = 'channel' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_channel_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            if len(existing_channel_signals)>0:
                existing_channel_signals['max_time'] = ''
                for row_index in range(0,len(existing_channel_signals)):
                    if VERBOSE==True:print "evaluating row {index} in existing_channel_signals".format(index = row_index)
                    existing_channel_signals['max_time'].iloc[row_index] = existing_channel_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_channel_signals['max_time_override'].iloc[row_index]) else existing_channel_signals['max_time_override'].iloc[row_index]

            else:
                existing_channel_signals=pd.DataFrame({'device_id' : None, 'max_time' : None},index = range(0,0))
            if VERBOSE==True:print "existing_channel_signals: {}".format(existing_channel_signals)
    
            empty_device_ids = []
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for device_id in all_gateway_ids:
                # Determine the processed_after date either from prior signal or using window start
                if VERBOSE==True:print "examining gateway deviceid: {devid}".format(devid = device_id)
                if device_id in existing_channel_signals['device_id'].values:
                    raw_max_time = existing_channel_signals[existing_channel_signals['device_id']==device_id].max_time.values[0]
                    if raw_max_time is None:
                        max_time = ANALYSIS_WINDOW_START_DATE
                    else:
                        max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
                    time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                    flag_existing_signal = True
                else:
                    max_time = ANALYSIS_WINDOW_START_DATE
                    time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
                    flag_existing_signal = False
                if VERBOSE==True:print "Pulling channel history for {devid}".format(devid=device_id)
                newdevices_data_query = """SELECT
                                        deviceid::text deviceid, readingdate::timestamp(0) as reading_date, createdate::timestamp(0) as processed_date, CONCAT('DeviceID: ',deviceid) as str_device_id
                                      FROM adw.dw.f_gateway_power
                                      WHERE
                                        {time_clause}
                                         AND deviceid = '{devid}' """.format(time_clause = time_clause,
                                                                              devid = device_id)
                if VERBOSE==True:print "submitting query: {query}".format(query = newdevices_data_query)
                results_df = read_query(SNOWFLAKE_DATABASE_CONN,newdevices_data_query)
                if len(results_df) > 0:
#                     print "results_df.head(): {}".format(results_df.head())
                    all_results_df = all_results_df.append(results_df)
                    all_results_df['channel']=99.0
                else:
                    print "no data found"
                    # if max_time is not None:
                    empty_device_ids.append(device_id)
    
            print "pre-ManyDeviceSignals all_results_df.head(): {}".format(all_results_df.head())



    
            if len(all_results_df) > 0:
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL)
    
                updated_signals = mds.update_all_signals(READING_COL_GATEWAYS,
                                                         segment_daily=True,
                                                         # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES(device_id),
                                                        min_processed_time_override=device_mintime_override_lookup(device_id,None,None),
                                                         min_time_override=device_mintime_override_lookup(device_id,None,None),
                                                         max_processed_time_override=device_maxtime_override_lookup(device_id,None,None),
                                                         max_time_override=device_maxtime_override_lookup
                                                         )
                print "Number of Updated Channel Signals: {0}".format(len(updated_signals))
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    print "empty device data update for channel info"
                    if empty_device_id not in existing_channel_signals['device_id'].values:
                        print "New channel signal, no data"
                        ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_GATEWAYS,
                                                                 segment_daily=True,
                                                                 reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                                 min_processed_time_override=device_mintime_override_lookup(empty_device_id,None,None),
    																 min_time_override=device_mintime_override_lookup(empty_device_id,None,None),
                                                                             max_time_override=device_maxtime_override_lookup(empty_device_id,None,None),
                                                                             max_processed_time_override=device_maxtime_override_lookup(empty_device_id,None,None)  
                                                                 )
                    else:
                        empty_mds = ManyDeviceSignals('Empty_MDS','Empty_MDS','Empty_MDS')
                        empty_mds.update_empty_device_signal(empty_device_id, READING_COL_GATEWAYS,
                                                             segment_daily=True,
                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES(empty_device_id),
                                                             min_processed_time_override=device_mintime_override_lookup(empty_device_id,None,None),
    																 min_time_override=device_mintime_override_lookup(empty_device_id,None,None),
                                                             max_time_override=device_maxtime_override_lookup(
                                                                 empty_device_id, READING_COL_NODES, -1),
                                                             max_processed_time_override=device_maxtime_override_lookup(
                                                                 empty_device_id, READING_COL_NODES, -1))

                print "updated {num} empty signals".format(num = len(empty_device_ids))
            # print "updated_signals: {}".format(updated_signals)

	    for device_id in all_gateway_ids:
	    	print device_id,'This test round'
		update_gateway_query = """update signals set coverage= null where device_id = '{devid}' and data_type='channel' """.format(devid = device_id)
	        if VERBOSE==True:print update_gateway_query
		update_segments_gateway_query = """update signal_segments c set coverage= null from signals b where b.id = c.signal_id and b.device_id = '{devid}' """.format(devid = device_id)

		cs = WAREHOUSE_DATABASE_CONN.cursor() 
		print 'Running first query'
		cs.execute(update_gateway_query) 
		print 'Running second query'
		cs.execute(update_segments_gateway_query)       
 		WAREHOUSE_DATABASE_CONN.commit()
		cs.close()   
            print "group runtime: {}".format(datetime.now() - group_starttime)
            group_id_index += 1
    
    
        EndTime = datetime.now()
        print "Combined Analysis Time: {0}".format(EndTime - StartTime)
