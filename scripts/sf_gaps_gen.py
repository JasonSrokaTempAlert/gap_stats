#  Set up libraries/modules
import os
import sys
import yaml
import time

import numpy as np
import pandas as pd
import psycopg2
import snowflake.connector

from timing import timing

from ta_utils import ManyDeviceSignals
from ta_utils import database
from ta_utils import create_tables, drop_tables

from name_tags import name_tags

from config import user, pg_host, pg_port, pg_user, pg_password, sf_password, sf_account, sf_warehouse


#  Execution configuation
FLAG_SAVE_SIGNALS = True
VERBOSE = True
RESET = False
REDO=False
CATCHUP=False

DEVICE_ID_COL = 'deviceid'
DATE_COL = 'reading_date'
READING_COL_NODES = 'temperature'
READING_COL_GATEWAYS = 'channel'
PROCESSED_COL = 'processed_date'
PORT_COL = 'port'
GROUP_ID_COL = 'groupid'

QUERIES_FILEPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'queries.yaml')
print("QUERIES_FILEPATH: {}".format(QUERIES_FILEPATH))
QUERIES = yaml.load(open(QUERIES_FILEPATH))

DB_NAME = 'warehouse'
# DB_NAME='gapstats_sandbox'
# Parse command line arguments
# first command line arg is for which type of data is being reviewed
DATASOURCE = sys.argv[1].upper()
print("DataSource: {}".format(DATASOURCE))
if DATASOURCE not in ['SAFE', 'SMART', 'TEMPALERT', 'SAFEFRESH']:
    raise RuntimeError('Unexpected DATASOURCE, {}'.format(DATASOURCE))

# second command line arg is comma-separated string of NameTags - must
# match a value in name_tags.py
DATATAGS = sys.argv[2].upper().split(",")

# SmartTemps and pure SafeTemps data were loaded from csv files, so always
# looking at January data
if DATASOURCE == 'SMART' or DATASOURCE == 'SAFE':
    ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2018-01-01 00:00:00')
else:
    ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2018-04-01 00:00:00')  # pd.to_datetime('2018-02-15 00:00:00')
#  Establish Postgres DB connection
conn_string = "host='{host}' dbname='{db}' user='{user}' password='{password}' ".format(
    host=pg_host, db=DB_NAME, user=pg_user, password=pg_password)
# DB_NAME = 'test_cumbie'
# conn_string = "host='{host}' dbname='{db}' user='{user}'".format(
#     host='localhost', db=DB_NAME, user='areagle')
print("Database: {}".format(conn_string))
DATABASE_CONN = psycopg2.connect(conn_string)
# database.init(DB_NAME)
database.init(DB_NAME, user=pg_user, password=pg_password,
              host=pg_host,
              port='5432')
SNOWFLAKE_DATABASE_CONN = snowflake.connector.connect(user=user,
                                                      password=sf_password,
                                                      account=sf_account,
                                                      warehouse=sf_warehouse)

#  Give operator a warning before clearing everything out of the DB!!
SECONDS_DELAY_BEFORE_TABLE_DROP = 15 if DB_NAME == 'warehouse' else 5

MIN_TIME_MAP = {}
MAX_TIME_MAP = {}
CATCHUP_MAX_TIME_MAP = {}

def reset_tables():
    print("""\nWARNING!\nWARNING!\n
    Prepared to drop tables in {tag} - you have {secs} seconds to quite before it happens...""".format(
        tag=DB_NAME, secs=SECONDS_DELAY_BEFORE_TABLE_DROP))
    time.sleep(SECONDS_DELAY_BEFORE_TABLE_DROP)
    print("time's up, dropping tables")
    drop_tables()
    print('dropped tables')
    create_tables()
    print('created tables')


def reporting_interval_minutes(devices_info):
    def _reporting_interval_minutes(device_id, signal_col, port):
        # Estabish correct REPORTING_INTERVAL based on data type
        if DATASOURCE == 'TEMPALERT':
            return 60 * 15
        elif DATASOURCE == 'SMART':
            x = devices_info[devices_info[DEVICE_ID_COL]
                             == device_id].transmitrate.values[0]
            return 60 * x
        else:
            # Can change reporting Interval
            return 60 * 15

    return _reporting_interval_minutes


def verbose_print(*args):
    if VERBOSE:
        print(*args)


def read_query(connection, query):
    verbose_print('Running query on {}::\n{}'.format(connection.host, query))
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        names = [x[0].lower() for x in cursor.description]
        rows = cursor.fetchall()
        return pd.DataFrame(rows, columns=names, dtype=object)
    finally:
        if cursor is not None:
            cursor.close()


def device_mintime_override_lookup(deviceid, reading_col=None, port=None):
    index = deviceid if port is None else (deviceid, port)
    trundle_min_time = MIN_TIME_MAP.get(index, ANALYSIS_WINDOW_START_DATE)
    final_min_time = max(ANALYSIS_WINDOW_START_DATE, trundle_min_time)
    verbose_print(
        "device_mintime_override_lookup({}) = {}".format(
            deviceid,
            final_min_time))
    return final_min_time


def save_catchup_time_map(all_device_ids):
    catchup_trundle_query = QUERIES['catchup_max_time_map']
    catchuptrundle_results = pd.read_sql(catchup_trundle_query, DATABASE_CONN).format(
            devid_list="','".join(all_device_ids))
    catchuptrundle_results['update_time'] = pd.to_datetime(
        catchuptrundle_results['update_time'])
    verbose_print("catchup_time: {}".format(trundle_results))

    global CATCHUP_MAX_TIME_MAP
    CATCHUP_MAX_TIME_MAP = {
        deviceid: result[0] for deviceid,
        result in catchuptrundle_results.set_index(deviceid).T.to_dict('list').items()}



def device_maxtime_override_lookup(deviceid, reading_col=None, port=None):
    trundle_max_time = MAX_TIME_MAP.get(deviceid, None)
    verbose_print(
        "device_maxtime_override_lookup({}) = {}".format(
            deviceid,
            trundle_max_time))
    if CATCHUP == True and DATASOURCE== 'TEMPALERT':
        print("Using Catchup")
        trundle_max_time=CATCHUP_MAX_TIME_MAP.get(deviceid, None)
    return trundle_max_time

def account_max_time(device_ids_str):
    device_ids_query_signals = "device_id IN ('{}')".format(device_ids_str)
    account_max_time_query = QUERIES['existing_account_max_time_map'].format(
    device_ids_query_signals=device_ids_query_signals)
    account_max_time = pd.read_sql(account_max_time_query, DATABASE_CONN)
    min_date_temp = account_max_time['max_time'].values[0]
    min_date_temp=pd.to_datetime(
        min_date_temp, format='%Y-%m-%d %H:%M:%S')
    verbose_print(min_date_temp)
    return min_date_temp



@timing
def get_unfiltered_results(device_ids):
    device_ids_str = "','".join(device_ids)
    verbose_print(device_ids_str)
    if REDO== True:
        redo_segment_query = QUERIES['delete_redo_signal_segments'].format(
        devid_list=device_ids_str)
        redo_signals_query = QUERIES['delete_redo_signals'].format(
        devid_list=device_ids_str)
        cs = DATABASE_CONN.cursor()
        cs.execute(redo_segment_query)
        cs.execute(redo_signals_query)
        DATABASE_CONN.commit()
        cs.close()
            		
    device_ids_str = "','".join(device_ids)
    verbose_print(device_ids_str)
    device_ids_query = "deviceid IN ('{}')".format(device_ids_str)
    min_date_temp= account_max_time(device_ids_str)
    if DATASOURCE=='SAFE':
    	min_date_temp_query="and reading_date >= ('{}')".format(min_date_temp)
    else:
        min_date_temp_query=" " if (CATCHUP==True or min_date_temp is None ) else "and processed_date >= ('{}')".format(
            min_date_temp)
    newdevices_data_query = QUERIES['unfiltered_results'][DATASOURCE].format(
        device_ids_query=device_ids_query,min_date_temp_query =min_date_temp_query)
    verbose_print(newdevices_data_query)
    results_df = read_query(
        SNOWFLAKE_DATABASE_CONN, newdevices_data_query)

    if DATASOURCE == 'SMART':
        results_df.loc[:, PORT_COL] = 10
        verbose_print(results_df.head())
    elif DATASOURCE == 'SAFEFRESH':
        results_df.loc[:, PORT_COL] = 10
        # results_df[READING_COL_NODES] = 0.0
        results_df.loc[:, PROCESSED_COL] = results_df[DATE_COL]
    elif DATASOURCE == 'SAFE':
        results_df.loc[:, PORT_COL] = 10
        results_df.loc[:, PROCESSED_COL] = results_df[DATE_COL]

    results_df[DEVICE_ID_COL] = results_df[DEVICE_ID_COL].astype('str')
    results_df[READING_COL_NODES] = results_df[READING_COL_NODES].astype(
        'float64')
    results_df[DATE_COL] = pd.to_datetime(
        results_df[DATE_COL], format='%Y-%m-%d %H:%M:%S')
    results_df[PROCESSED_COL] = pd.to_datetime(
        results_df[PROCESSED_COL], format='%Y-%m-%d %H:%M:%S')
    results_df[PORT_COL] = results_df[PORT_COL].apply(int)

    return results_df


def get_results_for_device(
        device_id,
        unfiltered_results,
        sensors_max_time_map):
    max_time = sensors_max_time_map.get(device_id, ANALYSIS_WINDOW_START_DATE)
    time_column = PROCESSED_COL if device_id in sensors_max_time_map else DATE_COL

    return unfiltered_results.loc[(unfiltered_results[DEVICE_ID_COL] == device_id) & (
        unfiltered_results[time_column] > max_time), :]


def get_snowflake_results(device_id, gateway_max_time_map):
    verbose_print(
        "Examining gateway deviceid: {}".format(device_id))

    max_time = gateway_max_time_map.get(device_id, ANALYSIS_WINDOW_START_DATE)
    time_column = PROCESSED_COL if device_id in gateway_max_time_map else DATE_COL

    verbose_print(
        "Pulling channel history for {}".format(device_id))
    newdevices_data_query = QUERIES['gateway_results'].format(
        max_time=max_time, time_column=time_column, devid=device_id)
    return read_query(SNOWFLAKE_DATABASE_CONN, newdevices_data_query)


@timing
def get_devices_info():
    print("Reading devices info from vu_deviceandstoreinfo")
    device_query = QUERIES['devices_info'][DATASOURCE]
    device_results = read_query(SNOWFLAKE_DATABASE_CONN, device_query)

    device_results[DEVICE_ID_COL] = device_results[DEVICE_ID_COL].astype('str')
    print("Raw number of devices: {}".format(len(device_results)))

    device_results = device_results[device_results['datedeleted'].isnull(
    ) | (device_results['datedeleted'] >= ANALYSIS_WINDOW_START_DATE)]
    print("Filtered number of devices after {}: {}".format(
        ANALYSIS_WINDOW_START_DATE, len(device_results)))

    return device_results


@timing
def get_sensor_info(devices_info):
    # Only TempAlert distinguishes devices from sensors)
    if DATASOURCE == 'TEMPALERT':
        sensor_query = QUERIES['sensor_info']
        sensor_info = read_query(SNOWFLAKE_DATABASE_CONN, sensor_query)
        sensor_info[DEVICE_ID_COL] = sensor_info[DEVICE_ID_COL].astype('str')
    else:
        print("Should go here for digi companies")
        sensor_info = devices_info[[DEVICE_ID_COL]]
        sensor_info.loc[:, PORT_COL] = 10

    print("Number of non-hidden sensors = {}".format(len(sensor_info)))

    return sensor_info


def save_min_time_map(all_device_ids):
    install_date_query = QUERIES['min_time_map'][DATASOURCE]
    install_results = read_query(
        SNOWFLAKE_DATABASE_CONN,
        install_date_query.format(
            devid_list="','".join(all_device_ids)))
    install_results['deviceinstalldate'] = pd.to_datetime(
        install_results['deviceinstalldate'])
    verbose_print("install_results: {}".format(install_results))

    if DATASOURCE == 'TEMPALERT':
        first_reading_query = QUERIES["first_reading"]
        first_reading_results = read_query(
            SNOWFLAKE_DATABASE_CONN, first_reading_query.format(
                devid_list="','".join(all_device_ids)))
        first_reading_results['deviceinstalldate'] = pd.to_datetime(
            first_reading_results['deviceinstalldate'])
        verbose_print(
            "first_reading_results: {}".format(first_reading_results))
        min_time_map_additional = {index: result for index, result in first_reading_results.set_index(
            [DEVICE_ID_COL, PORT_COL]).T.to_dict('index').items()}['deviceinstalldate']
    else:
         min_time_map_additional=None

    min_time_map = {
        index: result[0] for index,
        result in install_results.set_index(DEVICE_ID_COL).T.to_dict('list').items()}


    global MIN_TIME_MAP
    MIN_TIME_MAP = min_time_map if min_time_map_additional is None else {
        **min_time_map, **min_time_map_additional}


def save_max_time_map(all_device_ids):
    trundle_query = QUERIES['max_time_map'][DATASOURCE]
    trundle_results = read_query(
        SNOWFLAKE_DATABASE_CONN, trundle_query.format(
            devid_list="','".join(all_device_ids)))
    trundle_results['update_time'] = pd.to_datetime(
        trundle_results['update_time'])
    verbose_print("trundle_results: {}".format(trundle_results))

    global MAX_TIME_MAP
    MAX_TIME_MAP = {
        device_id: result[0] for device_id,
        result in trundle_results.set_index(DEVICE_ID_COL).T.to_dict('list').items()}


def get_existing_max_time_map(all_device_ids, data_type):
    # build table of existing signal dates to streamline subsequent trundle
    # queries
    group_signals_query = QUERIES["existing_max_time_map"].format(
        data_type=data_type, devid_list="','".join(all_device_ids))
    existing_channel_signals = pd.read_sql(
        group_signals_query, DATABASE_CONN)

    return {
        device_id: result[0] for device_id,
        result in existing_channel_signals.set_index('device_id').T.to_dict('list').items()}


@timing
def save_group_data(monitored_devices_info, sensor_info, group_id):
    if np.isnan(group_id):
        verbose_print("Special processing for 'nan' groupid")
        group_devices = monitored_devices_info[np.isnan(
            monitored_devices_info[GROUP_ID_COL])]
    else:
        group_devices = monitored_devices_info[monitored_devices_info[GROUP_ID_COL] == group_id]

    all_device_ids = group_devices[DEVICE_ID_COL].unique()
    all_sensor_ids = group_devices[group_devices['devicetype']
                                   == 'Node'][DEVICE_ID_COL].unique()
    all_gateway_ids = group_devices[group_devices['devicetype']
                                    == 'Gateway'][DEVICE_ID_COL].unique()

    if all_device_ids.size == 0:
        print("WARNING: there are no devices in group '{}'!".format(group_id))
    else:
        save_max_time_map(all_device_ids)
        save_min_time_map(all_device_ids)

        if all_sensor_ids.size == 0:
            print("WARNING: there are no sensors in group '{}'!".format(group_id))
        else:
            sensors_max_time_map = get_existing_max_time_map(
                all_sensor_ids, READING_COL_NODES)

            verbose_print("All Sensor IDs: {}".format(all_sensor_ids))
            unfiltered_results = get_unfiltered_results(all_sensor_ids)

            all_results_df, empty_sensor_ids = get_sensor_data(
                all_sensor_ids, unfiltered_results, sensors_max_time_map)
            if all_results_df is not None:
                update_all_signals(
                    all_results_df,
                    READING_COL_NODES,
                    devices_info,
                    port_col=PORT_COL)

            save_empty_devices(
                empty_sensor_ids,
                devices_info,
                sensor_info=sensor_info)

        if all_gateway_ids.size == 0:
            print("WARNING: there are no gateways in group '{}'!".format(group_id))
        else:
            # Second, process gateways using channel data
            gateway_max_time_map = get_existing_max_time_map(
                all_gateway_ids, READING_COL_GATEWAYS)

            gateway_results_df, empty_gateway_ids = get_gateway_data(
                all_gateway_ids, gateway_max_time_map)
            if gateway_results_df is not None:
                update_all_signals(
                    gateway_results_df,
                    READING_COL_GATEWAYS,
                    devices_info)

            save_empty_devices(empty_gateway_ids, devices_info)
            delete_coverage_for_tableau(all_gateway_ids)


@timing
def update_all_signals(all_results_df, column, devices_info, port_col=None):
    verbose_print(
        "update_all_signals({}) with {} rows".format(
            column, len(all_results_df)))

    mds = ManyDeviceSignals(all_results_df,
                            DEVICE_ID_COL,
                            DATE_COL,
                            processed_date_col=PROCESSED_COL,
                            port_col=port_col)

    us = mds.update_all_signals(
        column,
        segment_daily=True,
        reporting_interval_override=reporting_interval_minutes(devices_info),
        min_processed_time_override=device_mintime_override_lookup,
        min_time_override=device_mintime_override_lookup,
        max_processed_time_override=device_maxtime_override_lookup,
        max_time_override=device_maxtime_override_lookup)


@timing
def get_sensor_data(all_sensor_ids, unfiltered_results, sensors_max_time_map):
    empty_sensor_ids = []
    all_results = []

    for index, sensor_id in enumerate(all_sensor_ids):
        verbose_print(
            "Sensor ID: {} #{} of {}".format(
                sensor_id,
                index,
                len(all_sensor_ids)))
        results_df = get_results_for_device(
            sensor_id, unfiltered_results, sensors_max_time_map)
        if results_df is not None:
            if not results_df.empty:
                all_results.append(results_df)
            else:
                print("No data found for {}".format(sensor_id))
                empty_sensor_ids.append(sensor_id)

    all_results_df = pd.concat(all_results) if len(all_results) > 0 else None
    return all_results_df, empty_sensor_ids


@timing
def get_gateway_data(all_gateway_ids, gateway_max_time_map):
    empty_gateway_ids = []
    all_results = []

    for gateway_id in all_gateway_ids:
        results_df = get_snowflake_results(gateway_id, gateway_max_time_map)
        if not results_df.empty:
            results_df.loc[:, READING_COL_GATEWAYS] = 99.0
            all_results.append(results_df)
        else:
            print("No data found for {}".format(gateway_id))
            empty_gateway_ids.append(gateway_id)

    all_results_df = pd.concat(all_results) if len(all_results) > 0 else None
    return all_results_df, empty_gateway_ids


@timing
def save_empty_devices(empty_device_ids, devices_info, sensor_info=None):
    for empty_device_id in empty_device_ids:
        if sensor_info is None:
            ports = np.array([None])
            column = READING_COL_GATEWAYS
        else:
            ports = sensor_info[sensor_info[DEVICE_ID_COL]
                                == empty_device_id][PORT_COL]
            column = READING_COL_NODES

        for port in ports.tolist():
            port = None if pd.isnull(port) else int(port)
            verbose_print(
                "update_empty_device_signal({}, {}, port={})".format(
                    empty_device_id, column, port))

            ManyDeviceSignals.update_empty_device_signal(
                empty_device_id,
                column,
                port=port,
                segment_daily=True,
                reporting_interval_override=reporting_interval_minutes(
                    devices_info),
                min_time_override=device_mintime_override_lookup,
                max_time_override=device_maxtime_override_lookup,
                max_processed_time_override=device_maxtime_override_lookup)


@timing
def delete_coverage_for_tableau(gateway_ids):
    # TODO: This removes all the coverage stats from signals and signal_segments because it was breaking the
    # tableau-based tools that produce summary metrics;  we need to figure out how to fix the Tableau reports
    # and keep these stats
    update_gateway_query = QUERIES['delete_gateway_coverage'].format(
        devid_list="','".join(gateway_ids))
    update_segments_gateway_query = QUERIES['delete_gateway_segment_coverage'].format(
        devid_list="','".join(gateway_ids))

    cs = DATABASE_CONN.cursor()
    cs.execute(update_gateway_query)
    cs.execute(update_segments_gateway_query)
    DATABASE_CONN.commit()
    cs.close()


def print_metadata(monitored_devices_info, tag):
    verbose_print(monitored_devices_info)
    monitored_device_ids = monitored_devices_info[DEVICE_ID_COL]
    monitored_gateway_devices_info = monitored_devices_info[
        monitored_devices_info['devicetype'] == 'Gateway']
    verbose_print("""Monitored Devices for {tag}:
             NumDevices: {devcnt}
             NumGateways: {grpcnt}
             """.format(tag=tag,
                        devcnt=len(monitored_device_ids),
                        grpcnt=len(monitored_gateway_devices_info)))


@timing
def process_data_tag(data_tag, devices_info, sensor_info):
    monitored_devices_info = name_tags(
        devices_info, data_tag)
    print_metadata(monitored_devices_info, data_tag)
    if FLAG_SAVE_SIGNALS is True:
        # Get the set of groups being monitored
        all_group_ids = monitored_devices_info[GROUP_ID_COL].unique()
        for group_id_index, group_id in enumerate(all_group_ids):
            print(
                '\n\nprocessing group: {}, #{} of {}'.format(
                    group_id,
                    group_id_index + 1,
                    len(all_group_ids)))

            save_group_data(monitored_devices_info, sensor_info, group_id)


if __name__ == "__main__":
    if RESET:
        reset_tables()

    devices_info = get_devices_info()
    sensor_info = get_sensor_info(devices_info)

    verbose_print('devices_info.head(): {}'.format(devices_info.head()))
    # Main loop for executing gap metrics generation
    for data_tag in DATATAGS:
        process_data_tag(data_tag, devices_info, sensor_info)
