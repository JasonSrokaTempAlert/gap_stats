"""
Test Defrost Analysis
"""
import unittest
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal
from ta_utils.analysis.defrost import has_defrost_cycle_naive

now = datetime.now()
dates = [now + timedelta(days=i) for i in range(0, 10)]

non_defrost_signal = Signal(pd.Series(data=[0., 1., 0., 1., 10., 1., 0., 1., 0., 0.], index=dates))
defrost_signal = Signal(pd.Series(data=[0., 1., 0., 5., 10., 5., 0., 1., 0., 0.], index=dates))
inverse_defrost_signal = Signal(pd.Series(data=[10., 9., 10., 5., 0., 5., 10., 9., 10., 10.], index=dates))


class DefrostTestCase(unittest.TestCase):
    def test_has_defrost(self):
        self.assertEqual(True, has_defrost_cycle_naive(defrost_signal))

    def test_has_no_defrost(self):
        self.assertEqual(False, has_defrost_cycle_naive(non_defrost_signal))

    def test_inverse_defrost(self):
        self.assertEqual(True, has_defrost_cycle_naive(inverse_defrost_signal))
