"""
Signal Module
"""
import warnings
import math
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from scipy import stats

from ta_utils import logger
from ta_utils import Delay
from ta_utils import OfflinePeriod
from ta_utils import Gap
from ta_utils import database
from ta_utils import PersistableSignal, PersistableSignalSegment,\
    PersistableGap, PersistableOfflinePeriod, PersistableDelay

DEFAULT_REPORTING_PERIOD_MINUTES = 15


class SignalError(Exception):
    pass


class Signal(object):
    """
    The Signal Object represents a signal in a time-series.
    """
    def __init__(self, series,
                 device_id=None,
                 port=None,
                 data_type=None,
                 min_time_override=None,
                 max_time_override=None,
                 min_processed_time_override=None,
                 max_processed_time_override=None,
                 reporting_interval_override=None,
                 processed_dates=None):
        """
        Initialize the Signal with a series of signals signals.

        :param series: A Pandas Series of numbers for a given time
        :type series: pd.Series | None
        :param device_id: A device id for this Signal
        :type device_id: str
        :param port: A port for this Signal
        :type port: int
        :param data_type: A data type for this Signal
        :type data_type: str
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time
        :type min_time_override: datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time
        :type max_time_override: datetime
        :param min_processed_time_override: An optional processed time used to bound the beginning of this signal
        :type min_processed_time_override: datetime
        :param max_processed_time_override: An optional processed time used to bound the end of this signal
        :type max_processed_time_override: datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :param processed_dates: An optional sequence of proccessed times use to generate delays.
        :type processed_dates: pd.Series
        """
        if series is None:
            self._series = None
        else:
            self._series = series.sort_index()
        
        self._processed_dates = processed_dates

        self.device_id = device_id
        self.port = port
        self.data_type = data_type

        self._intervals = None
        self._mode_minute_interval = None
        self._gaps = None
        self._delays = None
        self._offline_periods = None

        # Temporarily set these to None so we can run some checks
        self.min_processed_time_override = None
        self.max_processed_time_override = None

        if self._series is not None and self._processed_dates is not None\
                and len(self._processed_dates) != len(self._series):
            raise SignalError("Processed dates length doesn't match the series length.")

        if self._series is not None and self.count > 0:
            if max_time_override and max_time_override < self.max_reading_time:
                logger.warning("""You are setting a max_time_override ({mto}) < max_reading_time ({mrt})
                on the Signal for Device {dev} port {port}".format(dev=self.device_id;
                going to use max_reading_time""".format(
                    dev=device_id,
                    mto=max_time_override,
                    mrt=self.max_reading_time,
                    port=port))
                max_time_override = self.max_reading_time
            if min_time_override and min_time_override > self.min_reading_time:
                logger.warning("You are setting a min_time_override > min_reading_time on the Signal for Device {}".format(self.device_id))

        if self._processed_dates is not None and self.count > 0:
            if max_processed_time_override and max_processed_time_override < self.max_processed_time:
                logger.warning("You are setting a max_processed_time_override < max_processed_time on the Signal for Device {}".format(self.device_id))
            if min_processed_time_override and min_processed_time_override > self.min_processed_time:
                logger.warning("You are setting a min_processed_time_override > min_processed_time on the Signal for Device {}".format(self.device_id))

        self.min_time_override = min_time_override
        self.max_time_override = max_time_override
        self.min_processed_time_override = min_processed_time_override
        self.max_processed_time_override = max_processed_time_override
        self.reporting_interval_override = reporting_interval_override

    def save(self, force_insert=False, only=None):
        """
        Save the signal in a table, along with any gaps, delays, and offline periods.

        :param force_insert: boolean to force an insertion query rather than update
        :param only: the parameters you want to include in this save, or None for all
        :return: The persisted signal
        :rtype: PersistableSignal
        """
        persistable_signal = PersistableSignal(signal=self)
        persistable_signal.save(force_insert, only)

        with database.atomic():
            for gap in self.gaps():
                persistable_gap = PersistableGap(gap)
                persistable_gap.signal = persistable_signal
                persistable_gap.save()

            for offline_period in self.offline_periods():
                persistable_offline_period = PersistableOfflinePeriod(offline_period)
                persistable_offline_period.signal = persistable_signal
                persistable_offline_period.save()

            for delay in self.delays():
                persistable_delay = PersistableDelay(delay)
                persistable_delay.signal = persistable_signal
                persistable_delay.save()

        logger.info("Signal.save()::  calling persistable_signal.recompute_metrics()")
        persistable_signal.recompute_metrics()
        persistable_signal.save()

        return persistable_signal

    @property
    def values(self):
        """
        Get the values of the Signal.

        :return: Signal values
        :rtype: np.ndarray
        """
        if self._series is None:
            return np.array([])

        return self._series.values

    @property
    def count(self):
        """
        Get the count of values in the Signal.

        :return: Count
        :rtype: int
        """
        if self._series is None:
            return 0

        return len(self._series)

    @property
    def times(self):
        """
        Get the times of the Signal.

        :return: Signal times
        :rtype: list
        """
        if self._series is None:
            return []

        return list(self._series.index)
        
    @property
    def processed_times(self):
        """
        Get the processed times of the Signal.  Use reading times if processed times were not provided.

        :return: Signal processed times
        :rtype: list
        """
        if self._processed_dates is None:
            return None

        return list(self._processed_dates)
        
    @property
    def bounded_times(self):
        """
        Get the augmented times of the signal, including any bounding box times for start or end.

        :return: Signal times augmented by up to two 'ghost' readings before or after the signal.
        :rtype: list
        """
        bounded_times = self.times

        if self.min_time_override is not None:
            bounded_times.insert(0, self.min_time_override)

        if self.max_time_override is not None:
            bounded_times.append(self.max_time_override)

        return bounded_times

    @property
    def bounded_processed_times(self):
        """
        Get the augmented processed times of the signal, including any bounding box times for start or end.

        :return: Signal processed times augmented by up to two 'ghost' readings before or after the signal.
        :rtype: list
        """
        bounded_processed_times = self.processed_times or []

        if self.min_processed_time_override is not None:
            bounded_processed_times.insert(0, self.min_processed_time_override)
        elif self.min_time_override is not None:
            bounded_processed_times.insert(0, self.min_time_override)

        if self.max_processed_time_override is not None:
            bounded_processed_times.append(self.max_processed_time_override)
        elif self.max_time_override is not None:
            bounded_processed_times.append(self.max_time_override)

        if len(bounded_processed_times) < 2:
            return None

        return bounded_processed_times

    @property
    def bounded_ages(self):
        """
        Compute the ages of each reading (processed_time - reading_time) as Timedeltas.

        :return: Age of each reading in seconds.
        :rtype: pd.Series[pd.Timedelta]
        """
        if self.bounded_processed_times is None:
            raise SignalError("Cannot calculate ages without processed dates.")

        if len(self.bounded_processed_times) != len(self.bounded_times):
            return None

        ages = np.array(self.bounded_processed_times) - self.bounded_times

        return ages

    @property
    def ages(self):
        """
        Compute the ages of each reading (processed_time - reading_time) as Timedeltas.

        :return: Age of each reading in seconds.
        :rtype: pd.Series[pd.Timedelta]
        """
        if self._processed_dates is None:
            raise SignalError("Cannot calculate ages without processed dates.")

        if len(self._processed_dates) == 0:
            return []

        ages = self._processed_dates - self._series.index

        return ages

    @property
    def intervals(self):
        """
        Get the time intervals in this Signal.

        :return: A list of time intervals
        :rtype: list[pd.Timedelta]
        """
        if self._intervals is not None:
            return self._intervals

        intervals = [pd.Timedelta(bounded_time) for bounded_time in np.diff(self.bounded_times)]
        self._intervals = intervals

        return intervals

    @property
    def readings_only_intervals(self):
        """
        Get the time intervals in this Signal, IGNORING any 'ghost' times that reflect segment boundaries.

        :return: A list of time intervals
        :rtype: list[pd.Timedelta]
        """
        if self._series is None:
            return []

        return [pd.Timedelta(time_diff) for time_diff in np.diff(self._series.index)]

    @property
    def minute_intervals(self):
        """
        Get the time intervals in this Signal rounded to the nearest minute.

        :return: A list of rounded number of minutes of each time interval in the signal.
        :rtype: list[int]
        """
        return [int(round(float(interval.total_seconds()) / 60.)) for interval in self.intervals]

    @property
    def mean(self):
        """
        The mean value of this signal.

        :return: Mean of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self._series.mean()

    @property
    def std(self):
        """
        The standard deviation of this signal.

        :return: Standard deviation of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self._series.std(ddof=0)

    @property
    def var(self):
        """
        The variance of this signal.

        :return: Variance of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self._series.var(ddof=0)

    @property
    def min(self):
        """
        The minimum value of this signal.

        :return: Minimum value of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self._series.min()

    @property
    def max(self):
        """
        The maximum value of this signal.

        :return: Maximum value of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self._series.max()

    @property
    def mid(self):
        """
        The average of the min and max values of this signal.

        :return: Mid value of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return float(self.min + self.max) / 2.

    @property
    def min_reading_time(self):
        """
        The minimum reading time of this signal.

        :return: Minimum reading time of signal
        :rtype: datetime
        """
        if self.count == 0:
            return None

        return self._series.index.min()

    @property
    def max_reading_time(self):
        """
        The maximum reading time of this signal.

        :return: Maximum reading time of signal
        :rtype: datetime
        """
        if self.count == 0:
            return None

        return self._series.index.max()

    @property
    def min_time(self):
        """
        The minimum time of this signal, or the min_time_override.

        :return: Minimum time of signal
        :rtype: datetime
        """
        if self.min_time_override:
            return self.min_time_override

        return self.min_reading_time

    @property
    def max_time(self):
        """
        The maximum time of this signal, or the max_time_override.

        :return: Maximum time of signal
        :rtype: datetime
        """
        if self.max_time_override:
            return self.max_time_override

        if self.count == 0:
            return None

        return self.max_reading_time

    @property
    def min_processed_time(self):
        """
        The minimum processed time of this signal.

        :return: Minimum processed time of signal
        :rtype: datetime | None
        """
        if self.min_processed_time_override:
            return self.min_processed_time_override

        if self._processed_dates is None:
            return None

        if self.count == 0:
            return None

        return self._processed_dates.min()

    @property
    def max_processed_time(self):
        """
        The maximum processed time of this signal.

        :return: Maximum processed time of signal
        :rtype: datetime | None
        """
        if self.max_processed_time_override:
            return self.max_processed_time_override

        if self._processed_dates is None:
            return None

        if self.count == 0:
            return None

        return self._processed_dates.max()

    @property
    def days(self):
        """
        The number of days this signal includes (rounded up).

        :return: Number of days in signal.
        :rtype: int
        """
        if self.max_time is None or self.min_time is None:
            return 0

        return int(math.ceil(self.time_span.total_seconds() / (60 * 60 * 24)))

    def quantile(self, q, interpolation='nearest'):
        """
        Returns the value at a given quantile, interpolating with the nearest point.

        :param q: The 0 <= q <= 1, the quantile to compute
        :type q: float
        :param interpolation: The interpolation method to use, defaults to the nearest value. Can take any of the
            following: `{'linear', 'lower', 'higher', 'midpoint', 'nearest'}`
        :type interpolation: str
        :return: The value at that point closest to that quantile
        :rtype: float
        """
        if self._series is None:
            return None

        if interpolation not in {'linear', 'lower', 'higher', 'midpoint', 'nearest'}:
            warnings.warn("Invalid interpolation method: {0}".format(interpolation))
            interpolation = 'nearest'

        return self._series.quantile(q, interpolation=interpolation)

    @property
    def time_span(self):
        """
        The difference between minimum and maximum time in this signal.

        If this is 0, return 1 second in order to not break metrics using this in the denominator.

        :return: The time span of the signal
        :rtype: pd.Timedelta
        """
        if self.max_time is None or self.min_time is None:
            return pd.Timedelta(seconds=1)

        return pd.Timedelta(self.max_time - self.min_time)

    @property
    def mean_interval(self):
        """
        The mean interval between times in the Signal.

        :return: Mean interval Timedelta
        :rtype: pd.Timedelta
        """
        if len(self.intervals) == 0:
            return None

        return np.mean(self.intervals)

    @property
    def min_interval(self):
        """
        The minimum interval between times in the Signal.
        NOTE this calculation ONLY considers gaps between readings, NOT bounding times.

        :return: Minimum interval Timedelta
        :rtype: pd.Timedelta
        """
        if len(self.readings_only_intervals) == 0:
            return None

        return np.min(self.readings_only_intervals)

    @property
    def max_interval(self):
        """
        The maximum interval between times in the Signal.

        :return: Maximum interval Timedelta
        :rtype: pd.Timedelta
        """
        if len(self.readings_only_intervals) == 0:
            return None

        return np.max(self.intervals)

    @property
    def mode_minute_interval(self):
        """
        The mode of the interval between times in the Signal, rounded to minutes, in seconds.

        :return: Mode of the interval, rounded to minutes, in seconds
        :rtype: int
        """
        if self._mode_minute_interval is not None:
            return self._mode_minute_interval

        non_zero_minute_intervals = [interval for interval in self.minute_intervals if interval != 0]
        # TODO:  improve this implementation
        
        if len(non_zero_minute_intervals) == 0:
            return timedelta(minutes=DEFAULT_REPORTING_PERIOD_MINUTES).total_seconds()

        mode_interval_as_minutes = int(stats.mode(non_zero_minute_intervals)[0][0])
        mode_minute_interval = timedelta(minutes=mode_interval_as_minutes).total_seconds()

        self._mode_minute_interval = mode_minute_interval

        return mode_minute_interval

    def after(self, after_date, inclusive=False, use_processed=False):
        """
        Gets the signal after a date.

        :param after_date: The date to use as the lower bound (exclusive) for the new signal.
        :param inclusive: Whether or not this between is fully inclusive.
        :param use_processed: Whether or not to use the processed times to filter.
        :return: A Signal after the given date.
        :rtype: Signal
        """
        if self._series is None:
            after_series = None
            after_processed_dates = None
        else:
            if use_processed:
                if inclusive:
                    series_filter = self._processed_dates >= after_date
                else:
                    series_filter = self._processed_dates > after_date
            else:
                if inclusive:
                    series_filter = self._series.index >= after_date
                else:
                    series_filter = self._series.index > after_date

            after_series = self._series[series_filter]
            if self._processed_dates is not None:
                after_processed_dates = self._processed_dates[series_filter]
            else:
                after_processed_dates = None

        return Signal(after_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=after_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def before(self, before_date, inclusive=False, use_processed=False):
        """
        Gets the signal before a date.

        :param before_date: The date to use as the upper bound (exclusive) for the new signal.
        :param inclusive: Whether or not this between is fully inclusive.
        :param use_processed: Whether or not to use the processed times to filter.
        :return: A Signal before the given date.
        :rtype: Signal
        """
        if self._series is None:
            before_series = None
            before_processed_dates = None
        else:
            if use_processed:
                if inclusive:
                    series_filter = self._processed_dates <= before_date
                else:
                    series_filter = self._processed_dates < before_date
            else:
                if inclusive:
                    series_filter = self._series.index <= before_date
                else:
                    series_filter = self._series.index < before_date

            before_series = self._series[series_filter]
            if self._processed_dates is not None:
                before_processed_dates = self._processed_dates[series_filter]
            else:
                before_processed_dates = None

        return Signal(before_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=before_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def between(self, after_date, before_date, inclusive=False, use_processed=False):
        """
        Gets the signal before and after two dates.

        :param after_date: The date to use as the lower bound (inclusive) for the new signal.
        :param before_date: The date to use as the upper bound (exclusive) for the new signal.
        :param inclusive: Whether or not this between is fully inclusive.
        :param use_processed: Whether or not to use the processed times to filter.
        :return: A Signal between the two given dates.
        :rtype: Signal
        """
        if self._series is None:
            between_series = None
            between_processed_dates = None
        else:
            if use_processed:
                if inclusive:
                    series_filter = (self._processed_dates >= after_date) & (self._processed_dates <= before_date)
                else:
                    series_filter = (self._processed_dates >= after_date) & (self._processed_dates < before_date)
            else:
                if inclusive:
                    series_filter = (self._series.index >= after_date) & (self._series.index <= before_date)
                else:
                    series_filter = (self._series.index >= after_date) & (self._series.index < before_date)

            between_series = self._series[series_filter]
            if self._processed_dates is not None:
                between_processed_dates = self._processed_dates[series_filter]
            else:
                between_processed_dates = None

        return Signal(between_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=between_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def not_between(self, after_date, before_date, inclusive=False):
        """
        Gets the signal not before or after two dates.

        :param after_date: The date to use as the lower bound (exclusive) for the removed signal.
        :param before_date: The date to use as the upper bound (exclusive) for the removed signal.
        :param inclusive: Whether or not this between is fully inclusive.
        :return: A Signal with everything but the values between the two given dates.
        :rtype: Signal
        """
        if self._series is None:
            not_between_series = None
            not_between_processed_dates = None
        else:
            if inclusive:
                series_filter = (self._series.index <= after_date) | (self._series.index >= before_date)
            else:
                series_filter = (self._series.index < after_date) | (self._series.index > before_date)

            not_between_series = self._series[series_filter]
            if self._processed_dates is not None:
                not_between_processed_dates = self._processed_dates[series_filter]
            else:
                not_between_processed_dates = None

        return Signal(not_between_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=not_between_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def above(self, above_value, buf=0.):
        """
        Returns a new Signal for all values above a specific value (with an optional buffer).

        :param above_value: The value to set as the minimum value for the new signal (inclusive).
        :type above_value: float
        :param buf: Optionally a buffer to allow values near the above_value.
        :type buf: float
        :return: A Signal with values above the above_value.
        :rtype: Signal
        """
        if self._series is None:
            above_series = None
            above_processed_dates = None
        else:
            above_series = self._series[self._series >= above_value - buf]
            if self._processed_dates is not None:
                above_processed_dates = self._processed_dates[self._series >= above_value - buf]
            else:
                above_processed_dates = None

        return Signal(above_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=above_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def below(self, below_value, buf=0.):
        """
        Returns a new Signal for all values below a specific value (with an optional buffer).

        :param below_value: The value to set as the maximum value for the new signal (exclusive).
        :type below_value: float
        :param buf: Optionally a buffer to allow values near the below_value.
        :type buf: float
        :return: A Signal with values below the below_value.
        :rtype: Signal
        """
        if self._series is None:
            below_series = None
            below_processed_dates = None
        else:
            below_series = self._series[self._series < below_value + buf]
            if self._processed_dates is not None:
                below_processed_dates = self._processed_dates[self._series < below_value + buf]
            else:
                below_processed_dates = None

        return Signal(below_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=below_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def within(self, above_value, below_value, buf=0.):
        """
        Returns a new Signal for all values below and above two specific values (with an optional buffer).

        :param above_value: The value to set as the minimum value for the new signal (exclusive).
        :type above_value: float
        :param below_value: The value to set as the maximum value for the new signal (exclusive).
        :type below_value: float
        :param buf: Optionally a buffer to allow values near the below_value or above_value.
        :type buf: float
        :return: A Signal with values above the above_value and below the below_value.
        :rtype: Signal
        """
        if self._series is None:
            within_series = None
            within_processed_dates = None
        else:
            within_series = self._series[((self._series >= above_value - buf) & (self._series < below_value + buf))]
            if self._processed_dates is not None:
                within_processed_dates = self._processed_dates[((self._series >= above_value - buf) &
                                                                (self._series < below_value + buf))]
            else:
                within_processed_dates = None

        return Signal(within_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=within_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def not_within(self, above_value, below_value, buf=0.):
        """
        Returns a new Signal for all values not below and above two specific values (with an optional buffer).

        :param above_value: The value to set as the minimum value for the removed signal (exclusive).
        :type above_value: float
        :param below_value: The value to set as the maximum value for the removed signal (exclusive).
        :type below_value: float
        :param buf: Optionally a buffer to allow values near the below_value or above_value.
        :type buf: float
        :return: A Signal with values above the above_value and below the below_value.
        :rtype: Signal
        """
        if self._series is None:
            not_within_series = None
            not_within_processed_dates = None
        else:
            not_within_series = self._series[((self._series < above_value - buf) | (self._series > below_value + buf))]
            if self._processed_dates is not None:
                not_within_processed_dates = self._processed_dates[((self._series < above_value - buf) |
                                                                    (self._series > below_value + buf))]
            else:
                not_within_processed_dates = None

        return Signal(not_within_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=not_within_processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def excursions(self, above_value, below_value, buf=0.):
        """
        Returns a list of Signals for all excursions below and above two specific values (with an optional buffer).

        :param above_value: The value to set as the minimum value for the removed signal (exclusive).
        :type above_value: float
        :param below_value: The value to set as the maximum value for the removed signal (exclusive).
        :type below_value: float
        :param buf: Optionally a buffer to allow values near the below_value or above_value.
        :type buf: float
        :return: A list of Signals representing each excursion.
        :rtype: list[Signal]
        """
        if self._series is None:
            return []

        signals = []
        cur_times = []
        cur_values = []
        if self._processed_dates is not None:
            cur_processed_dates = []
        else:
            cur_processed_dates = None

        for time, value in self._series.iteritems():
            if value < above_value - buf or value > below_value + buf:
                cur_times.append(time)
                cur_values.append(value)
                if self._processed_dates is not None:
                    cur_processed_dates.append(self._processed_dates[time])
            elif len(cur_times) > 0:
                processed_dates_series = pd.Series(data=cur_processed_dates, index=cur_times)
                new_series = pd.Series(data=cur_values, index=cur_times)
                new_signal = Signal(new_series,
                                    processed_dates=processed_dates_series,
                                    min_time_override=self.min_time_override,
                                    max_time_override=self.max_time_override,
                                    min_processed_time_override=self.min_processed_time_override,
                                    max_processed_time_override=self.max_processed_time_override,
                                    reporting_interval_override=self.reporting_interval_override)
                signals.append(new_signal)
                cur_times = []
                cur_values = []
                if self._processed_dates is not None:
                    cur_processed_dates = []
                else:
                    cur_processed_dates = None

        return signals

    def segment_at(self, segment_type=None, start_time=None, end_time=None):
        """
        Segment this signal at a start/end time. You must have one or both of these
        times specified.

        :param segment_type: An optional type string for this segment.
        :type segment_type: str
        :param start_time: The start time of this signal segment, or None if you would like it to have no left bound.
        :type start_time: datetime
        :param end_time: The end time of this signal segment, or None if you would like it to have no right bound.
        :type end_time: datetime
        :return: A signal segment for before the end_time.
        :rtype: SignalSegment
        """
        return SignalSegment(self,
                             segment_type=segment_type,
                             start_time=start_time,
                             end_time=end_time)

    def segment_by(self, td, segment_type='periodic'):
        """
        Segment this signal by a specific timedelta. See the documentation for pd.Grouper for more information.

        :param td: A timedelta to segment this series.
        :type td: timedelta
        :param segment_type: An optional type string for this segment.
        :type segment_type: str
        :return: A list of signal segments segmented by the timedelta.
        :rtype: list[SignalSegment]
        """
        if self._series is None or self.count == 0:
            if self.min_time_override and self.max_time_override:
                empty_series = pd.Series([np.nan, np.nan], index=[self.min_time_override,
                                                                  self.max_time_override])
                empty_groups = empty_series.groupby(pd.Grouper(freq=td))

                return [SignalSegment(self,
                                      segment_type=segment_type,
                                      start_time=max(self.min_time_override, empty_group[0]),
                                      end_time=min(self.max_time_override, (empty_group[0] + td)),
                                      final_segment=(self.max_time_override <= empty_group[0] + td))
                        for empty_group in empty_groups]
            else:
                return []
        else:
            groups = self._series.groupby(pd.Grouper(freq=td))

            if self.min_time_override and self.min_time_override.date() < self.min_reading_time.date():
                first_empty_series = pd.Series([np.nan, np.nan], index=[self.min_time_override,
                                                                        self.min_reading_time - td])
                first_empty_groups = first_empty_series.groupby(pd.Grouper(freq=td))

                first_empty_segments = [SignalSegment(self,
                                                      segment_type=segment_type,
                                                      start_time=max(self.min_time_override, empty_group[0]),
                                                      end_time=empty_group[0] + td,
                                                      final_segment=(self.max_time < empty_group[0] + td))
                                        for empty_group in first_empty_groups]
            else:
                first_empty_segments = []

            if self.max_time_override and self.max_time_override.date() > self.max_reading_time.date():
                start_time = self.max_reading_time.replace(hour=0, minute=0, second=0, microsecond=0) + td
                last_empty_series = pd.Series([np.nan, np.nan], index=[start_time,
                                                                       self.max_time_override])
                last_empty_groups = last_empty_series.groupby(pd.Grouper(freq=td))

                last_empty_segments = [SignalSegment(self,
                                                     segment_type=segment_type,
                                                     start_time=empty_group[0],
                                                     end_time=min(self.max_time_override, (empty_group[0] + td)),
                                                     final_segment=(self.max_time < empty_group[0] + td))
                                       for empty_group in last_empty_groups]
            else:
                last_empty_segments = []

        # On the first day, don't start at midnight. Start at the first reading time.
        signal_segments = [SignalSegment(self,
                                         segment_type=segment_type,
                                         start_time=max(self.min_time, group[0]),
                                         end_time=min(self.max_time, (group[0] + td)),
                                         final_segment=(self.max_time < group[0] + td))
                           for group in groups]

        return first_empty_segments + signal_segments + last_empty_segments

    def segment_daily(self):
        """
        Segment by calendar day. The start time of the first day will be equal to first reading time and the end
        time will be equal to the last reading time, or else the gap calculations will be incorrect.

        :return: A list of signals segmented by calendar day.
        :rtype: list[SignalSegment]
        """
        return self.segment_by(pd.Timedelta(days=1), segment_type="daily")

    def gaps(self):
        """
        Gets the gaps in a signal. A gap is defined as follows:

        A gap occurs when there is no signals for a period that is >= 2 * Normal Reporting Period
         - The gap begins at <LastActualReportTime>+<NormalReportingPeriod>
         - The gap ends at <NextActualReportTime>

        :return: A list of gaps in the signal.
        :rtype: list[ta_utils.Gap]
        """
        if self._gaps is not None:
            return self._gaps

        gaps = []

        if self.bounded_times is None:
            return gaps

        if self.reporting_interval_override is None:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
        else:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

        bounded_times = self.bounded_times

        gap_flags = np.where(np.array(self.intervals) >= 2 * mode_minutes_timedelta)
        for index in gap_flags[0]:
            previous_reading = bounded_times[index]
            # If this is the artifical gap at the beginning, set the start time to the previous reading time
            # which is equal to the min_time_override
            if index == 0 and self.min_time_override is not None:
                start = previous_reading
            else:
                start = previous_reading + mode_minutes_timedelta  # type: datetime
            end = bounded_times[index + 1]
            gap = Gap(previous_reading, start, end)
            gaps.append(gap)

        self._gaps = gaps

        return gaps

    @property
    def num_gaps(self):
        """
        Get the total number of gaps in this Signal.

        :return: Number of gaps in this signal.
        :rtype: int
        """
        if self.count == 0:
            return 1

        return len(self.gaps())

    @property
    def total_gap_time(self):
        """
        Computes the total gap time of a signal.

        :return: Total gap time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        gap_lengths = [gap.length for gap in self.gaps()]
        return sum(gap_lengths)

    @property
    def max_gap_time(self):
        """
        Computes the max gap time of a signal.

        :return: Max gap time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        gaps = self.gaps()
        if len(gaps) == 0:
            return 0

        gap_lengths = [gap.length for gap in gaps]
        return max(gap_lengths)

    @property
    def min_gap_time(self):
        """
        Computes the min gap time of a signal.

        :return: Min gap time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        gaps = self.gaps()
        if len(gaps) == 0:
            return 0

        gap_lengths = [gap.length for gap in gaps]
        return min(gap_lengths)

    def offline_periods(self):
        """
        Gets the offline_periods in a signal. An offline_period is defined as follows:

        An offline_period occurs when there is no signals for a period that is >= 2 * Normal Reporting Period
         - The offline_period begins at <LastActualReportTime>+<NormalReportingPeriod>
         - The offline_period ends at <NextActualReportTime>

        :return: A list of offline_periods in the signal.
        :rtype: list[ta_utils.OfflinePeriod]
        """
        if self._offline_periods is not None:
            return self._offline_periods

        offline_periods = []

        if self.bounded_processed_times is None:
            return offline_periods

        bounded_processed_times = np.array(self.bounded_processed_times)

        if self.reporting_interval_override is None:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
        else:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

        if self.bounded_ages is None:
            return []

        ##  Determine which of the processed dates are associated with delayed readings
        #
        #  JS Note:  A problem was found in that if all the readings come in delayed then this
        #  results in failure to detect any offline period because there is no bounding of the
        #  offline periods.  As a result, going to force the first and last processed dates to
        #  not be flagged as delayed, specifically to create the bounding periods needed if all
        #  reading are in fact delayed.
        delays = self.bounded_ages >= 2 * mode_minutes_timedelta
        delays[0] = False
        delays[-1] = False
        non_delayed_bounded_processed_times = bounded_processed_times[~delays]

        # TODO: processed times need to remove old readings
        sorted_bounded_processed_times = sorted(non_delayed_bounded_processed_times)
        sorted_processed_intervals = np.diff(sorted_bounded_processed_times)

        offline_period_flags = np.where(sorted_processed_intervals >= 2 * mode_minutes_timedelta)
        for index in offline_period_flags[0]:
            previous_processed = sorted_bounded_processed_times[index]
            # If this is the artifical gap at the beginning, set the start time to the previous processed time
            # which is equal to the min_processed_time_override
            if index == 0 and self.min_processed_time_override is not None:
                start = previous_processed
            else:
                start = previous_processed + mode_minutes_timedelta
            end = sorted_bounded_processed_times[index + 1]
            offline_period = OfflinePeriod(previous_processed, start, end)
            offline_periods.append(offline_period)

        self._offline_periods = offline_periods

        return offline_periods

    @property
    def num_offline_periods(self):
        """
        Get the total number of offline_periods in this Signal.

        :return: Number of offline_periods in this signal.
        :rtype: int
        """
        if self.count == 0:
            return 1

        return len(self.offline_periods())

    @property
    def total_offline_period_time(self):
        """
        Computes the total offline_period time of a signal.

        :return: Total offline_period time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        offline_period_lengths = [offline_period.length for offline_period in self.offline_periods()]
        return sum(offline_period_lengths)

    @property
    def max_offline_period_time(self):
        """
        Computes the max offline_period time of a signal.

        :return: Max offline_period time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        offline_periods = self.offline_periods()
        if len(offline_periods) == 0:
            return 0

        offline_period_lengths = [offline_period.length for offline_period in offline_periods]
        return max(offline_period_lengths)

    @property
    def min_offline_period_time(self):
        """
        Computes the min offline_period time of a signal.

        :return: Min offline_period time, in seconds.
        :rtype: int
        """
        if self.count == 0:
            return self.time_span.total_seconds()

        offline_periods = self.offline_periods()
        if len(offline_periods) == 0:
            return 0

        offline_period_lengths = [offline_period.length for offline_period in offline_periods]
        return min(offline_period_lengths)

    def delays(self):
        """
        Gets the delays in a signal. A delay is defined as follows:

        A delay occurs when the reading age is >= 2 * Normal Reporting Period

        :return: A list of delays in the signal.
        :rtype: list[ta_utils.Delay]
        """
        if self._delays is not None:
            return self._delays

        if self._processed_dates is None:
            return []

        if self._series is None:
            return []

        if self.reporting_interval_override is None:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
        else:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

        if len(self.ages) == 0:
            return []

        # The code below should never occur because the type of self.ages is always np.ndarray.
        # The ndarray can contains different content (e.g. integers or timedeltas) but I believe self.ages should
        # always return either an empty array or an array of timedeltas.

        # if type(self.ages) == int:
        #     delay_flags = np.where(self.ages >= 2 * mode_minutes_timedelta.total_seconds())
        # else:
        delay_flags = np.where(self.ages >= 2 * mode_minutes_timedelta)

        delays = []
        for index in delay_flags[0]:
            reading_date = self._series.index[index]
            processed_date = self._processed_dates[index]
            delay = Delay(reading_date, processed_date)
            delays.append(delay)

        self._delays = delays

        return delays

    @property
    def num_delays(self):
        """
        Get the total number of delays in this Signal.

        :return: Number of delays in this signal.
        :rtype: int
        """
        return len(self.delays())

    @property
    def total_delay_time(self):
        """
        Computes the total delay time of a signal.

        :return: Total delay time, in seconds.
        :rtype: int
        """
        delay_ages = [delay.age for delay in self.delays()]
        return sum(delay_ages)

    @property
    def max_delay_time(self):
        """
        Computes the max delay time of a signal.

        :return: Max delay time, in seconds.
        :rtype: int
        """
        if self.num_delays == 0:
            return 0

        delay_ages = [delay.age for delay in self.delays()]
        return max(delay_ages)

    @property
    def min_delay_time(self):
        """
        Computes the min delay time of a signal.

        :return: Max delay time, in seconds.
        :rtype: int
        """
        if self.num_delays == 0:
            return 0

        delay_ages = [delay.age for delay in self.delays()]
        return min(delay_ages)

    @property
    def coverage(self):
        """
        Computes the "coverage" of a signal, which is the percentage of time the signal is not in gap.

        :return: Percentage of the time the signal is not in a gap.
        :rtype: float
        """

        logger.info("Signal.coverage()::  calcaulating coverage")
        if self.count == 0:
            return 0.0

        if self.time_span.total_seconds() == 0:
            return 0.0

        logger.info("Signal.coverage()::  calcaulating coverage using terms:\nfloat(self.total_gap_time): {gt}\nself.time_span.total_seconds(): {tt}\n{cov}".format(
            gt= float(self.total_gap_time), tt = self.time_span.total_seconds(), cov = 1.0 - float(self.total_gap_time) / self.time_span.total_seconds()))

        return 1.0 - float(self.total_gap_time) / self.time_span.total_seconds()

    @property
    def online(self):
        """
        Computes the online ratio of a signal, which is the percentage of time the signal is not offline.

        :return: Percentage of the time the signal is not offline.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        if self.time_span.total_seconds() == 0:
            return 0.0

        return 1.0 - float(self.total_offline_period_time) / self.time_span.total_seconds()

    @property
    def on_time_of_delivered(self):
        """
        Computes the percentage of time the signal's reporting is "on-time" GIVEN that it was delivered,
        i.e. ignoring any coverage gaps

        :return: The on-time ratio.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - float(self.num_delays) / self.count

    @property
    def on_time(self):
        """
        Computes the percentage of time of the monitored period that the signal's reported AND was "on-time", 
        i.e. a measurement was taken and processed 

        :return: The on-time ratio.
        :rtype: float
        """
        return self.coverage * self.on_time_of_delivered

    def celsius_to_fahrenheit(self):
        """
        Converts a celsius signal to fahrenheit via a unit conversion formula.

        :return: A series of fahrenheit temperature values.
        :rtype: Signal
        """
        if self._series is None:
            new_series = None
        else:
            new_series = self._series.apply(lambda x: x * (9. / 5.) + 32.)

        return Signal(new_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=self._processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def fahrenheit_to_celsius(self):
        """
        Converts a fahrenheit signal to celsius via a unit conversion formula.

        :return: A series of celsius temperature values.
        :rtype: Signal
        """
        if self._series is None:
            new_series = None
        else:
            new_series = self._series.apply(lambda x: (x - 32.) * (5. / 9.))

        return Signal(new_series,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=self._processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)

    def __eq__(self, other):
        """
        Checks if two Signals are equal based on their series values.

        :param other: The other object to check for equality.
        :return: Whether or not the other object is equivalent to this Signal.
        :rtype: bool
        """
        if other is None:
            return False

        if not isinstance(other, Signal):
            return False

        device_id_eq = self.device_id == other.device_id
        port_eq = self.port == other.port
        data_type_eq = self.data_type == other.data_type
        min_time_override_eq = self.min_time_override == other.min_time_override
        max_time_override_eq = self.max_time_override == other.max_time_override
        min_processed_time_override_eq = self.min_processed_time_override == other.min_processed_time_override
        max_processed_time_override_eq = self.max_processed_time_override == other.max_processed_time_override
        reporting_interval_override_eq = self.reporting_interval_override == other.reporting_interval_override

        if not (device_id_eq and port_eq and data_type_eq and min_time_override_eq and
                max_time_override_eq and reporting_interval_override_eq and min_processed_time_override_eq
                and max_processed_time_override_eq):
            return False

        if self._series is None and other._series is None:
            series_eq = True
        else:
            series_eq = self._series.equals(other._series)

        if self._processed_dates is None and other._processed_dates is None:
            processed_dates_eq = True
        else:
            processed_dates_eq = self._processed_dates.equals(other._processed_dates)

        return series_eq and processed_dates_eq

    def __len__(self):
        if self._series is None:
            return 0

        return len(self._series)

    def __getitem__(self, key):
        if not isinstance(key, slice):
            raise SignalError("Signal __getitem__ only supports slices.")

        if self._series is None:
            item = None
        else:
            item = self._series[key]

        if self._processed_dates is None:
            processed_dates = None
        else:
            processed_dates = self._processed_dates[key]

        return Signal(item,
                      device_id=self.device_id,
                      port=self.port,
                      data_type=self.data_type,
                      processed_dates=processed_dates,
                      min_time_override=self.min_time_override,
                      max_time_override=self.max_time_override,
                      min_processed_time_override=self.min_processed_time_override,
                      max_processed_time_override=self.max_processed_time_override,
                      reporting_interval_override=self.reporting_interval_override)


class SignalSegmentError(BaseException):
    pass


class SignalSegment(Signal):
    """
    A SignalSegment is a segment of a Signal, updated with all relevant metadata for a Signal
    for a given reading time bounds.
    """
    def __init__(self, signal, segment_type=None, start_time=None, end_time=None, final_segment=False):
        """
        Initialize a SignalSegment with an existing Signal and a start/end time. You must have one or both of these
        times specified.

        :param signal: The Signal object to create the segment from.
        :type signal: Signal
        :param segment_type: An optional type string for this segment.
        :type segment_type: str
        :param start_time: The start time of this signal segment, or None if you would like it to have no left bound.
        :type start_time: datetime
        :param end_time: The end time of this signal segment, or None if you would like it to have no right bound.
        :type end_time: datetime
        :param final_segment: Whether or not this is the last segment in a sequence of segments.
        :type final_segment: bool
        """
        self.signal_source = signal
        self.segment_type = segment_type
        self.final_segment = final_segment

        if start_time is None and end_time is None:
            raise SignalSegmentError("A Signal Segment must have a start_time, end_time, or both!")

        self.start_time = start_time
        self.end_time = end_time

        self.processed_signal = None
        if self.start_time and self.end_time:
            inclusive = self.final_segment or self.segment_type not in ['daily', 'periodic']
            sub_signal = signal.between(self.start_time, self.end_time, inclusive=inclusive)
            if signal._processed_dates is not None:
                self.processed_signal = signal.between(self.start_time, self.end_time,
                                                       inclusive=inclusive, use_processed=True)
        elif self.start_time:
            sub_signal = signal.after(self.start_time, inclusive=True)
            if signal._processed_dates is not None:
                self.processed_signal = signal.after(self.start_time, inclusive=True, use_processed=True)
        else:
            sub_signal = signal.before(self.end_time, inclusive=True)
            if signal._processed_dates is not None:
                self.processed_signal = signal.after(self.start_time, inclusive=True, use_processed=True)

        super(SignalSegment, self).__init__(sub_signal._series,
                                            device_id=signal.device_id,
                                            port=signal.port,
                                            data_type=signal.data_type,
                                            min_time_override=self.start_time,
                                            max_time_override=self.end_time,
                                            min_processed_time_override=self.start_time,
                                            max_processed_time_override=self.end_time,
                                            reporting_interval_override=signal.reporting_interval_override,
                                            processed_dates=sub_signal._processed_dates)

    @property
    def time_span(self):
        """
        The difference between minimum and maximum time in this signal.

        If this is 0, return 1 second in order to not break metrics using this in the denominator.

        :return: The time span of the signal
        :rtype: pd.Timedelta
        """
        if (self.start_time is None or self.end_time is None) and self.count == 0:
            return pd.Timedelta(seconds=1)

        if self.end_time is None:
            end_time = self.max_time
        else:
            end_time = self.end_time

        if self.start_time is None:
            start_time = self.min_time
        else:
            start_time = self.start_time

        return pd.Timedelta(end_time - start_time)

    def offline_periods(self):
        """
        Gets the offline_periods in a signal. An offline_period is defined as follows:

        An offline_period occurs when there is no signals for a period that is >= 2 * Normal Reporting Period
         - The offline_period begins at <LastActualReportTime>+<NormalReportingPeriod>
         - The offline_period ends at <NextActualReportTime>

        :return: A list of offline_periods in the signal.
        :rtype: list[ta_utils.OfflinePeriod]
        """
        if self._offline_periods is not None:
            return self._offline_periods

        if self.processed_signal is None:
            return []

        if self.count == 0:
            return [OfflinePeriod(self.start_time, self.start_time, self.end_time)]

        offline_periods = []

        if self.processed_signal.bounded_processed_times is None:
            return []

        bounded_processed_times = np.array(self.processed_signal.bounded_processed_times)

        if self.reporting_interval_override is None:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
        else:
            mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

        if self.bounded_ages is None:
            return []

        delays = self.bounded_ages >= 2 * mode_minutes_timedelta
        non_delayed_bounded_processed_times = bounded_processed_times[~delays]

        # TODO: processed times need to remove old readings
        sorted_bounded_processed_times = sorted(non_delayed_bounded_processed_times)
        sorted_processed_intervals = np.diff(sorted_bounded_processed_times)

        offline_period_flags = np.where(sorted_processed_intervals >= 2 * mode_minutes_timedelta)
        for index in offline_period_flags[0]:
            previous_processed = sorted_bounded_processed_times[index]
            start = previous_processed + mode_minutes_timedelta
            end = sorted_bounded_processed_times[index + 1]
            offline_period = OfflinePeriod(previous_processed, start, end)
            offline_periods.append(offline_period)

        self._offline_periods = offline_periods

        return offline_periods

    def save(self, force_insert=False, only=None):
        """
        Save the signal segment in a table.

        :param force_insert: boolean to force an insertion query rather than update
        :param only: the parameters you want to include in this save, or None for all
        :return: The persisted signal segment
        :rtype: PersistableSignalSegment
        """
        persistable_signal_segment = PersistableSignalSegment(signal_segment=self)
        persistable_signal_segment.save(force_insert, only)

        # The metadata (Delays, Gaps, OfflinePeriods) should already be saved in the signal object

        return persistable_signal_segment
