"""
Persistable Object Module
"""
from datetime import datetime

import peewee

from ta_utils import database


class Persistable(peewee.Model):
    """
    Basic persistence object for any "persistence" object.

    Peewee Attributes:

    >>> creation_datetime = peewee.DateTimeField()
    >>> modified_datetime = peewee.DateTimeField()
    """
    creation_datetime = peewee.DateTimeField()
    modified_datetime = peewee.DateTimeField(null=True)

    class Meta:
        database = database
        validate_backrefs = False

    def save(self, force_insert=False, only=None):
        """
        Save the record in the table, updating the creation_datetime or modified_datetime as necessary.

        :param force_insert: boolean to force an insertion query rather than update
        :param only: the parameters you want to include in this save, or None for all
        :return: The number of rows affected
        :rtype: int
        """
        if self.creation_datetime is None:
            self.creation_datetime = datetime.now()
        else:
            self.modified_datetime = datetime.now()

        return super(Persistable, self).save(force_insert, only)
