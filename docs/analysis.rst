.. analysis:

Analysis
========

TempAlert Utilities contains a set of analysis functions for structuring and understanding device data.

Below are examples of different analysis functions for various data types:

************
Device Label
************

.. automodule:: ta_utils.analysis.device_label
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__

*******
Defrost
*******

.. automodule:: ta_utils.analysis.defrost
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__

**********
Compliance
**********

.. automodule:: ta_utils.analysis.compliance
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__
