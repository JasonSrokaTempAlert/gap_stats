pip install psycopg2

apt-get install gfortran
apt-get install libblas-dev
apt-get install liblapack-dev
apt-get install g++
apt-get install postgresql

vi /etc/postgresql/9.5/main/pg_hba.conf
    change the line
local	all	postgres		peer
so that peer is now ‘trust’ (means no password will be required)

service postgresql restart

psql -U postgres -c “create user root; create database test_ta_utils with owner root;”


In the trundle virtualenv:

python setup.py install
python setup.py test
