.. signal:

Signal
======

.. autoclass:: ta_utils.Signal
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__

******************
Persistable Signal
******************

.. autoclass:: ta_utils.PersistableSignal
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__