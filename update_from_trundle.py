"""
Update From Trundle Scripts

Usage: python update.py [reset]
"""
import sys
from datetime import datetime, timedelta

import pandas as pd
from peewee import fn

from trundle.trundle_client import TrundleClient

from ta_utils import logger
from ta_utils import database
from ta_utils import create_tables, drop_tables
from ta_utils import ManyDeviceSignals
from ta_utils import PersistableSignal


COMMAND_IDS = ['80', '150.150.370']

USAGE_STR = "Usage: python update.py [reset]"

DEFAULT_RESET_DAYS = 60

STR_DEVICE_ID_PREFIX = "Device:"

DEVICE_ID_KEY = 'DeviceID'
STR_DEVICE_ID_KEY = 'StrDeviceID'
READING_DATE_KEY = 'readingDate'
READING_KEY = 'readingValue'
PROCESSED_DATE_KEY = 'processedDate'
ALARM_KEY = 'alarm'

# database.init('test_ta_utils')
# TODO: eventually have this read from another repo or config files
database.init('warehouse',
              user='jason',
              password='qvz2LFRzMMVPku7',
              host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com',
              port='5432')

# TRUNDLE_CLIENT = None
TRUNDLE_CLIENT = TrundleClient('/Users/jheel/gap_stats/prod-trundle-analyticsconfig/cvs.json')


def get_dataframe_from_trundle(start_time, device_ids=None):
    if device_ids is None:
        devices_df = TRUNDLE_CLIENT.get_all_device_readings(command_ids=COMMAND_IDS,
                                                            reading_start_dt=start_time,
                                                            verbose=True)
    else:
        device_dfs = []
        for device_id in device_ids:
            device_data = TRUNDLE_CLIENT.get_device_readings_by_device_id(device_id=device_id,
                                                                          command_ids=COMMAND_IDS,
                                                                          reading_start_dt=start_time,
                                                                          verbose=True)
            device_df = pd.DataFrame(device_data)

            if len(device_df) == 0:
                logger.info('No data found for device {0}'.format(device_id))
                continue

            device_df[DEVICE_ID_KEY] = str(device_id)
            device_dfs.append(device_df)

        devices_df = pd.DataFrame(pd.concat(device_dfs))

    devices_df[STR_DEVICE_ID_KEY] = STR_DEVICE_ID_PREFIX + devices_df[DEVICE_ID_KEY]
    devices_df[READING_DATE_KEY] = pd.to_datetime(devices_df[READING_DATE_KEY])
    devices_df[PROCESSED_DATE_KEY] = pd.to_datetime(devices_df[PROCESSED_DATE_KEY])
    devices_df.drop(ALARM_KEY, axis=1, inplace=True)

    return devices_df


def update(reset=False):
    if reset:
        drop_tables()
        create_tables()
        start_time = datetime.now() - timedelta(days=DEFAULT_RESET_DAYS)
    else:
        start_time = (PersistableSignal
                      .select(fn.MAX(PersistableSignal.max_time))
                      .scalar())

    logger.info("Getting device data from Trundle...")
    device_data_df = get_dataframe_from_trundle(start_time)

    many_device_signals = ManyDeviceSignals(device_data_df,
                                            DEVICE_ID_KEY,
                                            READING_DATE_KEY,
                                            processed_date_col=PROCESSED_DATE_KEY)

    logger.info("Updating device signals...")
    many_device_signals.update_all_signals(READING_KEY)

if __name__ == "__main__":
    arguments = sys.argv

    if len(arguments) == 1:
        logger.info("You must specify a database for this update.")
        logger.info(USAGE_STR)
    elif len(arguments) == 2:
        reset_flag = arguments[2].lower() == "reset"
        update(reset_flag)
    else:
        logger.info("You have too many arguments.")
        logger.info(USAGE_STR)
