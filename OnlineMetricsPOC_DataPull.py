####  Environment setup
import os
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta

from ta_utils import ManyDeviceSignals, PersistableSignal
from ta_utils import database
from ta_utils import create_tables, drop_tables

def FormatDatetime(RawDatetime):
    result=RawDatetime.replace('T',' ')
    ReturnDatetime=result.split('.')[0]
#     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
    return ReturnDatetime

####  Device Metadata - used for filtering to a good set of Device IDs
DevicesInfoFile='/Users/Jason/Desktop/Local_Data_Store/vu_DeviceAndStoreInfo.csv'
DevicesInfo=pd.read_csv(DevicesInfoFile)

##  Filter to RI devices for now
RI_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='RI') & (DevicesInfo['DateDeleted'].isnull()) ]
# Target_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='OR') and 
#                                          (('Store #16' in i for i in DevicesInfo['StoreNumber']) or ('Store #17' in i for i in DevicesInfo['StoreNumber']))]
# OR_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='OR') ]

##  Quick test
Quicktest_GroupIDs=[9613]
QuicktestDeviceIndices=np.where(DevicesInfo['GroupID'].isin(Quicktest_GroupIDs))
Quicktest_DeviceInfo=DevicesInfo.iloc[QuicktestDeviceIndices]



##  Processing to set up gruops for Oregon Target stores
OR_Target_GroupIDs=[9613,9614,9615,9616,9617,9618,91619,9620,9623,9624,9625,9626,9628,9629,9631,9636,9637,9638]
TargetDeviceIndices=np.where(DevicesInfo['GroupID'].isin(OR_Target_GroupIDs))
OR_Target_DeviceInfo=DevicesInfo.iloc[TargetDeviceIndices]
# Monitored_DevicesInfo=Monitored_DevicesInfo.append(OR_Monitored_DevicesInfo)

###  Processing to set up groups for minute clinic stores
MCRolloutFileName='/Users/Jason/Desktop/Analytics/Projects/ANALYTICS_106_StoreResetFailures/TempAlertRolloutSchedule.csv'
MCRolloutDF=pd.DataFrame.from_csv(MCRolloutFileName)


# MinuteClinicStoresList=['Store #459','Store #750','Store #1043','Store #1180','Store #2220','Store #2525','Store #447','Store #957','Store #969',
# 'Store #1094','Store #1095','Store #38','Store #299','Store #507','Store #704','Store #920','Store #946','Store #1882','Store #2172','Store #41',
# 'Store #207','Store #335','Store #394','Store #913','Store #938','Store #1877','Store #6466','Store #212','Store #326','Store #590','Store #729',
# 'Store #1544','Store #2065','Store #5107','Store #717','Store #1010','Store #1018','Store #1022','Store #1254','Store #1531','Store #6505',
# 'Store #8437','Store #137','Store #929','Store #1006','Store #1238','Store #1297','Store #1852','Store #2325','Store #281','Store #634','Store #937',
# 'Store #1945','Store #2138','Store #2401','Store #2600','Store #73','Store #217','Store #915','Store #1011','Store #1174','Store #1845','Store #7109',
# 'Store #654','Store #730','Store #973','Store #1121','Store #1179','Store #1875','Store #2125','Store #329','Store #806','Store #841','Store #1003',
# 'Store #1004','Store #2257','Store #6596']
MCRolloutFileName='/Users/Jason/Desktop/Analytics/Projects/ANALYTICS_106_StoreResetFailures/TempAlertRolloutSchedule.csv'
MCRolloutDF=pd.DataFrame.from_csv(MCRolloutFileName)
MCRolloutDF.head()
FilteredMCRolloutDF=MCRolloutDF[MCRolloutDF['HostStoreNum'].str.contains("MC")==False]

PilotStoreNums=FilteredMCRolloutDF[FilteredMCRolloutDF['Status']=='Pilot'].HostStoreNum.apply(lambda x:int(x))
PilotStoreNums=FilteredMCRolloutDF.HostStoreNum.apply(lambda x:int(x))
PilotStoreNums[0]
MinuteClinicStoresList=PilotStoreNums.apply(lambda(x): "Store #{}".format(x))
# PilotStoreNames=PilotStoreNames.append('Store #0MC98')

NumStores=len(MinuteClinicStoresList)
MinuteClinicGroupsDict={}
for StoreName in MinuteClinicStoresList:
    thisGroupID=DevicesInfo['GroupID'][DevicesInfo['StoreNumber']==StoreName].unique()
    print "thisGroupID: {GID} for store name:  {name} ".format(GID=thisGroupID,name=StoreName)
    if len(thisGroupID)==0:
        print "no devices found for group: {}".format(StoreName)
        continue
    MinuteClinicGroupsDict[StoreName]=int(thisGroupID[0])
MinuteClinicGroupIDs=MinuteClinicGroupsDict.values()
##  First get the devices that are in the store's group
MinuteClinic_DeviceIndices=np.where(DevicesInfo['GroupID'].isin(MinuteClinicGroupIDs))
MinuteClinic_DeviceInfo=DevicesInfo.iloc[MinuteClinic_DeviceIndices]
MinuteClinic_DeviceInfo['StoreGroupID']=MinuteClinic_DeviceInfo['GroupID']
##  Next get the devices that are in the children of a store's group
MinuteClinic_DeviceIndices_ChildrenGroups=np.where(DevicesInfo['ParentGroupID'].isin(MinuteClinicGroupIDs))
MinuteClinic_DeviceInfo_ChildrenGroups=DevicesInfo.iloc[MinuteClinic_DeviceIndices_ChildrenGroups]
MinuteClinic_DeviceInfo_ChildrenGroups['StoreGroupID']=MinuteClinic_DeviceInfo_ChildrenGroups['ParentGroupID']
##  Put them together
MinuteClinic_DeviceInfo=MinuteClinic_DeviceInfo.append(MinuteClinic_DeviceInfo_ChildrenGroups)

MinuteClinic_DevGroup_DeviceInfo=MinuteClinic_DeviceInfo[MinuteClinic_DeviceInfo['GroupID']==1652]

OmnicareDeepFreezersList=['11666000000120943688','11666000000120963823','11666000000121005851','11666000000121022328','11666000000121464331',
'11666000000121487753','11666000000121502766','11666000000121520937','11666000000121525293','11666000000121859660','11666000000122819335',
'11666000000127948954','11666000000128006004','11666000000133579667']
OmniDeepFreeze_DeviceInfo=DevicesInfo[DevicesInfo['DeviceID'].isin(OmnicareDeepFreezersList)]

##  Final selection
# Monitored_DevicesInfo=OR_Target_DeviceInfo
# DataTag='OR_Target'
Monitored_DevicesInfo=MinuteClinic_DeviceInfo
DataTag='MinuteClinic'
# Monitored_DevicesInfo=MinuteClinic_DevGroup_DeviceInfo
# DataTag='MinuteClinic_DevGroup_DeviceInfo'
# Monitored_DevicesInfo=RI_Monitored_DevicesInfo
# DataTag='RI_CVS'
# Monitored_DevicesInfo=Quicktest_DeviceInfo
# DataTag='Quicktest'


# Monitored_DevicesInfo=MinuteClinic_DevGroup_DeviceInfo
# Monitored_DevicesInfo=OmniDeepFreeze_DeviceInfo

Monitored_DeviceIDs=Monitored_DevicesInfo['DeviceID']
NumMonitoredDevices=len(Monitored_DeviceIDs)


####  Configuration/Function params
ZigbeeDevicePowerDataFile='/Users/Jason/Desktop/Local_Data_Store/ZigbeeDevicePowerHistory_DataPull.csv'
ZigbeeSensorInfoDataFile='/Users/Jason/Desktop/Local_Data_Store/TrundleCVSSensorData_DataPull.csv'
TrundleDevicesInfoFile='/Users/Jason/Desktop/Local_Data_Store/TrundleDeviceInfo.csv'
DeviceReadingGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DeviceReadingGaps.csv'
SensorReadingGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_SensorReadingGaps.csv'
SensorTransmissionGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_SensorTransmissionGaps.csv'
DeviceTransmissionGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DeviceTransmissionGaps.csv'
GapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DataFlowGaps.csv'
GroupGatewayOnlineStatsFile='/Users/Jason/Desktop/Local_Data_Store/GroupGatewayOnline_DailyStats.csv'
PowerPull_BadDevicesFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_PowerPull_BadDevicesList.csv'.format(DataTag)
TemperaturePull_BadDevicesFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_TemperaturePull_BadDevicesList.csv'.format(DataTag)

NumDevicesToProcess=800

AnalysisWindowInDays = 20
AnalysisWindow_StartDate = datetime.now() - timedelta(days=AnalysisWindowInDays)
AnalysisWindow_StartDate=datetime(2017,03,25,0,0,0)


GapStats_DeviceInfoFields=['DeviceID','DeviceLabel','GroupID','GroupName','Environment','Address','City','StateOrProvince','PostalCode','StoreNumber']
thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')

RunTimestamp=datetime.now().date()

FLAG_PullData_GWPower=False
FLAG_PullData_SensorReadings=True


##
##   Get the data for Gateway Power analysis
##
if FLAG_PullData_GWPower:
    StartTime=datetime.now()
    import json
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
    # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')


    FLAG_Header=True
    WriteMode='w'

    Monitored_Gateways_DeviceIDs=Monitored_DevicesInfo[Monitored_DevicesInfo['HardwareFamily'].isin([2,3])].DeviceID
    NumMonitoredGatewayDevices=len(Monitored_Gateways_DeviceIDs)

    BadDevicesDF=pd.DataFrame()
    BadDevicesCount=1
    # for DeviceIter in range(0,NumDevicesToProcess):
    for DeviceIter in range(0,NumMonitoredGatewayDevices):
        thisDeviceID = Monitored_Gateways_DeviceIDs.iloc[DeviceIter]
        thisStrDeviceID = "Device:"+str(thisDeviceID)

        print"Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisStrDeviceID,DeviceIter=DeviceIter,TotalDevices=NumMonitoredGatewayDevices)

        thisDeviceDataDF=pd.DataFrame(thisClient.get_device_readings_by_device_id(device_id=thisDeviceID,verbose=True,
                                                                                  command_ids=['80','150.150.370'],
                                                                          reading_start_dt=AnalysisWindow_StartDate))
        if(len(thisDeviceDataDF)==0):
            print('No data found for device, skipping to next')
            BadDevicesDF=BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No data returned by trundle']}))
            print BadDevicesDF
            BadDevicesCount=BadDevicesCount+1
            continue
        thisDeviceDataDF['DeviceID']=str(thisDeviceID)
        thisDeviceDataDF['StrDeviceID']=thisStrDeviceID
        thisDeviceDataDF['readingDate']=thisDeviceDataDF['readingDate'].apply(lambda x: FormatDatetime(x))
        thisDeviceDataDF['processedDate']=thisDeviceDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
        thisDeviceDataDF.drop('alarm',axis=1,inplace=True)
        print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisDeviceDataDF))

        #     print thisDeviceDataDF

        thisDeviceDataDF.to_csv(ZigbeeDevicePowerDataFile,header=FLAG_Header,mode=WriteMode,index=False)
        FLAG_Header=False
        WriteMode='a'
    EndTime=datetime.now()
    TimeDuration=EndTime-StartTime
    if len(BadDevicesDF)>0:
        BadDevicesDF=pd.merge(BadDevicesDF,DevicesInfo[GapStats_DeviceInfoFields],
                                       on=['DeviceID'],how='left')
        BadDevicesDF.to_csv(PowerPull_BadDevicesFile,index=False)
    print "Gateway power Data Pull Time: {TD}".format(TD=TimeDuration)
    




##
##   Get the sensor data
##
if FLAG_PullData_SensorReadings:
    StartTime=datetime.now()
    import json
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
    # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')
    print 'setting start time to {}'.format(AnalysisWindow_StartDate)

    RunTimestamp=datetime.now().date()

    FLAG_Header =  False
    WriteMode='a'
    NumMonitoredDevices=len(Monitored_DeviceIDs)


    TempPull_BadDevicesDF=pd.DataFrame()
    BadDevicesCount=1
    for DeviceIter in range(9363,NumMonitoredDevices):
#     for DeviceIter in range(0,NumMonitoredDevices):
        
        thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
#         thisDeviceID = 11666000000133509832
        print"Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisDeviceID,DeviceIter=DeviceIter,TotalDevices=NumMonitoredDevices)

        thisDeviceDataDF=pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
#         print thisDeviceDataDF
        if(len(thisDeviceDataDF)==0):
            print'No data found for device, skipping to next'
            TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No data found for device']}))
            BadDevicesCount=BadDevicesCount+1
            continue
        if('sensors' in thisDeviceDataDF.columns):
            print'No sensors for this device, skipping to next'
            TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No sensors for this device']}))
            BadDevicesCount=BadDevicesCount+1
            continue
        thisNumSensors=len(thisDeviceDataDF.port)
        for SensorIndex in range(0,thisNumSensors):
#             print "SensorIndex: {index}  of {total}".format(index=SensorIndex,total=thisNumSensors)
            thisPortID = thisDeviceDataDF.port[SensorIndex]
            print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID,index=SensorIndex,total=thisNumSensors)
            thisSensorDataDF=pd.DataFrame(thisClient.get_sensor_readings_by_port_id(thisDeviceID,thisPortID,reading_start_dt=AnalysisWindow_StartDate))
            if('processedDate' not in thisSensorDataDF.columns):
                print "no processed date in data returned, skipping"
                TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No processed date in data returned']}))
                BadDevicesCount=BadDevicesCount+1
                continue
    #         print type(thisSensorDataDF['processedDate'][0])
            thisSensorDataDF['processedDate']=thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
            thisSensorDataDF['readingDate']=thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))

            thisSensorDataDF['age']=(pd.to_datetime(thisSensorDataDF['processedDate'])-pd.to_datetime(thisSensorDataDF['readingDate'])).astype('timedelta64[s]')
            thisSensorDataDF['port']=thisPortID
            thisSensorDataDF['DeviceID']=thisDeviceID
            thisSensorDataDF['PortID']=thisPortID
            thisSensorDataDF['StrDeviceID']="Device:"+str(thisDeviceID)+":"+str(thisPortID)

            thisSensorDataDF.drop('alarm',axis=1,inplace=True)

            print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisSensorDataDF))
    #         print"writing to file: {Outfile}".format(Outfile=ZigbeeSensorInfoDataFile)
            thisSensorDataDF.to_csv(ZigbeeSensorInfoDataFile,header=FLAG_Header,mode=WriteMode,index=False)
    #         print"done with write"
            FLAG_Header=False
            WriteMode='a'

    EndTime=datetime.now()
    if len(TempPull_BadDevicesDF)>0:
        TempPull_BadDevicesDF=pd.merge(TempPull_BadDevicesDF,DevicesInfo[GapStats_DeviceInfoFields],
                                       on=['DeviceID'],how='left')
        TempPull_BadDevicesDF.to_csv(TemperaturePull_BadDevicesFile,index=False)

    TimeDuration=EndTime-StartTime
    print "Data Pull Time: {TD}".format(TD=TimeDuration)



