####  Environment setup
import os
# import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import psycopg2
# import json
import re

script_start_date = datetime.now()
print "script_start_date: {}".format(script_start_date)

##  Environment Switch
Environment = 'local'
# Environment = 'ubuntu'

##
##  Controls
##2
flag_redo_all = True
flag_pull_vu = False
flag_redo_devicesummary = True
flag_redo_groupsummary = True

##
###  Configuration
##
if Environment == 'local':
    data_dir = '/Users/Jason/Desktop/Local_Data_Store/'
    results_dir = '/Users/Jason/Desktop/Results_GapMetrics/'
else:
    data_dir = '/home/jason/Data'
    results_dir = '/home/jason/Results'

# devices_info_file=os.path.join(data_dir,'vu_DeviceAndStoreInfo.csv')
vu_oga_file = os.path.join(data_dir,'vu_daily_channel_onlinegroupadjusted.csv')
device_oga_summary_filename = os.path.join(data_dir, 'device_oga_summarystats.csv')
group_oga_summary_filename = os.path.join(data_dir, 'group_oga_summarystats.csv')

##
##  Help functions
##
def find_loc(name) :
    # print "find_loc()::  called with string '{}'".format(name)
    if re.search('MC', name) :
        return 'MC'
    elif re.search('RX', name):
        return 'RX'
    elif re.search('FS', name):
        return 'FS'
    else :
        return 'Unknown'


RED_GAP_THRESHOLD_MINUTES = 4 * 60
YELLOW_GAP_THRESHOLD_MINUTES = 1 * 60
def assign_color_label(max_gap_minutes):
    if max_gap_minutes < YELLOW_GAP_THRESHOLD_MINUTES:
        return 'GREEN'
    if max_gap_minutes < RED_GAP_THRESHOLD_MINUTES:
        return 'YELLOW'
    return 'RED'


##
##
##   Load relevant data
##
##

####   DevicesInfo from Vu_DeviceAndStoreInfo
# print "Reading DevicesInfo from file: {file}".format(file = devices_info_file)
# DevicesInfo=pd.read_csv(devices_info_file)
print "Reading DevicesInfo from view"
conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
conn = psycopg2.connect(conn_string)
DevicesInfo = pd.read_sql("SELECT * from public.vu_deviceandstoreinfo where accountid = 4226", conn)
conn.close()

####  Online Group Adjusted
if flag_redo_all or not os.path.exists(vu_oga_file):
    conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    conn = psycopg2.connect(conn_string)

    OGA_data_query = """SELECT * from public.vu_daily_channel_onlinegroupadjusted;"""
    print "group_data_query: {}".format(OGA_data_query)
    OGA_DF = pd.read_sql(OGA_data_query, conn)
    print "read {numrows} rows from db".format(numrows = len(OGA_DF))
    conn.close()
    OGA_DF.to_csv(vu_oga_file)
else:
    print "reading OGA_DF from file: {file}".format(file = vu_oga_file)
    OGA_DF = pd.DataFrame.from_csv(vu_oga_file)
    OGA_DF['assessmentdate'] = OGA_DF['assessmentdate'].apply(lambda x: datetime.strptime(str(x),"%Y-%m-%d"))

###
###  Prep data for summary stats generation
###


# base_data_columns = ['DeviceID','GroupID','GroupName','StoreNumber','Environment','StateOrProvince','DeviceLabel','DeviceType','LastPower','DeviceLat','DeviceLong']
base_data_columns = ['deviceid','groupid','groupname','storenumber','environment','state','devicelabel','devicetype','firmware',
                     'lastpower','devicelat','devicelong','datedeleted']
oga_aug = OGA_DF.merge(DevicesInfo[base_data_columns], left_on = 'deviceid', right_on = 'deviceid', how = 'left')
print "oga_aug.columns: {}".format(oga_aug.columns)
print "after augmenting oga, {numrows} rows of data".format(numrows = len(oga_aug))

##
##  Workaround for expected improvement to not normalize gateways
##
# NE_OGA = oga_aug[(oga_aug['state'].isin(['ME','MA','NH','VT','CT','RI']) |   oga_aug['firmware'].isin([1.102, 1.103, 1.104, 1.105]) )  ]
NE_OGA = oga_aug[(                   (
                                      oga_aug['groupname'].str.contains(' MA ') |
                                      oga_aug['groupname'].str.contains(' ME ') |
                                      oga_aug['groupname'].str.contains(' RI ') |
                                      oga_aug['groupname'].str.contains(' CT ') |
                                      oga_aug['groupname'].str.contains(' VT ') |
                                      oga_aug['groupname'].str.contains(' NH ') |
                                      oga_aug['groupname'].str.contains(' CA ') |
                                      oga_aug['firmware'].isin([1.102, 1.103, 1.104, 1.105])) &
                                     (oga_aug['datedeleted'].isnull() ) &
                                     ~(  oga_aug['groupid'].isin([8878, 18524])  )
                                     )]
print "NE_OGA.columns: {}".format(NE_OGA.columns)
print "after filtering for state, firmware, and test groups, {numrows} rows of data".format(numrows = len(NE_OGA))
###
###
###    Device-Level Summary Stats
###
###
if flag_redo_all or flag_redo_devicesummary or not os.path.exists(device_oga_summary_filename):


    recent_history_numdays = 7

    # all_strdeviceids = NE_OGA[NE_OGA['storegroupid']==4602].strdeviceid.unique()
    all_strdeviceids = NE_OGA.strdeviceid.unique()
    oga_summary_df = pd.DataFrame()
    num_devices = len(all_strdeviceids)
    num_processed_devices = 1
    total_device_count = len(all_strdeviceids)

    FLAG_HEADER = True
    WRITE_MODE = 'w'

    for this_strdeviceid in all_strdeviceids:
        print "processing summary stats for device {sdi}, number {index} of {total}".format(sdi=this_strdeviceid,
                                                                                            index=num_processed_devices,
                                                                                            total=total_device_count)
        this_device_df = NE_OGA[NE_OGA['strdeviceid'] == this_strdeviceid]

        this_device_deviceid = this_strdeviceid.split(':')[1]
        if len(this_device_df) < 2:
            continue
        this_device_latestdate = max(this_device_df['assessmentdate'])
        this_device_mindate = min(this_device_df['assessmentdate'])
        this_device_latestdate_df = this_device_df[this_device_df['assessmentdate'] == str(this_device_latestdate)]
        this_device_yesterday = this_device_latestdate - timedelta(days=1)
        print "this_device_yesterday: {}".format(this_device_yesterday)
        this_device_yesterday_oga = this_device_df[this_device_df['assessmentdate'] == this_device_yesterday].coverage.values
        # print "this_device_yesterday_oga: {}".format(this_device_yesterday_oga)
        # print "this_device_latestdate_df: {}".format(this_device_latestdate_df)
        # print "this_device_df[this_device_df['assessmentdate'] == this_device_yesterday]: {}".format(this_device_df[this_device_df['assessmentdate'] == this_device_yesterday])
        # print "this_device_yesterday_oga: {}".format(this_device_yesterday_oga)

        recent_history_mindate = this_device_yesterday - timedelta(days=recent_history_numdays)
        recent_history_maxdate = this_device_yesterday - timedelta(days=1)
        this_device_recent_history_notyesterday = this_device_df[
            (this_device_df['assessmentdate'] >= recent_history_mindate) &
            (this_device_df['assessmentdate'] <= recent_history_maxdate)]
        this_device_recent_history_notyesterday_oga = this_device_recent_history_notyesterday['coverage'].mean()
        this_device_recent_history = this_device_df[(this_device_df['assessmentdate'] >= recent_history_mindate) &
                                                    (this_device_df['assessmentdate'] <= this_device_yesterday)]
        this_device_recent_history_oga_mean = this_device_recent_history['coverage'].mean()

        this_device_recent_history = this_device_recent_history_notyesterday.append(
            this_device_df[this_device_df['assessmentdate'] == this_device_yesterday])
        this_device_recent_history_numdays = len(this_device_recent_history)
        this_device_recent_history_numreddays = len(
            this_device_recent_history[this_device_recent_history['maxgap_minutes'] >= RED_GAP_THRESHOLD_MINUTES])
        this_device_recent_history_numyellowdays = len(
            this_device_recent_history[this_device_recent_history['maxgap_minutes'] >= YELLOW_GAP_THRESHOLD_MINUTES])

        this_device_gapstats_summary = DevicesInfo[DevicesInfo['deviceid'] == this_device_deviceid]

        this_device_gapstats_summary['storegroupid'] = this_device_df.storegroupid.values[0]
        this_device_gapstats_summary['gatewayids'] = this_device_df.gatewayids.values[0]
        this_device_gapstats_summary['numgateways'] = this_device_df.numgateways.values[0]
        this_device_gapstats_summary['gw_online'] = this_device_df.gw_online.values[0]
        this_device_gapstats_summary['strdeviceid'] = this_strdeviceid
        this_device_gapstats_summary['location'] = find_loc(this_device_df['devicelabel'].values[0])
        this_device_gapstats_summary['oga_yesterday'] = this_device_yesterday_oga
        this_device_gapstats_summary['yesterday'] = this_device_yesterday
        this_device_gapstats_summary['recent_history_mindate'] = recent_history_mindate
        this_device_gapstats_summary['recent_history_maxdate'] = recent_history_maxdate
        this_device_gapstats_summary['recent_history_oga_mean'] = this_device_recent_history_oga_mean
        this_device_gapstats_summary[
            'oga_yesterdaychange'] = this_device_yesterday_oga - this_device_recent_history_notyesterday_oga
        this_device_gapstats_summary[
            'oga_yesterday_gapminutes_max'] = this_device_df[this_device_df['assessmentdate'] == this_device_yesterday].maxgap_minutes.values
        this_device_gapstats_summary[
            'oga_yesterday_gapminutes_total'] = this_device_df[this_device_df['assessmentdate'] == this_device_yesterday].totalgappage_minutes.values
        this_device_gapstats_summary['recent_history_numdays'] = this_device_recent_history_numdays
        this_device_gapstats_summary['recent_history_numreddays'] = this_device_recent_history_numreddays
        this_device_gapstats_summary['recent_history_numyellowdays'] = this_device_recent_history_numyellowdays
        this_device_gapstats_summary['recent_history_maxgapminutes'] = this_device_recent_history['maxgap_minutes'].max()
        this_device_gapstats_summary['recent_history_totalnumgaps'] = this_device_recent_history['numgaps'].sum()
        this_device_gapstats_summary['recent_history_totalgappageminutes'] = this_device_recent_history['totalgappage_minutes'].sum()

        #     print "this_device_gapstats_summary: {}".format(this_device_gapstats_summary)
        this_device_gapstats_summary.to_csv(device_oga_summary_filename, index=False, header=FLAG_HEADER, mode=WRITE_MODE)
        #    oga_summary_df = oga_summary_df.append(this_device_gapstats_summary)
        num_processed_devices = num_processed_devices + 1
        FLAG_HEADER = False
        WRITE_MODE = 'a'

###
###   Store-level summaries
###

if flag_redo_all or flag_redo_devicesummary or flag_redo_groupsummary or not os.path.exists(group_oga_summary_filename):

    oga_summary_df = pd.DataFrame.from_csv(device_oga_summary_filename)

    all_storegroupids = oga_summary_df['storegroupid'].unique()
    num_stores = len(all_storegroupids)

    base_data_columns = ['groupid', 'groupname', 'accountid', 'address', 'city', 'state', 'zipcode',
                         'storenumber']
    group_basedata_df = DevicesInfo[base_data_columns]
    group_basedata_df = group_basedata_df.drop_duplicates()

    print "num_stores: {}".format(num_stores)

    FLAG_HEADER = True
    WRITE_MODE = 'w'

    group_gapstats_summary_df = pd.DataFrame()
    running_group_count = 1
    for this_storegroupid in all_storegroupids:
        # for this_groupname in ['Store #10197 - 13 Vermont 15 East, Morrisville VT 05661']:

        print "processing storegroupid {name}   number {index} of {total}".format(name=this_storegroupid,
                                                                           index=running_group_count,
                                                                           total=num_stores)
        this_group_gapstats_summary = group_basedata_df[group_basedata_df['groupid'] == this_storegroupid]

        this_group_devices_df = oga_summary_df[oga_summary_df['storegroupid'] == this_storegroupid]
        print "this_group_devices_df: {}".format(this_group_devices_df)
        num_devices = len(this_group_devices_df)
        group_oga_recenthistory_mean = this_group_devices_df['recent_history_oga_mean'].mean()
        group_oga_yesterday_mean = this_group_devices_df['oga_yesterday'].mean()
        group_oga_yesterdaychange_mean = this_group_devices_df['oga_yesterdaychange'].mean()
        group_oga_recenthistory_numreddays_total = this_group_devices_df['recent_history_numreddays'].sum()
        group_oga_recenthistory_numyellowdays_total = this_group_devices_df['recent_history_numyellowdays'].sum()

        this_group_gapstats_summary['num_devices'] = num_devices
        this_group_gapstats_summary['group_oga_recenthistory_mean'] = group_oga_recenthistory_mean
        this_group_gapstats_summary['group_oga_yesterday_mean'] = group_oga_yesterday_mean
        this_group_gapstats_summary['group_oga_yesterdaychange_mean'] = group_oga_yesterdaychange_mean
        this_group_gapstats_summary['group_oga_recenthistory_numreddays_total'] = group_oga_recenthistory_numreddays_total
        this_group_gapstats_summary[
            'group_oga_recenthistory_numyellowdays_total'] = group_oga_recenthistory_numyellowdays_total
        this_group_gapstats_summary['lat'] = this_group_devices_df.devicelat.values[0]
        this_group_gapstats_summary['long'] = this_group_devices_df.devicelong.values[0]

        group_gapstats_summary_df = group_gapstats_summary_df.append(this_group_gapstats_summary)
        #     print "group_gapstats_summary_df: {}".format(group_gapstats_summary_df)
        running_group_count = running_group_count + 1
        FLAG_HEADER = False
        WRITE_MODE = 'a'

    group_gapstats_summary_df.to_csv(group_oga_summary_filename)





script_end_date = datetime.now()
runtime = script_end_date - script_start_date
print "Script complete at time {end};  runtime = {timediff}".format(end = script_end_date, timediff = runtime)