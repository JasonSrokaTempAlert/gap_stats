"""
Test Defrost Analysis
"""
import unittest
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal
from ta_utils import find_compliance, YES_COMPLIANCE, NO_COMPLIANCE, CLOSE_COMPLIANCE

now = datetime.now()
dates = [now + timedelta(days=i) for i in range(0, 10)]

RANGE_MIN = 0.
RANGE_MAX = 2.
MAX_EXCURSION_LENGTH = 2
MAX_EXCURSION_TEMP = 4.

non_defrost_signal = Signal(pd.Series(data=[0., 1., 0., 1., 5., 1., 0., 1., 0., 0.], index=dates))
always_within_range_signal = Signal(pd.Series(data=[0., 1., 2., 1., 0., 1., 2., 1., 0., 1.], index=dates))
close_signal = Signal(pd.Series(data=[0., 1., 2., 1., 0., 1., 2., 3., 2., 1.], index=dates))
non_compliant_signal = Signal(pd.Series(data=[0., 2., 4., 6., 4., 2., 0., 2., 4., 6.], index=dates))


class ComplianceTestCase(unittest.TestCase):
    def test_close_compliant_non_defrost(self):
        compliance = find_compliance(non_defrost_signal,
                                     RANGE_MIN, RANGE_MAX, MAX_EXCURSION_LENGTH, MAX_EXCURSION_TEMP)
        self.assertEqual(CLOSE_COMPLIANCE, compliance)

    def test_compliant(self):
        compliance = find_compliance(always_within_range_signal,
                                     RANGE_MIN, RANGE_MAX, MAX_EXCURSION_LENGTH, MAX_EXCURSION_TEMP)
        self.assertEqual(YES_COMPLIANCE, compliance)

    def test_close_compliance(self):
        compliance = find_compliance(close_signal,
                                     RANGE_MIN, RANGE_MAX, MAX_EXCURSION_LENGTH, MAX_EXCURSION_TEMP)
        self.assertEqual(CLOSE_COMPLIANCE, compliance)

    def test_not_compliant(self):
        compliance = find_compliance(non_compliant_signal,
                                     RANGE_MIN, RANGE_MAX, MAX_EXCURSION_LENGTH, MAX_EXCURSION_TEMP)
        self.assertEqual(NO_COMPLIANCE, compliance)
