.. signal_segment:

Signal Segment
==============

.. autoclass:: ta_utils.SignalSegment
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__
