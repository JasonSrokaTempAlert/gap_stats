"""
Test Signal Module
"""

import unittest
import math
from datetime import datetime, timedelta

from ta_utils import logger
from ta_utils import Signal, SignalError
from ta_utils import Gap
from ta_utils import Delay
from ta_utils import OfflinePeriod
import pandas as pd
import numpy as np

MINUTES_IN_DAY = 60 * 24

now = datetime.now()
dates = [now + timedelta(days=(i ** 2)) for i in range(0, 3)]
min_date = min(dates)
max_date = max(dates)

null_signal = Signal(None)
empty_signal = Signal(pd.Series())
trivial_signal = Signal(pd.Series(data=[0., 1., 2.], index=dates))
trivial_signal_clone = Signal(pd.Series(data=[0., 1., 2.], index=dates))
celsius_signal = Signal(pd.Series(data=[-40., 0., 100.], index=dates))
fahrenheit_signal = Signal(pd.Series(data=[-40., 32., 212.], index=dates))

mode_dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10]]
mode_signal = Signal(pd.Series(data=[0., 1., 2., 3., 4.], index=mode_dates))

gap_dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]

# Transmission dates are crafted so there's still on offline period from 14 - 20
# and there's an offline period from 3 - 5 and 7 - 10 because there's a delay at 7 .
transmission_dates = [now + timedelta(minutes=i) for i in [0, 1, 7, 5, 10, 12, 14, 20, 22]]
gap_signal = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=gap_dates),
                    processed_dates=pd.Series(data=transmission_dates, index=gap_dates))

null_signal_with_overrides = Signal(None,
                                    min_time_override=now,
                                    min_processed_time_override=now,
                                    max_time_override=now + timedelta(minutes=30),
                                    max_processed_time_override=now + timedelta(minutes=30),
                                    reporting_interval_override=timedelta(minutes=15).total_seconds())


class SignalTestCase(unittest.TestCase):
    def test_null_signal(self):
        self.assertEqual(len(null_signal), 0)
        self.assertTrue(np.array_equal(null_signal.values, np.array([])))
        self.assertEqual(null_signal, null_signal[0:])

    def test_eq(self):
        self.assertFalse(trivial_signal is None)
        self.assertFalse(trivial_signal == trivial_signal._series)
        self.assertTrue(trivial_signal == trivial_signal_clone)

    def test_invalid_processed_dates(self):
        with self.assertRaises(SignalError):
            Signal(pd.Series(), processed_dates=pd.Series(dates, index=dates))

    def test_invalid_slice(self):
        with self.assertRaises(SignalError):
            logger.info(trivial_signal['invalid_slice'])

    def test_invalid_ages(self):
        with self.assertRaises(SignalError):
            logger.info(trivial_signal.ages)

    def test_count_len_properties(self):
        self.assertEqual(0, len(empty_signal))
        self.assertEqual(0, empty_signal.count)

        self.assertEqual(3, len(trivial_signal))
        self.assertEqual(3, trivial_signal.count)

    def test_values_property(self):
        self.assertTrue(np.array_equal(np.array([0., 1., 2.]), trivial_signal.values))

    def test_times_property(self):
        self.assertListEqual(trivial_signal.times, dates)

    def test_intervals_property(self):
        self.assertListEqual(trivial_signal.intervals, [timedelta(days=1), timedelta(days=3)])

    def test_minute_intervals_property(self):
        self.assertListEqual(trivial_signal.minute_intervals, [MINUTES_IN_DAY, 3 * MINUTES_IN_DAY])

    def test_mean_interval_property(self):
        self.assertEqual(trivial_signal.mean_interval, timedelta(days=2))

    def test_min_interval_property(self):
        self.assertEqual(trivial_signal.min_interval, timedelta(days=1))

    def test_max_interval_property(self):
        self.assertEqual(trivial_signal.max_interval, timedelta(days=3))

    def test_mode_interval_property(self):
        self.assertEqual(mode_signal.mode_minute_interval, timedelta(minutes=2).total_seconds())

    def test_gaps(self):
        gaps = mode_signal.gaps()
        self.assertEqual(1, len(gaps))

        previous_reading = now + timedelta(minutes=5)
        start = now + timedelta(minutes=7)
        end = now + timedelta(minutes=10)
        expected_gap = Gap(previous_reading, start, end)
        self.assertEqual(expected_gap, gaps[0])

    def test_num_gaps(self):
        self.assertEqual(2, gap_signal.num_gaps)

    def test_total_gap_time(self):
        self.assertEqual(timedelta(minutes=7).total_seconds(), gap_signal.total_gap_time)

    def test_min_gap_time(self):
        self.assertEqual(timedelta(minutes=3).total_seconds(), gap_signal.min_gap_time)

    def test_max_gap_time(self):
        self.assertEqual(timedelta(minutes=4).total_seconds(), gap_signal.max_gap_time)

    def test_delays(self):
        delays = gap_signal.delays()
        self.assertEqual(1, len(delays))

        reading_date = now + timedelta(minutes=3)
        transmission_date = now + timedelta(minutes=7)
        expected_delay = Delay(reading_date, transmission_date)
        self.assertEqual(expected_delay, delays[0])

    def test_num_delays(self):
        self.assertEqual(1, gap_signal.num_delays)

    def test_total_delay_time(self):
        self.assertEqual(timedelta(minutes=4).total_seconds(), gap_signal.total_delay_time)

    def test_min_delay_time(self):
        self.assertEqual(timedelta(minutes=4).total_seconds(), gap_signal.min_delay_time)

    def test_max_delay_time(self):
        self.assertEqual(timedelta(minutes=4).total_seconds(), gap_signal.max_delay_time)

    def test_offline_periods(self):
        offline_periods = gap_signal.offline_periods()

        self.assertEqual(3, len(offline_periods))

        previous_transmission = now + timedelta(minutes=1)
        start = now + timedelta(minutes=3)
        end = now + timedelta(minutes=5)
        expected_offline_period1 = OfflinePeriod(previous_transmission, start, end)
        self.assertEqual(expected_offline_period1, offline_periods[0])

        previous_transmission = now + timedelta(minutes=5)
        start = now + timedelta(minutes=7)
        end = now + timedelta(minutes=10)
        expected_offline_period1 = OfflinePeriod(previous_transmission, start, end)
        self.assertEqual(expected_offline_period1, offline_periods[1])

        previous_transmission = now + timedelta(minutes=14)
        start = now + timedelta(minutes=16)
        end = now + timedelta(minutes=20)
        expected_offline_period2 = OfflinePeriod(previous_transmission, start, end)
        self.assertEqual(expected_offline_period2, offline_periods[2])

    def test_num_offline_periods(self):
        self.assertEqual(3, gap_signal.num_offline_periods)

    def test_total_offline_period_time(self):
        self.assertEqual(timedelta(minutes=9).total_seconds(), gap_signal.total_offline_period_time)

    def test_min_offline_period_time(self):
        self.assertEqual(timedelta(minutes=2).total_seconds(), gap_signal.min_offline_period_time)

    def test_max_offline_period_time(self):
        self.assertEqual(timedelta(minutes=4).total_seconds(), gap_signal.max_offline_period_time)

    def test_coverage(self):
        expected_coverage = 7. / 10.
        self.assertEqual(expected_coverage, mode_signal.coverage)

    def test_on_time_of_delivered(self):
        expected_on_time_of_delivered = 10. / 10.
        self.assertEqual(expected_on_time_of_delivered, mode_signal.on_time_of_delivered)

        expected_on_time_of_delivered = 8. / 9.
        self.assertEqual(expected_on_time_of_delivered, gap_signal.on_time_of_delivered)

    def test_on_time(self):
        # expected_on_time = self.coverage * self.on_time_of_delivered = 0.7 * 1.
        expected_on_time = 7. / 10.
        self.assertEqual(expected_on_time, mode_signal.on_time)

        expected_on_time = 8. / 9. * 15. / 22.
        self.assertAlmostEqual(expected_on_time, gap_signal.on_time)

    def test_mean_property(self):
        self.assertEqual(trivial_signal.mean, 1.)

    def test_std_property(self):
        self.assertEqual(trivial_signal.std, math.sqrt(2. / 3.))

    def test_var_property(self):
        self.assertEqual(trivial_signal.var, 2. / 3.)

    def test_min_property(self):
        self.assertEqual(trivial_signal.min, 0.)

    def test_max_property(self):
        self.assertEqual(trivial_signal.max, 2.)

    def test_mid_property(self):
        self.assertEqual(trivial_signal.mid, 1.)

    def test_quantile(self):
        self.assertAlmostEqual(trivial_signal.quantile(0.1), 0.)
        self.assertAlmostEqual(trivial_signal.quantile(0.5), 1.)
        self.assertAlmostEqual(trivial_signal.quantile(0.9), 2.)
        self.assertAlmostEqual(trivial_signal.quantile(0.9, interpolation='default_to_nearest'), 2.)

    def test_min_time_property(self):
        self.assertEqual(trivial_signal.min_time, min_date)

    def test_max_time_property(self):
        self.assertEqual(trivial_signal.max_time, max_date)

    def test_min_reading_time_property(self):
        self.assertEqual(trivial_signal.min_reading_time, min_date)

    def test_max_reading_time_property(self):
        self.assertEqual(trivial_signal.max_reading_time, max_date)

    def test_time_span_property(self):
        self.assertEqual(trivial_signal.time_span, timedelta(days=4))

    def test_before_time(self):
        before_signal = trivial_signal.before(max_date)
        self.assertEqual(before_signal.count, 2)
        self.assertEqual(before_signal.min_time, min_date)

    def test_after_time(self):
        after_signal = trivial_signal.after(min_date + pd.Timedelta(minutes=1))
        self.assertEqual(after_signal.count, 2)
        self.assertEqual(after_signal.max_time, max_date)

    def test_between_time(self):
        between_signal = trivial_signal.between(min_date + pd.Timedelta(minutes=1), max_date)
        self.assertEqual(between_signal.count, 1)

    def test_not_between_time(self):
        between_signal = trivial_signal.not_between(min_date + timedelta(hours=6), max_date - timedelta(hours=6))
        self.assertEqual(between_signal.count, 2)
        self.assertEqual(between_signal.min_time, trivial_signal.min_time)
        self.assertEqual(between_signal.max_time, trivial_signal.max_time)

        gap_not_between_signal = gap_signal.not_between(gap_signal.min_time, gap_signal.max_time, inclusive=True)
        self.assertEqual(gap_not_between_signal.count, 2)
        self.assertEqual(gap_not_between_signal.min_time, gap_signal.min_time)
        self.assertEqual(gap_not_between_signal.max_time, gap_signal.max_time)

        gap_not_between_signal_empty = gap_signal.not_between(gap_signal.min_time, gap_signal.max_time, inclusive=False)
        self.assertEqual(gap_not_between_signal_empty.count, 0)

    def test_above_value(self):
        above_signal = trivial_signal.above(0.1)
        self.assertEqual(above_signal.count, 2)
        self.assertEqual(above_signal.max, trivial_signal.max)

        above_gap_signal = gap_signal.above(0.1)
        self.assertEqual(above_gap_signal.count, 7)
        self.assertEqual(above_gap_signal.max, gap_signal.max)

    def test_below_value(self):
        below_signal = trivial_signal.below(2.)
        self.assertEqual(below_signal.count, 2)
        self.assertEqual(below_signal.min, trivial_signal.min)

        below_gap_signal = gap_signal.below(2.)
        self.assertEqual(below_gap_signal.count, 4)
        self.assertEqual(below_gap_signal.min, gap_signal.min)

    def test_within_values(self):
        within_signal = trivial_signal.within(0., 2.)
        self.assertEqual(within_signal.count, 2)

        within_gap_signal = gap_signal.within(0., 2.)
        self.assertEqual(within_gap_signal.count, 4)

    def test_not_within_values(self):
        within_signal = trivial_signal.not_within(0.5, 1.5)
        self.assertEqual(within_signal.count, 2)
        self.assertEqual(within_signal.min, trivial_signal.min)
        self.assertEqual(within_signal.max, trivial_signal.max)

        within_gap_signal = gap_signal.not_within(0.5, 1.5)
        self.assertEqual(within_gap_signal.count, 7)

    def test_get_item(self):
        sub_signal = trivial_signal[1:]
        self.assertEqual(sub_signal.count, 2)
        sub_signal = trivial_signal[:1]
        self.assertEqual(sub_signal.count, 1)
        sub_signal = trivial_signal[0:]
        self.assertEqual(sub_signal, trivial_signal)

    def test_celsius_to_fahrenheit(self):
        converted_fahrenheit = celsius_signal.celsius_to_fahrenheit()
        self.assertEqual(converted_fahrenheit, fahrenheit_signal)

    def test_fahrenheit_to_celsius(self):
        converted_celsius = fahrenheit_signal.fahrenheit_to_celsius()
        self.assertEqual(converted_celsius, celsius_signal)

    def test_null_signal_with_overrides_gaps_and_offline_periods(self):
        self.assertEqual(1, len(null_signal_with_overrides.gaps()))
        self.assertEqual(1, len(null_signal_with_overrides.offline_periods()))