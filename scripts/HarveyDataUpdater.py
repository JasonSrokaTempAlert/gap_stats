####  Controls
FLAG_pull_temp_data = True
FLAG_pull_power_data = True
AnalysisWindow_StartDate_str = '2018-01-03 00:00:00'


####  Environment setup
import os
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import psycopg2
import json
import re

AnalysisWindow_StartDate = pd.to_datetime(AnalysisWindow_StartDate_str)

# ####  These worked
# # database.init('test_ta_utils',)
# # database.init('Local_DataWarehouse',)
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)
cursor = DATABASE_CONN.cursor()

def FormatDatetime(RawDatetime):
    if isinstance(RawDatetime,basestring):
        result = RawDatetime.replace('T', ' ')
        ReturnDatetime = result.split('.')[0]
        #     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
        return ReturnDatetime
    return RawDatetime

##TODO:  add unit tests for FormatDatetime function



##TODO:  How do we create a generic config file starting point to make the code portable across local/EC2 setups?
if os.path.isdir('/Users/Jason/'):
    Environment = 'local'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
else:
    Environment = 'ubuntu'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/prod-trundle-analyticsconfig'
thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'universal.json'))

####  Device Metadata - used for filtering to a good set of Device IDs
# DevicesInfoFile=os.path.join(data_dir,'vu_DeviceAndStoreInfo.csv')
# print "Reading devices info from file: {file}".format(file = DevicesInfoFile)
# DevicesInfo = pd.read_csv(DevicesInfoFile)
print "Reading devices info from vu_deviceandstoreinfo"
DevicesInfo = pd.read_sql("select * from vu_deviceandstoreinfo  ", DATABASE_CONN)
DevicesInfo['deviceinstalldate']=DevicesInfo['deviceinstalldate'].apply(FormatDatetime)
print "done with devices info select, raw number of devices: {count}".format(count=len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(
    DevicesInfo[DevicesInfo['datedeleted'] >= AnalysisWindow_StartDate])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(
    start=AnalysisWindow_StartDate, count=len(DevicesInfo))

##  Set up account ids
enterprise_acct_ids = [4226]
DevicesInfo = DevicesInfo[DevicesInfo['accountid'].isin(enterprise_acct_ids)]

print "after filtering for enterprise_acct_ids, {rows} devices left".format(rows = len(DevicesInfo))

DevicesInfo = DevicesInfo[DevicesInfo['state'].isin(['CT', 'DE', 'FL', 'GA', 'MA', 'MD', 'ME', 'NC', 'NH', 'NJ', 'NY', 'RI', 'SC', 'VA'])]
# Region31_StoreNums = [2063, 2345, 2346, 2385, 2469, 2503, 2587, 2588, 2678, 2932, 3028, 3032, 3035, 3041, 3083, 3088, 3090, 3092, 3168, 3169,
#  3183, 3301, 3306, 3312, 3314, 3317, 3320, 3321, 3322, 3326, 3329, 3332, 3333, 3334, 3337, 3338, 3339, 3340, 3343, 3346,
#  3347, 3353, 3355, 3356, 3358, 3359, 3360, 3362, 3369, 3376, 3377, 3393, 3406, 3432, 3471, 3634, 3645, 3697, 3817, 3980,
#  3997, 4034, 4054, 4057, 4078, 4082, 4101, 4208, 4216, 4282, 4300, 4301, 4304, 4305, 4309, 4312, 4316, 4320, 4327, 4330,
#  4331, 4333, 4336, 4338, 4345, 4347, 4348, 4349, 4350, 4351, 4353, 4359, 4360, 4366, 4393, 4402, 4437, 4445, 4447, 4488,
#  4499, 4572, 4605, 4607, 4800, 4802, 4803, 4804, 4805, 4807, 4811, 5088, 5095, 5098, 5366, 5666, 5813, 5941, 6167, 6169,
#  6171, 6172, 6173, 6175, 6176, 6177, 6180, 6181, 7370, 7371, 7684, 7686, 7687, 7997, 8248, 8932, 10209, 10246, 10248, 10372,
#  10457, 10961, 11049, 11050]
# Region31_StoreNums_Strs = ['%05.f' % x for x in Region31_StoreNums]
# DevicesInfo = DevicesInfo[DevicesInfo['store_number'].isin( Region31_StoreNums_Strs )]

print "after filtering for relevant states, {rows} devices left".format(rows = len(DevicesInfo))

print "DevicesInfo.head(): {}".format(DevicesInfo.head())

Monitored_DeviceIDs = DevicesInfo['deviceid']
NumMonitoredDevices = len(Monitored_DeviceIDs)

numrows_per_insert = 100
StartTime = datetime.now()
print 'StartTime: {}'.format(StartTime)

RunTimestamp = datetime.now().date()
NumMonitoredDevices = len(Monitored_DeviceIDs)

run_date = datetime.utcnow()

for DeviceIter in range(0, NumMonitoredDevices):

    thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
    print"Harvey Data Pull:  Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(
        DeviceID=thisDeviceID,
        DeviceIter=DeviceIter,
        TotalDevices=NumMonitoredDevices)

    #     print "DevicesInfo[DevicesInfo['deviceid']==thisDeviceID]['hardwarefamily']: {}".format(DevicesInfo[DevicesInfo['deviceid']==thisDeviceID]['hardwarefamily'])
    if DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID]['hardwarefamily'].values[0] == 50:
        print "processing for temperature"

        thisDeviceDataDF = pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
        if (len(thisDeviceDataDF) == 0):
            print'No data found for device, skipping to next'
            continue
        if ('sensors' in thisDeviceDataDF.columns):
            print'No sensors for this device, skipping to next'
            continue
        thisNumSensors = len(thisDeviceDataDF.port)
        for SensorIndex in range(0, thisNumSensors):
            thisPortID = thisDeviceDataDF.port[SensorIndex]
            print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID, index=SensorIndex,
                                                                              total=thisNumSensors)

            sensor_temp_query = "select max(processed_date) MaxProcDate from harvey_temperature_history where device_id = '{DevID}' and port = '{port}'".format(
                DevID=thisDeviceID, port=thisPortID)
            sensor_temp_data = pd.read_sql(sensor_temp_query, DATABASE_CONN)
            max_processed_date = sensor_temp_data['maxprocdate'].values[0]
            print "max_processed_date: {}".format(max_processed_date)
            if max_processed_date is not None:
                print "Temperature Data Pull:  Found prior data, using {time} as MaxLookbackTime".format(
                    time=max_processed_date)
                max_proc_str = str(max_processed_date)
                print "max_proc_str: {}".format(max_proc_str)
                max_proc_dt = datetime.strptime(max_proc_str.split('.', 1)[0], '%Y-%m-%dT%H:%M:%S')
                thisSensorDataDF = pd.DataFrame(
                    thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                              reading_start_dt=AnalysisWindow_StartDate,
                                                              processed_after_dt=max_proc_dt,
                                                              reading_end_dt= run_date))
            else:
                thisSensorDataDF = pd.DataFrame(
                    thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                              reading_start_dt=AnalysisWindow_StartDate,
                                                              reading_end_dt = run_date))

            if ('processedDate' not in thisSensorDataDF.columns):
                print "no processed date in data returned, skipping"
                continue

            thisSensorDataDF['processed_date'] = thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
            thisSensorDataDF['reading_date'] = thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))

            thisSensorDataDF['age'] = (
                pd.to_datetime(thisSensorDataDF['processedDate']) - pd.to_datetime(
                    thisSensorDataDF['readingDate'])).astype(
                'timedelta64[s]')
            thisSensorDataDF['portid'] = thisPortID
            thisSensorDataDF['deviceid'] = thisDeviceID

            print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisSensorDataDF))

            num_data_rows = len(thisSensorDataDF)
            while (num_data_rows > 0):
                thisupload_rowcount = min(num_data_rows, numrows_per_insert)
                thisuploaddata = thisSensorDataDF[0:thisupload_rowcount]
                thisSensorDataDF = thisSensorDataDF[thisupload_rowcount:]
                num_data_rows = len(thisSensorDataDF)

                ##  Build the insert statement
                insert_statement = 'insert into harvey_temperature_history(device_id,reading_date,processed_date,temperature,age,port) values'
                for iter in range(0, (thisupload_rowcount - 1)):
                    #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                    insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port}),".format(
                        DevID=str(thisDeviceID), ReadingDate=thisuploaddata['readingDate'].values[iter],
                        ProcessedDate=thisuploaddata['processedDate'].values[iter],
                        Temp=thisuploaddata['readingValue'].values[iter],
                        Age=thisuploaddata['age'].values[iter],
                        Port=thisuploaddata['portid'].values[iter])
                insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port})".format(
                    DevID=str(thisDeviceID),
                    ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                    ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                    Temp=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                    Age=thisuploaddata['age'].values[thisupload_rowcount - 1],
                    Port=thisuploaddata['portid'].values[thisupload_rowcount - 1])
                insert_statement = insert_statement + " on conflict do nothing"
                #                 print 'insert_statement:'
                #                 print insert_statement
                #                 pd.read_sql(insert_statement, DATABASE_CONN)
                ret = cursor.execute(insert_statement)
                ret = DATABASE_CONN.commit()

                print "after commit, {} rows remaining".format(num_data_rows)

    else:
        print "processing as gateway, looking for power data"

        gw_power_query = "select max(processed_date) MaxProcDate from harvey_power_history where device_id = '{DevID}'".format(
            DevID=thisDeviceID)
        gw_power_data = pd.read_sql(gw_power_query, DATABASE_CONN)
        max_processed_date = gw_power_data['maxprocdate'].values[0]
        print "max_processed_date: {}".format(max_processed_date)

        if max_processed_date is not None:
            print "Power Data Pull:  Found prior data, using {time} as MaxLookbackTime".format(time=max_processed_date)
            max_proc_str = str(max_processed_date)
            print "max_proc_str: {}".format(max_proc_str)
            max_proc_dt = datetime.strptime(max_proc_str.split('.', 1)[0], '%Y-%m-%dT%H:%M:%S')

            this_gw_data = pd.DataFrame(
                thisClient.get_device_readings_by_device_id(thisDeviceID, reading_start_dt=AnalysisWindow_StartDate,
                                                            processed_after_dt=max_proc_dt, command_ids=['80'],
                                                              reading_end_dt= run_date))
        else:
            this_gw_data = pd.DataFrame(thisClient.get_device_readings_by_device_id(thisDeviceID,
                                                                                    reading_start_dt=AnalysisWindow_StartDate,
                                                                                    command_ids=['80'],
                                                              reading_end_dt= run_date))

        if ('processedDate' not in this_gw_data.columns):
            print "no processed date in data returned, skipping"
            continue

        # print "this_gw_data: \n{}".format(this_gw_data)
        this_gw_data['processed_date'] = this_gw_data['processedDate'].apply(lambda x: FormatDatetime(x))
        this_gw_data['reading_date'] = this_gw_data['readingDate'].apply(lambda x: FormatDatetime(x))

        this_gw_data['age'] = (
            pd.to_datetime(this_gw_data['processedDate']) - pd.to_datetime(this_gw_data['readingDate'])).astype(
            'timedelta64[s]')
        this_gw_data['deviceid'] = thisDeviceID

        print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(this_gw_data))

        num_data_rows = len(this_gw_data)
        while (num_data_rows > 0):
            thisupload_rowcount = min(num_data_rows, numrows_per_insert)
            thisuploaddata = this_gw_data[0:thisupload_rowcount]
            this_gw_data = this_gw_data[thisupload_rowcount:]
            num_data_rows = len(this_gw_data)

            ##  Build the insert statement
            insert_statement = 'insert into harvey_power_history(device_id,reading_date,processed_date,power_score) values'
            for iter in range(0, (thisupload_rowcount - 1)):
                #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcessedDate}',{Temp}),".format(
                    DevID=str(thisDeviceID), ReadingDate=thisuploaddata['readingDate'].values[iter],
                    ProcessedDate=thisuploaddata['processedDate'].values[iter],
                    Temp=thisuploaddata['readingValue'].values[iter],
                    Age=thisuploaddata['age'].values[iter])
            insert_statement = insert_statement + "('{DevID}','{ReadingDate}','{ProcessedDate}',{Temp})".format(
                DevID=str(thisDeviceID), ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                Temp=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                Age=thisuploaddata['age'].values[thisupload_rowcount - 1])
            insert_statement = insert_statement + " on conflict do nothing"
            #                 print 'insert_statement:'
            #                 print insert_statement
            #                 pd.read_sql(insert_statement, DATABASE_CONN)
            ret = cursor.execute(insert_statement)
            ret = DATABASE_CONN.commit()

            print "after commit, {} rows remaining".format(num_data_rows)

EndTime = datetime.now()

TimeDuration = EndTime - StartTime
print "Data Pull Time: {TD}".format(TD=TimeDuration)



cursor.close()
DATABASE_CONN.close()

