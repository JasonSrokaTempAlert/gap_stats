#Testt
import os
from datetime import datetime,timedelta
import time

import numpy as np
import pandas as pd
import psycopg2

from trundle.trundle_client import TrundleClient

from ta_utils import ManyDeviceSignals
from ta_utils import database
from ta_utils import create_tables, drop_tables

FLAG_SAVE_SIGNALS = True

REPORTING_INTERVAL_MINUTES = 60 * 15

RESET = True
#ENVIRONMENT = 'local'
#ENVIRONMENT = 'ubuntu'
#ENVIRONMENT = 'local'
# ENVIRONMENT = 'ubuntu'

# DATATAG = 'MC_and_NodeNanny'
#DATATAGS = ['GroupTest']
#DATATAGS= ['DeviceTest']
# DATATAGS = ['NewEngland']
#DATATAGS = ['Racetrac']
# DATATAG = 'AllCVS'
#DATATAGS = ['TacoBell','Racetrac']
#DATATAGS = ['Racetrac','TacoBell','GoLiveFS']
#DATATAGS = ['Racetrac','GoLive']
#DATATAGS = ['GoLive']
#DATATAGS = ['Google']
DATATAGS = ['GoLiveFS']
#DATATAGS = ['VA']

#ONLY CHANGED TO STATE TAGS FOR GO LIVE
DEVICE_ID_COL = 'device_id'
DATE_COL = 'reading_date'
READING_COL_NODES = 'temperature'
READING_COL_GATEWAYS = 'channel'
PROCESSED_COL = 'processed_date'
PORT_COL = 'port'

#Da5
ANALYSIS_WINDOW_START_DATE = pd.to_datetime('2017-05-01 00:00:00')

#conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='gapstats_sandbox' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)
warehouse_conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
WAREHOUSE_DATABASE_CONN = psycopg2.connect(warehouse_conn_string)

# TODO:  How do we create a generic config file starting point to make the code portable across local/EC2 setups?
# if ENVIRONMENT == 'local':
#     DATA_DIR = '/Users/jheel/gap_stats/Data'
#     # ACCESS_CREDS_DIR = '/Users/jheel/gap_stats/prod-trundle-analyticsconfig'
#     ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig'
# 	#WAREHOUSE_DATABASE_CONN = psycopg2.connect(conn_string)
#
# else:
#     DATA_DIR = '/home/jason/Data'
#     ACCESS_CREDS_DIR = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
#     WAREHOUSE_DATABASE_CONN = psycopg2.connect(warehouse_conn_string)
# 	#WAREHOUSE_DATABASE_CONN = psycopg2.connect(conn_string)

# Environment
if os.path.isdir('/Users/Jason/'):
    Environment = 'jason_local'
    DATA_DIR = '/Users/Jason/Desktop/Local_Data_Store'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
elif os.path.isdir('/Users/jheel/'):
    Environment = 'jheel_local'
    DATA_DIR = '/Users/jheel/gap_stats/Data'
    ACCESS_CREDS_DIR = '/Users/jheel/gap_stats/prod-trundle-analyticsconfig'
elif os.path.isdir('/home/jason/'):
    Environment = 'ubuntu'
    DATA_DIR = '/home/jason/Data'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
else:
    error("Don't recognize environment")

print "Environment: {env}  \n DATA_DIR: {dd} \n ACCESS_CRED_DIR: {ad} \n conn_string: {cs}".format(
    env = Environment, dd = DATA_DIR, ad = ACCESS_CREDS_DIR, cs = conn_string
)
    
##  this controls the database used for storing GapStats results
if 'gapstats_sandbox' in conn_string :
    database.init('gapstats_sandbox', user='jason', password='qvz2LFRzMMVPku7',
                   host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
                   port='5432')
    database_tag = 'gapstats_sandbox'
else:
    database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7',
              host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
              port='5432')
    database_tag = 'warehouse'

SECONDS_DELAY_BEFORE_TABLE_DROP = 15 if database_tag == 'warehouse' else 5
if RESET:
    print "\n\nWARNING!\nWARNING!\nprepared to drop tables in {tag}- you have {secs} seconds to quite before it happens...".format(
        tag = database_tag, secs = SECONDS_DELAY_BEFORE_TABLE_DROP)
    time.sleep(SECONDS_DELAY_BEFORE_TABLE_DROP)
    print "time's up, dropping tables"
    drop_tables()
    print 'dropped tables'
    create_tables()
    print 'created tables'




# TODO:  add unit tests for FormatDatetime function
def format_datetime(raw_datetime):
    # print "format_datetime()::  called with raw_datetime: {}".format(raw_datetime)
    result = raw_datetime.replace('T', ' ')
    return_datetime = result.split('.')[0]
    # print "format_datetime()::  generated result: {}".format(return_datetime)
    return return_datetime



print "Reading devices info from vu_deviceandstoreinfo"
DevicesInfo = pd.read_sql("select * from vu_deviceandstoreinfo where deviceid not in ('11666000000127914368','11666000000122678103') and groupname not like '%RMA - Replace or Repair%' and groupname like 'Front%'",WAREHOUSE_DATABASE_CONN)
print "done with devices info  read, raw number of devices: {count}".format(count = len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(DevicesInfo[DevicesInfo['datedeleted']>=ANALYSIS_WINDOW_START_DATE])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(start = ANALYSIS_WINDOW_START_DATE, count = len(DevicesInfo))

print "Reading sensors info from sensor table"
sensor_info = pd.read_sql("select sensorid, deviceid, portid from sensor where hidden = false and datedeleted is NULL", WAREHOUSE_DATABASE_CONN)
print "done with sensor read, number of non-hidden sensors = {numsens}".format(numsens = len(sensor_info))

def device_mintime_override_lookup(deviceid, reading_col, port):
    print "device_mintime_override_lookup():: called with deviceid = '{devid}' and port: {port}".format(devid = deviceid, port = port)

    install_date_query = "select deviceinstalldate from vu_deviceandstoreinfo where deviceid = '{devid}'  ".format(devid = deviceid)

    install_results = pd.read_sql(install_date_query, WAREHOUSE_DATABASE_CONN)
    print install_results, 'x'
    if len(install_results) > 1 :
    	if pd.notnull(install_results.values[0]) :
    		port_string_1 = '' if port is None else "and portid = {}".format(port)
    		print port_string_1
    		if port_string_1 == '' :
    			print("going in gateway loop")
    			install_date_query_null = "select MIN(readingdate) as deviceinstalldate from gateway_power where deviceid = '{devid}' ".format(devid = deviceid)
    		else :
    			print("going in node loop")
        		install_date_query_null = "select first_reading_time as deviceinstalldate from first_reading where deviceid = '{devid}' {port}".format(devid = deviceid, port = port_string_1)
    		install_results_null = pd.read_sql(install_date_query_null, WAREHOUSE_DATABASE_CONN)
    		print install_results_null
    		if pd.isnull(install_results_null.values[0]) :
        		install_results_null.values[0]  = ANALYSIS_WINDOW_START_DATE
    		if install_results.values[0] != ANALYSIS_WINDOW_START_DATE :
				install_results = min(install_results.values[0],install_results_null.values[0])
				print "IF"
    		else :
	    		install_results = install_results_null.values[0]
	    		print "ELSE"
    		install_date_dt64 = install_results
    		print "YEs works"
    		print install_results
    		print type(install_results[0])
    		print install_date_dt64
    		print type(install_date_dt64[0])
    		install_date_str = format_datetime(str(install_date_dt64[0]))
    		print install_date_str
    		install_date = datetime.strptime(install_date_str,'%Y-%m-%d %H:%M:%S')
    # print "device_mintime_override_lookup install_date: {}".format(install_date)
    		print "Stupid"
    		return max(ANALYSIS_WINDOW_START_DATE, install_date)
        else :
			return ANALYSIS_WINDOW_START_DATE
    else :
    	return ANALYSIS_WINDOW_START_DATE

def device_maxtime_override_lookup(deviceid, reading_col, port):
    print "device_maxtime_override_lookup():: called with deviceid = '{devid}' and port: {port}".format(devid = deviceid, port = port)

    port_string = '' if port is None else "and portid = {}".format(port)
    ##  order of priority:  datedeleted (from Monitored_DeviceIDs), channel, temperature
    trundle_query = """select * from trundle_access_history where deviceid = '{devid}' {port_string} and data_type = '{type}'""".format(devid = deviceid,
                                                                                                                                        type = reading_col,
                                                                                                                                        port_string = port_string)
    print "trundle_query: {}".format(trundle_query)
    trundle_results = pd.read_sql(trundle_query, WAREHOUSE_DATABASE_CONN)
    print "trundle_results: {}".format(trundle_results)
    if len(trundle_results)>1:
        print "multiple results from trundle_access_history table - which should be used? - using min for now"
        trundle_max_time = datetime.strptime(format_datetime(str(min(trundle_results['update_time'].values))),'%Y-%m-%d %H:%M:%S')
        print "len > 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
    elif len(trundle_results)==1:
        trundle_max_time = datetime.strptime(format_datetime(str(trundle_results['update_time'].values[0])),'%Y-%m-%d %H:%M:%S')
        print "len = 1 result, setting trundle_max_time to {maxtime}".format(maxtime = trundle_max_time)
    else:
        print "no trundle access in history, using signals date"
        signals_query = "select * from signals where device_id = '{devid}' and data_type = '{type}';".format(devid = device_id, type = reading_col)
        print 'submitting query: {query}'.format(query = signals_query)
        signals_entry = pd.read_sql(signals_query, DATABASE_CONN)
        print "signals query return: \n{}".format(signals_entry)
        if len(signals_entry) == 1:
            max_time = signals_entry['max_time_override'].values[0] if signals_entry['max_time_override'].values[0] is not None else signals_entry['max_processed_time'].values[0]
            print "signals entry max_time: {}".format(max_time)
            if max_time is None:
                trundle_max_time = None
            else:
                trundle_max_time = datetime.strptime(format_datetime(str(max_time)),'%Y-%m-%d %H:%M:%S')
            print "trundle_max_time: {}".format(trundle_max_time)
        else:
            trundle_max_time = None

    datedeleted = Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid']==deviceid].datedeleted.values[0]
    print "datedeleted: {}".format(datedeleted)
    if ( (datedeleted is not None) & ~(pd.isnull(datedeleted))):
        datedeleted = datetime.strptime(format_datetime(str(datedeleted)),'%Y-%m-%d %H:%M:%S')
        if trundle_max_time is None:
            max_time = datedeleted
        else:
            max_time = min(trundle_max_time, datedeleted)
    else:
        max_time = trundle_max_time - timedelta(days=14)
    print "device_maxtime_override_lookup()::  max_time: {}".format(max_time)
    return max_time

# max_time = device_maxtime_override_lookup('11666000000122784366','noop')
# print "max_time: {}".format(max_time)

###
###
###  Set up which data will be monitored
###
###


print "setting up devicesinfo with DATATAGS: {tag}".format(tag=DATATAGS)


def TranslateGroupIDsToDeviceInfo(GroupIDs):
    ##  First get the devices that are in the store's group
    print DevicesInfo.head()
    DeviceIndices = np.where(DevicesInfo['groupid'].isin(GroupIDs))[0]
    DeviceInfo = DevicesInfo.iloc[DeviceIndices]
    #     DeviceInfo['StoreGroupID'] = DeviceInfo['GroupID']
    ##  Next get the devices that are in the children of a store's group
    DeviceIndices_ChildrenGroups = np.where(DevicesInfo['parentgroupid'].isin(GroupIDs))[0]
    DeviceInfo_ChildrenGroups = DevicesInfo.iloc[DeviceIndices_ChildrenGroups]
    #     DeviceInfo_ChildrenGroups['StoreGroupID'] = DeviceInfo_ChildrenGroups['ParentGroupID']
    ##  Put them together
    DeviceInfo = DeviceInfo.append(DeviceInfo_ChildrenGroups)
    return DeviceInfo


def AugmentDevicesInfoWithStoreGroupID(DevicesInfo):
    num_devices = len(DevicesInfo)
    print "AugmentDevicesInfoWithStoreGroupID():: called with {numdevs} devices".format(numdevs=num_devices)
    AugDevicesInfo = DevicesInfo
    AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid'].where(AugDevicesInfo['accountid'] != 4226 | AugDevicesInfo['groupname'].str.contains('Store #'), AugDevicesInfo['parentgroupid'])

    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    # print "beginning storegroupid lookup at time {now}".format(now=datetime.now())
    # for row_index in range(0, num_devices):
    #     this_parent_group_id = AugDevicesInfo['parentgroupid'].values[row_index]
    #     num_devices_in_parent_group = len(DevicesInfo[DevicesInfo['groupid'] == this_parent_group_id])
    #     #         print "processing row {row}, with parent groupid {gid}, found {numdev} devices in parent".format(row = row_index, gid = this_parent_group_id, numdev = num_devices_in_parent_group)
    #     if num_devices_in_parent_group > 0:
    #         #             print "found parent group with devices, resetting store group id"
    #         AugDevicesInfo['storegroupid'].iloc[row_index] = this_parent_group_id
    # print "completed storegroupid lookup at time {now}".format(now=datetime.now())
    return AugDevicesInfo


for this_data_tag in DATATAGS:
    
    ##  For all MC stores
    if this_data_tag =='MC_and_NodeNanny':
        filtering_devices_info = DevicesInfo
        print "all devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
        filtering_devices_info = filtering_devices_info[filtering_devices_info['AccountID'] == 4226]
        print "cvs devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
        excluded_groups = [18861, 11886, 12545, 8878, 12088, 8949, 9486, 17460, 11564]
        filtering_devices_info = filtering_devices_info[
            ~filtering_devices_info['GroupID'].isin(excluded_groups) ^
            ~filtering_devices_info['ParentGroupID'].isin(excluded_groups) ^
            ~filtering_devices_info['ParentParentGroupID'].isin(excluded_groups)
            ]
        print "number of CVS devices in non-excluded groups: {}".format(len(filtering_devices_info))
    
        filtering_devices_info = filtering_devices_info[filtering_devices_info['DeviceLabel'].str.contains('MC') == True]
        print "MC devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
        MC_group_ids = filtering_devices_info['GroupID'].unique()
        print "MC Group IDs length: {}".format(len(MC_group_ids))
        MC_parent_group_ids = filtering_devices_info['ParentGroupID'].unique()
        print "MC_parent_group_ids length: {}".format(len(MC_parent_group_ids))
        print type(MC_parent_group_ids)
        gen2_group_ids = np.append(MC_group_ids, MC_parent_group_ids)
        print "gen2_group_ids length: {}".format(len(gen2_group_ids))
        gen2_group_ids = np.unique(gen2_group_ids)
        print "gen2_group_ids length: {}".format(len(gen2_group_ids))
    
        mc_store_devices_info_raw = TranslateGroupIDsToDeviceInfo(gen2_group_ids)
        mc_store_devices_info = AugmentDevicesInfoWithStoreGroupID(mc_store_devices_info_raw)
        print "{numdev} devices in {numgroups} groups".format(numdev=len(mc_store_devices_info['DeviceID'].unique()),
                                                              numgroups=len(mc_store_devices_info['GroupID'].unique()))
        # conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
        ## get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
        # print 'conn: {}'.format(conn)
        NodeNannyOps_ResetHistory = pd.read_sql(
            "select * from public.gatewayreset where accountid=4226 and successful='true'", conn)
        # print 'thisDevice_ChannelHistory: {}'.format(thisDevice_ChannelHistory)
        NodeNannyOpsGroupIDs = NodeNannyOps_ResetHistory['groupid'].unique()
        print 'NodeNannyOpsGroupIDs: {}'.format(NodeNannyOpsGroupIDs)
        NodeNannyOps_DeviceInfo = TranslateGroupIDsToDeviceInfo(NodeNannyOpsGroupIDs)
    
        NodeNannyTestGroupIDs = [4016, 6321, 6972, 8047, 6994, 1276, 5812, 5695, 7627, 1991, 1562, 1445, 5997, 3034, 4814,
                                 7932, 7038]
        NodeNannyPilot_DeviceInfo = TranslateGroupIDsToDeviceInfo(NodeNannyTestGroupIDs)
    
        NodeNanny_and_MCStores_GroupIDs = np.unique(np.concatenate((NodeNannyOpsGroupIDs, gen2_group_ids)))
        NodeNanny_and_MCStores_DevicesInfo_Raw = TranslateGroupIDsToDeviceInfo(NodeNanny_and_MCStores_GroupIDs)
        NodeNanny_and_MCStores_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(NodeNanny_and_MCStores_DevicesInfo_Raw)
        Monitored_DevicesInfo = NodeNanny_and_MCStores_DevicesInfo
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='GroupTest':
        ###  Manual Test Group Selection
        TestGroupIDs = [34258,40425,40425,34290,30335,6427,24140,34290,24140,30335,40425,34290,24140,18807,34290,6427,34290,18807,34258,24172,30335,34290,1396,24172,34258,40425,40425,34290,18805,18805,40425,24172,24172,30335,34290,40425,1911,24140,34258,24140,24172,1911,40425,34290,34258,30335,24140,24140,1911,6427,642]
        Test_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(TranslateGroupIDsToDeviceInfo(TestGroupIDs))
        Monitored_DevicesInfo = Test_DevicesInfo
        print "Monitored_DevicesInfo: {}".format(Monitored_DevicesInfo)
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='DeviceTest':
        ###  Manual Test Device Selection
        test_device_ids = ['11666000000133550980'] #,'11666000000133542860']  #  wacky coverage-less-than-online case
#         test_device_ids = ['11666000000127608049']  #  device with negative online
#         test_device_ids = ['11666000000133550980','11666000000120806608','11666000000127990378','11666000000133542860','11666000000133549786']  #  device with negative online
        Test_DevicesInfo = DevicesInfo[DevicesInfo['deviceid'].isin(test_device_ids)]
        print Test_DevicesInfo
        Test_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(Test_DevicesInfo)
        print Test_DevicesInfo
        Monitored_DevicesInfo = Test_DevicesInfo
        print "Monitored_DevicesInfo: {}".format(Monitored_DevicesInfo)
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='NewEngland':
        # NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['StateOrProvince'].isin(['ME', 'MA', 'RI', 'CT', 'VT', 'NH'])]
        DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
        print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
        NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['state'] == 'MA') |
                                              (DevicesInfo['state'] == 'ME') |
                                              (DevicesInfo['state'] == 'RI') |
                                              (DevicesInfo['state'] == 'CT') |
                                              (DevicesInfo['state'] == 'VT') |
                                              (DevicesInfo['state'] == 'NH') &
                                             ~(DevicesInfo['groupid'].isin([8878, 18524])))]
        all_groups_list = NewEnglandDevicesInfo['groupid'].unique()
        NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list) ]
        print "NewEnglandDevicesInfo.columns: {}".format(NewEnglandDevicesInfo.columns)
        NewEnglandDevicesInfo = AugmentDevicesInfoWithStoreGroupID(NewEnglandDevicesInfo)
        print "after calling AugmentDevicesInfoWithStoreGroupID:"
        Monitored_DevicesInfo = NewEnglandDevicesInfo
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='GoLive':
        # NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['StateOrProvince'].isin(['ME', 'MA', 'RI', 'CT', 'VT', 'NH'])]
        DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
        print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
        NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['state'] == 'MS') |
        									 (DevicesInfo['state'] == 'VA') |
        									  (DevicesInfo['state'] == 'RI') |
                                              (DevicesInfo['state'] == 'DE') |
                                              (DevicesInfo['state'] == 'GA') |
                                              (DevicesInfo['state'] == 'LA') |
                                              (DevicesInfo['state'] == 'MD') |
                                              (DevicesInfo['state'] == 'AL') |
                                              (DevicesInfo['state'] == 'SC') |
                                              (DevicesInfo['state'] == 'TN') &
                                             ~(DevicesInfo['groupid'].isin([8878, 18524])))]
        all_groups_list = NewEnglandDevicesInfo['groupid'].unique()
        NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list) ]
        print "NewEnglandDevicesInfo.columns: {}".format(NewEnglandDevicesInfo.columns)
        NewEnglandDevicesInfo = AugmentDevicesInfoWithStoreGroupID(NewEnglandDevicesInfo)
        print "after calling AugmentDevicesInfoWithStoreGroupID:"
        Monitored_DevicesInfo = NewEnglandDevicesInfo
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='VA':
        # NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['StateOrProvince'].isin(['ME', 'MA', 'RI', 'CT', 'VT', 'NH'])]
        DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
        print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
        NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['state'] == 'VA')  &
                                             ~(DevicesInfo['groupid'].isin([8878, 18524])))]
        all_groups_list = NewEnglandDevicesInfo['groupid'].unique()
        NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list) ]
        print "NewEnglandDevicesInfo.columns: {}".format(NewEnglandDevicesInfo.columns)
        NewEnglandDevicesInfo = AugmentDevicesInfoWithStoreGroupID(NewEnglandDevicesInfo)
        print "after calling AugmentDevicesInfoWithStoreGroupID:"
        Monitored_DevicesInfo = NewEnglandDevicesInfo
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='AllCVS':
        DevicesInfo = DevicesInfo[ DevicesInfo['accountid'] == 4226]
        print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
        AllCVS_DevicesInfo = DevicesInfo[ ~(DevicesInfo['groupid'].isnull()) & ~(DevicesInfo['groupid'].isin([18861, 11886, 12545, 8878, 12088, 8949, 9486, 17460, 11564, 18524]))]
        all_groups_list = AllCVS_DevicesInfo['groupid'].unique()
        AllCVS_DevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list)]
        print "AllCVS_DevicesInfo.columns: {}".format(AllCVS_DevicesInfo.columns)
        AllCVS_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(AllCVS_DevicesInfo)
        print "after calling AugmentDevicesInfoWithStoreGroupID:"
        Monitored_DevicesInfo = AllCVS_DevicesInfo
        TRUNDLE_CLIENT = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if this_data_tag =='Racetrac':
        RacetracDevicesInfo = DevicesInfo[DevicesInfo['accountid']==7306]
        print "RacetracDevicesInfo: {}".format(RacetracDevicesInfo)
        RacetracDevicesInfo = AugmentDevicesInfoWithStoreGroupID(RacetracDevicesInfo)
        Monitored_DevicesInfo = RacetracDevicesInfo
    if this_data_tag =='TacoBell':
        TacoBellDevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 7115]
        TacoBellDevicesInfo = AugmentDevicesInfoWithStoreGroupID(TacoBellDevicesInfo)
        Monitored_DevicesInfo = TacoBellDevicesInfo
        thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'tacobell.json'))
    if this_data_tag =='Google':
        GoogleDevicesInfo = DevicesInfo[DevicesInfo['accountid']==7164]
        print "GoogleDevicesInfo: {}".format(GoogleDevicesInfo)
        GoogleDevicesInfo = AugmentDevicesInfoWithStoreGroupID(GoogleDevicesInfo)
        Monitored_DevicesInfo = GoogleDevicesInfo
        thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'google.json')) 
    if this_data_tag =='GoLiveFS':
        DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
        print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
#        NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['state'] == 'LA') & ~(DevicesInfo['groupid'].isin([8878, 18524])))]
        NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['state'] == 'RI') |
                                              (DevicesInfo['state'] == 'VA') &
#                                               (DevicesInfo['state'] == 'VA') &
                                         ~(DevicesInfo['groupid'].isin([8878, 18524])) 
#                                         & ~(DevicesInfo['deviceid'].isin(['11666000000127914368']))
		)]
        all_groups_list = NewEnglandDevicesInfo['groupid'].unique()
        NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list) ]
        print "NewEnglandDevicesInfo.columns: {}".format(NewEnglandDevicesInfo.columns)
        NewEnglandDevicesInfo = AugmentDevicesInfoWithStoreGroupID(NewEnglandDevicesInfo)
        print "after calling AugmentDevicesInfoWithStoreGroupID:"
        Monitored_DevicesInfo = NewEnglandDevicesInfo
        account_id = 4226
        #thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))
    if len(Monitored_DevicesInfo) == 0:
        exit('No devices found for tag {tag}'.format(tag = this_data_tag))
    
    
    
    
    Monitored_DeviceIDs = Monitored_DevicesInfo['deviceid']
    NumMonitoredDevices = len(Monitored_DeviceIDs)
    print "Monitored_DevicesInfo.head(): {}".format(Monitored_DevicesInfo.head())
    Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['devicetype']=='Gateway']
    
    print """Done with group selection.
             DATATAG: {tag}
             NumDevices: {devcnt}
             NumGateways: {grpcnt}
             """.format(tag=this_data_tag,
                       devcnt=NumMonitoredDevices,
                       grpcnt=len(Monitored_Gateway_DevicesInfo))
    
    
    # def lookup_maxtime(deviceid, datatype):
    #     trundle_access_query = """ select * from trundle_access_history where deviceid = {devid} and datatype = {type}""".format(devid = deviceid,
    #                                                                                                                              type = datatype)
    #     trundle_response = pd.read_sql(trundle_access_query)
    #     if len(trundle_response) == 0:
    #         return None
    #     else:
    #         return(trundle_response['access_date'].values[0])
    
    
    if FLAG_SAVE_SIGNALS is True:
        StartTime = datetime.now()
        print "Beginning combined analysis using Postgres Temperature at time {now}".format(now=StartTime)
    
        ###   Get the set of groups being monitored
    
        all_group_ids = Monitored_DevicesInfo['groupid'].unique()
        num_group_ids = len(all_group_ids)
        group_id_index = 1
        for group_id in all_group_ids:
            group_starttime = datetime.now()
            group_index_string = 'processing group: {id},  #{index} of {total}'.format(id = group_id,index = group_id_index, total = num_group_ids)
            print group_index_string
    
            ##
            this_group_devicesinfo = Monitored_DevicesInfo[Monitored_DevicesInfo['groupid'] == group_id]
            this_group_nodesinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='ZPoint Node']
    
            if np.isnan(group_id):
                print "special processing for 'nan' groupid"
                all_device_ids = this_group_devicesinfo[np.isnan(this_group_devicesinfo['groupid'])].deviceid
            else:
                all_device_ids = this_group_devicesinfo[this_group_devicesinfo['groupid'] == group_id]['deviceid'].unique()
    
            num_devices = len(all_device_ids)
    
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, port, max_processed_time, max_time_override from signals
            where data_type = 'temperature' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_temperature_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            # print "Found {numrows} existing signals".format(numrows = len(existing_temperature_signals))
            print "existing_temperature_signals: {}".format(existing_temperature_signals)
            if len(existing_temperature_signals)>0:
                existing_temperature_signals['max_time'] = ''
                for row_index in range(0,len(existing_temperature_signals)):
                    print "evaluating row {index} in existing_temperature_signals".format(index = row_index)
                    existing_temperature_signals['max_time'].iloc[row_index] = existing_temperature_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_temperature_signals['max_time_override'].iloc[row_index]) else existing_temperature_signals['max_time_override'].iloc[row_index]
    
                # existing_temperature_signals['max_time'] = existing_temperature_signals['max_processed_time'].where(
                #     existing_temperature_signals['max_time_override'].isnull(),existing_temperature_signals['max_time_override'])
                print "After Initial max_time set:    existing_temperature_signals: {}".format(existing_temperature_signals)
                # existing_temperature_signals['max_time'] = existing_temperature_signals['max_time_override'].where(
                #     pd.isnull(existing_temperature_signals['max_time']), existing_temperature_signals['max_time'])
                # for row_index in range(0,len(existing_temperature_signals)):
                #     existing_temperature_signals['max_time'].iloc[row_index] = existing_temperature_signals['max_time'].iloc[row_index] if pd.isnull(existing_temperature_signals['max_time'].iloc[row_index]) else existing_temperature_signals['max_processed_time'].iloc[row_index]
                # existing_temperature_signals['max_time'] = np.where(pd.isnull(existing_temperature_signals['max_time']),
                #                                                               existing_temperature_signals['max_processed_time'],
                #                                                               existing_temperature_signals['max_time'])
                # print "After max_processed_time fill:    existing_temperature_signals: {}".format(existing_temperature_signals)
            else:
                existing_temperature_signals=pd.DataFrame({'device_id' : None, 'max_time' : None}, index = range(0,0))
            print "existing_temperature_signals: {}".format(existing_temperature_signals)
    
            ##
            ##   First, process all devices using temperature data
            ##
    
            empty_device_ids = []
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for dev_index, device_id in enumerate(all_device_ids):
                print group_index_string + "    deviceid: {devid} #{dev_index} of {num_devids}".format(devid = device_id,
                                                                                                   dev_index = dev_index,
                                                                                                   num_devids = len(all_device_ids))

                #  for devices with one or more non-hidden sensor, process for temperature
                if device_id in sensor_info.deviceid.values:
                    print "sensor_info[sensor_info['deviceid']==device_id]: {}".format(sensor_info[sensor_info['deviceid']==device_id])
                    # Determine the processed_after date either from prior signal or using window start
                    if device_id in existing_temperature_signals['device_id'].values:
                        print "existing_temperature_signals[existing_temperature_signals['device_id']==device_id]: {}".format(existing_temperature_signals[existing_temperature_signals['device_id']==device_id])
                        raw_max_time = existing_temperature_signals[existing_temperature_signals['device_id']==device_id].max_time.values[0]
                        if raw_max_time is None:
                            max_time = ANALYSIS_WINDOW_START_DATE
                        else:
                            max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
                        time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                        print "found device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
                        flag_existing_signal = True
                    else:
                        max_time = ANALYSIS_WINDOW_START_DATE
                        time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
                        print "did not find device in existing signals, using time_clause: {time_clause}".format(time_clause = time_clause)
                        flag_existing_signal = False
                    print "Pulling temperature history for {devid}".format(devid=device_id)
                    newdevices_data_query = """SELECT
                                            device_id::text device_id, reading_date, processed_date, temperature, age, port, str_device_id
                                          FROM public.sensor_temperature_history
                                          WHERE {time_clause}
                                             AND device_id = '{devid}' """.format(time_clause = time_clause,
                                                                                  devid = device_id)
                    print "submitting new devices query: {query}".format(query = newdevices_data_query)
                    results_df = pd.read_sql(newdevices_data_query, WAREHOUSE_DATABASE_CONN)
                    if len(results_df) > 0:
                        print "results_df.head(): {}".format(results_df.head())
                        all_results_df = all_results_df.append(results_df)
                    else:
                        print "no data found"
                        # if max_time is not None:
                        empty_device_ids.append(device_id)
    
            if len(all_results_df) > 0:
                print "temperature call to ManyDeviceSignals"
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL,
                                        port_col=PORT_COL)
    
                print "temperature call to update_all_signals"
                updated_signals = mds.update_all_signals(READING_COL_NODES,
                                                         segment_daily=False,
                                                         # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                         min_processed_time_override=device_mintime_override_lookup,
                                                         min_time_override=device_mintime_override_lookup,
                                                         max_processed_time_override=device_maxtime_override_lookup,
                                                         max_time_override=device_maxtime_override_lookup)
                print "Number of Updated Temperature Signals: {0}".format(len(updated_signals))

            print "Before handling empty temperature signals, time is: {}".format(datetime.now())
            print "empty_device_ids: {}".format(empty_device_ids)
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    print "temperature call to update_empty_device_signal"
                    if empty_device_id not in existing_temperature_signals['device_id'].values:
#                         ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES,
#                                                                      segment_daily=True,
#                                                                      # min_time_override=ANALYSIS_WINDOW_START_DATE,
#                                                                      reporting_interval_override=REPORTING_INTERVAL_MINUTES,
#                                                                      min_time_override=device_mintime_override_lookup,
#                                                                      max_time_override=device_maxtime_override_lookup)
                        ports = sensor_info[sensor_info['deviceid']==empty_device_id].portid.values
                        print ports,'x- Port loop'
                        for port in ports:
                            if pd.isnull(port):
                            	print 'NULL LOOP'
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=False,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup,
                                                                             max_time_override=device_maxtime_override_lookup)
                            else:
                            	print port,empty_device_id
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=False,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup,
                                                                             max_time_override=device_maxtime_override_lookup)

                    else:
                        ports = existing_temperature_signals[existing_temperature_signals['device_id']==empty_device_id].port.values
                        print ports,'y'
                        for port in ports:
                            if pd.isnull(port):
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES,
                                                                             segment_daily=False,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup,
                                                                             max_time_override=device_maxtime_override_lookup)
                            else:
                                ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_NODES, port = int(port),
                                                                             segment_daily=False,
                                                                             # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                                             reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                             min_time_override=device_mintime_override_lookup,
                                                                             max_time_override=device_maxtime_override_lookup)

                print "Number of empty Temperature Signals: {0}".format(len(empty_device_ids))
    
            # print "updated_signals: {}".format(updated_signals)
    
    
    
    
            ##
            ##   Second, process gateways using channel data
            ##
            this_group_gatewaysinfo = this_group_devicesinfo[this_group_devicesinfo['devicetype']=='Gateway']
            all_gateway_ids = this_group_gatewaysinfo['deviceid'].unique()
            num_gateways = len(all_gateway_ids)
    
            ##  build table of existing signal dates to streamline subsequent trundle queries
            this_group_signals_query = """select device_id, max_processed_time, max_time_override from signals
            where data_type = 'channel' and device_id in ('{devid_list}'); """.format(devid_list = "','".join(all_device_ids))
            # print "Pulling existing signals info for group"
            existing_channel_signals = pd.read_sql(this_group_signals_query, DATABASE_CONN)
            # print "Found {numrows} existing signals".format(numrows = len(existing_channel_signals))
            # print "existing_channel_signals: {}".format(existing_channel_signals)
            if len(existing_channel_signals)>0:
                existing_channel_signals['max_time'] = ''
                for row_index in range(0,len(existing_channel_signals)):
                    print "evaluating row {index} in existing_channel_signals".format(index = row_index)
                    existing_channel_signals['max_time'].iloc[row_index] = existing_channel_signals['max_processed_time'].iloc[row_index] if \
                        pd.isnull(existing_channel_signals['max_time_override'].iloc[row_index]) else existing_channel_signals['max_time_override'].iloc[row_index]
                # print "After Initial max_time set:    existing_temperature_signals: {}".format(existing_temperature_signals)
                # existing_channel_signals['max_time'] = existing_channel_signals['max_processed_time'].where(
                #     existing_channel_signals['max_time_override'].isnull(),existing_channel_signals['max_time_override'])
                # existing_channel_signals['max_time'] = existing_channel_signals['max_time_override'].where(
                #     pd.isnull(existing_channel_signals['max_time']), existing_channel_signals['max_time'])
    
            else:
                existing_channel_signals=pd.DataFrame({'device_id' : None, 'max_time' : None},index = range(0,0))
            print "existing_channel_signals: {}".format(existing_channel_signals)
    
            empty_device_ids = []
            all_results_df = pd.DataFrame()
            ##  Iterate over the set of devices
            for device_id in all_gateway_ids:
                # Determine the processed_after date either from prior signal or using window start
                print "examining gateway deviceid: {devid}".format(devid = device_id)
                if device_id in existing_channel_signals['device_id'].values:
                    raw_max_time = existing_channel_signals[existing_channel_signals['device_id']==device_id].max_time.values[0]
                    if raw_max_time is None:
                        max_time = ANALYSIS_WINDOW_START_DATE
                    else:
                        max_time = datetime.strptime(format_datetime(str(raw_max_time)), '%Y-%m-%d %H:%M:%S')
                    time_clause = "processed_date > '{max_time}'".format(max_time = max_time)
                    flag_existing_signal = True
                else:
                    max_time = ANALYSIS_WINDOW_START_DATE
                    time_clause = "reading_date > '{read_date}'".format(read_date = ANALYSIS_WINDOW_START_DATE)
                    flag_existing_signal = False
                print "Pulling channel history for {devid}".format(devid=device_id)
                newdevices_data_query = """SELECT
                                        device_id::text device_id, reading_date, processed_date, channel, str_device_id
                                      FROM public.device_channel_history
                                      WHERE
                                        {time_clause}
                                         AND device_id = '{devid}' """.format(time_clause = time_clause,
                                                                              devid = device_id)
                print "submitting query: {query}".format(query = newdevices_data_query)
                results_df = pd.read_sql(newdevices_data_query, WAREHOUSE_DATABASE_CONN)
                if len(results_df) > 0:
                    print "results_df.head(): {}".format(results_df.head())
                    all_results_df = all_results_df.append(results_df)
                else:
                    print "no data found"
                    # if max_time is not None:
                    empty_device_ids.append(device_id)
    
            print "pre-ManyDeviceSignals all_results_df.head(): {}".format(all_results_df.head())
    
            if len(all_results_df) > 0:
                mds = ManyDeviceSignals(all_results_df,
                                        DEVICE_ID_COL,
                                        DATE_COL,
                                        processed_date_col=PROCESSED_COL)
    
                updated_signals = mds.update_all_signals(READING_COL_GATEWAYS,
                                                         segment_daily=False,
                                                         # min_time_override=ANALYSIS_WINDOW_START_DATE,
                                                         reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                         min_time_override=device_mintime_override_lookup,
                                                         max_time_override=device_maxtime_override_lookup)
                print "Number of Updated Channel Signals: {0}".format(len(updated_signals))
    
            if len(empty_device_ids) > 0:
                for empty_device_id in empty_device_ids:
                    ManyDeviceSignals.update_empty_device_signal(empty_device_id, READING_COL_GATEWAYS,
                                                                 segment_daily=False,
                                                                 reporting_interval_override=REPORTING_INTERVAL_MINUTES,
                                                                 min_time_override=device_mintime_override_lookup,
                                                                 max_time_override=device_maxtime_override_lookup)
                print "updated {num} empty signals".format(num = len(empty_device_ids))
            # print "updated_signals: {}".format(updated_signals)
    
    
    
            print "group runtime: {}".format(datetime.now() - group_starttime)
            group_id_index += 1
    
    
        EndTime = datetime.now()
        print "Combined Analysis Time: {0}".format(EndTime - StartTime)
