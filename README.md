# TempAlert Utils

TempAlert Utilities is a series of python utilities to enable efficient analysis of signals data within TempAlert.

##  Standard TempAlert Installation
We saw variations in how the code base was running as we installed this utility within the virtual environment built as part of 
the Trundle Wrapper effort.  As a result, we developed the following standard sequence of stages that produce a repeatable installation:

Prep:  run from a directory in which you want both the Trundle API repository and the Gap Stats repository to be cloned in 
subfolders to.  This script hard-codes names for the directories in which things will be cloned - that can be edited.

```
mkdir Trundle_II
cd Trundle_II
git clone git@bitbucket.org:tempalert/trundle-api-client.git
cd trundle-api-client
virtualenv trundle-env
source trundle-env/bin/activate
pip install -e .
pip install numpy
pip install pandas
pip install scipy
pip install openpyxl
brew install freetype
brew install pkg-config
cd ../..
mkdir GapStats_II
cd GapStats_II
git clone git@bitbucket.org:JasonSrokaTempAlert/gap_stats.git
cd gap_stats
conda create -n gap_stats python=3.6 anaconda
source activate gap_stats
python setup.py install
cd scripts/FakeData
python Run_All_Tests.py


---
This should result in a set of Test_Run*.xlsx files being created, one for each of the Test_Run*.py scripts

## Basic Installation

Before installing the package, make sure you have a working version of python and pip.

The package uses a Postgres database by default. If you are on a Mac, I highly recommend using [Postgres.app](http://postgresapp.com/).

To install the package, you need to clone the repo and run the setup script.

```
git clone git@bitbucket.org:JasonSrokaTempAlert/gap_stats.git
cd gap_stats/
conda create -n gap_stats python=3.6 anaconda
source activate gap_stats
python setup.py install
```

If you don't have pg_config, you might need to run the following command (on mac):

```
brew install postgresql
```

If you are developing on this application, you should replace the
last command with the following:

```
conda create -n gap_stats python=3.6 anaconda
source activate gap_stats
pip install -r dev-requirements.txt
python setup.py develop
```

## Testing and Documentation

To run the tests located in `ta_utils/tests`, run the following:

```
python setup.py test
```

To measure the coverage of the tests, run the following:

```
coverage run --source ta_utils setup.py test
```

In order to generate documentation HTML, you should simply run the following:

```
cd docs/
make html
```

Then simply view the HTML located in `docs/_build/html`.

## Usage

See documentation.

## Todo

- In gaps_gen, incremental update should update from max_processed_time
- Fix the tests for persistable_signal_segment gaps and offline_periods
- Check new offline_periods with delays filters with Jason

- Generalize IncrementalSignal for non-daily SignalSegments
- Write better tests for edge cases of SignalSegments (as discussed with diagram)

- Clean up module for *plotting* signals
- Write tests for compliance and device_label

## Done

- Write better tests for delays
- Fixed `__getitem__` bug that didn't pass processed_dates properly
- Write SignalSegment tests
- Improved performance through memoization
- Write tests for new metrics
- Sort transmission times for intervals for offline_periods
- Signal Segments: table with start_time, end_time, tag and stats
- Overall End Date: deal with signals that should have had data beyond a certain point
    - Use min_time_override
- Overall Start Date: deal with the same issue but with the start date
    - Use max_time_override
- Override reporting period (15 minutes in example)
    - Use reporting_interval_override
- Save SignalSegment and Signal metadata (gaps/offline periods/delays) in the Database
- Memoize _gap_times and similar functions
- Check if gaps will record if starting before and ending after a day
- Check that SignalSegments have day segments where there are no readings
- Change Signal name to device_id, port, data_type columns
- Create override of ManyDeviceSignals for reporting_interval_override, min_time_override, etc.
- Cleaned up gaps_gen insertion SQL
- Check the end time for the final SignalSegment to make sure it's not ending at midnight
- Deal with completely empty Signals for cases of failure (create "null" Signal)
- For offline periods, don't count transmissions that are older than a certain age (default to 2x reporting period)
- Write better tests for offline periods
- Test incremental null Signal
- Fix update signals to remove any processed_time less than the exisiting max_processed_time
- Check that you can incrementally update with empty signal with bounding times
- Allow for custom max_processed_time and min_processed_time




To run python setup.py test on Ubuntu:

Follow these setups

1. Login as default user: sudo -i -u postgres
2. Create new User: createuser --interactive
3. When prompted for role name, enter linux username, and select Yes to superuser question.
4. Still logged in as postgres user, create a database: createdb <username_from_step_3>
5. Confirm error(s) are gone by entering: psql at the command prompt.
6. Output should show psql (x.x.x) Type "help" for help


To run python setup.py test on your local machine:

Follow these setups:
- Install the PostgresSQL 9.6 app and move it to applications and then run it


If you face python as a frame work issue on local machine then run:
pip install matplotlib==1.4.3

and if you face display path problems with matplotlib then add this before you import anything from matplotlib :
import matplotlib
matplotlib.use('Agg')


To run something on a branch from command line :

git fetch && git checkout <branch-name>

Random commands that help getting everything working:

brew link postgresql



How to create a virtual env:
1. MY system configurations at this point: python 2.7 installed, pip and conda 
2. Go to the directory where you normally make virtual env in. 
3. Create a folder like this:  mkdir python3-virtual
4. Then go to it: cd python3-virtual
5. Select the required python package or version using this command:  conda search --full-name python
6. Create appropriately named and versioned python using following command: conda create -n trundle-env-python3 python=3.6.4 anaconda     (where  trundle-env-python3 is name of my virtual env)
7. To activate: source activate trundle-env-python3
8. Sanity Check: python --version
9. python -m pip install --upgrade pip
10. pip install --upgrade snowflake-connector-python
11. Move Config file to appropriate directory. 


If you are on Ubuntu:
Do this before all steps if you don't have Anaconda on commandline
1. wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda-2.3.0-Linux-x86_64.sh
2. bash Anaconda-2.3.0-Linux-x86_64.sh
3. Restart terminal


Really annoying to accept installation (kept missing on agreeing to license)
1.Make a copy of your config file on your desktop and scp it:
2.make sure on branch:  git checkout areagle-python3
3. then scp command on mac terminal for me atleast is this:
scp -r /Users/jheel/Desktop/config.py jheel@52.207.138.183:/home/jheel/repository/gap_stats/scripts/
4. Had to run this as well: pip install pyyaml
5. Dint forget to run python setup.py (ran into issues)



Extra Readme associated with Gapstats

https://tempalert.atlassian.net/wiki/spaces/AN/pages/433979393/GapStats+Readme+Performance+and+Digi+companies

