"""
Incremental Signal Module
"""
import math
from datetime import timedelta

from ta_utils.persistence.models import PersistableSignalSegment


class IncrementalSignalSegment(object):
    """
    The Incremental Signal Object represents an existing signal and an incremental signal as a time-series.

    You can easily update an existing persisted signal with a new Signal object by doing the following::

        >>> incremental_signal_segment = IncrementalSignalSegment(persisted_signal_segment, new_signal)
        >>> incremental_signal_segment.save()

    The metadata and gaps objects will be updated properly, with the exception of any "repeat signal data".
    Any exact repeats will cause minor unexpected behavior for the count, mean, std, and var properties.
    """
    def __init__(self, persisted_signal_segment, incremental_signal):
        """
        Initialize the Incremental Signal with a persisted signal and an incremental signal.

        :param persisted_signal_segment: An existing Persistable Signal Segment
        :type persisted_signal_segment: PersistableSignalSegment
        :param incremental_signal: An incremental Signal update
        :type incremental_signal: Signal
        """
        self.persisted_signal_segment = persisted_signal_segment

        # In the case of the final daily segment, the end_time is not the end of the day and needs to be updated
        if self.persisted_signal_segment.segment_type == 'daily' and self.persisted_signal_segment.final_segment:
            self.persisted_signal_segment.end_time = min(self.persisted_signal_segment.start_time + timedelta(days=1),
                                                         incremental_signal.max_time)

        if self.persisted_signal_segment.start_time and self.persisted_signal_segment.end_time:
            inclusive = self.persisted_signal_segment.final_segment or self.persisted_signal_segment.segment_type not in ['daily', 'periodic']
            sub_signal = incremental_signal.between(self.persisted_signal_segment.start_time,
                                                    self.persisted_signal_segment.end_time,
                                                    inclusive=inclusive)
        elif self.persisted_signal_segment.start_time:
            sub_signal = incremental_signal.after(self.persisted_signal_segment.start_time, inclusive=True)
        else:
            sub_signal = incremental_signal.before(self.persisted_signal_segment.end_time, inclusive=True)

        self.incremental_signal = sub_signal

    @property
    def count(self):
        """
        Get the count of values in the Signal.

        :return: Count
        :rtype: int
        """
        return self.persisted_signal_segment.count + self.incremental_signal.count

    @property
    def mean(self):
        """
        The mean value of this signal.

        :return: Mean of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        if self.persisted_signal_segment.mean is None:
            persisted_sum = 0
        else:
            persisted_sum = 1. * self.persisted_signal_segment.mean * self.persisted_signal_segment.count

        if self.incremental_signal.mean is None:
            incremental_sum = 0
        else:
            incremental_sum = 1. * self.incremental_signal.mean * self.incremental_signal.count

        return float(persisted_sum + incremental_sum) / self.count

    @property
    def std(self):
        """
        The standard deviation of this signal.

        See: http://stats.stackexchange.com/a/56000

        :return: Standard deviation of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        numerator = 0.
        if self.persisted_signal_segment.std:
            numerator += self.persisted_signal_segment.count * (self.persisted_signal_segment.std ** 2)
            numerator += self.persisted_signal_segment.count * ((self.persisted_signal_segment.mean - self.mean) ** 2)

        if self.incremental_signal.std:
            numerator += self.incremental_signal.count * (self.incremental_signal.std ** 2)
            numerator += self.incremental_signal.count * ((self.incremental_signal.mean - self.mean) ** 2)

        return math.sqrt(numerator / self.count)

    @property
    def var(self):
        """
        The variance of this signal.

        :return: Variance of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self.std ** 2

    @property
    def min(self):
        """
        The minimum value of this signal.

        :return: Minimum value of signal
        :rtype: float
        """
        if self.persisted_signal_segment.min is None and self.incremental_signal.min is None:
            return None
        elif self.persisted_signal_segment.min is None:
            return self.incremental_signal.min
        elif self.incremental_signal.min is None:
            return self.persisted_signal_segment.min

        return min(self.persisted_signal_segment.min, self.incremental_signal.min)

    @property
    def max(self):
        """
        The maximum value of this signal.

        :return: Maximum value of signal
        :rtype: float
        """
        if self.persisted_signal_segment.max is None and self.incremental_signal.max is None:
            return None
        elif self.persisted_signal_segment.max is None:
            return self.incremental_signal.max
        elif self.incremental_signal.max is None:
            return self.persisted_signal_segment.max

        return max(self.persisted_signal_segment.max, self.incremental_signal.max)

    @property
    def mid(self):
        """
        The average of the min and max values of this signal.

        :return: Mid value of signal
        :rtype: float
        """
        if self.min is None or self.max is None:
            return None

        return float(self.min + self.max) / 2.

    @property
    def min_time(self):
        """
        The minimum time of this signal.

        :return: Minimum time of signal
        """
        if self.incremental_signal.min_time is None:
            return self.persisted_signal_segment.min_time

        return min(self.persisted_signal_segment.min_time, self.incremental_signal.min_time)

    @property
    def max_time(self):
        """
        The maximum time of this signal.

        :return: Maximum time of signal
        """
        if self.incremental_signal.max_time is None:
            return self.persisted_signal_segment.max_time

        return max(self.persisted_signal_segment.max_time, self.incremental_signal.max_time)

    @property
    def min_reading_time(self):
        """
        The minimum reading time of this signal.

        :return: Minimum reading time of signal
        """
        if self.incremental_signal.min_reading_time is None and self.persisted_signal_segment.min_reading_time is None:
            return None

        if self.incremental_signal.min_reading_time is None:
            return self.persisted_signal_segment.min_reading_time

        if self.persisted_signal_segment.min_reading_time is None:
            return self.incremental_signal.min_reading_time

        return min(self.persisted_signal_segment.min_reading_time, self.incremental_signal.min_reading_time)

    @property
    def max_reading_time(self):
        """
        The maximum reading time of this signal.

        :return: Maximum reading time of signal
        """
        if self.incremental_signal.max_reading_time is None and self.persisted_signal_segment.max_reading_time is None:
            return None

        if self.incremental_signal.max_reading_time is None:
            return self.persisted_signal_segment.max_reading_time

        if self.persisted_signal_segment.max_reading_time is None:
            return self.incremental_signal.max_reading_time

        return max(self.persisted_signal_segment.max_reading_time, self.incremental_signal.max_reading_time)

    @property
    def min_processed_time(self):
        """
        The minimum processed time of this signal.

        :return: Minimum processed time of signal
        """
        if self.incremental_signal.min_processed_time is None and self.persisted_signal_segment.min_processed_time is None:
            return None

        if self.incremental_signal.min_processed_time is None:
            return self.persisted_signal_segment.min_processed_time

        if self.persisted_signal_segment.min_processed_time is None:
            return self.incremental_signal.min_processed_time

        return min(self.persisted_signal_segment.min_processed_time, self.incremental_signal.min_processed_time)

    @property
    def max_processed_time(self):
        """
        The maximum processed time of this signal.

        :return: Maximum processed time of signal
        """
        if self.incremental_signal.max_processed_time is None and self.persisted_signal_segment.max_processed_time is None:
            return None

        if self.incremental_signal.max_processed_time is None:
            return self.persisted_signal_segment.max_processed_time

        if self.persisted_signal_segment.max_processed_time is None:
            return self.incremental_signal.max_processed_time

        return max(self.persisted_signal_segment.max_processed_time, self.incremental_signal.max_processed_time)

    @property
    def mode_minute_interval(self):
        """
        The mode of the interval between times in the Signal, rounded to minutes, in seconds.

        TODO: this might not be correct, but let's just assume the mode never changes.

        :return: Mode of the interval, rounded to minutes, in seconds
        :rtype: int
        """
        return self.persisted_signal_segment.mode_minute_interval

    def save(self, force_insert=False, only=None):
        """
        Save the signal in a table, along with any gaps.

        :param force_insert: boolean to force an insertion query rather than update
        :param only: the parameters you want to include in this save, or None for all
        :return: The persisted signal
        :rtype: PersistableSignal
        """
        count = self.count
        mean = self.mean
        std = self.std
        var = self.var
        min = self.min
        max = self.max
        mid = self.mid
        min_time = self.min_time
        max_time = self.max_time
        min_reading_time = self.min_reading_time
        max_reading_time = self.max_reading_time
        min_processed_time = self.min_processed_time
        max_processed_time = self.max_processed_time
        mode_minute_interval = self.mode_minute_interval

        self.persisted_signal_segment.count = count
        self.persisted_signal_segment.mean = mean
        self.persisted_signal_segment.std = std
        self.persisted_signal_segment.var = var
        self.persisted_signal_segment.min = min
        self.persisted_signal_segment.max = max
        self.persisted_signal_segment.mid = mid
        self.persisted_signal_segment.min_time = min_time
        self.persisted_signal_segment.max_time = max_time
        self.persisted_signal_segment.min_reading_time = min_reading_time
        self.persisted_signal_segment.max_reading_time = max_reading_time
        self.persisted_signal_segment.min_processed_time = min_processed_time
        self.persisted_signal_segment.max_processed_time = max_processed_time
        self.persisted_signal_segment.mode_minute_interval = mode_minute_interval

        self.persisted_signal_segment.save(force_insert, only)

        return self.persisted_signal_segment
