
# coding: utf-8

# In[1]:

####  Environment setup
import os
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta

from ta_utils import ManyDeviceSignals, PersistableSignal
from ta_utils import database
from ta_utils import create_tables, drop_tables

def FormatDatetime(RawDatetime):
    result=RawDatetime.replace('T',' ')
    ReturnDatetime=result.split('.')[0]
#     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
    return ReturnDatetime


# In[2]:

####  Device Metadata - used for filtering to a good set of Device IDs
DevicesInfoFile='/Users/Jason/Desktop/Local_Data_Store/vu_DeviceAndStoreInfo.csv'
DevicesInfo=pd.read_csv(DevicesInfoFile)

##  Filter to RI devices for now
RI_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='RI') & (DevicesInfo['DateDeleted'].isnull()) ]
# Target_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='OR') and 
#                                          (('Store #16' in i for i in DevicesInfo['StoreNumber']) or ('Store #17' in i for i in DevicesInfo['StoreNumber']))]
# OR_Monitored_DevicesInfo=DevicesInfo[(DevicesInfo['StateOrProvince']=='OR') ]

##  Quick test
Quicktest_GroupIDs=[9613]
QuicktestDeviceIndices=np.where(DevicesInfo['GroupID'].isin(Quicktest_GroupIDs))
Quicktest_DeviceInfo=DevicesInfo.iloc[QuicktestDeviceIndices]



##  Processing to set up gruops for Oregon Target stores
OR_Target_GroupIDs=[9613,9614,9615,9616,9617,9618,91619,9620,9623,9624,9625,9626,9628,9629,9631,9636,9637,9638]
TargetDeviceIndices=np.where(DevicesInfo['GroupID'].isin(OR_Target_GroupIDs))
OR_Target_DeviceInfo=DevicesInfo.iloc[TargetDeviceIndices]
# Monitored_DevicesInfo=Monitored_DevicesInfo.append(OR_Monitored_DevicesInfo)

###  Processing to set up groups for minute clinic stores
MCRolloutFileName='/Users/Jason/Desktop/Analytics/Projects/ANALYTICS_106_StoreResetFailures/TempAlertRolloutSchedule.csv'
MCRolloutDF=pd.DataFrame.from_csv(MCRolloutFileName)


# MinuteClinicStoresList=['Store #459','Store #750','Store #1043','Store #1180','Store #2220','Store #2525','Store #447','Store #957','Store #969',
# 'Store #1094','Store #1095','Store #38','Store #299','Store #507','Store #704','Store #920','Store #946','Store #1882','Store #2172','Store #41',
# 'Store #207','Store #335','Store #394','Store #913','Store #938','Store #1877','Store #6466','Store #212','Store #326','Store #590','Store #729',
# 'Store #1544','Store #2065','Store #5107','Store #717','Store #1010','Store #1018','Store #1022','Store #1254','Store #1531','Store #6505',
# 'Store #8437','Store #137','Store #929','Store #1006','Store #1238','Store #1297','Store #1852','Store #2325','Store #281','Store #634','Store #937',
# 'Store #1945','Store #2138','Store #2401','Store #2600','Store #73','Store #217','Store #915','Store #1011','Store #1174','Store #1845','Store #7109',
# 'Store #654','Store #730','Store #973','Store #1121','Store #1179','Store #1875','Store #2125','Store #329','Store #806','Store #841','Store #1003',
# 'Store #1004','Store #2257','Store #6596']
MCRolloutFileName='/Users/Jason/Desktop/Analytics/Projects/ANALYTICS_106_StoreResetFailures/TempAlertRolloutSchedule.csv'
MCRolloutDF=pd.DataFrame.from_csv(MCRolloutFileName)
MCRolloutDF.head()
FilteredMCRolloutDF=MCRolloutDF[MCRolloutDF['HostStoreNum'].str.contains("MC")==False]

PilotStoreNums=FilteredMCRolloutDF[FilteredMCRolloutDF['Status']=='Pilot'].HostStoreNum.apply(lambda x:int(x))
PilotStoreNums=FilteredMCRolloutDF.HostStoreNum.apply(lambda x:int(x))
PilotStoreNums[0]
MinuteClinicStoresList=PilotStoreNums.apply(lambda(x): "Store #{}".format(x))
# PilotStoreNames=PilotStoreNames.append('Store #0MC98')

NumStores=len(MinuteClinicStoresList)
MinuteClinicGroupsDict={}
for StoreName in MinuteClinicStoresList:
    thisGroupID=DevicesInfo['GroupID'][DevicesInfo['StoreNumber']==StoreName].unique()
    print "thisGroupID: {GID} for store name:  {name} ".format(GID=thisGroupID,name=StoreName)
    if len(thisGroupID)==0:
        print "no devices found for group: {}".format(StoreName)
        continue
    MinuteClinicGroupsDict[StoreName]=int(thisGroupID[0])
MinuteClinicGroupIDs=MinuteClinicGroupsDict.values()
##  First get the devices that are in the store's group
MinuteClinic_DeviceIndices=np.where(DevicesInfo['GroupID'].isin(MinuteClinicGroupIDs))
MinuteClinic_DeviceInfo=DevicesInfo.iloc[MinuteClinic_DeviceIndices]
MinuteClinic_DeviceInfo['StoreGroupID']=MinuteClinic_DeviceInfo['GroupID']
##  Next get the devices that are in the children of a store's group
MinuteClinic_DeviceIndices_ChildrenGroups=np.where(DevicesInfo['ParentGroupID'].isin(MinuteClinicGroupIDs))
MinuteClinic_DeviceInfo_ChildrenGroups=DevicesInfo.iloc[MinuteClinic_DeviceIndices_ChildrenGroups]
MinuteClinic_DeviceInfo_ChildrenGroups['StoreGroupID']=MinuteClinic_DeviceInfo_ChildrenGroups['ParentGroupID']
##  Put them together
MinuteClinic_DeviceInfo=MinuteClinic_DeviceInfo.append(MinuteClinic_DeviceInfo_ChildrenGroups)

MinuteClinic_DevGroup_DeviceInfo=MinuteClinic_DeviceInfo[MinuteClinic_DeviceInfo['GroupID']==1652]

OmnicareDeepFreezersList=['11666000000120943688','11666000000120963823','11666000000121005851','11666000000121022328','11666000000121464331',
'11666000000121487753','11666000000121502766','11666000000121520937','11666000000121525293','11666000000121859660','11666000000122819335',
'11666000000127948954','11666000000128006004','11666000000133579667']
OmniDeepFreeze_DeviceInfo=DevicesInfo[DevicesInfo['DeviceID'].isin(OmnicareDeepFreezersList)]

##  Final selection
# Monitored_DevicesInfo=OR_Target_DeviceInfo
# DataTag='OR_Target'
Monitored_DevicesInfo=MinuteClinic_DeviceInfo
DataTag='MinuteClinic'
# Monitored_DevicesInfo=MinuteClinic_DevGroup_DeviceInfo
# DataTag='MinuteClinic_DevGroup_DeviceInfo'
# Monitored_DevicesInfo=RI_Monitored_DevicesInfo
# DataTag='RI_CVS'
# Monitored_DevicesInfo=Quicktest_DeviceInfo
# DataTag='Quicktest'


# Monitored_DevicesInfo=MinuteClinic_DevGroup_DeviceInfo
# Monitored_DevicesInfo=OmniDeepFreeze_DeviceInfo

Monitored_DeviceIDs=Monitored_DevicesInfo['DeviceID']
NumMonitoredDevices=len(Monitored_DeviceIDs)


# In[6]:

####  Configuration/Function params
ZigbeeDevicePowerDataFile='/Users/Jason/Desktop/Local_Data_Store/ZigbeeDevicePowerHistory_DataPull.csv'
ZigbeeSensorInfoDataFile='/Users/Jason/Desktop/Local_Data_Store/TrundleCVSSensorData_DataPull.csv'
TrundleDevicesInfoFile='/Users/Jason/Desktop/Local_Data_Store/TrundleDeviceInfo.csv'
DeviceReadingGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DeviceReadingGaps.csv'
SensorReadingGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_SensorReadingGaps.csv'
SensorTransmissionGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_SensorTransmissionGaps.csv'
DeviceTransmissionGapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DeviceTransmissionGaps.csv'
GapStatsResultsFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_DataFlowGaps.csv'
GroupGatewayOnlineStatsFile='/Users/Jason/Desktop/Local_Data_Store/GroupGatewayOnline_DailyStats.csv'
PowerPull_BadDevicesFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_PowerPull_BadDevicesList.csv'.format(DataTag)
TemperaturePull_BadDevicesFile='/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_TemperaturePull_BadDevicesList.csv'.format(DataTag)

NumDevicesToProcess=800

AnalysisWindowInDays=20
AnalysisWindow_StartDate=datetime.now()-timedelta(days=AnalysisWindowInDays)


GapStats_DeviceInfoFields=['DeviceID','DeviceLabel','GroupID','GroupName','Environment','Address','City','StateOrProvince','PostalCode','StoreNumber']
thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')

RunTimestamp=datetime.now().date()

FLAG_PullData=True


# In[ ]:

len(Monitored_DevicesInfo)


# In[ ]:

####  Reset/Cleanse the overall system
if os.path.exists(DeviceReadingGapStatsResultsFile):
    os.remove(DeviceReadingGapStatsResultsFile)
if os.path.exists(SensorReadingGapStatsResultsFile):
    os.remove(SensorReadingGapStatsResultsFile)
if os.path.exists(SensorTransmissionGapStatsResultsFile):
    os.remove(SensorTransmissionGapStatsResultsFile)
if os.path.exists(DeviceTransmissionGapStatsResultsFile):
    os.remove(DeviceTransmissionGapStatsResultsFile)
if os.path.exists(GapStatsResultsFile):
    os.remove(GapStatsResultsFile)
# if os.path.exists(ZigbeeSensorInfoDataFile):
#     os.remove(ZigbeeSensorInfoDataFile)
# if os.path.exists(ZigbeeDevicePowerDataFile):
#     os.remove(ZigbeeDevicePowerDataFile)
if os.path.exists(GroupGatewayOnlineStatsFile):
    os.remove(GroupGatewayOnlineStatsFile)


# In[ ]:

##
##   Get the data for Gateway Power analysis
##
if FLAG_PullData:
    StartTime=datetime.now()
    import json
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
    # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')


    FLAG_Header=True
    WriteMode='w'

    Monitored_Gateways_DeviceIDs=Monitored_DevicesInfo[Monitored_DevicesInfo['HardwareFamily'].isin([2,3])].DeviceID
    NumMonitoredGatewayDevices=len(Monitored_Gateways_DeviceIDs)

    BadDevicesDF=pd.DataFrame()
    BadDevicesCount=1
    # for DeviceIter in range(0,NumDevicesToProcess):
    for DeviceIter in range(0,NumMonitoredGatewayDevices):
        thisDeviceID = Monitored_Gateways_DeviceIDs.iloc[DeviceIter]
        thisStrDeviceID = "Device:"+str(thisDeviceID)

        print"Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisStrDeviceID,DeviceIter=DeviceIter,TotalDevices=NumMonitoredGatewayDevices)

        thisDeviceDataDF=pd.DataFrame(thisClient.get_device_readings_by_device_id(device_id=thisDeviceID,verbose=True,
                                                                                  command_ids=['80','150.150.370'],
    #                                                                               command_ids=['153.122.270',
    #                                                                                   '153.122.370',
    #                                                                                   '153.122.470',
    #                                                                                   '153.120.370',
    #                                                                                   '153.120.470',
    #                                                                                   '153.121.370',
    #                                                                                   '80','82'],
                                                                          reading_start_dt=AnalysisWindow_StartDate))
        if(len(thisDeviceDataDF)==0):
            print('No data found for device, skipping to next')
            BadDevicesDF=BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No data returned by trundle']}))
            print BadDevicesDF
            BadDevicesCount=BadDevicesCount+1
            continue
        thisDeviceDataDF['DeviceID']=str(thisDeviceID)
        thisDeviceDataDF['StrDeviceID']=thisStrDeviceID
        thisDeviceDataDF['readingDate']=thisDeviceDataDF['readingDate'].apply(lambda x: FormatDatetime(x))
        thisDeviceDataDF['processedDate']=thisDeviceDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
        thisDeviceDataDF.drop('alarm',axis=1,inplace=True)
        print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisDeviceDataDF))

        #     print thisDeviceDataDF

        thisDeviceDataDF.to_csv(ZigbeeDevicePowerDataFile,header=FLAG_Header,mode=WriteMode,index=False)
        FLAG_Header=False
        WriteMode='a'
    EndTime=datetime.now()
    TimeDuration=EndTime-StartTime
    if len(BadDevicesDF)>0:
        BadDevicesDF=pd.merge(BadDevicesDF,DevicesInfo[GapStats_DeviceInfoFields],
                                       on=['DeviceID'],how='left')
        BadDevicesDF.to_csv(PowerPull_BadDevicesFile,index=False)
    print "Gateway power Data Pull Time: {TD}".format(TD=TimeDuration)
    



# In[11]:

# DeviceDataDF = pd.read_csv(ZigbeeDevicePowerDataFile)
# DeviceDataDF.head()
# len(DeviceDataDF['StrDeviceID'].unique())
DeviceDataDF_Full=DeviceDataDF
print len(DeviceDataDF_Full)
DeviceDataDF=DeviceDataDF_Full[DeviceDataDF_Full['readingType']=='Power']
print len(DeviceDataDF)


# In[17]:

StartTime=datetime.now()
print "Beginning Gateway Power Coverage analysis"

# TODO: change this value if you'd like to reset the table
RESET = True

DEVICE_ID_COL = 'StrDeviceID'
DATE_COL = 'readingDate'
READING_COL = 'readingValue'

SAMPLE_DEVICE = '11666000000117734806'

# database.init('test_ta_utils')
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com', port='5432')


if RESET:
    print "Resetting"
    drop_tables()
    print "dropped tables"
    create_tables()
    print "created tables"
    print "reading from file {}".format(ZigbeeDevicePowerDataFile)
    DeviceDataDF_Full = pd.read_csv(ZigbeeDevicePowerDataFile)
    print "filtering data"
    DeviceDataDF=DeviceDataDF_Full[DeviceDataDF_Full['readingType']=='Power']
    print "creating ManyDeviceSignals object"
    networksignals_device_signals = ManyDeviceSignals(DeviceDataDF, DEVICE_ID_COL, DATE_COL)
    print "calling update_all_signals"
    networksignals_device_signals.update_all_signals(READING_COL)
    print "updated signals"
    FLAG_HEADER=True
    WRITE_MODE='w'
else:
    FLAG_HEADER=False
    WRITE_MODE='a'

AllStrDeviceIDs = DeviceDataDF['StrDeviceID'].unique()
NumStrDeviceIDs=len(AllStrDeviceIDs)

GapStatsID=1
DevIndex=1
for thisStrDeviceID in AllStrDeviceIDs:
    print ""
    print "Processing Online Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(DevID=thisStrDeviceID,index=DevIndex,total=NumStrDeviceIDs)

    # Get a sample signal from the ManyDeviceSignals object
    sample_device_readings = networksignals_device_signals.get_device_signal(thisStrDeviceID, READING_COL)
    day_segments = sample_device_readings.segment_daily()

    thisDeviceID=DeviceDataDF['DeviceID'][DeviceDataDF['StrDeviceID']==thisStrDeviceID].values[0]
    if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0])==float:
        thisDevice_StartDate=AnalysisWindow_StartDate
    else:
        thisDevice_StartDate=max(datetime.strptime(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0],'%Y-%m-%d %H:%M:%S.%f'),
                             AnalysisWindow_StartDate)
    thisDevice_EndDate=RunTimestamp
    thisDevice_AllDates=pd.date_range(start=datetime.date(thisDevice_StartDate),end=thisDevice_EndDate,freq='1D')
#     print thisDevice_AllDates
    GapStatsDF=pd.DataFrame()



    for thisSegment in day_segments:
        thisSegmentDF=pd.DataFrame({"GapStatsID":GapStatsID, "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": thisSegment.on_time, "Coverage": thisSegment.coverage, 
                                    "OnTime_OfDelivered": thisSegment.on_time_of_delivered, "WindowStart":thisSegment.min_time, "WindowEnd":thisSegment.max_time,
                                   # "FirstReadingDate":thisSegment.min_reading_time, "LastReadingDate":thisSegment.max_reading_time, 
                                    "NumDelays":thisSegment.num_delays, "MaxDelay_Minutes":round(thisSegment.max_delay_time/60,1),
                                    "NumGaps": thisSegment.num_gaps, "MaxGap_Minutes":round(thisSegment.max_gap_time/60,1), 
                                    "TotalGappage_Minutes":round(thisSegment.total_gap_time/60,1),"Online":thisSegment.online,
                                    "NumOfflineGaps": thisSegment.num_offline_periods, "MaxOfflineGap_Minutes":round(thisSegment.max_offline_period_time/60,1), 
                                    "TotalOfflineGappage_Minutes":round(thisSegment.total_offline_period_time/60,1),"DeviceID": thisDeviceID},index=[GapStatsID])

        GapStatsDF=GapStatsDF.append(thisSegmentDF)
        GapStatsID=GapStatsID+1
        
        thisDevice_AllDates=thisDevice_AllDates[thisDevice_AllDates!=str(datetime.date(thisSegment.min_time))]

    if len(thisDevice_AllDates)>0:
        for thisDate in thisDevice_AllDates:
            print "adding missing date: {} to device: {}".format(thisDate,thisDeviceID)
            thisSegmentDF=pd.DataFrame({"GapStatsID":GapStatsID, "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": 0, "Coverage": 0, 
                                        "OnTime_OfDelivered": 0, "WindowStart": str(thisDate), "WindowEnd":thisDate+timedelta(days=1),
                                        "FirstReadingDate":None, "LastReadingDate":None, 
                                        "NumDelays":0, "MaxDelay_Minutes":0,
                                        "NumGaps": 1, "MaxGap_Minutes":24*60, 
                                        "TotalGappage_Minutes":24*60,"Online":0,
                                        "NumOfflineGaps": 1, "MaxOfflineGap_Minutes":24*60, 
                                    "TotalOfflineGappage_Minutes":24*60,"DeviceID": thisDeviceID},index=[GapStatsID])
            GapStatsDF=GapStatsDF.append(thisSegmentDF)
            GapStatsID=GapStatsID+1
#     GapStatsDF=pd.merge(GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
#                     on='DeviceID',how='left')
    GapStatsDF.to_csv(DeviceReadingGapStatsResultsFile,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
    FLAG_HEADER=False
    WRITE_MODE='a'
    DevIndex=DevIndex+1

    
EndTime=datetime.now()
TimeDuration=EndTime-StartTime
print "Gateway Power Analysis Time: {TD}".format(TD=TimeDuration)


# In[13]:

StartTime=datetime.now()
print "Beginning Gateway Online analysis"

# TODO: change this value if you'd like to reset the table
RESET = True

DEVICE_ID_COL = 'StrDeviceID'
DATE_COL = 'processedDate'
READING_COL = 'readingValue'

SAMPLE_DEVICE = '11666000000117734806'

# database.init('test_ta_utils')
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com', port='5432')

DeviceDataDF_Full = pd.read_csv(ZigbeeDevicePowerDataFile)
DeviceDataDF=DeviceDataDF_Full[DeviceDataDF_Full['readingType']=='Power']
networksignals_device_signals = ManyDeviceSignals(DeviceDataDF, DEVICE_ID_COL, DATE_COL)

if RESET:
    print "Resetting"
    drop_tables()
    print "dropped tables"
    create_tables()
    print "created tables"
    print "reading from file {}".format(ZigbeeDevicePowerDataFile)
    DeviceDataDF_Full = pd.read_csv(ZigbeeDevicePowerDataFile)
    print "filtering data"
    DeviceDataDF=DeviceDataDF_Full[DeviceDataDF_Full['readingType']=='Power']
    print "creating ManyDeviceSignals object"
    networksignals_device_signals = ManyDeviceSignals(DeviceDataDF, DEVICE_ID_COL, DATE_COL)
    print "calling update_all_signals"
    networksignals_device_signals.update_all_signals(READING_COL)
    print "updated signals"
    FLAG_HEADER=True
    WRITE_MODE='w'
else:
    FLAG_HEADER=False
    WRITE_MODE='a'

AllStrDeviceIDs = DeviceDataDF['StrDeviceID'].unique()
NumStrDeviceIDs=len(AllStrDeviceIDs)

TransmissionGapStatsID=1
DevIndex=1

# for thisStrDeviceID in ['Device:99270113184909688731']:
for thisStrDeviceID in AllStrDeviceIDs:
    print ""
    print "Processing Online Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(DevID=thisStrDeviceID,index=DevIndex,total=NumStrDeviceIDs)

    # Get a sample signal from the ManyDeviceSignals object
    sample_device_readings = networksignals_device_signals.get_device_signal(thisStrDeviceID, READING_COL)
    day_segments = sample_device_readings.segment_daily()

    thisDeviceID=DeviceDataDF['DeviceID'][DeviceDataDF['StrDeviceID']==thisStrDeviceID].values[0]
    if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0])==float:
        thisDevice_StartDate=AnalysisWindow_StartDate
    else:
        thisDevice_StartDate=max(datetime.strptime(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0],'%Y-%m-%d %H:%M:%S.%f'),
                             AnalysisWindow_StartDate)
    thisDevice_EndDate=RunTimestamp
    thisDevice_AllDates=pd.date_range(start=datetime.date(thisDevice_StartDate),end=thisDevice_EndDate,freq='1D')
#     print thisDevice_AllDates
    TransmissionGapStatsDF=pd.DataFrame()



    
    for thisSegment in day_segments:
        thisSegmentDF=pd.DataFrame({"TransmissionGapStatsID":TransmissionGapStatsID, "StrDeviceID": thisStrDeviceID, 
                                    "WindowStart":thisSegment.min_time, "WindowEnd":thisSegment.max_time, "Online_Run2":thisSegment.coverage,
                                    "NumTransmissionGaps": thisSegment.num_gaps, "MaxTransmissionGap_Minutes":round(thisSegment.max_gap_time/60,1), 
                                    "TotalTransmissionGappage_Minutes":round(thisSegment.total_gap_time/60,1),"DeviceID": thisDeviceID},index=[TransmissionGapStatsID])
        TransmissionGapStatsDF=TransmissionGapStatsDF.append(thisSegmentDF)
        TransmissionGapStatsID=TransmissionGapStatsID+1
        thisDevice_AllDates=thisDevice_AllDates[thisDevice_AllDates!=str(datetime.date(thisSegment.min_time))]



    if len(thisDevice_AllDates)>0:
        for thisDate in thisDevice_AllDates:
            print "adding missing date: {}".format(thisDate)
            thisSegmentDF=pd.DataFrame({"TransmissionGapStatsID":TransmissionGapStatsID, "StrDeviceID": thisStrDeviceID, 
                                        "WindowStart": str(thisDate), "WindowEnd":thisDate+timedelta(days=1), "Online_Run2":0,
                                        "NumTransmissionGaps": 1, "MaxTransmissionGap_Minutes":24*60, 
                                    "TotalTransmissionGappage_Minutes":24*60,"DeviceID": thisDeviceID},index=[TransmissionGapStatsID])
            TransmissionGapStatsDF=TransmissionGapStatsDF.append(thisSegmentDF)
            TransmissionGapStatsID=TransmissionGapStatsID+1

#     TransmissionGapStatsDF=pd.merge(TransmissionGapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
#                     on='DeviceID',how='left')
    TransmissionGapStatsDF.to_csv(DeviceTransmissionGapStatsResultsFile,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
    FLAG_HEADER=False
    WRITE_MODE='a'
    DevIndex=DevIndex+1

    
EndTime=datetime.now()
TimeDuration=EndTime-StartTime
print "Online Analysis Time: {TD}".format(TD=TimeDuration)


# In[ ]:

##
##  Develop a group-level online metric
## Initial implementation is a basic version that is effective in single-gateway scenarios but could be inaccurate in multi-gateway scenarios
StartTime=datetime.now()
GapStatsDF=pd.read_csv(DeviceTransmissionGapStatsResultsFile,)
AllDates=pd.date_range(start=datetime.date(AnalysisWindow_StartDate),end=RunTimestamp,freq='1D')
AllStoreGroupIDs=Monitored_DevicesInfo['StoreGroupID'].unique()
GapStatsDF['WindowStart_DateOnly']=GapStatsDF['WindowStart'].apply(lambda x: x[0:10])

FLAG_Header =  True
WRITE_MODE='w'

GroupGWID=1
for thisGroupID in AllStoreGroupIDs:
    thisGroup_GatewaysInfo=DevicesInfo[(DevicesInfo['GroupID']==thisGroupID) & 
                                   ((DevicesInfo['HardwareFamily'] == 2) | (DevicesInfo['HardwareFamily'] == 3))]
    thisGroup_NumGateways=len(thisGroup_GatewaysInfo)
    thisGroup_Gateways=thisGroup_GatewaysInfo['DeviceID'].values
    print "processing group: {groupid} found {numgw} gateways: {gwids}".format(groupid=thisGroupID,
                                                                              numgw=thisGroup_NumGateways,
                                                                              gwids=str(thisGroup_Gateways))
    thisGroupGatewayGapStats=GapStatsDF[GapStatsDF['DeviceID'].isin(thisGroup_Gateways)]
#     print "thisGroupGatewayGapStats gap stats length: {len}".format(len=len(thisGroupGatewayGapStats))
    thisGroup_GWStatsDF=pd.DataFrame()
    for thisDate in AllDates:
        thisDateGatewayGapStats=thisGroupGatewayGapStats[thisGroupGatewayGapStats['WindowStart_DateOnly']==str(datetime.date(thisDate))]
        if len(thisDateGatewayGapStats)==0:
            thisDate_GWOnline=-1
        else:
            thisDate_GWOnline=max(thisDateGatewayGapStats['Online_Run2'])
        thisEntry=pd.DataFrame({'StoreGroupID':thisGroupID,
                                'AssessmentDate':thisDate,
                                'NumGateways':thisGroup_NumGateways,
                                'GatewayIDs':str(thisGroup_Gateways),
                                'GW_Online':thisDate_GWOnline,
                               'GroupGWID':GroupGWID},index=[GroupGWID])
        GroupGWID=GroupGWID+1
        thisGroup_GWStatsDF=thisGroup_GWStatsDF.append(thisEntry)
    thisGroup_GWStatsDF.to_csv(GroupGatewayOnlineStatsFile,index=False,header=FLAG_Header,mode=WRITE_MODE)
    FLAG_Header=False
    WRITE_MODE='a'
                                
EndTime=datetime.now()
TimeDuration=EndTime-StartTime
print "Group Power Analysis Time: {TD}".format(TD=TimeDuration)


# In[ ]:

##
##   Get the sensor data
##
if FLAG_PullData:
    StartTime=datetime.now()
    import json
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
    # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')
    print 'setting start time to {}'.format(AnalysisWindow_StartDate)

    RunTimestamp=datetime.now().date()

    FLAG_Header =  False
    WriteMode='a'
    NumMonitoredDevices=len(Monitored_DeviceIDs)


    TempPull_BadDevicesDF=pd.DataFrame()
    BadDevicesCount=1
    for DeviceIter in range(11617,NumMonitoredDevices):
#     for DeviceIter in range(0,NumMonitoredDevices):
        
        thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
#         thisDeviceID = 11666000000133509832
        print"Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisDeviceID,DeviceIter=DeviceIter,TotalDevices=NumMonitoredDevices)

        thisDeviceDataDF=pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
#         print thisDeviceDataDF
        if(len(thisDeviceDataDF)==0):
            print'No data found for device, skipping to next'
            TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No data found for device']}))
            BadDevicesCount=BadDevicesCount+1
            continue
        if('sensors' in thisDeviceDataDF.columns):
            print'No sensors for this device, skipping to next'
            TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No sensors for this device']}))
            BadDevicesCount=BadDevicesCount+1
            continue
        thisNumSensors=len(thisDeviceDataDF.port)
        for SensorIndex in range(0,thisNumSensors):
#             print "SensorIndex: {index}  of {total}".format(index=SensorIndex,total=thisNumSensors)
            thisPortID = thisDeviceDataDF.port[SensorIndex]
            print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID,index=SensorIndex,total=thisNumSensors)
            thisSensorDataDF=pd.DataFrame(thisClient.get_sensor_readings_by_port_id(thisDeviceID,thisPortID,reading_start_dt=AnalysisWindow_StartDate))
            if('processedDate' not in thisSensorDataDF.columns):
                print "no processed date in data returned, skipping"
                TempPull_BadDevicesDF=TempPull_BadDevicesDF.append(pd.DataFrame({'BadDeviceIndex':[BadDevicesCount],'DeviceID':[thisDeviceID],'ProcessingNote':['No processed date in data returned']}))
                BadDevicesCount=BadDevicesCount+1
                continue
    #         print type(thisSensorDataDF['processedDate'][0])
            thisSensorDataDF['processedDate']=thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
            thisSensorDataDF['readingDate']=thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))

            thisSensorDataDF['age']=(pd.to_datetime(thisSensorDataDF['processedDate'])-pd.to_datetime(thisSensorDataDF['readingDate'])).astype('timedelta64[s]')
            thisSensorDataDF['port']=thisPortID
            thisSensorDataDF['DeviceID']=thisDeviceID
            thisSensorDataDF['PortID']=thisPortID
            thisSensorDataDF['StrDeviceID']="Device:"+str(thisDeviceID)+":"+str(thisPortID)

            thisSensorDataDF.drop('alarm',axis=1,inplace=True)

            print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisSensorDataDF))
    #         print"writing to file: {Outfile}".format(Outfile=ZigbeeSensorInfoDataFile)
            thisSensorDataDF.to_csv(ZigbeeSensorInfoDataFile,header=FLAG_Header,mode=WriteMode,index=False)
    #         print"done with write"
            FLAG_Header=False
            WriteMode='a'

    EndTime=datetime.now()
    if len(TempPull_BadDevicesDF)>0:
        TempPull_BadDevicesDF=pd.merge(TempPull_BadDevicesDF,DevicesInfo[GapStats_DeviceInfoFields],
                                       on=['DeviceID'],how='left')
        TempPull_BadDevicesDF.to_csv(TemperaturePull_BadDevicesFile,index=False)

    TimeDuration=EndTime-StartTime
    print "Data Pull Time: {TD}".format(TD=TimeDuration)


# In[ ]:

# StartTime=datetime.now()

# print "Beginning coverage analysis"
# # TODO: change this value if you'd like to reset the table
# RESET = True

# DEVICE_ID_COL = 'StrDeviceID'
# DATE_COL = 'readingDate'
# READING_COL = 'readingValue'
# PROCESSED_COL = 'processedDate'

# SAMPLE_DEVICE = '11666000000117734806'
# FLAG_HEADER=True
# WRITE_MODE='w'

# SensorDataDF = pd.read_csv(ZigbeeSensorInfoDataFile)
# # SensorDataDF=SensorDataDF[0:20000]
# print "SensorDataDF length: {}".format(len(SensorDataDF))
# print "Read SensorDataDF"
# # networksignals_device_signals = ManyDeviceSignals(SensorDataDF, DEVICE_ID_COL, DATE_COL, processed_date_col=PROCESSED_COL)
# # print "Loaded networksignals_device_signals"

# SensorDataDF=pd.merge(SensorDataDF,DevicesInfo[['DeviceID','GroupID']],on=['DeviceID'],how='left')
# AllGroupIDs=SensorDataDF['GroupID'].unique()
# NumGroups=len(AllGroupIDs)


# ####  These worked
# # database.init('test_ta_utils',)
# # database.init('Local_DataWarehouse',)

# ####  These have not been proven yet
# # database.init("host='localhost' dbname='Local_DataWarehouse'",)
# # database.init("host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com:5432' dbname='warehouse'",user='jason',password='qvz2LFRzMMVPku7')
# print "calling database.init"
# # database.init("dbname='warehouse', user='jason2', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com',port='5432'")
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com', port='5432')

# # database.init('warehouse')
# print "database.init complete"
# if RESET:
#     drop_tables()
#     print 'dropped tables'
#     create_tables()
#     print 'created tables'
# #     networksignals_device_signals.update_all_signals(READING_COL)
# #     print 'updated signals'


GapStatsID=1
GapStatsID=9729001
DevIndex=1
DevIndex=9729
for GroupIndex in range(883,NumGroups):
    thisGroupID=AllGroupIDs[GroupIndex]
    print "processing coverage for group {GID}, number {iter} of {total}".format(GID=thisGroupID,iter=GroupIndex,total=NumGroups)
    thisGroupSensorDataDF=SensorDataDF[SensorDataDF['GroupID']==thisGroupID]

    networksignals_device_signals = ManyDeviceSignals(thisGroupSensorDataDF, DEVICE_ID_COL, DATE_COL, processed_date_col=PROCESSED_COL)                                                  
    print "Loaded networksignals_device_signals"
    networksignals_device_signals.update_all_signals(READING_COL)
    print 'updated signals'

    AllStrDeviceIDs = thisGroupSensorDataDF['StrDeviceID'].unique()
    NumStrDeviceIDs=len(AllStrDeviceIDs)

    for thisStrDeviceID in AllStrDeviceIDs:
        print ""
        print "Processing Coverage Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(DevID=thisStrDeviceID,index=DevIndex,total=NumStrDeviceIDs)

        # Get a sample signal from the ManyDeviceSignals object
        sample_device_readings = networksignals_device_signals.get_device_signal(thisStrDeviceID, READING_COL)
        day_segments = sample_device_readings.segment_daily()

        thisDeviceID=SensorDataDF['DeviceID'][SensorDataDF['StrDeviceID']==thisStrDeviceID].values[0]
        if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0])==float:
            thisDevice_StartDate=AnalysisWindow_StartDate
        else:
            thisDevice_StartDate=max(datetime.strptime(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0],'%Y-%m-%d %H:%M:%S.%f'),
                                 AnalysisWindow_StartDate)
        thisDevice_EndDate=RunTimestamp
        thisDevice_AllDates=pd.date_range(start=datetime.date(thisDevice_StartDate),end=thisDevice_EndDate,freq='1D')


        GapStatsDF=pd.DataFrame()
        for thisSegment in day_segments:
            thisSegmentDF=pd.DataFrame({"GapStatsID":GapStatsID, "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": thisSegment.on_time, "Coverage": thisSegment.coverage, 
                                        "OnTime_OfDelivered": thisSegment.on_time_of_delivered, "WindowStart":thisSegment.min_time, "WindowEnd":thisSegment.max_time,
                                        "FirstReadingDate":thisSegment.min_reading_time, "LastReadingDate":thisSegment.max_reading_time, 
                                        "NumDelays":thisSegment.num_delays, "MaxDelay_Minutes":round(thisSegment.max_delay_time/60,1),
                                        "NumGaps": thisSegment.num_gaps, "MaxGap_Minutes":round(thisSegment.max_gap_time/60,1), 
                                        "TotalGappage_Minutes":round(thisSegment.total_gap_time/60,1),"Online":thisSegment.online,
                                        "NumOfflineGaps": thisSegment.num_offline_periods, "MaxOfflineGap_Minutes":round(thisSegment.max_offline_period_time/60,1), 
                                        "TotalOfflineGappage_Minutes":round(thisSegment.total_offline_period_time/60,1),"DeviceID": thisDeviceID},index=[GapStatsID])
            GapStatsDF=GapStatsDF.append(thisSegmentDF)
            GapStatsID=GapStatsID+1

            thisDevice_AllDates=thisDevice_AllDates[thisDevice_AllDates!=str(datetime.date(thisSegment.min_time))]
        if len(thisDevice_AllDates)>0:
            for thisDate in thisDevice_AllDates:
                print "adding missing date: {} to device: {}".format(thisDate,thisDeviceID)
                thisSegmentDF=pd.DataFrame({"GapStatsID":GapStatsID, "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": 0, "Coverage": 0, 
                                            "OnTime_OfDelivered": 0, "WindowStart": str(thisDate), "WindowEnd":thisDate+timedelta(days=1),
                                            "FirstReadingDate":None, "LastReadingDate":None, 
                                            "NumDelays":0, "MaxDelay_Minutes":0,
                                            "NumGaps": 1, "MaxGap_Minutes":24*60, 
                                            "TotalGappage_Minutes":24*60,"Online":0,
                                            "NumOfflineGaps": 1, "MaxOfflineGap_Minutes":24*60, 
                                        "TotalOfflineGappage_Minutes":24*60,"DeviceID": thisDeviceID},index=[GapStatsID])
                GapStatsDF=GapStatsDF.append(thisSegmentDF)
                GapStatsID=GapStatsID+1
    #     GapStatsDF=pd.merge(GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
    #                     on='DeviceID',how='left')
        GapStatsDF.to_csv(SensorReadingGapStatsResultsFile,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
        WRITE_MODE='a'
        FLAG_HEADER=False
        DevIndex=DevIndex+1

EndTime=datetime.now()
TimeDuration=EndTime-StartTime
print "Coverage Analysis Time: {TD}".format(TD=TimeDuration)


# In[ ]:

StartTime=datetime.now()
print "Beginning Online analysis"

# TODO: change this value if you'd like to reset the table
RESET = True

DEVICE_ID_COL = 'StrDeviceID'
DATE_COL = 'processedDate'
READING_COL = 'readingValue'

SAMPLE_DEVICE = '11666000000117734806'

# database.init('test_ta_utils')
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com', port='5432')
if RESET:
    drop_tables()
    create_tables()
    FLAG_HEADER=True
    WRITE_MODE='w'
    SensorDataDF = pd.read_csv(ZigbeeSensorInfoDataFile)
    SensorDataDF = pd.merge(SensorDataDF,DevicesInfo[['DeviceID','GroupID']],on=['DeviceID'],how='left')
else:
    FLAG_HEADER=False
    WRITE_MODE='a'

####     networksignals_device_signals.update_all_signals(READING_COL)

#### SensorDataDF=SensorDataDF[0:20000]
print "SensorDataDF length: {}".format(len(SensorDataDF))

###  For a small batch of signals, can do everything at once - switched later to small groups for recoverability
# networksignals_device_signals = ManyDeviceSignals(SensorDataDF, DEVICE_ID_COL, DATE_COL)

TransmissionGapStatsID=806001
DevIndex=1


AllGroupIDs=SensorDataDF['GroupID'].unique()
NumGroups=len(AllGroupIDs)

for GroupIndex in range(806,NumGroups):
    thisGroupID=AllGroupIDs[GroupIndex]
    print "processing coverage for group {GID}, number {iter} of {total}".format(GID=thisGroupID,iter=GroupIndex,total=NumGroups)
    thisGroupSensorDataDF=SensorDataDF[SensorDataDF['GroupID']==thisGroupID]

    networksignals_device_signals = ManyDeviceSignals(thisGroupSensorDataDF, DEVICE_ID_COL, DATE_COL)                                                  
    print "Loaded networksignals_device_signals"
    networksignals_device_signals.update_all_signals(READING_COL)
    print "Updated signals"
    
#     AllStrDeviceIDs = SensorDataDF['StrDeviceID'].unique()
#     NumStrDeviceIDs=len(AllStrDeviceIDs)
    AllStrDeviceIDs = thisGroupSensorDataDF['StrDeviceID'].unique()
    NumStrDeviceIDs=len(AllStrDeviceIDs)


    for thisStrDeviceID in AllStrDeviceIDs:
        print ""
        print "Processing Online Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(DevID=thisStrDeviceID,index=DevIndex,total=NumStrDeviceIDs)

        # Get a sample signal from the ManyDeviceSignals object
        sample_device_readings = networksignals_device_signals.get_device_signal(thisStrDeviceID, READING_COL)
        day_segments = sample_device_readings.segment_daily()

        thisDeviceID=SensorDataDF['DeviceID'][SensorDataDF['StrDeviceID']==thisStrDeviceID].values[0]
        if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0])==float:
            thisDevice_StartDate=AnalysisWindow_StartDate
        else:
            thisDevice_StartDate=max(datetime.strptime(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID']==thisDeviceID].values[0],'%Y-%m-%d %H:%M:%S.%f'),
                                 AnalysisWindow_StartDate)
        thisDevice_EndDate=RunTimestamp
        thisDevice_AllDates=pd.date_range(start=datetime.date(thisDevice_StartDate),end=thisDevice_EndDate,freq='1D')
    #     print thisDevice_AllDates
        TransmissionGapStatsDF=pd.DataFrame()




        for thisSegment in day_segments:
            thisSegmentDF=pd.DataFrame({"TransmissionGapStatsID":TransmissionGapStatsID, "StrDeviceID": thisStrDeviceID, 
                                        "WindowStart":thisSegment.min_time, "WindowEnd":thisSegment.max_time, "Online_Run2":thisSegment.coverage,
                                        "NumTransmissionGaps": thisSegment.num_gaps, "MaxTransmissionGap_Minutes":round(thisSegment.max_gap_time/60,1), 
                                        "TotalTransmissionGappage_Minutes":round(thisSegment.total_gap_time/60,1),"DeviceID": thisDeviceID},index=[TransmissionGapStatsID])
            TransmissionGapStatsDF=TransmissionGapStatsDF.append(thisSegmentDF)
            TransmissionGapStatsID=TransmissionGapStatsID+1
            thisDevice_AllDates=thisDevice_AllDates[thisDevice_AllDates!=str(datetime.date(thisSegment.min_time))]



        if len(thisDevice_AllDates)>0:
            for thisDate in thisDevice_AllDates:
                print "adding missing date: {}".format(thisDate)
                thisSegmentDF=pd.DataFrame({"TransmissionGapStatsID":TransmissionGapStatsID, "StrDeviceID": thisStrDeviceID, 
                                            "WindowStart": str(thisDate), "WindowEnd":thisDate+timedelta(days=1), "Online_Run2":0,
                                            "NumTransmissionGaps": 1, "MaxTransmissionGap_Minutes":24*60, 
                                        "TotalTransmissionGappage_Minutes":24*60,"DeviceID": thisDeviceID},index=[TransmissionGapStatsID])
                TransmissionGapStatsDF=TransmissionGapStatsDF.append(thisSegmentDF)
                TransmissionGapStatsID=TransmissionGapStatsID+1

    #     TransmissionGapStatsDF=pd.merge(TransmissionGapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
    #                     on='DeviceID',how='left')
        TransmissionGapStatsDF.to_csv(SensorTransmissionGapStatsResultsFile,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
        FLAG_HEADER=False
        WRITE_MODE='a'
        DevIndex=DevIndex+1

    
EndTime=datetime.now()
TimeDuration=EndTime-StartTime
print "Online Analysis Time: {TD}".format(TD=TimeDuration)


# In[ ]:

##
##       Final packaging and polishing
##

def GenBasicDailyStats(GapStatsDF,NumDays,AnchorDate=None):
    if AnchorDate is None:
        print max(GapStatsDF['WindowStart'])
        AnchorDate=max(GapStatsDF['WindowStart'])
        print "AnchorDate: {}".format(AnchorDate)
    MinAllowedDate=str(datetime.strptime(AnchorDate,'%Y-%m-%d %H:%M:%S')-timedelta(days=NumDays-1))
    print"GenBasicDailyStats():: called with GapStats having {NumRows} rows and using AnchorDate: {AnchorDate} and cutoff date: {Cutoff}".format(NumRows=len(GapStatsDF),
                                                                                                                       AnchorDate=AnchorDate,
                                                                                                                      Cutoff=MinAllowedDate)
    ##  Filter by date
    RecentGapStatsDF=GapStatsDF[GapStatsDF['WindowStart']>=MinAllowedDate]
    print "RecentGapStatsDF num rows: {NumRows}".format(NumRows=len(RecentGapStatsDF))
    ##  Means
    GapSummaryStatsDF_Means=RecentGapStatsDF.groupby(['StrDeviceID']).mean()
    GapSummaryStatsDF_Means.columns=GapSummaryStatsDF_Means.columns+"_Mean_{NumDays}Day".format(NumDays=NumDays)
    GapSummaryStatsDF_Means.reset_index(inplace=True,level=0)
    ##  Means
    GapSummaryStatsDF_StDev=RecentGapStatsDF.groupby(['StrDeviceID']).std()
    GapSummaryStatsDF_StDev.columns=GapSummaryStatsDF_StDev.columns+"_StDev_{NumDays}Day".format(NumDays=NumDays)
    GapSummaryStatsDF_StDev.reset_index(inplace=True,level=0)
    ##  Sums
    GapSummaryStatsDF_Sums=RecentGapStatsDF.groupby(['StrDeviceID']).sum()
    GapSummaryStatsDF_Sums.columns=GapSummaryStatsDF_Sums.columns+"_Sum_{NumDays}Day".format(NumDays=NumDays)
    GapSummaryStatsDF_Sums.reset_index(inplace=True,level=0)
    
    CompositeGapSummaryStatsDF=pd.merge(GapSummaryStatsDF_Sums,GapSummaryStatsDF_Means,on=['StrDeviceID'],how='outer')
    CompositeGapSummaryStatsDF=pd.merge(CompositeGapSummaryStatsDF,GapSummaryStatsDF_StDev,on=['StrDeviceID'],how='outer')
    return CompositeGapSummaryStatsDF

def GenLastDayDF(GapStatsDF,AnchorDate=None):
    if AnchorDate is None:
        AnchorDate=max(GapStatsDF.WindowStart)
    LastDayDF=GapStatsDF[GapStatsDF['WindowStart']==AnchorDate]
    LastDayDF.columns=LastDayDF.columns+"_LastDay"
    LastDayDF=LastDayDF.rename(columns={'StrDeviceID_LastDay':'StrDeviceID'})
    return LastDayDF


##  Sensor reading merge of coverage and online
SensorTransmissionGapStatsDF=pd.read_csv(SensorTransmissionGapStatsResultsFile)
SensorTransmissionGapStatsDF['AssessmentDate']=SensorTransmissionGapStatsDF['WindowStart'].apply(lambda x: x[0:10])
print "SensorTransmissionGapStatsDF:  NumRows = {NumRows}".format(NumRows=len(SensorTransmissionGapStatsDF))
SensorReadingGapStatsDF=pd.read_csv(SensorReadingGapStatsResultsFile)
SensorReadingGapStatsDF['AssessmentDate']=SensorReadingGapStatsDF['WindowStart'].apply(lambda x: x[0:10])
print "SensorReadingGapStatsDF:  NumRows = {NumRows}".format(NumRows=len(SensorReadingGapStatsDF))
FullSensorGapStats=pd.merge(SensorReadingGapStatsDF,SensorTransmissionGapStatsDF,on=['StrDeviceID','AssessmentDate','DeviceID'],how='outer')
print "FullSensorGapStats:  NumRows = {NumRows}".format(NumRows=len(FullSensorGapStats))
FullSensorGapStats.to_csv('/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_FullSensorGapStats.csv',index=False)

##  Device reading merge of coverage and online
DeviceTransmissionGapStatsDF=pd.read_csv(DeviceTransmissionGapStatsResultsFile)
DeviceTransmissionGapStatsDF['AssessmentDate']=DeviceTransmissionGapStatsDF['WindowStart'].apply(lambda x: x[0:10])
print "DeviceTransmissionGapStatsDF:  NumRows = {NumRows}".format(NumRows=len(DeviceTransmissionGapStatsDF))
DeviceReadingGapStatsDF=pd.read_csv(DeviceReadingGapStatsResultsFile)
DeviceReadingGapStatsDF['AssessmentDate']=DeviceReadingGapStatsDF['WindowStart'].apply(lambda x: x[0:10])
print "DeviceReadingGapStatsDF:  NumRows = {NumRows}".format(NumRows=len(DeviceReadingGapStatsDF))
FullDeviceGapStats=pd.merge(DeviceReadingGapStatsDF,DeviceTransmissionGapStatsDF,on=['StrDeviceID','AssessmentDate','DeviceID'])
print "FullDeviceGapStats:  NumRows = {NumRows}".format(NumRows=len(FullDeviceGapStats))

##  Group-level online status
GroupGatewayOnlineStatsDF=pd.read_csv(GroupGatewayOnlineStatsFile)
print "GroupGatewayOnlineStatsDF:  NumRows = {NumRows}".format(NumRows=len(GroupGatewayOnlineStatsDF))

##  Combine gap info across sensors and devices.  concat used to allow different columns from the sources
SensorAndDevice_GapStatsDF=pd.concat([FullSensorGapStats,FullDeviceGapStats], axis=0, ignore_index=True)
print "SensorAndDevice_GapStatsDF:  NumRows = {NumRows}".format(NumRows=len(SensorAndDevice_GapStatsDF))

##  Augment with Device Metadata 
SensorAndDevice_GapStatsDF=pd.merge(SensorAndDevice_GapStatsDF,Monitored_DevicesInfo[['DeviceID','StoreGroupID']],on=['DeviceID'],how='left')
AugSensorAndDevice_GapStatsDF=pd.merge(SensorAndDevice_GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
                                       on=['DeviceID'],how='left')
AugSensorAndDevice_GapStatsDF=pd.merge(AugSensorAndDevice_GapStatsDF,GroupGatewayOnlineStatsDF,on=['StoreGroupID','AssessmentDate'],how='left')
AugSensorAndDevice_GapStatsDF['Online_GroupAdjusted_Raw']=AugSensorAndDevice_GapStatsDF['Online_Run2']/AugSensorAndDevice_GapStatsDF['GW_Online']
AugSensorAndDevice_GapStatsDF['Online_GroupAdjusted']=AugSensorAndDevice_GapStatsDF['Online_GroupAdjusted_Raw'].apply(lambda x: min(1,x))
AugSensorAndDevice_GapStatsDF['WindowStart']=AugSensorAndDevice_GapStatsDF['WindowStart_x']
# NanIndices=pd.where(np.isnan(AugSensorAndDevice_GapStatsDF['WindowStart']))


##  Add some recent data summary/info
RecentWindow_DailySummaryStatsDF=GenBasicDailyStats(AugSensorAndDevice_GapStatsDF[['StrDeviceID','Online_GroupAdjusted','Coverage','WindowStart']],4)
Days45Window_DailySummaryStatsDF=GenBasicDailyStats(AugSensorAndDevice_GapStatsDF[['StrDeviceID','Online_GroupAdjusted','Coverage','WindowStart']],AnalysisWindowInDays)
WindowedStatsDF=pd.merge(RecentWindow_DailySummaryStatsDF,Days45Window_DailySummaryStatsDF,on=['StrDeviceID'])
YesterdayDF=GenLastDayDF(AugSensorAndDevice_GapStatsDF[['StrDeviceID','Online','Coverage','WindowStart']])
DeviceRecentGapStats=pd.merge(YesterdayDF,WindowedStatsDF,on=['StrDeviceID'],how='outer')
AugSensorAndDevice_GapStatsDF=pd.merge(AugSensorAndDevice_GapStatsDF,DeviceRecentGapStats,on=['StrDeviceID'],how='left')
AugSensorAndDevice_GapStatsDF.to_csv(GapStatsResultsFile,index=False)

##  Final output save
GapStatsResultsFileCustom="/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_DataFlowGaps.csv".format(DataTag)
AugSensorAndDevice_GapStatsDF.to_csv(GapStatsResultsFileCustom,index=False)





# In[ ]:

# def GenBasicDailyStats(GapStatsDF,NumDays,AnchorDate=None):
#     if AnchorDate is None:
#         AnchorDate=max(GapStatsDF.WindowStart)
#     MinAllowedDate=str(datetime.strptime(AnchorDate,'%Y-%m-%d %H:%M:%S')-timedelta(days=NumDays-1))
#     print"GenBasicDailyStats():: called with GapStats having {NumRows} rows and using AnchorDate: {AnchorDate} and cutoff date: {Cutoff}".format(NumRows=len(GapStatsDF),
#                                                                                                                        AnchorDate=AnchorDate,
#                                                                                                                       Cutoff=MinAllowedDate)
#     ##  Filter by date
#     RecentGapStatsDF=GapStatsDF[GapStatsDF['WindowStart']>=MinAllowedDate]
#     print "RecentGapStatsDF num rows: {NumRows}".format(NumRows=len(RecentGapStatsDF))
#     ##  Means
#     GapSummaryStatsDF_Means=RecentGapStatsDF.groupby(['StrDeviceID']).mean()
#     GapSummaryStatsDF_Means.columns=GapSummaryStatsDF_Means.columns+"_Mean_{NumDays}Day".format(NumDays=NumDays)
#     GapSummaryStatsDF_Means.reset_index(inplace=True,level=0)
#     ##  Means
#     GapSummaryStatsDF_StDev=RecentGapStatsDF.groupby(['StrDeviceID']).std()
#     GapSummaryStatsDF_StDev.columns=GapSummaryStatsDF_StDev.columns+"_StDev_{NumDays}Day".format(NumDays=NumDays)
#     GapSummaryStatsDF_StDev.reset_index(inplace=True,level=0)
#     ##  Sums
#     GapSummaryStatsDF_Sums=RecentGapStatsDF.groupby(['StrDeviceID']).sum()
#     GapSummaryStatsDF_Sums.columns=GapSummaryStatsDF_Sums.columns+"_Sum_{NumDays}Day".format(NumDays=NumDays)
#     GapSummaryStatsDF_Sums.reset_index(inplace=True,level=0)
    
#     CompositeGapSummaryStatsDF=pd.merge(GapSummaryStatsDF_Sums,GapSummaryStatsDF_Means,on=['StrDeviceID'],how='outer')
#     CompositeGapSummaryStatsDF=pd.merge(CompositeGapSummaryStatsDF,GapSummaryStatsDF_StDev,on=['StrDeviceID'],how='outer')
#     return CompositeGapSummaryStatsDF

# def GenLastDayDF(GapStatsDF,AnchorDate=None):
#     if AnchorDate is None:
#         AnchorDate=max(GapStatsDF.WindowStart)
#     LastDayDF=GapStatsDF[GapStatsDF['WindowStart']==AnchorDate]
#     LastDayDF.columns=LastDayDF.columns+"_LastDay"
#     LastDayDF=LastDayDF.rename(columns={'StrDeviceID_LastDay':'StrDeviceID'})
#     return LastDayDF


# RecentWindowDailySummaryStatsDF=GenBasicDailyStats(AugSensorAndDevice_GapStatsDF[['StrDeviceID','Online','Coverage','WindowStart']],4)
# YesterdayDF=GenLastDayDF(AugSensorAndDevice_GapStatsDF[['StrDeviceID','Online','Coverage','WindowStart']])
# DeviceRecentGapStats=pd.merge(YesterdayDF,RecentWindowDailySummaryStatsDF,on=['StrDeviceID'],how='outer')
# DeviceRecentGapStatsFile="/Users/Jason/Desktop/Results_GapMetrics/TrundleMonitoring_{}_DeviceRecentGapSummary.csv".format(DataTag)
# DeviceRecentGapStats.to_csv(DeviceRecentGapStatsFile,index=False)
# DeviceRecentGapStats.head()


##  Unit tests
# Setting anchor date is effective


# In[ ]:

import psycopg2
import sys
import pprint

def testconnect():
    #Define our connection string
    conn_string = "host='localhost' dbname='Local_DataWarehouse' "
###     Web example
    print "testing psycopg2.connect call"
    conn = psycopg2.connect(database='warehouse', user='jason', password='qvz2LFRzMMVPku7', host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com',
                            port='5432', sslmode='require')
    print "connect test returned: {}".format(conn)
    cursor=conn.cursor()
#     cursor.execute("create table TestTable(tableid int,entrytime timestamp,primary key (tableid));")
    cursor.execute("select * from  TestTable;")
    conn.commit()
    print "cursor table create executed"
#     conn_string = "host='analytics-datawarehouse.cboe3jk7dgeq.us-east-2.rds.amazonaws.com' dbname='warehouse' "
 
#     # print the connection string we will use to connect
#     print "Connecting to database\n	->%s" % (conn_string)
 
#     # get a connection, if a connect cannot be made an exception will be raised here
#     conn = psycopg2.connect(conn_string)
 
#     # conn.cursor will return a cursor object, you can use this cursor to perform queries
#     cursor = conn.cursor()
#     print "Connected!\n"
 
testconnect()


# conn_string = "host='localhost' dbname='Local_DataWarehouse' "
# # print the connection string we will use to connect
# print "Connecting to database\n	->%s" % (conn_string)
 
# # get a connection, if a connect cannot be made an exception will be raised here
# conn = psycopg2.connect(conn_string)
 
# # conn.cursor will return a cursor object, you can use this cursor to perform queries
# cursor = conn.cursor()
 
# # execute our Query
# cursor.execute("SELECT * FROM device_channel_history")
 
# # retrieve the records from the database
# records = cursor.fetchall()
 
# # print out the records using pretty print
# # note that the NAMES of the columns are not shown, instead just indexes.
# # for most people this isn't very useful so we'll show you how to return
# # columns as a dictionary (hash) in the next example.
# pprint.pprint(records)


# In[ ]:



