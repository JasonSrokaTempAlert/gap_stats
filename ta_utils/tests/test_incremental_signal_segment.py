"""
Test Signal Module
"""

import unittest
import logging
from datetime import datetime, timedelta
from subprocess import call

import pandas as pd

from ta_utils import create_tables, drop_tables
from ta_utils import database
from ta_utils import Signal
from ta_utils import PersistableSignalSegment
from ta_utils import IncrementalSignal

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

now = datetime.now()

REPORTING_INTERVAL_OVERRIDE = 60 * 2

dates = [now + timedelta(days=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]
processed_dates = [now + timedelta(days=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 26]]
signal = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                processed_dates=pd.Series(data=processed_dates, index=dates),
                device_id='test_signal')

first_dates = [now + timedelta(days=i) for i in [0, 1, 3, 5, 10]]
first_processed_dates = [now + timedelta(days=i) for i in [0, 1, 3, 5, 10]]
first_signal = Signal(pd.Series(data=[0., 1., 2., 3., 3.], index=first_dates),
                      processed_dates=pd.Series(data=first_processed_dates, index=first_dates),
                      device_id='test_signal')

second_dates = [now + timedelta(days=i) for i in [12, 14, 20, 22]]
second_processed_dates = [now + timedelta(days=i) for i in [12, 14, 20, 26]]
second_signal = Signal(pd.Series(data=[4., 2., 1., 0.], index=second_dates),
                       processed_dates=pd.Series(data=second_processed_dates, index=second_dates),
                       device_id='test_signal')

START_MULTIPLE_DAYS = datetime(year=2017, month=1, day=1)
multiple_days_dates = [START_MULTIPLE_DAYS + timedelta(hours=i) for i in range(0, 26)]
multiple_days_signal = Signal(pd.Series(data=range(0, 26), index=multiple_days_dates),
                              processed_dates=pd.Series(data=multiple_days_dates, index=multiple_days_dates),
                              device_id='test_signal')

multiple_days_signal_first = multiple_days_signal[:24]
multiple_days_signal_second = multiple_days_signal[24:]


call(['createdb', 'test_ta_utils'])
database.init('test_ta_utils')


class IncrementalSignalTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_multiple_day_incremental_signal_segment_save(self):
        first_persisted_signal = multiple_days_signal_first.save()

        for daily_segment in multiple_days_signal_first.segment_daily():
            daily_segment.save()

        final_segment = (PersistableSignalSegment
                         .select()
                         .where((PersistableSignalSegment.signal == first_persisted_signal)
                                & PersistableSignalSegment.final_segment)
                         .get())

        old_final_segment_start = final_segment.start_time

        self.assertEqual(final_segment.end_time, first_persisted_signal.max_time)

        incremental_signal = IncrementalSignal(first_persisted_signal, multiple_days_signal_second)
        incremental_signal.save()

        segments_count = (PersistableSignalSegment
                          .select()
                          .where(PersistableSignalSegment.signal == first_persisted_signal)
                          .count())

        self.assertEqual(segments_count, 2)

        new_final_segment = (PersistableSignalSegment
                             .select()
                             .where((PersistableSignalSegment.signal == first_persisted_signal)
                                    & PersistableSignalSegment.final_segment)
                             .get())

        self.assertEqual(new_final_segment.start_time, START_MULTIPLE_DAYS + timedelta(days=1))
        self.assertEqual(new_final_segment.end_time, multiple_days_signal_second.max_time)

        new_first_segment = (PersistableSignalSegment
                             .select()
                             .where((PersistableSignalSegment.signal == first_persisted_signal)
                                    & (PersistableSignalSegment.start_time == old_final_segment_start))
                             .get())

        self.assertEqual(new_first_segment.start_time, START_MULTIPLE_DAYS)
        self.assertEqual(new_first_segment.end_time, START_MULTIPLE_DAYS + timedelta(days=1))

    def test_incremental_signal_segment_save(self):
        first_persisted_signal = first_signal.save()

        for daily_segment in first_signal.segment_daily():
            daily_segment.save()

        after_5_days_segment = first_signal.segment_at(segment_type='after_5_days',
                                                       start_time=(now + timedelta(days=5)))
        after_5_days_segment.save()

        before_14_days_segment = first_signal.segment_at(segment_type='before_14_days',
                                                         end_time=(now + timedelta(days=14)))
        before_14_days_segment.save()

        empty_signal_segment = first_signal.segment_at(segment_type='after_100_days',
                                                       start_time=(now + timedelta(days=100)))
        empty_signal_segment.save()

        empty_signal_segment2 = first_signal.segment_at(segment_type='before_-1_days',
                                                        end_time=(now - timedelta(days=1)))
        empty_signal_segment2.save()

        incremental_signal = IncrementalSignal(first_persisted_signal, second_signal)
        persisted_incremental_signal = incremental_signal.save()

        self.assertEqual(signal.device_id, persisted_incremental_signal.device_id)
        self.assertEqual(signal.port, persisted_incremental_signal.port)
        self.assertEqual(signal.data_type, persisted_incremental_signal.data_type)
        self.assertEqual(signal.count, persisted_incremental_signal.count)
        self.assertEqual(signal.mean, persisted_incremental_signal.mean)
        self.assertAlmostEqual(signal.std, persisted_incremental_signal.std)
        self.assertAlmostEqual(signal.var, persisted_incremental_signal.var)
        self.assertEqual(signal.min, persisted_incremental_signal.min)
        self.assertEqual(signal.max, persisted_incremental_signal.max)
        self.assertEqual(signal.min_time, persisted_incremental_signal.min_time)
        self.assertEqual(signal.max_time, persisted_incremental_signal.max_time)
        self.assertEqual(signal.min_reading_time, persisted_incremental_signal.min_reading_time)
        self.assertEqual(signal.max_reading_time, persisted_incremental_signal.max_reading_time)
        self.assertEqual(signal.min_processed_time, persisted_incremental_signal.min_processed_time)
        self.assertEqual(signal.max_processed_time, persisted_incremental_signal.max_processed_time)
        self.assertEqual(signal.mode_minute_interval, persisted_incremental_signal.mode_minute_interval)

        self.assertEqual(signal.num_gaps, persisted_incremental_signal.num_gaps)
        self.assertEqual(signal.total_gap_time, persisted_incremental_signal.total_gap_time)
        self.assertEqual(signal.max_gap_time, persisted_incremental_signal.max_gap_time)
        self.assertEqual(signal.min_gap_time, persisted_incremental_signal.min_gap_time)
        self.assertEqual(signal.coverage, persisted_incremental_signal.coverage)
        self.assertEqual(signal.on_time, persisted_incremental_signal.on_time)

        self.assertEqual(signal.num_delays, persisted_incremental_signal.num_delays)
        self.assertEqual(signal.total_delay_time, persisted_incremental_signal.total_delay_time)
        self.assertEqual(signal.max_delay_time, persisted_incremental_signal.max_delay_time)
        self.assertEqual(signal.min_delay_time, persisted_incremental_signal.min_delay_time)

        self.assertEqual(signal.num_offline_periods, persisted_incremental_signal.num_offline_periods)
        self.assertEqual(signal.total_offline_period_time, persisted_incremental_signal.total_offline_period_time)
        self.assertEqual(signal.max_offline_period_time, persisted_incremental_signal.max_offline_period_time)
        self.assertEqual(signal.min_offline_period_time, persisted_incremental_signal.min_offline_period_time)

        num_daily_segments = (PersistableSignalSegment
                              .select()
                              .where((PersistableSignalSegment.signal == first_persisted_signal)
                                     & (PersistableSignalSegment.segment_type == 'daily'))
                              .count())

        self.assertEqual(signal.days, num_daily_segments)

        persistable_after_5_days_segment = (PersistableSignalSegment
                                            .select()
                                            .where((PersistableSignalSegment.signal == first_persisted_signal)
                                                   & (PersistableSignalSegment.segment_type == 'after_5_days'))
                                            .get())  # type: PersistableSignalSegment
        self.assertEqual(persistable_after_5_days_segment.count, 6)

        persistable_before_14_days_segment = (PersistableSignalSegment
                                              .select()
                                              .where((PersistableSignalSegment.signal == first_persisted_signal)
                                                     & (PersistableSignalSegment.segment_type == 'before_14_days'))
                                              .get())  # type: PersistableSignalSegment
        self.assertEqual(persistable_before_14_days_segment.count, 7)

        persistable_empty_signal_segment = (PersistableSignalSegment
                                            .select()
                                            .where((PersistableSignalSegment.signal == first_persisted_signal)
                                                   & (PersistableSignalSegment.segment_type == 'after_100_days'))
                                            .get())  # type: PersistableSignalSegment
        self.assertEqual(persistable_empty_signal_segment.count, 0)

        persistable_empty_signal_segment2 = (PersistableSignalSegment
                                             .select()
                                             .where((PersistableSignalSegment.signal == first_persisted_signal)
                                                    & (PersistableSignalSegment.segment_type == 'before_-1_days'))
                                             .get())  # type: PersistableSignalSegment
        self.assertEqual(persistable_empty_signal_segment2.count, 0)
