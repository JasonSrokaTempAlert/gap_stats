from name_tags import name_tags
# coding: utf-8

# In[1]:

####  Controls
FLAG_pull_temp_data = True
FLAG_pull_chan_data = True
AnalysisWindow_StartDate_str = '2017-10-07 00:00:00'

# DATATAGS= ['MC_and_NodeNanny']
# DATATAGS= ['GroupTest']
#DATATAGS = ['DeviceTest']
# DATATAGS = ['NewEngland']
# DATATAGS = ['Racetrac', 'Agne']
#DATATAGS = ['AllCVS']
# DATATAGS = ['TacoBell']
#DATATAGS = ['GoLiveFS']
# DATATAGS = ['GoLive']
#DATATAGS = ['Google']
3DATATAGS = ['CVSStorm']
#DATATAGS = ['GoLiveFS','TacoBell','Racetrac']
#DATATAGS = ['MS']
#DATATAGS =['99']
# DATATAGS=['Dominos']
import os
thisdir = os.getcwd()
if 'sers/Jason' in thisdir:
    Environment = 'local_jason'
elif 'ome/jason' in thisdir:
    Environment = 'ubuntu_jason'
elif 'sers/jheel' in thisdir:
    Environment = 'local_jheel'
elif 'ome/jheel' in thisdir:
    Environment = 'ubuntu_jheel'
else:
    error("don't know how to set Environment for cwd: {}".format(thisdir))

# DB_TABLE = 'sensor_temperature_history'
# if DB_TABLE == 'sensor_temperature_history':
DEVICE_ID_COL = 'str_device_id'
DATE_COL = 'reading_date'
READING_COL = 'temperature'
PROCESSED_COL = 'processed_date'
PORT_COL = 'port'
# else:
#     DATE_COL = 'readingdate'
#     READING_COL = 'temperature'
#     PROCESSED_COL = 'createdate'
#     PORT_COL = 'port'

####  Environment setup
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import psycopg2
import json
import re

from ta_utils import ManyDeviceSignals, PersistableSignal, Signal
from ta_utils import database
from ta_utils import create_tables, drop_tables

AnalysisWindow_StartDate = pd.to_datetime(AnalysisWindow_StartDate_str)

# ####  These worked
# # database.init('test_ta_utils',)
# # database.init('Local_DataWarehouse',)
print "calling database.init"
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7',
              host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
              port='5432')
print "database.init complete"
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)


def FormatDatetime(RawDatetime):
    if isinstance(RawDatetime,basestring):
        result = RawDatetime.replace('T', ' ')
        ReturnDatetime = result.split('.')[0]
        #     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
        return ReturnDatetime
    return RawDatetime

##TODO:  add unit tests for FormatDatetime function



##TODO:  How do we create a generic config file starting point to make the code portable across local/EC2 setups?
if Environment == 'local_jason' :
    data_dir = '/Users/Jason/Desktop/Local_Data_Store'
    results_dir = '/Users/Jason/Desktop/Results_GapMetrics'
    ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
elif Environment == 'local_jheel':
#    error("Jheel - update 'update_tempandchan_history_tables.py with your local_jheel ACCESS_CREDS_DIR, data_dir, and results_dir!")
    data_dir = '/Users/jheel/Desktop/Local_Data_Store'
    results_dir = '/Users/jheel/Desktop/Results_GapMetrics'
    ACCESS_CREDS_DIR = '/Users/jheel/Repository/Prod/'
elif Environment == 'ubuntu_jason':
    data_dir = '/home/jason/Data'
    results_dir = '/home/jason/Results'
    ACCESS_CREDS_DIR = '/home/jason/Repositories/prod-trundle-analyticsconfig'
elif Environment == 'ubuntu_jheel':
    data_dir = '/home/jheel/Data'
    results_dir = '/home/jheel/Results'   
    ACCESS_CREDS_DIR = '/home/jheel/Repo/prod-trundle-analyticsconfig'
else:
    print "unrecognized environmnet: {}".format(Environment)
    error()

thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'universal.json'))

####  Device Metadata - used for filtering to a good set of Device IDs
# DevicesInfoFile=os.path.join(data_dir,'vu_DeviceAndStoreInfo.csv')
# print "Reading devices info from file: {file}".format(file = DevicesInfoFile)
# DevicesInfo = pd.read_csv(DevicesInfoFile)
print "Reading devices info from vu_deviceandstoreinfo"
DevicesInfo = pd.read_sql("select * from vu_deviceandstoreinfo ", DATABASE_CONN)
DevicesInfo['deviceinstalldate']=DevicesInfo['deviceinstalldate'].apply(FormatDatetime)
print "done with devices info select, raw number of devices: {count}".format(count=len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(
    DevicesInfo[DevicesInfo['datedeleted'] >= AnalysisWindow_StartDate])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(
    start=AnalysisWindow_StartDate, count=len(DevicesInfo))

###  StrDeviceId table creation, only for sensors that are not currently hidden
str_deviceid_table_query = """select v.deviceid, s.portid, v.groupid, v.parentgroupid, v.parentparentgroupid, v.devicelabel, 'Device:' || v.deviceid || ':' || s.portid::text strdeviceid
from vu_deviceandstoreinfo v
join sensor s on s.deviceid = v.deviceid
where v.datedeleted is NULL and s.hidden = false ;"""
str_deviceid_df = pd.read_sql(str_deviceid_table_query,DATABASE_CONN)
print "loaded StrDeviceID table, has {numrows} rows".format(numrows = len(str_deviceid_df))

###
###
###  Set up which data will be monitored
###
###


print "setting up devicesinfo with DATATAGS: {tag}".format(tag=DATATAGS)


def TranslateGroupIDsToDeviceInfo(GroupIDs):
    ##  First get the devices that are in the store's group
    print DevicesInfo.head()
    DeviceIndices = np.where(DevicesInfo['groupid'].isin(GroupIDs))[0]
    DeviceInfo = DevicesInfo.iloc[DeviceIndices]
    #     DeviceInfo['StoreGroupID'] = DeviceInfo['GroupID']
    ##  Next get the devices that are in the children of a store's group
    DeviceIndices_ChildrenGroups = np.where(DevicesInfo['parentgroupid'].isin(GroupIDs))[0]
    DeviceInfo_ChildrenGroups = DevicesInfo.iloc[DeviceIndices_ChildrenGroups]
    #     DeviceInfo_ChildrenGroups['StoreGroupID'] = DeviceInfo_ChildrenGroups['ParentGroupID']
    ##  Put them together
    DeviceInfo = DeviceInfo.append(DeviceInfo_ChildrenGroups)
    return DeviceInfo


def AugmentDevicesInfoWithStoreGroupID(DevicesInfo):
    num_devices = len(DevicesInfo)
    print "AugmentDevicesInfoWithStoreGroupID():: called with {numdevs} devices".format(numdevs=num_devices)
    AugDevicesInfo = DevicesInfo
    AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid'].where(AugDevicesInfo['accountid'] != 4226 | AugDevicesInfo['groupname'].str.contains('Store #'), AugDevicesInfo['parentgroupid'])

    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    # print "beginning storegroupid lookup at time {now}".format(now=datetime.now())
    # for row_index in range(0, num_devices):
    #     this_parent_group_id = AugDevicesInfo['parentgroupid'].values[row_index]
    #     num_devices_in_parent_group = len(DevicesInfo[DevicesInfo['groupid'] == this_parent_group_id])
    #     #         print "processing row {row}, with parent groupid {gid}, found {numdev} devices in parent".format(row = row_index, gid = this_parent_group_id, numdev = num_devices_in_parent_group)
    #     if num_devices_in_parent_group > 0:
    #         #             print "found parent group with devices, resetting store group id"
    #         AugDevicesInfo['storegroupid'].iloc[row_index] = this_parent_group_id
    # print "completed storegroupid lookup at time {now}".format(now=datetime.now())
    return AugDevicesInfo


for this_data_tag in DATATAGS:
    print "beginning data pull for data tag: {tag}".format(tag = this_data_tag)
    ##  For all MC stores
    Monitored_DevicesInfo=pd.DataFrame()
    Monitored_DevicesInfo = name_tags(DevicesInfo, this_data_tag)


    Monitored_DeviceIDs = Monitored_DevicesInfo['deviceid']
    NumMonitoredDevices = len(Monitored_DeviceIDs)
    print "NumMonitoredDevices: {}".format(NumMonitoredDevices)
    # Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['hardwarefamily'].isin([2, 3])]
    Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['devicetype']=='Gateway']
    print "Done with group selection.  DATATAG: {tag}   NumDevices: {devcnt}   NumGateways: {grpcnt}".format(tag=this_data_tag,
                                                                                                           devcnt=NumMonitoredDevices,
                                                                                                           grpcnt=len(
                                                                                                               Monitored_Gateway_DevicesInfo))

    ###
    ###
    ####  Configuration/Function params
    ###
    ###

    ZigbeeDevicePowerDataFile = os.path.join(data_dir, 'ZigbeeDevicePowerHistory_{DT}.csv'.format(DT=this_data_tag))
    ZigbeeSensorInfoDataFile = os.path.join(data_dir, 'TrundleCVSSensorData_{DT}.csv'.format(DT=this_data_tag))
    TrundleDevicesInfoFile = os.path.join(data_dir, 'TrundleDeviceInfo.csv')
    DeviceReadingGapStatsResultsFile = os.path.join(results_dir,
                                                    'TrundleMonitoring_DeviceReadingGaps_{DT}.csv'.format(DT=this_data_tag))
    SensorReadingGapStatsResultsFile = os.path.join(results_dir,
                                                    'TrundleMonitoring_SensorReadingGaps_{DT}.csv'.format(DT=this_data_tag))
    SensorTransmissionGapStatsResultsFile = os.path.join(results_dir,
                                                         'TrundleMonitoring_SensorTransmissionGaps_{DT}.csv'.format(
                                                             DT=this_data_tag))
    DeviceTransmissionGapStatsResultsFile = os.path.join(results_dir,
                                                         'TrundleMonitoring_DeviceTransmissionGaps_{DT}.csv'.format(
                                                             DT=this_data_tag))
    GapStatsResultsFile = os.path.join(results_dir, 'TrundleMonitoring_DataFlowGaps_{DT}.csv'.format(DT=this_data_tag))
    GroupGatewayOnlineStatsFile = os.path.join(results_dir, 'GroupGatewayOnline_DailyStats_{DT}.csv'.format(DT=this_data_tag))
    PowerPull_BadDevicesFile = os.path.join(results_dir,
                                            'TrundleMonitoring_{}_PowerPull_BadDevicesList.csv'.format(this_data_tag))
    TemperaturePull_BadDevicesFile = os.path.join(results_dir,
                                                  'TrundleMonitoring_{}_TemperaturePull_BadDevicesList.csv'.format(this_data_tag))
    SensorTransmissionGapStatsResults_ChannelBlock_File = os.path.join(results_dir,
                                                                       'TrundleMonitoring_SensorTransmissionGaps_ChannelBlock_{DT}.csv'.format(
                                                                           DT=this_data_tag))
    ChannelBlockHistory_File = os.path.join(data_dir, 'ChannelBlockHistory_{}.csv'.format(this_data_tag))
    SensorTransmissionGapStatsResults_NodeNanny_File = os.path.join(results_dir,
                                                                    'TrundleMonitoring_{}_GWResets_OnlineStats.csv'.format(
                                                                        this_data_tag))
    node_nanny_online_monitoring_filename = os.path.join(results_dir,
                                                         'TrundleMonitoring_{}_Combined_OnlineStats.csv'.format(this_data_tag))
    summary_store_metrics_filename = os.path.join(results_dir, 'Daily_Gapstats_Store_Metrics_{}.csv'.format(this_data_tag))

    channel_coverage_gapstats_filename = os.path.join(results_dir,
                                                      'channel_coverage_gapstats_{tag}.csv'.format(tag=this_data_tag))
    channel_online_gapstats_filename = os.path.join(results_dir, 'channel_online_gapstats_{tag}.csv'.format(tag=this_data_tag))
    temperature_coverage_gapstats_filename = os.path.join(results_dir,
                                                          'temperature_coverage_gapstats_{tag}.csv'.format(tag=this_data_tag))
    temperature_online_gapstats_filename = os.path.join(results_dir,
                                                        'temperature_online_gapstats_{tag}.csv'.format(tag=this_data_tag))
    daily_gapstats_filename = os.path.join(results_dir, 'Daily_Gapstats_{tag}.csv'.format(tag=this_data_tag))
    NumDevicesToProcess = 800

    # AnalysisWindowInDays=7
    # AnalysisWindow_StartDate=datetime.now()-timedelta(days=AnalysisWindowInDays)

    GapStats_DeviceInfoFields = ['deviceid', 'devicelabel', 'groupid', 'groupname', 'environment', 'address', 'city',
                                 'state', 'zipcode', 'storenumber']

    RunTimestamp = datetime.now().date()
    print "RunTimestamp: {}".format(RunTimestamp)

    RESET = False
    if RESET:
        drop_tables()
        print 'dropped tables'
        create_tables()
        print 'created tables'

    ###
    ###
    ###   Update Temperature Data in Postgres Warehouse
    ###
    ###

    numrows_per_insert = 1000
    if FLAG_pull_temp_data:
        StartTime = datetime.now()
        print 'StartTime: {}'.format(StartTime)

        #  Formulate the connection string
        #     conn_string = "host='localhost' dbname='Local_DataWarehouse' "
        # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"

        # get a connection, if a connect cannot be made an exception will be raised here
        # conn = psycopg2.connect(conn_string)
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        cursor = DATABASE_CONN.cursor()

        # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
        # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
        # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')

        RunTimestamp = datetime.now().date()
        NumMonitoredDevices = len(Monitored_DeviceIDs)

        # for DeviceIter in range(NumMonitoredDevices-1, 0-1, -1):
        for DeviceIter in range(0, NumMonitoredDevices):

            thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
            print"Temperature Data Pull:  Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisDeviceID,
                                                                                           DeviceIter=DeviceIter,
                                                                                           TotalDevices=NumMonitoredDevices)

            ####  Avoiding 500 response codes by not submitting devices that have triggered them in the past
            # if thisDeviceID in ['11666000000121465382','11666000000122693314','11666000000133724472','11666000000133607918']:
            #     print "device is in list of bad temperature devices (caused status 500 fails) - skipping"
            #     continue
            ####  This device was causing an endless loop where each GET had the same URL as the Next
            if thisDeviceID in ['11666000000121817091','11666000000122819154','11666000000121466290','11666000000120966203']:
                print "device is in list of neverending loop devices - skipping"
                continue


            thisDeviceDataDF = pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
            # print thisDeviceDataDF
            if (len(thisDeviceDataDF) == 0):
                print'No data found for device, skipping to next'
                continue
            if ('sensors' in thisDeviceDataDF.columns):
                print'No sensors for this device, skipping to next'
                continue
            thisNumSensors = len(thisDeviceDataDF.port)
            for SensorIndex in range(0, thisNumSensors):
                #             print "SensorIndex: {index}  of {total}".format(index=SensorIndex,total=thisNumSensors)
                thisPortID = thisDeviceDataDF.port[SensorIndex]
                thisStrDeviceID = "Device:" + str(thisDeviceID) + ":" + str(thisPortID)
                print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID, index=SensorIndex,
                                                                                  total=thisNumSensors)

                cursor.execute(
                    "select max(processed_date) from sensor_temperature_history where str_device_id = '{DevID}'".format(
                        DevID=thisStrDeviceID))
                records = cursor.fetchall()
                thistime = records[0]
                trundle_access_time = datetime.now()
                print "raw trundle_access_time: {}".format(trundle_access_time)
                if Environment == 'local':
                    trundle_access_time = trundle_access_time + timedelta(hours = 4)
                    print "after adjusting for local environment, trundle_access_time: {}".format(trundle_access_time)
                if thistime[0] is not None:
                    max_processed_date = thistime[0]
                    print "Temperature Data Pull:  Found prior data, using {time} as MaxLookbackTime".format(time=max_processed_date)
                    thisSensorDataDF = pd.DataFrame(
                        thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                                  reading_start_dt=AnalysisWindow_StartDate,
                                                                  processed_after_dt=max_processed_date))
                else:
                    thisSensorDataDF = pd.DataFrame(
                        thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                                  reading_start_dt=AnalysisWindow_StartDate))

                trundle_history_table_update_query = """
                insert into trundle_access_history (deviceid, portid, update_time, data_type)
    values ('{devid}', {portid}, '{time}', 'temperature')
    on conflict on constraint trundle_access_history_pkey do update set
    deviceid = Excluded.deviceid, portid = Excluded.portid, update_time = Excluded.update_time, data_type = Excluded.data_type;""".format(
                    devid = thisDeviceID,
                    portid = thisPortID,
                    time = trundle_access_time
                )
                print "updating trundle history using query:\n{query}".format(query = trundle_history_table_update_query)
                print "DATABASE_CONN: {}".format(DATABASE_CONN)
                # pd.read_sql(trundle_history_table_update_query,DATABASE_CONN)
                ret = cursor.execute(trundle_history_table_update_query)
                ret = DATABASE_CONN.commit()

                if ('processedDate' not in thisSensorDataDF.columns):
                    print "no processed date in data returned, skipping"
                    continue

                thisSensorDataDF[PROCESSED_COL] = thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
                thisSensorDataDF[READING_COL] = thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))

                thisSensorDataDF['age'] = (
                pd.to_datetime(thisSensorDataDF['processedDate']) - pd.to_datetime(thisSensorDataDF['readingDate'])).astype(
                    'timedelta64[s]')
                thisSensorDataDF['portid'] = thisPortID
                thisSensorDataDF['deviceid'] = thisDeviceID

                print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisSensorDataDF))

                num_data_rows = len(thisSensorDataDF)
                while (num_data_rows > 0):
                    thisupload_rowcount = min(num_data_rows, numrows_per_insert)
                    thisuploaddata = thisSensorDataDF[0:thisupload_rowcount]
                    thisSensorDataDF = thisSensorDataDF[thisupload_rowcount:]
                    num_data_rows = len(thisSensorDataDF)

                    ##  Build the insert statement
                    #             insert_statement='insert into device_channel_history("DeviceID","ReadingDate","ProcessedDate","Channel") values'
                    insert_statement = 'insert into sensor_temperature_history(device_id,reading_date,processed_date,temperature,age,port,str_device_id) values'
                    for iter in range(0, (thisupload_rowcount - 1)):
                        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                        insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port},'{StrDevID}'),".format(
                            DevID=thisDeviceID, ReadingDate=thisuploaddata['readingDate'].values[iter],
                            ProcessedDate=thisuploaddata['processedDate'].values[iter],
                            Temp=thisuploaddata['readingValue'].values[iter],
                            Age=thisuploaddata['age'].values[iter],
                            Port=thisuploaddata['portid'].values[iter],
                            StrDevID=thisStrDeviceID)
                    insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port},'{StrDevID}')".format(
                        DevID=thisDeviceID, ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                        ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                        Temp=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                        Age=thisuploaddata['age'].values[thisupload_rowcount - 1],
                        Port=thisuploaddata['portid'].values[thisupload_rowcount - 1],
                        StrDevID=thisStrDeviceID)
                    insert_statement = insert_statement + " on conflict do nothing"
                    #         print 'insert_statement:'
                    #         print insert_statement
                    ret = cursor.execute(insert_statement)
                    ret = DATABASE_CONN.commit()
                    #             if flag_verbose:
                    #             print "after commit, {} rows remaining".format(num_data_rows)

        cursor.close()
        EndTime = datetime.now()

        TimeDuration = EndTime - StartTime
        print "Data Pull Time: {TD}".format(TD=TimeDuration)


    ###
    ###
    ###  Channel Data Update
    ###
    ###

    def ChannelHistoryUpdate(DeviceID, MaxLookbackDate=AnalysisWindow_StartDate, flag_verbose=False,
                             numrows_per_insert=1000,
                             flag_db_store=True, flag_full_trundle_pull=False, flag_tmp_data_store=False):
        print"ChannelHistoryUpdate()::  called with DeviceID: {Dev}  MaxLookback: {MLB}".format(
            Dev=DeviceID, MLB=MaxLookbackDate)

        #  Formulate the connection string
        #     conn_string = "host='localhost' dbname='Local_DataWarehouse' "
        # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"

        # get a connection, if a connect cannot be made an exception will be raised here
        # conn = psycopg2.connect(conn_string)
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        cursor = DATABASE_CONN.cursor()
        # execute our Query
        #     cursor.execute('select max("ProcessedDate") from device_channel_history where "DeviceID"={DevID}'.format(DevID=DeviceID))
        cursor.execute(
            'select max("processed_date") from device_channel_history where "device_id"={DevID}'.format(DevID=DeviceID))
        # retrieve the records from the database
        records = cursor.fetchall()
        thistime = records[0]

        if flag_full_trundle_pull is False:
            if thistime[0] is not None:
                MaxLookbackDate = thistime[0]
                if flag_verbose:
                    print "Found prior data, using {time} as MaxLookbackTime".format(time=MaxLookbackDate)
        # else:
        #         MaxLookbackDate=datetime.strptime(MaxLookbackDate,'%Y-%m-%d %H:%M:%S')

        print MaxLookbackDate
        trundle_access_time = datetime.now()
        print "raw trundle_access_time: {}".format(trundle_access_time)
        if Environment == 'local':
            trundle_access_time = trundle_access_time + timedelta(hours=4)
            print "after adjusting for local environment, trundle_access_time: {}".format(trundle_access_time)
        thisDeviceDataDF = pd.DataFrame(
            thisClient.get_device_readings_by_device_id(device_id=DeviceID, verbose=flag_verbose,
                                                        command_ids=['150.150.370', '153.122.270'],
                                                        processed_after_dt=MaxLookbackDate))
        #     print thisDeviceDataDF
        trundle_history_table_update_query = """
                    insert into trundle_access_history (deviceid, update_time, data_type)
        values ('{devid}', '{time}', 'channel')
        on conflict on constraint trundle_access_history_pkey do update set
        deviceid = Excluded.deviceid, update_time = Excluded.update_time, data_type = Excluded.data_type;""".format(
            devid=DeviceID,
            time=trundle_access_time
        )
        print "updating trundle history using query:\n{query}".format(query=trundle_history_table_update_query)
        print "DATABASE_CONN: {}".format(DATABASE_CONN)
        # pd.read_sql(trundle_history_table_update_query,DATABASE_CONN)
        ret = cursor.execute(trundle_history_table_update_query)
        ret = DATABASE_CONN.commit()

        thisStrDeviceID = "Device:" + str(DeviceID)

        if flag_tmp_data_store is True:
            tmp_file_name = '/Users/Jason/Desktop/tmp/device_channel_history.csv'
            print 'Saving data to file {file}'.format(file=tmp_file_name)
            thisDeviceDataDF.to_csv(tmp_file_name)
        num_data_rows = len(thisDeviceDataDF)
        print 'num_data_rows: {}'.format(num_data_rows)
        if num_data_rows == 0:
            if flag_verbose:
                print "No data found for device: {dev}".format(dev=DeviceID)
            return
        thisDeviceDataDF['readingDate'] = thisDeviceDataDF['readingDate'].apply(lambda x: FormatDatetime(x))
        thisDeviceDataDF['processedDate'] = thisDeviceDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
        #     while iter_num<2:
        if flag_db_store is True:
            while (num_data_rows > 0):
                thisupload_rowcount = min(num_data_rows, numrows_per_insert)
                thisuploaddata = thisDeviceDataDF[0:thisupload_rowcount]
                thisDeviceDataDF = thisDeviceDataDF[thisupload_rowcount:]
                num_data_rows = len(thisDeviceDataDF)

                ##  Build the insert statement
                #             insert_statement='insert into device_channel_history("DeviceID","ReadingDate","ProcessedDate","Channel") values'
                insert_statement = 'insert into device_channel_history(device_id,reading_date,processed_date,channel,str_device_id) values'
                for iter in range(0, (thisupload_rowcount - 1)):
                    #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                    insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Channel},'{StrID}'),".format(
                        DevID=DeviceID, ReadingDate=thisuploaddata['readingDate'].values[iter],
                        ProcessedDate=thisuploaddata['processedDate'].values[iter],
                        Channel=thisuploaddata['readingValue'].values[iter], StrID=thisStrDeviceID)
                insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Channel},'{StrID}')".format(
                    DevID=DeviceID, ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                    ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                    Channel=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                    StrID=thisStrDeviceID)
                insert_statement = insert_statement + " on conflict do nothing"
                #         print 'insert_statement:'
                #         print insert_statement
                ret = cursor.execute(insert_statement)
                ret = DATABASE_CONN.commit()
            #             if flag_verbose:
            #             print "after commit, {} rows remaining".format(num_data_rows)
        cursor.close()
        return


    StartTime = datetime.now()
    counter = 1
    if FLAG_pull_chan_data is True:
        # for TestDeviceID in [11666000000128028917]:
        for TestDeviceID in Monitored_Gateway_DevicesInfo['deviceid']:
            print "Updating channel history for device: {did}, number {cnt} of {tot}".format(did=TestDeviceID, cnt=counter,
                                                                                             tot=len(
                                                                                                 Monitored_Gateway_DevicesInfo[
                                                                                                     'deviceid']))
            ChannelHistoryUpdate(TestDeviceID, MaxLookbackDate=pd.to_datetime('2017-03-28'),
                                 flag_verbose=True,
                                 flag_db_store=True,
                                 flag_full_trundle_pull=False,
                                 flag_tmp_data_store=False)
            counter = counter + 1
        EndTime = datetime.now()
        TimeDuration = EndTime - StartTime
        print "Channel Data Pull Time: {TD}".format(TD=TimeDuration)

    StartTime = datetime.now()



DATABASE_CONN.close()
