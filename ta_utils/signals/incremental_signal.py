"""
Incremental Signal Module
"""
import math
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import logger
from ta_utils import IncrementalSignalSegment
from ta_utils import Gap
from ta_utils import OfflinePeriod
from ta_utils import database
from ta_utils import PersistableSignal, PersistableSignalSegment
from ta_utils import PersistableGap, PersistableDelay, PersistableOfflinePeriod


def min_or_none(*args):
    if all(x is None for x in args):
        return None

    return min(x for x in args if x is not None)


def max_or_none(*args):
    if all(x is None for x in args):
        return None

    return max(x for x in args if x is not None)


class IncrementalSignal(object):
    """
    The Incremental Signal Object represents an existing signal and an incremental signal as a time-series.

    You can easily update an existing persisted signal with a new Signal object by doing the following::

        incremental_signal = IncrementalSignal(persisted_signal, new_signal)
        incremental_signal.save()

    The metadata and gaps objects will be updated properly, with the exception of any "repeat signal data".
    Any exact repeats will cause minor unexpected behavior for the count, mean, std, and var properties.
    """
    def __init__(self, persisted_signal, incremental_signal):
        """
        Initialize the Incremental Signal with a persisted signal and an incremental signal.

        :param persisted_signal: An existing Persistable Signal
        :type persisted_signal: PersistableSignal
        :param incremental_signal: An incremental Signal update
        :type incremental_signal: Signal
        """
        logger.info("IncrementalSignal.__init__():  called")
        self.persisted_signal = persisted_signal

        # Don't include any data with a processed time before the persisted signal's max
        if incremental_signal.processed_times and self.persisted_signal.max_processed_time:
            incremental_after = incremental_signal.after(self.persisted_signal.max_processed_time,
                                                         use_processed=True)
            self.incremental_signal = incremental_after
        else:
            self.incremental_signal = incremental_signal

        # The incremental signal must use the same reporting_interval_override or mode_minute_interval
        # as the persisted signal.
        if self.reporting_interval_override:
            self.incremental_signal.reporting_interval_override = self.reporting_interval_override
        elif self.mode_minute_interval:
            self.incremental_signal.reporting_interval_override = self.mode_minute_interval

    @property
    def count(self):
        """
        Get the count of values in the Signal.

        :return: Count
        :rtype: int
        """
        return self.persisted_signal.count + self.incremental_signal.count

    @property
    def mean(self):
        """
        The mean value of this signal.

        :return: Mean of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        if self.persisted_signal.mean is not None:
            persisted_sum = 1. * self.persisted_signal.mean * self.persisted_signal.count
        else:
            persisted_sum = 0

        if self.incremental_signal.mean is not None:
            incremental_sum = 1. * self.incremental_signal.mean * self.incremental_signal.count
        else:
            incremental_sum = 0

        return float(persisted_sum + incremental_sum) / self.count

    @property
    def std(self):
        """
        The standard deviation of this signal.

        See: http://stats.stackexchange.com/a/56000

        :return: Standard deviation of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        numerator = 0
        if self.persisted_signal.std is not None:
            numerator += 1. * self.persisted_signal.count * (self.persisted_signal.std ** 2)
            numerator += self.persisted_signal.count * ((self.persisted_signal.mean - self.mean) ** 2)

        if self.incremental_signal.std is not None:
            numerator += self.incremental_signal.count * (self.incremental_signal.std ** 2)
            numerator += self.incremental_signal.count * ((self.incremental_signal.mean - self.mean) ** 2)

        return math.sqrt(numerator / self.count)

    @property
    def var(self):
        """
        The variance of this signal.

        :return: Variance of signal
        :rtype: float
        """
        if self.count == 0:
            return None

        return self.std ** 2

    @property
    def min(self):
        """
        The minimum value of this signal.

        :return: Minimum value of signal
        :rtype: float
        """
        return min_or_none(self.incremental_signal.min, self.persisted_signal.min)

    @property
    def max(self):
        """
        The maximum value of this signal.

        :return: Maximum value of signal
        :rtype: float
        """
        return max_or_none(self.incremental_signal.max, self.persisted_signal.max)

    @property
    def mid(self):
        """
        The average of the min and max values of this signal.

        :return: Mid value of signal
        :rtype: float
        """
        if self.min is None or self.max is None:
            return None

        return float(self.min + self.max) / 2.

    @property
    def min_reading_time(self):
        """
        The minimum time of this signal.

        :return: Minimum time of signal
        """
        return min_or_none(self.incremental_signal.min_reading_time, self.persisted_signal.min_reading_time)

    @property
    def max_reading_time(self):
        """
        The maximum time of this signal.

        :return: Maximum time of signal
        """
        return max_or_none(self.incremental_signal.max_reading_time, self.persisted_signal.max_reading_time)

    @property
    def min_time(self):
        """
        The minimum time of this signal.

        :return: Minimum time of signal
        """
        return min_or_none(self.incremental_signal.min_time, self.persisted_signal.min_time)

    @property
    def max_time(self):
        """
        The maximum time of this signal.

        :return: Maximum time of signal
        """
        return max_or_none(self.incremental_signal.max_time, self.persisted_signal.max_time)

    @property
    def min_processed_time(self):
        """
        The minimum processed time of this signal.

        :return: Minimum processed time of signal
        """
        return min_or_none(self.incremental_signal.min_processed_time, self.persisted_signal.min_processed_time)

    @property
    def max_processed_time(self):
        """
        The maximum processed time of this signal.

        :return: Maximum processed time of signal
        """
        return max_or_none(self.incremental_signal.max_processed_time, self.persisted_signal.max_processed_time)

    @property
    def min_time_override(self):
        """
        The minimum time override of this signal.

        :return: Minimum time override of signal
        """
        return min_or_none(self.incremental_signal.min_time_override, self.persisted_signal.min_time_override)

    @property
    def max_time_override(self):
        """
        The maximum time override of this signal.

        :return: Maximum time override of signal
        """
        return max_or_none(self.incremental_signal.max_time_override, self.persisted_signal.max_time_override)

    @property
    def min_processed_time_override(self):
        """
        The minimum processed time override of this signal.

        :return: Minimum processed time override of signal
        """
        return min_or_none(self.incremental_signal.min_processed_time_override,
                           self.persisted_signal.min_processed_time_override)

    @property
    def max_processed_time_override(self):
        """
        The maximum processed time override of this signal.

        :return: Maximum processed time override of signal
        """
        return max_or_none(self.incremental_signal.max_processed_time_override,
                           self.persisted_signal.max_processed_time_override)

    @property
    def mode_minute_interval(self):
        """
        The mode_minute_interval of the incremental signal.

        :return: The mode minute interval
        :rtype: int
        """
        if self.persisted_signal.mode_minute_interval is not None and self.persisted_signal.count > 0:
            return self.persisted_signal.mode_minute_interval

        return self.incremental_signal.mode_minute_interval

    @property
    def reporting_interval_override(self):
        """
        An override for the interval between readings as either a number of seconds.

        :return: Override of the interval in seconds
        :rtype: int
        """
        if self.persisted_signal.reporting_interval_override is not None:
            return self.persisted_signal.reporting_interval_override

        return self.incremental_signal.reporting_interval_override

    def incremental_gaps(self):
        """
        Gets the gaps to delete and add in the signal.

        :return: A list of gaps to delete and add in the signal.
        :rtype: list[PersistableGap], list[Gap]
        """
        gaps_to_delete = []
        gaps_to_add = []

        # TODO: any incremental signal before entire persisted signal?

        logger.info("IncrementalSignal.incremental_gaps()::  called")
        logger.info("self.persisted_signal.max_time: {}".format(self.persisted_signal.max_time))
        logger.info("self.incremental_signal.min_time: {}".format(self.incremental_signal.min_time))

        if (self.persisted_signal.max_time is not None
            and self.incremental_signal.min_time is not None
            and self.persisted_signal.max_time >= self.incremental_signal.min_time):
            logger.info("IncrementalSignal.incremental_gaps()::  examining for gaps to add/delete")
            incremental_before = self.incremental_signal.before(self.persisted_signal.max_time)

            if self.reporting_interval_override is None:
                mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
            else:
                mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

            for before_timestamp in incremental_before.bounded_times:
                if not isinstance(before_timestamp, datetime):
                    before_time = before_timestamp.to_datetime()
                else:
                    before_time = before_timestamp
                try:
                    # Find existing gaps that will be affected by the new signal
                    affected_gap = (PersistableGap
                                    .select()
                                    .where((PersistableGap.signal == self.persisted_signal) &
                                           (PersistableGap.previous_reading < before_time) &
                                           (PersistableGap.end > before_time))
                                    .get())  # type: PersistableGap

                    gaps_to_delete.append(affected_gap)

                    left_gap_length = pd.Timedelta(affected_gap.end - before_time)
                    right_gap_length = pd.Timedelta(before_time - affected_gap.previous_reading)

                    if left_gap_length >= 2 * mode_minutes_timedelta:
                        start = before_time + mode_minutes_timedelta
                        end = affected_gap.end
                        gap = Gap(before_time, start, end)
                        gaps_to_add.append(gap)

                    if right_gap_length >= 2 * mode_minutes_timedelta:
                        start = affected_gap.previous_reading + mode_minutes_timedelta
                        end = before_time
                        gap = Gap(affected_gap.previous_reading, start, end)
                        gaps_to_add.append(gap)

                except PersistableGap.DoesNotExist:
                    continue

        if self.persisted_signal.max_time is None:
            incremental_after = self.incremental_signal
        else:
            incremental_after = self.incremental_signal.after(self.persisted_signal.max_time)
            incremental_after.min_time_override = self.persisted_signal.max_time
            incremental_after.min_processed_time_override = self.persisted_signal.max_processed_time

        gaps_to_add.extend(incremental_after.gaps())
        logger.info("IncrementalSignal.incremental_gaps():: gaps_to_delete: {}".format(gaps_to_delete))
        logger.info("IncrementalSignal.incremental_gaps():: gaps_to_add: {}".format(gaps_to_add))

        return gaps_to_delete, gaps_to_add

    def incremental_offline_periods(self):
        """
        Gets the offline periods to delete and add in the signal.

        :return: A list of offline periods to delete and add in the signal.
        :rtype: list[PersistableOfflinePeriod], list[OfflinePeriod]
        """
        offline_periods_to_delete = []
        offline_periods_to_add = []

        if self.incremental_signal.min_processed_time is None and self.persisted_signal.max_processed_time is None:
            return offline_periods_to_delete, offline_periods_to_add

        if self.incremental_signal.min_processed_time is None:
            self.incremental_signal.min_processed_time_override = self.persisted_signal.max_processed_time

        # TODO: any incremental signal before entire persisted signal?
        if self.persisted_signal.max_processed_time is not None:
            incremental_before = self.incremental_signal.before(self.persisted_signal.max_processed_time,
                                                                use_processed=True)

            if incremental_before.bounded_processed_times is not None:
                if self.reporting_interval_override is None:
                    mode_minutes_timedelta = pd.Timedelta(seconds=self.mode_minute_interval)
                else:
                    mode_minutes_timedelta = pd.Timedelta(seconds=self.reporting_interval_override)

                for before_time in incremental_before.bounded_processed_times:
                    try:
                        # Find existing gaps that will be affected by the new signal
                        affected_offline_period = (PersistableOfflinePeriod
                                                   .select()
                                                   .where((PersistableOfflinePeriod.signal == self.persisted_signal) &
                                                          (PersistableOfflinePeriod.previous_transmission < before_time) &
                                                          (PersistableOfflinePeriod.end > before_time))
                                                   .get())  # type: PersistableOfflinePeriod

                        previous_transmission = affected_offline_period.previous_transmission

                        offline_periods_to_delete.append(affected_offline_period)

                        left_offline_period_length = pd.Timedelta(affected_offline_period.end - before_time)
                        right_offline_period_length = pd.Timedelta(before_time - previous_transmission)

                        if left_offline_period_length >= 2 * mode_minutes_timedelta:
                            start = before_time + mode_minutes_timedelta
                            end = affected_offline_period.end
                            offline_period = OfflinePeriod(before_time, start, end)
                            offline_periods_to_add.append(offline_period)

                        if right_offline_period_length >= 2 * mode_minutes_timedelta:
                            start = previous_transmission + mode_minutes_timedelta
                            end = before_time
                            offline_period = OfflinePeriod(previous_transmission, start, end)
                            offline_periods_to_add.append(offline_period)

                    except PersistableOfflinePeriod.DoesNotExist:
                        continue

        if self.persisted_signal.max_processed_time is not None:
            incremental_after = self.incremental_signal.after(self.persisted_signal.max_processed_time,
                                                              use_processed=True)
            incremental_after.min_time_override = self.persisted_signal.max_time
            incremental_after.min_processed_time_override = self.persisted_signal.max_processed_time
        else:
            incremental_after = self.incremental_signal

        offline_periods_to_add.extend(incremental_after.offline_periods())
        logger.info("IncrementalSignal.incremental_offline_periods():: offline_periods_to_delete: {}".format(offline_periods_to_delete))
        logger.info("IncrementalSignal.incremental_offline_periods():: offline_periods_to_add: {}".format(offline_periods_to_add))

        return offline_periods_to_delete, offline_periods_to_add

    def save(self, force_insert=False, only=None):
        """
        Save the signal in a table, along with any gaps.

        :param force_insert: boolean to force an insertion query rather than update
        :param only: the parameters you want to include in this save, or None for all
        :return: The persisted signal
        :rtype: PersistableSignal
        """
        logger.info("IncrementalSignal.save()::  entering function")
        count = self.count
        mean = self.mean
        std = self.std
        var = self.var
        min_val = self.min
        max_val = self.max
        mid = self.mid
        min_time = self.min_time
        max_time = self.max_time
        min_reading_time = self.min_reading_time
        max_reading_time = self.max_reading_time
        min_processed_time = self.min_processed_time
        max_processed_time = self.max_processed_time
        min_time_override = self.min_time_override
        max_time_override = self.max_time_override
        min_processed_time_override = self.min_processed_time_override
        max_processed_time_override = self.max_processed_time_override
        mode_minute_interval = self.mode_minute_interval
        reporting_interval_override = self.reporting_interval_override
        gaps_to_delete, gaps_to_add = self.incremental_gaps()
        offline_periods_to_delete, offline_periods_to_add = self.incremental_offline_periods()

        self.persisted_signal.count = count
        self.persisted_signal.mean = mean
        self.persisted_signal.std = std
        self.persisted_signal.var = var
        self.persisted_signal.min = min_val
        self.persisted_signal.max = max_val
        self.persisted_signal.mid = mid
        self.persisted_signal.min_time = min_time
        self.persisted_signal.max_time = max_time
        self.persisted_signal.min_reading_time = min_reading_time
        self.persisted_signal.max_reading_time = max_reading_time
        self.persisted_signal.min_processed_time = min_processed_time
        self.persisted_signal.max_processed_time = max_processed_time
        self.persisted_signal.min_time_override = min_time_override
        self.persisted_signal.max_time_override = max_time_override
        self.persisted_signal.min_processed_time_override = min_processed_time_override
        self.persisted_signal.max_processed_time_override = max_processed_time_override
        self.persisted_signal.mode_minute_interval = mode_minute_interval
        self.persisted_signal.reporting_interval_override = reporting_interval_override

        self.persisted_signal.save(force_insert, only)

        # Update all Gap, Delay, and OfflinePeriod objects
        with database.atomic():
            for gap in gaps_to_delete:
                gap.delete().execute()

            for gap in gaps_to_add:
                persistable_gap = PersistableGap(gap)
                persistable_gap.signal = self.persisted_signal
                persistable_gap.save()

            for offline_period in offline_periods_to_delete:
                offline_period.delete().execute()

            for offline_period in offline_periods_to_add:
                persistable_offline_period = PersistableOfflinePeriod(offline_period)
                persistable_offline_period.signal = self.persisted_signal
                persistable_offline_period.save()

            for delay in self.incremental_signal.delays():
                persistable_delay = PersistableDelay(delay)
                persistable_delay.signal = self.persisted_signal
                persistable_delay.save()

        self.persisted_signal.recompute_metrics()
        self.persisted_signal.save(force_insert, only)

        final_segment = None
        # Update the final segment to include the end of the day
        if self.incremental_signal.max_time is not None:
            try:
                final_segment = (PersistableSignalSegment
                                 .select()
                                 .where((PersistableSignalSegment.signal == self.persisted_signal) &
                                        PersistableSignalSegment.final_segment)
                                 .get())

                if self.incremental_signal.max_time > final_segment.end_time:
                    # TODO: generalize this for all period lengths
                    if self.incremental_signal.max_time >= final_segment.start_time + timedelta(days=1):
                        final_segment.final_segment = False

                    final_segment.end_time = min(final_segment.start_time + timedelta(days=1), self.incremental_signal.max_time)
                    final_segment.save()
            except PersistableSignalSegment.DoesNotExist:
                final_segment = None

        # Update all SignalSegments
        signal_segments = (PersistableSignalSegment
                           .select()
                           .where((PersistableSignalSegment.signal == self.persisted_signal) &
                                  ((PersistableSignalSegment.start_time <= self.incremental_signal.max_time) |
                                   (PersistableSignalSegment.start_time.is_null())) &
                                  ((PersistableSignalSegment.end_time >= self.incremental_signal.min_time) |
                                   (PersistableSignalSegment.end_time.is_null()))))

        with database.atomic():
            for signal_segment in signal_segments:
                IncrementalSignalSegment(signal_segment, self.incremental_signal).save()

        # Extend any daily segments
        # TODO: generalize this for all period lengths
        if self.incremental_signal.max_time is not None and final_segment is not None:
            if self.incremental_signal.max_time > final_segment.end_time:
                incremental_after = self.incremental_signal.after(final_segment.end_time)
                incremental_after.min_time_override = final_segment.end_time
                for new_daily_segment in incremental_after.segment_daily():
                    new_daily_segment.save()

        return self.persisted_signal
