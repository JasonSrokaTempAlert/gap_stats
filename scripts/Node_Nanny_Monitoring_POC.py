# coding: utf-8

# In[1]:

####  Controls
FLAG_pull_temp_data = True
FLAG_pull_chan_data = True
FLAG_run_temp_coverage = False
FLAG_run_temp_online = False
FLAG_run_channel_online = False
FLAG_run_group_online = False
FLAG_run_report_processing = False
AnalysisWindow_StartDate_str = '2017-05-01 00:00:00'

# DATATAG='MC_and_NodeNanny'
# DATATAG='GroupTest'
# DATATAG= 'DeviceTest'
# DATATAG = 'NewEngland'
# DATATAG = 'Racetrac'
# DATATAG = 'AllCVS'
DATATAG = 'TacoBell'


Environment = 'local'
# Environment = 'ubuntu'

DB_TABLE = 'sensor_temperature_history'
if DB_TABLE == 'sensor_temperature_history':
    DEVICE_ID_COL = 'str_device_id'
    DATE_COL = 'reading_date'
    READING_COL = 'temperature'
    PROCESSED_COL = 'processed_date'
    PORT_COL = 'port'
else:
    DATE_COL = 'readingdate'
    READING_COL = 'temperature'
    PROCESSED_COL = 'createdate'
    PORT_COL = 'port'

####  Environment setup
import os
from trundle.trundle_client import TrundleClient
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import psycopg2
import json
import re

from ta_utils import ManyDeviceSignals, PersistableSignal, Signal
from ta_utils import database
from ta_utils import create_tables, drop_tables

AnalysisWindow_StartDate = pd.to_datetime(AnalysisWindow_StartDate_str)

# ####  These worked
# # database.init('test_ta_utils',)
# # database.init('Local_DataWarehouse',)
print "calling database.init"
database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7',
              host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
              port='5432')
print "database.init complete"
conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)


def FormatDatetime(RawDatetime):
    if isinstance(RawDatetime,basestring):
        result = RawDatetime.replace('T', ' ')
        ReturnDatetime = result.split('.')[0]
        #     print '{StartString} became {FinalString}'.format(StartString=RawDatetime,FinalString=ReturnDatetime)
        return ReturnDatetime
    return RawDatetime

##TODO:  add unit tests for FormatDatetime function



##TODO:  How do we create a generic config file starting point to make the code portable across local/EC2 setups?
if Environment == 'local':
    data_dir = '/Users/Jason/Desktop/Local_Data_Store'
    results_dir = '/Users/Jason/Desktop/Results_GapMetrics'
    access_creds_dir = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
else:
    data_dir = '/home/jason/Data'
    results_dir = '/home/jason/Results'
    access_creds_dir = '/home/jason/Repositories/Prod-Trundle-AnalyticsConfig/prod-trundle-analyticsconfig'
thisClient = TrundleClient(os.path.join(access_creds_dir, 'cvs.json'))

####  Device Metadata - used for filtering to a good set of Device IDs
# DevicesInfoFile=os.path.join(data_dir,'vu_DeviceAndStoreInfo.csv')
# print "Reading devices info from file: {file}".format(file = DevicesInfoFile)
# DevicesInfo = pd.read_csv(DevicesInfoFile)
print "Reading devices info from vu_deviceandstoreinfo"
DevicesInfo = pd.read_sql("select * from vu_deviceandstoreinfo ", DATABASE_CONN)
DevicesInfo['deviceinstalldate']=DevicesInfo['deviceinstalldate'].apply(FormatDatetime)
print "done with devices info select, raw number of devices: {count}".format(count=len(DevicesInfo))
DevicesInfo = DevicesInfo[DevicesInfo['datedeleted'].isnull()].append(
    DevicesInfo[DevicesInfo['datedeleted'] >= AnalysisWindow_StartDate])
print "filtered devices deleted before analysis start date ({start}), number of devices: {count}".format(
    start=AnalysisWindow_StartDate, count=len(DevicesInfo))

###  StrDeviceId table creation, only for sensors that are not currently hidden
str_deviceid_table_query = """select v.deviceid, s.portid, v.groupid, v.parentgroupid, v.parentparentgroupid, v.devicelabel, 'Device:' || v.deviceid || ':' || s.portid::text strdeviceid
from vu_deviceandstoreinfo v
join sensor s on s.deviceid = v.deviceid
where v.datedeleted is NULL and s.hidden = false ;"""
str_deviceid_df = pd.read_sql(str_deviceid_table_query,DATABASE_CONN)
print "loaded StrDeviceID table, has {numrows} rows".format(numrows = len(str_deviceid_df))

###
###
###  Set up which data will be monitored
###
###


print "setting up devicesinfo with DATATAG: {tag}".format(tag=DATATAG)


def TranslateGroupIDsToDeviceInfo(GroupIDs):
    ##  First get the devices that are in the store's group
    print DevicesInfo.head()
    DeviceIndices = np.where(DevicesInfo['groupid'].isin(GroupIDs))[0]
    DeviceInfo = DevicesInfo.iloc[DeviceIndices]
    #     DeviceInfo['StoreGroupID'] = DeviceInfo['GroupID']
    ##  Next get the devices that are in the children of a store's group
    DeviceIndices_ChildrenGroups = np.where(DevicesInfo['parentgroupid'].isin(GroupIDs))[0]
    DeviceInfo_ChildrenGroups = DevicesInfo.iloc[DeviceIndices_ChildrenGroups]
    #     DeviceInfo_ChildrenGroups['StoreGroupID'] = DeviceInfo_ChildrenGroups['ParentGroupID']
    ##  Put them together
    DeviceInfo = DeviceInfo.append(DeviceInfo_ChildrenGroups)
    return DeviceInfo


def AugmentDevicesInfoWithStoreGroupID(DevicesInfo):
    num_devices = len(DevicesInfo)
    print "AugmentDevicesInfoWithStoreGroupID():: called with {numdevs} devices".format(numdevs=num_devices)
    AugDevicesInfo = DevicesInfo
    AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid'].where(AugDevicesInfo['accountid'] != 4226 | AugDevicesInfo['groupname'].str.contains('Store #'), AugDevicesInfo['parentgroupid'])

    # AugDevicesInfo['storegroupid'] = AugDevicesInfo['groupid']
    # print "beginning storegroupid lookup at time {now}".format(now=datetime.now())
    # for row_index in range(0, num_devices):
    #     this_parent_group_id = AugDevicesInfo['parentgroupid'].values[row_index]
    #     num_devices_in_parent_group = len(DevicesInfo[DevicesInfo['groupid'] == this_parent_group_id])
    #     #         print "processing row {row}, with parent groupid {gid}, found {numdev} devices in parent".format(row = row_index, gid = this_parent_group_id, numdev = num_devices_in_parent_group)
    #     if num_devices_in_parent_group > 0:
    #         #             print "found parent group with devices, resetting store group id"
    #         AugDevicesInfo['storegroupid'].iloc[row_index] = this_parent_group_id
    # print "completed storegroupid lookup at time {now}".format(now=datetime.now())
    return AugDevicesInfo


##  For all MC stores
if DATATAG == 'MC_and_NodeNanny':
    filtering_devices_info = DevicesInfo
    print "all devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
    filtering_devices_info = filtering_devices_info[filtering_devices_info['AccountID'] == 4226]
    print "cvs devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
    excluded_groups = [18861, 11886, 12545, 8878, 12088, 8949, 9486, 17460, 11564]
    filtering_devices_info = filtering_devices_info[
        ~filtering_devices_info['GroupID'].isin(excluded_groups) ^
        ~filtering_devices_info['ParentGroupID'].isin(excluded_groups) ^
        ~filtering_devices_info['ParentParentGroupID'].isin(excluded_groups)
        ]
    print "number of CVS devices in non-excluded groups: {}".format(len(filtering_devices_info))

    filtering_devices_info = filtering_devices_info[filtering_devices_info['DeviceLabel'].str.contains('MC') == True]
    print "MC devices len(filtering_devices_info): {}".format(len(filtering_devices_info))
    MC_group_ids = filtering_devices_info['GroupID'].unique()
    print "MC Group IDs length: {}".format(len(MC_group_ids))
    MC_parent_group_ids = filtering_devices_info['ParentGroupID'].unique()
    print "MC_parent_group_ids length: {}".format(len(MC_parent_group_ids))
    print type(MC_parent_group_ids)
    gen2_group_ids = np.append(MC_group_ids, MC_parent_group_ids)
    print "gen2_group_ids length: {}".format(len(gen2_group_ids))
    gen2_group_ids = np.unique(gen2_group_ids)
    print "gen2_group_ids length: {}".format(len(gen2_group_ids))

    mc_store_devices_info_raw = TranslateGroupIDsToDeviceInfo(gen2_group_ids)
    mc_store_devices_info = AugmentDevicesInfoWithStoreGroupID(mc_store_devices_info_raw)
    print "{numdev} devices in {numgroups} groups".format(numdev=len(mc_store_devices_info['DeviceID'].unique()),
                                                          numgroups=len(mc_store_devices_info['GroupID'].unique()))

    ####  Actual Node Nanny activity
    ##  Formulate the connection string

    # conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
    ## get a connection, if a connect cannot be made an exception will be raised here
    # conn = psycopg2.connect(conn_string)
    # print 'conn: {}'.format(conn)
    NodeNannyOps_ResetHistory = pd.read_sql(
        "select * from public.gatewayreset where accountid=4226 and successful='true'", DATABASE_CONN)
    # print 'thisDevice_ChannelHistory: {}'.format(thisDevice_ChannelHistory)
    NodeNannyOpsGroupIDs = NodeNannyOps_ResetHistory['groupid'].unique()
    print 'NodeNannyOpsGroupIDs: {}'.format(NodeNannyOpsGroupIDs)
    NodeNannyOps_DeviceInfo = TranslateGroupIDsToDeviceInfo(NodeNannyOpsGroupIDs)

    NodeNannyTestGroupIDs = [4016, 6321, 6972, 8047, 6994, 1276, 5812, 5695, 7627, 1991, 1562, 1445, 5997, 3034, 4814,
                             7932, 7038]
    NodeNannyPilot_DeviceInfo = TranslateGroupIDsToDeviceInfo(NodeNannyTestGroupIDs)

    NodeNanny_and_MCStores_GroupIDs = np.unique(np.concatenate((NodeNannyOpsGroupIDs, gen2_group_ids)))
    NodeNanny_and_MCStores_DevicesInfo_Raw = TranslateGroupIDsToDeviceInfo(NodeNanny_and_MCStores_GroupIDs)
    NodeNanny_and_MCStores_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(NodeNanny_and_MCStores_DevicesInfo_Raw)
    Monitored_DevicesInfo = NodeNanny_and_MCStores_DevicesInfo
elif DATATAG == 'GroupTest':
    ###  Manual Test Group Selection
    TestGroupIDs = [8077]
    Test_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(TranslateGroupIDsToDeviceInfo(TestGroupIDs))
    Monitored_DevicesInfo = Test_DevicesInfo
    print "Monitored_DevicesInfo: {}".format(Monitored_DevicesInfo)
elif DATATAG == 'DeviceTest':
    ###  Manual Test Device Selection
    test_device_ids = ['89014103259033865752']
    Test_DevicesInfo = DevicesInfo[DevicesInfo['deviceid'].isin(test_device_ids)]
    Test_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(Test_DevicesInfo)
    Monitored_DevicesInfo = Test_DevicesInfo
    print "Monitored_DevicesInfo: {}".format(Monitored_DevicesInfo)
    thisClient = TrundleClient(os.path.join(access_creds_dir, 'racetrac.json'))
elif DATATAG == 'NewEngland':
    # NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['StateOrProvince'].isin(['ME', 'MA', 'RI', 'CT', 'VT', 'NH'])]
    DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
    print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
    NewEnglandDevicesInfo = DevicesInfo[((DevicesInfo['groupname'].str.contains(' MA ') |
                                         DevicesInfo['groupname'].str.contains(' ME ') |
                                         DevicesInfo['groupname'].str.contains(' RI ') |
                                         DevicesInfo['groupname'].str.contains(' CT ') |
                                         DevicesInfo['groupname'].str.contains(' VT ') |
                                         DevicesInfo['groupname'].str.contains(' NH ') |
                                         DevicesInfo['groupname'].str.contains(' CA ') |
                                         DevicesInfo['firmware'].isin([1.102, 1.103, 1.104, 1.105])) &
                                        ~(DevicesInfo['groupid'].isin([8878,18524])))]
    all_groups_list = NewEnglandDevicesInfo['groupid'].unique()
    NewEnglandDevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list)]
    print "NewEnglandDevicesInfo.columns: {}".format(NewEnglandDevicesInfo.columns)
    NewEnglandDevicesInfo = AugmentDevicesInfoWithStoreGroupID(NewEnglandDevicesInfo)
    print "after calling AugmentDevicesInfoWithStoreGroupID:"
    Monitored_DevicesInfo = NewEnglandDevicesInfo
elif DATATAG == 'AllCVS':
    DevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 4226]
    print "after CVS account filtering: {count}".format(count=len(DevicesInfo))
    AllCVS_DevicesInfo = DevicesInfo[~(DevicesInfo['groupid'].isnull()) & ~(
    DevicesInfo['groupid'].isin([18861, 11886, 12545, 8878, 12088, 8949, 9486, 17460, 11564, 18524]))]
    all_groups_list = AllCVS_DevicesInfo['groupid'].unique()
    AllCVS_DevicesInfo = DevicesInfo[DevicesInfo['groupid'].isin(all_groups_list)]
    print "AllCVS_DevicesInfo.columns: {}".format(AllCVS_DevicesInfo.columns)
    AllCVS_DevicesInfo = AugmentDevicesInfoWithStoreGroupID(AllCVS_DevicesInfo)
    print "after calling AugmentDevicesInfoWithStoreGroupID:"
    Monitored_DevicesInfo = AllCVS_DevicesInfo
elif DATATAG == 'Racetrac':
    RacetracDevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 7306]
    RacetracDevicesInfo = AugmentDevicesInfoWithStoreGroupID(RacetracDevicesInfo)
    Monitored_DevicesInfo = RacetracDevicesInfo
    thisClient = TrundleClient(os.path.join(access_creds_dir, 'racetrac.json'))
elif DATATAG == 'TacoBell':
    TacoBellDevicesInfo = DevicesInfo[DevicesInfo['accountid'] == 7115]
    TacoBellDevicesInfo = AugmentDevicesInfoWithStoreGroupID(TacoBellDevicesInfo)
    Monitored_DevicesInfo = TacoBellDevicesInfo
    thisClient = TrundleClient(os.path.join(access_creds_dir, 'tacobell.json'))
else:
    print 'Pick a data set!!!!'
    stop = badparamerror



Monitored_DeviceIDs = Monitored_DevicesInfo['deviceid']
NumMonitoredDevices = len(Monitored_DeviceIDs)
print "NumMonitoredDevices: {}".format(NumMonitoredDevices)
Monitored_Gateway_DevicesInfo = Monitored_DevicesInfo[Monitored_DevicesInfo['hardwarefamily'].isin([2, 3])]
print "Done with group selection.  DATATAG: {tag}   NumDevices: {devcnt}   NumGateways: {grpcnt}".format(tag=DATATAG,
                                                                                                       devcnt=NumMonitoredDevices,
                                                                                                       grpcnt=len(
                                                                                                           Monitored_Gateway_DevicesInfo))

###
###
####  Configuration/Function params
###
###

ZigbeeDevicePowerDataFile = os.path.join(data_dir, 'ZigbeeDevicePowerHistory_{DT}.csv'.format(DT=DATATAG))
ZigbeeSensorInfoDataFile = os.path.join(data_dir, 'TrundleCVSSensorData_{DT}.csv'.format(DT=DATATAG))
TrundleDevicesInfoFile = os.path.join(data_dir, 'TrundleDeviceInfo.csv')
DeviceReadingGapStatsResultsFile = os.path.join(results_dir,
                                                'TrundleMonitoring_DeviceReadingGaps_{DT}.csv'.format(DT=DATATAG))
SensorReadingGapStatsResultsFile = os.path.join(results_dir,
                                                'TrundleMonitoring_SensorReadingGaps_{DT}.csv'.format(DT=DATATAG))
SensorTransmissionGapStatsResultsFile = os.path.join(results_dir,
                                                     'TrundleMonitoring_SensorTransmissionGaps_{DT}.csv'.format(
                                                         DT=DATATAG))
DeviceTransmissionGapStatsResultsFile = os.path.join(results_dir,
                                                     'TrundleMonitoring_DeviceTransmissionGaps_{DT}.csv'.format(
                                                         DT=DATATAG))
GapStatsResultsFile = os.path.join(results_dir, 'TrundleMonitoring_DataFlowGaps_{DT}.csv'.format(DT=DATATAG))
GroupGatewayOnlineStatsFile = os.path.join(results_dir, 'GroupGatewayOnline_DailyStats_{DT}.csv'.format(DT=DATATAG))
PowerPull_BadDevicesFile = os.path.join(results_dir,
                                        'TrundleMonitoring_{}_PowerPull_BadDevicesList.csv'.format(DATATAG))
TemperaturePull_BadDevicesFile = os.path.join(results_dir,
                                              'TrundleMonitoring_{}_TemperaturePull_BadDevicesList.csv'.format(DATATAG))
SensorTransmissionGapStatsResults_ChannelBlock_File = os.path.join(results_dir,
                                                                   'TrundleMonitoring_SensorTransmissionGaps_ChannelBlock_{DT}.csv'.format(
                                                                       DT=DATATAG))
ChannelBlockHistory_File = os.path.join(data_dir, 'ChannelBlockHistory_{}.csv'.format(DATATAG))
SensorTransmissionGapStatsResults_NodeNanny_File = os.path.join(results_dir,
                                                                'TrundleMonitoring_{}_GWResets_OnlineStats.csv'.format(
                                                                    DATATAG))
node_nanny_online_monitoring_filename = os.path.join(results_dir,
                                                     'TrundleMonitoring_{}_Combined_OnlineStats.csv'.format(DATATAG))
summary_store_metrics_filename = os.path.join(results_dir, 'Daily_Gapstats_Store_Metrics_{}.csv'.format(DATATAG))

channel_coverage_gapstats_filename = os.path.join(results_dir,
                                                  'channel_coverage_gapstats_{tag}.csv'.format(tag=DATATAG))
channel_online_gapstats_filename = os.path.join(results_dir, 'channel_online_gapstats_{tag}.csv'.format(tag=DATATAG))
temperature_coverage_gapstats_filename = os.path.join(results_dir,
                                                      'temperature_coverage_gapstats_{tag}.csv'.format(tag=DATATAG))
temperature_online_gapstats_filename = os.path.join(results_dir,
                                                    'temperature_online_gapstats_{tag}.csv'.format(tag=DATATAG))
daily_gapstats_filename = os.path.join(results_dir, 'Daily_Gapstats_{tag}.csv'.format(tag=DATATAG))
NumDevicesToProcess = 800

# AnalysisWindowInDays=7
# AnalysisWindow_StartDate=datetime.now()-timedelta(days=AnalysisWindowInDays)

GapStats_DeviceInfoFields = ['deviceid', 'devicelabel', 'groupid', 'groupname', 'environment', 'address', 'city',
                             'state', 'zipcode', 'storenumber']

RunTimestamp = datetime.now().date()
print "RunTimestamp: {}".format(RunTimestamp)

RESET = False
if RESET:
    drop_tables()
    print 'dropped tables'
    create_tables()
    print 'created tables'

###
###
###   Update Temperature Data in Postgres Warehouse
###
###

numrows_per_insert = 1000
if FLAG_pull_temp_data:
    StartTime = datetime.now()
    print 'StartTime: {}'.format(StartTime)

    #  Formulate the connection string
    #     conn_string = "host='localhost' dbname='Local_DataWarehouse' "
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"

    # get a connection, if a connect cannot be made an exception will be raised here
    # conn = psycopg2.connect(conn_string)
    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = DATABASE_CONN.cursor()

    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/cvs.json')
    # thisClient=TrundleClient('/Users/Jason/Desktop/Repositories/AccessCredentials/config_files/omnicare.json')
    # thisStartTime=datetime.strptime('2017-02-01 00:00:00','%Y-%m-%d %H:%M:%S')

    RunTimestamp = datetime.now().date()
    NumMonitoredDevices = len(Monitored_DeviceIDs)

    for DeviceIter in range(0, NumMonitoredDevices):

        thisDeviceID = Monitored_DeviceIDs.iloc[DeviceIter]
        print"Temperature Data Pull:  Processing {DeviceID}; DeviceIter {DeviceIter} of {TotalDevices}".format(DeviceID=thisDeviceID,
                                                                                       DeviceIter=DeviceIter,
                                                                                       TotalDevices=NumMonitoredDevices)

        thisDeviceDataDF = pd.DataFrame(thisClient.get_active_device_by_device_id(device_id=thisDeviceID))
        #         print thisDeviceDataDF
        if (len(thisDeviceDataDF) == 0):
            print'No data found for device, skipping to next'
            continue
        if ('sensors' in thisDeviceDataDF.columns):
            print'No sensors for this device, skipping to next'
            continue
        thisNumSensors = len(thisDeviceDataDF.port)
        for SensorIndex in range(0, thisNumSensors):
            #             print "SensorIndex: {index}  of {total}".format(index=SensorIndex,total=thisNumSensors)
            thisPortID = thisDeviceDataDF.port[SensorIndex]
            thisStrDeviceID = "Device:" + str(thisDeviceID) + ":" + str(thisPortID)
            print "Processing port: {port}  index: {index} of {total}".format(port=thisPortID, index=SensorIndex,
                                                                              total=thisNumSensors)

            cursor.execute(
                "select max(processed_date) from sensor_temperature_history where str_device_id = '{DevID}'".format(
                    DevID=thisStrDeviceID))
            records = cursor.fetchall()
            thistime = records[0]
            trundle_access_time = datetime.now()
            print "raw trundle_access_time: {}".format(trundle_access_time)
            if Environment == 'local':
                trundle_access_time = trundle_access_time + timedelta(hours = 4)
                print "after adjusting for local environment, trundle_access_time: {}".format(trundle_access_time)
            if thistime[0] is not None:
                max_processed_date = thistime[0]
                print "Temperature Data Pull:  Found prior data, using {time} as MaxLookbackTime".format(time=max_processed_date)
                thisSensorDataDF = pd.DataFrame(
                    thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                              reading_start_dt=AnalysisWindow_StartDate,
                                                              processed_after_dt=max_processed_date))
            else:
                thisSensorDataDF = pd.DataFrame(
                    thisClient.get_sensor_readings_by_port_id(thisDeviceID, thisPortID, verbose=True,
                                                              reading_start_dt=AnalysisWindow_StartDate))

            trundle_history_table_update_query = """
            insert into trundle_access_history (deviceid, portid, update_time, data_type)
values ('{devid}', {portid}, '{time}', 'temperature')
on conflict on constraint trundle_access_history_pkey do update set
deviceid = Excluded.deviceid, portid = Excluded.portid, update_time = Excluded.update_time, data_type = Excluded.data_type;""".format(
                devid = thisDeviceID,
                portid = thisPortID,
                time = trundle_access_time
            )
            print "updating trundle history using query:\n{query}".format(query = trundle_history_table_update_query)
            print "DATABASE_CONN: {}".format(DATABASE_CONN)
            # pd.read_sql(trundle_history_table_update_query,DATABASE_CONN)
            ret = cursor.execute(trundle_history_table_update_query)
            ret = DATABASE_CONN.commit()

            if ('processedDate' not in thisSensorDataDF.columns):
                print "no processed date in data returned, skipping"
                continue

            thisSensorDataDF[PROCESSED_COL] = thisSensorDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
            thisSensorDataDF[READING_COL] = thisSensorDataDF['readingDate'].apply(lambda x: FormatDatetime(x))

            thisSensorDataDF['age'] = (
            pd.to_datetime(thisSensorDataDF['processedDate']) - pd.to_datetime(thisSensorDataDF['readingDate'])).astype(
                'timedelta64[s]')
            thisSensorDataDF['portid'] = thisPortID
            thisSensorDataDF['deviceid'] = thisDeviceID

            print"called get_device_readings, call returned {NumRows} rows".format(NumRows=len(thisSensorDataDF))

            num_data_rows = len(thisSensorDataDF)
            while (num_data_rows > 0):
                thisupload_rowcount = min(num_data_rows, numrows_per_insert)
                thisuploaddata = thisSensorDataDF[0:thisupload_rowcount]
                thisSensorDataDF = thisSensorDataDF[thisupload_rowcount:]
                num_data_rows = len(thisSensorDataDF)

                ##  Build the insert statement
                #             insert_statement='insert into device_channel_history("DeviceID","ReadingDate","ProcessedDate","Channel") values'
                insert_statement = 'insert into sensor_temperature_history(device_id,reading_date,processed_date,temperature,age,port,str_device_id) values'
                for iter in range(0, (thisupload_rowcount - 1)):
                    #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                    insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port},'{StrDevID}'),".format(
                        DevID=thisDeviceID, ReadingDate=thisuploaddata['readingDate'].values[iter],
                        ProcessedDate=thisuploaddata['processedDate'].values[iter],
                        Temp=thisuploaddata['readingValue'].values[iter],
                        Age=thisuploaddata['age'].values[iter],
                        Port=thisuploaddata['portid'].values[iter],
                        StrDevID=thisStrDeviceID)
                insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Temp},{Age},{Port},'{StrDevID}')".format(
                    DevID=thisDeviceID, ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                    ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                    Temp=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                    Age=thisuploaddata['age'].values[thisupload_rowcount - 1],
                    Port=thisuploaddata['portid'].values[thisupload_rowcount - 1],
                    StrDevID=thisStrDeviceID)
                insert_statement = insert_statement + " on conflict do nothing"
                #         print 'insert_statement:'
                #         print insert_statement
                ret = cursor.execute(insert_statement)
                ret = DATABASE_CONN.commit()
                #             if flag_verbose:
                #             print "after commit, {} rows remaining".format(num_data_rows)

    cursor.close()
    EndTime = datetime.now()

    TimeDuration = EndTime - StartTime
    print "Data Pull Time: {TD}".format(TD=TimeDuration)


###
###
###  Channel Data Update
###
###

def ChannelHistoryUpdate(DeviceID, MaxLookbackDate=AnalysisWindow_StartDate, flag_verbose=False,
                         numrows_per_insert=1000,
                         flag_db_store=True, flag_full_trundle_pull=False, flag_tmp_data_store=False):
    print"ChannelHistoryUpdate()::  called with DeviceID: {Dev}  MaxLookback: {MLB}".format(
        Dev=DeviceID, MLB=MaxLookbackDate)

    #  Formulate the connection string
    #     conn_string = "host='localhost' dbname='Local_DataWarehouse' "
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"

    # get a connection, if a connect cannot be made an exception will be raised here
    # conn = psycopg2.connect(conn_string)
    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = DATABASE_CONN.cursor()
    # execute our Query
    #     cursor.execute('select max("ProcessedDate") from device_channel_history where "DeviceID"={DevID}'.format(DevID=DeviceID))
    cursor.execute(
        'select max("processed_date") from device_channel_history where "device_id"={DevID}'.format(DevID=DeviceID))
    # retrieve the records from the database
    records = cursor.fetchall()
    thistime = records[0]

    if flag_full_trundle_pull is False:
        if thistime[0] is not None:
            MaxLookbackDate = thistime[0]
            if flag_verbose:
                print "Found prior data, using {time} as MaxLookbackTime".format(time=MaxLookbackDate)
    # else:
    #         MaxLookbackDate=datetime.strptime(MaxLookbackDate,'%Y-%m-%d %H:%M:%S')

    print MaxLookbackDate
    trundle_access_time = datetime.now()
    print "raw trundle_access_time: {}".format(trundle_access_time)
    if Environment == 'local':
        trundle_access_time = trundle_access_time + timedelta(hours=4)
        print "after adjusting for local environment, trundle_access_time: {}".format(trundle_access_time)
    thisDeviceDataDF = pd.DataFrame(
        thisClient.get_device_readings_by_device_id(device_id=DeviceID, verbose=flag_verbose,
                                                    command_ids=['150.150.370', '153.122.270'],
                                                    processed_after_dt=MaxLookbackDate))
    #     print thisDeviceDataDF
    trundle_history_table_update_query = """
                insert into trundle_access_history (deviceid, update_time, data_type)
    values ('{devid}', '{time}', 'channel')
    on conflict on constraint trundle_access_history_pkey do update set
    deviceid = Excluded.deviceid, update_time = Excluded.update_time, data_type = Excluded.data_type;""".format(
        devid=DeviceID,
        time=trundle_access_time
    )
    print "updating trundle history using query:\n{query}".format(query=trundle_history_table_update_query)
    print "DATABASE_CONN: {}".format(DATABASE_CONN)
    # pd.read_sql(trundle_history_table_update_query,DATABASE_CONN)
    ret = cursor.execute(trundle_history_table_update_query)
    ret = DATABASE_CONN.commit()

    thisStrDeviceID = "Device:" + str(DeviceID)

    if flag_tmp_data_store is True:
        tmp_file_name = '/Users/Jason/Desktop/tmp/device_channel_history.csv'
        print 'Saving data to file {file}'.format(file=tmp_file_name)
        thisDeviceDataDF.to_csv(tmp_file_name)
    num_data_rows = len(thisDeviceDataDF)
    print 'num_data_rows: {}'.format(num_data_rows)
    if num_data_rows == 0:
        if flag_verbose:
            print "No data found for device: {dev}".format(dev=DeviceID)
        return
    thisDeviceDataDF['readingDate'] = thisDeviceDataDF['readingDate'].apply(lambda x: FormatDatetime(x))
    thisDeviceDataDF['processedDate'] = thisDeviceDataDF['processedDate'].apply(lambda x: FormatDatetime(x))
    #     while iter_num<2:
    if flag_db_store is True:
        while (num_data_rows > 0):
            thisupload_rowcount = min(num_data_rows, numrows_per_insert)
            thisuploaddata = thisDeviceDataDF[0:thisupload_rowcount]
            thisDeviceDataDF = thisDeviceDataDF[thisupload_rowcount:]
            num_data_rows = len(thisDeviceDataDF)

            ##  Build the insert statement
            #             insert_statement='insert into device_channel_history("DeviceID","ReadingDate","ProcessedDate","Channel") values'
            insert_statement = 'insert into device_channel_history(device_id,reading_date,processed_date,channel,str_device_id) values'
            for iter in range(0, (thisupload_rowcount - 1)):
                #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
                insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Channel},'{StrID}'),".format(
                    DevID=DeviceID, ReadingDate=thisuploaddata['readingDate'].values[iter],
                    ProcessedDate=thisuploaddata['processedDate'].values[iter],
                    Channel=thisuploaddata['readingValue'].values[iter], StrID=thisStrDeviceID)
            insert_statement = insert_statement + "({DevID},'{ReadingDate}','{ProcessedDate}',{Channel},'{StrID}')".format(
                DevID=DeviceID, ReadingDate=thisuploaddata['readingDate'].values[(thisupload_rowcount - 1)],
                ProcessedDate=thisuploaddata['processedDate'].values[thisupload_rowcount - 1],
                Channel=thisuploaddata['readingValue'].values[(thisupload_rowcount - 1)],
                StrID=thisStrDeviceID)
            insert_statement = insert_statement + " on conflict do nothing"
            #         print 'insert_statement:'
            #         print insert_statement
            ret = cursor.execute(insert_statement)
            ret = DATABASE_CONN.commit()
        #             if flag_verbose:
        #             print "after commit, {} rows remaining".format(num_data_rows)
    cursor.close()
    return


StartTime = datetime.now()
counter = 1
if FLAG_pull_chan_data is True:
    # for TestDeviceID in [11666000000128028917]:
    for TestDeviceID in Monitored_Gateway_DevicesInfo['deviceid']:
        print "Updating channel history for device: {did}, number {cnt} of {tot}".format(did=TestDeviceID, cnt=counter,
                                                                                         tot=len(
                                                                                             Monitored_Gateway_DevicesInfo[
                                                                                                 'deviceid']))
        ChannelHistoryUpdate(TestDeviceID, MaxLookbackDate=pd.to_datetime('2017-03-28'),
                             flag_verbose=True,
                             flag_db_store=True,
                             flag_full_trundle_pull=False,
                             flag_tmp_data_store=False)
        counter = counter + 1
    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    print "Channel Data Pull Time: {TD}".format(TD=TimeDuration)

StartTime = datetime.now()


###
###
###  Temperature Coverage
###
###
def GenTempCovEmptyAssessmentDate(StrDeviceID, Date, StoreGroupID, DeviceID, PortID):
    EmptyData = pd.DataFrame(
        {"AnalysisType": 'Sensor_Coverage_Daily', "SourceData": "Temperature", "Metric": "Coverage",
         "Segmentation": "Daily",
         "StrDeviceID": StrDeviceID, "OnTimeDelivery": 0, "Coverage": 0,
         #                                             "OnTime_OfDelivered": 0, "WindowStart": str(Date), "WindowEnd":Date+timedelta(days=1),
         "OnTime_OfDelivered": 0, "WindowStart": Date + timedelta(days=0), "WindowEnd": Date + timedelta(days=1),
         "FirstReadingDate": None, "LastReadingDate": None, "MinProcessedDate": None, "MaxProcessedDate": None,
         "NumReadings": 0,
         "ReportingPeriod": 0, "NumDelays": 0, "MaxDelay_Minutes": 0,
         "NumGaps": 1, "MaxGap_Minutes": 24 * 60,
         "TotalGappage_Minutes": 24 * 60, "Online": 0,
         "NumOfflineGaps": 1, "MaxOfflineGap_Minutes": 24 * 60,
         "TotalOfflineGappage_Minutes": 24 * 60,
         "StoreGroupID": StoreGroupID, "DeviceID": DeviceID, "PortID": PortID}, index=[1])
    return EmptyData


def UpsertTempCoverageDFToWarehouse(TempCovDF):
    num_rows = len(TempCovDF)
    print "UpsertTempCoverageDFToWarehouse call with {numrows} rows of data".format(numrows=num_rows)
    insert_statement = 'INSERT INTO daily_temperature_coverage (deviceid, portid, assessmentdate, coverage, firstreadingdate,'
    insert_statement = insert_statement + 'lastreadingdate, maxdelay_minutes, maxgap_minutes, maxofflinegap_minutes, numdelays, numgaps,'
    insert_statement = insert_statement + 'numofflinegaps, numreadings, ontimedelivery, ontime_ofdelivered, online, reportingperiod,'
    insert_statement = insert_statement + 'storegroupid, strdeviceid, totalgappage_minutes, totalofflinegappage_minutes, windowend, windowstart) VALUES '
    TempCovDF.to_csv(channel_coverage_gapstats_filename)
    for iter in range(0, num_rows):
        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
        insert_statement = insert_statement + "('{deviceid}', {portid}, '{assessmentdate}',{coverage},{firstreadingdate},".format(
            deviceid=TempCovDF['DeviceID'].values[iter],
            portid=TempCovDF['PortID'].values[iter],
            assessmentdate=str(TempCovDF['WindowStart'].values[iter])[0:10],
            coverage=TempCovDF['Coverage'].values[iter],
            firstreadingdate="NULL" if TempCovDF['FirstReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['FirstReadingDate'].values[iter]))
        insert_statement = insert_statement + "{lastreadingdate},{maxdelay_minutes},{maxgap_minutes},{maxofflinegap_minutes},{numdelays},{numgaps},".format(
            lastreadingdate="NULL" if TempCovDF['LastReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['LastReadingDate'].values[iter]),
            maxdelay_minutes=TempCovDF['MaxDelay_Minutes'].values[iter],
            maxgap_minutes=TempCovDF['MaxGap_Minutes'].values[iter],
            maxofflinegap_minutes=TempCovDF['MaxOfflineGap_Minutes'].values[iter],
            numdelays=TempCovDF['NumDelays'].values[iter],
            numgaps=TempCovDF['NumGaps'].values[iter])
        insert_statement = insert_statement + "{numofflinegaps},{numreadings},{ontimedelivery},{ontime_ofdelivered},{online},{reportingperiod},".format(
            numofflinegaps=TempCovDF['NumOfflineGaps'].values[iter],
            numreadings=TempCovDF['NumReadings'].values[iter],
            ontimedelivery=TempCovDF['OnTimeDelivery'].values[iter],
            ontime_ofdelivered=TempCovDF['OnTime_OfDelivered'].values[iter],
            online=TempCovDF['Online'].values[iter],
            reportingperiod=TempCovDF['ReportingPeriod'].values[iter])
        insert_statement += "{storegroupid},'{strdeviceid}',{totalgappage_minutes},{totalofflinegappage_minutes},'{windowend}','{windowstart}')".format(
            storegroupid=TempCovDF['StoreGroupID'].values[iter] if ((TempCovDF['StoreGroupID'].values[
                                                                       iter] is not None) & ~np.isnan(TempCovDF['StoreGroupID'].values[iter])) else "NULL",
            strdeviceid=TempCovDF['StrDeviceID'].values[iter],
            totalgappage_minutes=TempCovDF['TotalGappage_Minutes'].values[iter],
            totalofflinegappage_minutes=TempCovDF['TotalOfflineGappage_Minutes'].values[iter],
            windowend=TempCovDF['WindowEnd'].values[iter],
            windowstart=TempCovDF['WindowStart'].values[iter])
        if iter < (num_rows - 1):
            insert_statement = insert_statement + ' , '
        else:
            insert_statement = insert_statement + " ON CONFLICT ON CONSTRAINT daily_temperature_coverage_pkey DO UPDATE set "
            insert_statement = insert_statement + "deviceid = Excluded.deviceid, portid = Excluded.portid, assessmentdate = Excluded.assessmentdate , coverage = Excluded.coverage , firstreadingdate = Excluded.firstreadingdate ,"
            insert_statement = insert_statement + " lastreadingdate = Excluded.lastreadingdate, maxdelay_minutes = Excluded.maxdelay_minutes , maxgap_minutes = Excluded.maxgap_minutes , maxofflinegap_minutes = Excluded.maxofflinegap_minutes,"
            insert_statement = insert_statement + " numdelays = Excluded.numdelays, numgaps = Excluded.numgaps, numofflinegaps = Excluded.numofflinegaps, numreadings = Excluded.numreadings,"
            insert_statement = insert_statement + " ontimedelivery = Excluded.ontimedelivery, ontime_ofdelivered = Excluded.ontime_ofdelivered, online = Excluded.online, reportingperiod = Excluded.reportingperiod,"
            insert_statement = insert_statement + " storegroupid = Excluded.storegroupid, strdeviceid = Excluded.strdeviceid, totalgappage_minutes = Excluded.totalgappage_minutes, totalofflinegappage_minutes = Excluded.totalofflinegappage_minutes,"
            insert_statement = insert_statement + " windowend = Excluded.windowend, windowstart = Excluded.windowstart;"
            # print 'insert_statement:'
            # print insert_statement
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()
    ret = cursor.execute(insert_statement)
    ret = DATABASE_CONN.commit()
    cursor.close()


def UpsertTempGapStatsDFToWarehouse(TempCovDF):
    num_rows = len(TempCovDF)
    print "TempCovDF.columns: {}".format(TempCovDF.columns)
    print "UpsertTempGapStatsDFToWarehouse call with {numrows} rows of data".format(numrows=num_rows)

    insert_statement = 'INSERT INTO daily_temperature_gapstats (deviceid, portid, assessmentdate, coverage, firstreadingdate,'
    insert_statement = insert_statement + 'lastreadingdate, maxdelay_minutes, maxgap_minutes, maxofflinegap_minutes, numdelays, numgaps,'
    insert_statement = insert_statement + 'numofflinegaps, numreadings, ontimedelivery, ontime_ofdelivered, online, reportingperiod,'
    insert_statement = insert_statement + 'minprocesseddate, maxprocesseddate, '
    insert_statement = insert_statement + 'storegroupid, strdeviceid, totalgappage_minutes, totalofflinegappage_minutes, windowend, windowstart) VALUES '
    TempCovDF.to_csv(channel_coverage_gapstats_filename)
    for iter in range(0, num_rows):
        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
        insert_statement = insert_statement + "('{deviceid}',{portid},'{assessmentdate}',{coverage},{firstreadingdate},".format(
            deviceid=TempCovDF['DeviceID'].values[iter],
            portid=TempCovDF['PortID'].values[iter],
            assessmentdate=str(TempCovDF['WindowStart'].values[iter])[0:10],
            coverage=TempCovDF['Coverage'].values[iter],
            firstreadingdate="NULL" if TempCovDF['FirstReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['FirstReadingDate'].values[iter]))
        insert_statement = insert_statement + "{lastreadingdate},{maxdelay_minutes},{maxgap_minutes},{maxofflinegap_minutes},{numdelays},{numgaps},".format(
            lastreadingdate="NULL" if TempCovDF['LastReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['LastReadingDate'].values[iter]),
            maxdelay_minutes=TempCovDF['MaxDelay_Minutes'].values[iter],
            maxgap_minutes=TempCovDF['MaxGap_Minutes'].values[iter],
            maxofflinegap_minutes=TempCovDF['MaxOfflineGap_Minutes'].values[iter],
            numdelays=TempCovDF['NumDelays'].values[iter],
            numgaps=TempCovDF['NumGaps'].values[iter])
        insert_statement = insert_statement + "{numofflinegaps},{numreadings},{ontimedelivery},{ontime_ofdelivered},{online},{reportingperiod},".format(
            numofflinegaps=TempCovDF['NumOfflineGaps'].values[iter],
            numreadings=TempCovDF['NumReadings'].values[iter],
            ontimedelivery=TempCovDF['OnTimeDelivery'].values[iter],
            ontime_ofdelivered=TempCovDF['OnTime_OfDelivered'].values[iter],
            online=TempCovDF['Online'].values[iter],
            reportingperiod=TempCovDF['ReportingPeriod'].values[iter])
        insert_statement = insert_statement + "{minprocesseddate},{maxprocesseddate},".format(
            minprocesseddate="NULL" if TempCovDF['MinProcessedDate'].values[iter] is None else "'{}'".format(
                TempCovDF['MinProcessedDate'].values[iter]),
            maxprocesseddate="NULL" if TempCovDF['MaxProcessedDate'].values[iter] is None else "'{}'".format(
                TempCovDF['MaxProcessedDate'].values[iter]))
        insert_statement = insert_statement + "{storegroupid},'{strdeviceid}',{totalgappage_minutes},{totalofflinegappage_minutes},'{windowend}','{windowstart}')".format(
            storegroupid=TempCovDF['StoreGroupID'].values[iter],
            strdeviceid=TempCovDF['StrDeviceID'].values[iter],
            totalgappage_minutes=TempCovDF['TotalGappage_Minutes'].values[iter],
            totalofflinegappage_minutes=TempCovDF['TotalOfflineGappage_Minutes'].values[iter],
            windowend=TempCovDF['WindowEnd'].values[iter],
            windowstart=TempCovDF['WindowStart'].values[iter])
        if iter < (num_rows - 1):
            insert_statement = insert_statement + ' , '
        else:
            insert_statement = insert_statement + " ON CONFLICT ON CONSTRAINT daily_temperature_gapstats_pkey DO UPDATE set "
            insert_statement = insert_statement + "deviceid = Excluded.deviceid, portid = Excluded.portid, assessmentdate = Excluded.assessmentdate , coverage = Excluded.coverage , firstreadingdate = Excluded.firstreadingdate ,"
            insert_statement = insert_statement + " lastreadingdate = Excluded.lastreadingdate, maxdelay_minutes = Excluded.maxdelay_minutes , maxgap_minutes = Excluded.maxgap_minutes , maxofflinegap_minutes = Excluded.maxofflinegap_minutes,"
            insert_statement = insert_statement + " numdelays = Excluded.numdelays, numgaps = Excluded.numgaps, numofflinegaps = Excluded.numofflinegaps, numreadings = Excluded.numreadings,"
            insert_statement = insert_statement + " ontimedelivery = Excluded.ontimedelivery, ontime_ofdelivered = Excluded.ontime_ofdelivered, online = Excluded.online, reportingperiod = Excluded.reportingperiod,"
            insert_statement = insert_statement + " minprocesseddate = Excluded.minprocesseddate, maxprocesseddate = Excluded.maxprocesseddate, "
            insert_statement = insert_statement + " storegroupid = Excluded.storegroupid, strdeviceid = Excluded.strdeviceid, totalgappage_minutes = Excluded.totalgappage_minutes, totalofflinegappage_minutes = Excluded.totalofflinegappage_minutes,"
            insert_statement = insert_statement + " windowend = Excluded.windowend, windowstart = Excluded.windowstart;"
    # print 'insert_statement:'
    # print insert_statement
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()
    ret = cursor.execute(insert_statement)
    ret = DATABASE_CONN.commit()
    cursor.close()


if FLAG_run_temp_coverage is True:
    print "Beginning coverage analysis using Postgres Temperature at time {now}".format(now=StartTime)
    # TODO: change this value if you'd like to reset the table
    RESET = True

    DEVICE_ID_COL = 'StrDeviceID'
    DATE_COL = 'readingDate'
    READING_COL = 'readingValue'
    PROCESSED_COL = 'processedDate'

    ###   Get the set of groups being monitored
    AllGroupIDs = Monitored_DevicesInfo['groupid'].unique()
    NumGroups = len(AllGroupIDs)
    num_devices = len(Monitored_DevicesInfo['deviceid'].unique())

    WRITE_MODE = 'a'
    if RESET:
        FLAG_HEADER = True
        WRITE_MODE = 'w'
    else:
        if not os.path.exists(temperature_coverage_gapstats_filename):
            FLAG_HEADER = True
            WRITE_MODE = 'w'
        else:
            FLAG_HEADER = False

    print "WRITE_MODE = {}".format(WRITE_MODE)

    ##  Set up the connection & cursor for querying
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)

    device_index = 1
    for GroupIndex in range(0, NumGroups):
        GapStatsDF = pd.DataFrame()
        this_group_id = AllGroupIDs[GroupIndex]
        print "processing coverage for group {GID}, number {iter} of {total}".format(GID=this_group_id, iter=GroupIndex,
                                                                                     total=NumGroups)

        if np.isnan(this_group_id):
            print "special processing for 'nan' groupid"
            this_group_device_ids = Monitored_DevicesInfo[np.isnan(Monitored_DevicesInfo['groupid'])].deviceid
        else:
            this_group_device_ids = Monitored_DevicesInfo[Monitored_DevicesInfo['groupid'] == this_group_id][
            'deviceid'].unique()
        print "this_group_device_ids: {}".format(this_group_device_ids)
        num_device_ids = len(this_group_device_ids)
        print "found {num} devices in group".format(num=num_device_ids)
        if num_device_ids == 0:
            print "no devices found for group {group}, skipping".format(group=this_group_id)
        devices_sql_list = "(" + ",".join(this_group_device_ids) + ")"

        ##  Query for this group's data
        group_data_query = "SELECT * FROM public.sensor_temperature_history WHERE device_id in" + devices_sql_list
        group_data_query = group_data_query + " and reading_date > '{read_date}'".format(
            read_date=AnalysisWindow_StartDate)

        print "Submitting at time {time} the query: {query}".format(time=datetime.now(), query=group_data_query)
        results_df = pd.read_sql(group_data_query, conn)
        print "Query returned at time {time} with {rows} rows".format(time=datetime.now(), rows=len(results_df))
        AllStrDeviceIDs = str_deviceid_df[(str_deviceid_df['groupid']==this_group_id) |
                                          (str_deviceid_df['parentgroupid'] == this_group_id) |
                                          (str_deviceid_df['parentparentgroupid'] == this_group_id)].strdeviceid.values
        NumStrDeviceIDs = len(AllStrDeviceIDs)

        missing_device_ids = this_group_device_ids

        for thisStrDeviceID in AllStrDeviceIDs:
            GapStatsDF = pd.DataFrame()
            print ""
            print "Processing Temperature Coverage Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(
                DevID=thisStrDeviceID,
                index=device_index,
                total=num_devices)
            thisPortID = thisStrDeviceID.split(':')[-1]
            thisStrDeviceID_DevOnly = ':'.join(thisStrDeviceID.split(':')[:-1])
            thisDeviceID = thisStrDeviceID_DevOnly.replace('Device:', '', 1)
            missing_device_ids = missing_device_ids[missing_device_ids != thisDeviceID]

            if len(Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                thisStoreGroupID = None
            else:
                thisStoreGroupID = int(
                    Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values[0])
                print "thisStoreGroupID: {}".format(thisStoreGroupID)

            ###  Get the data for this device
            sensor_temperature_history = results_df[results_df['str_device_id'] == thisStrDeviceID]

            ###  Single-Signal Batch with Reading Date Only
            data_series = pd.Series(data=sensor_temperature_history['temperature'].values,
                                    index=sensor_temperature_history['reading_date'].apply(
                                        lambda x: pd.to_datetime(x)).values)
            print "len(data_series): {}".format(len(data_series))
            this_signal = Signal(data_series,
                                 device_id=thisStrDeviceID,
                                 data_type="Temperature_ReadingDate")
            print "saving signal"
            this_signal.save()
            print "saved signal; "


            print "calling this_signal.segment_daily()"
            day_segments = this_signal.segment_daily()

            if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],basestring):
                thisDevice_StartDate = max(
                    DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                    AnalysisWindow_StartDate)
            else:
                thisDevice_StartDate = AnalysisWindow_StartDate
            thisDevice_EndDate = RunTimestamp
            print "thisDevice_EndDate: {}".format(thisDevice_EndDate)
            thisDevice_AllDates = pd.date_range(start=datetime.date(thisDevice_StartDate), end=thisDevice_EndDate,
                                                freq='1D')

            for thisSegment in day_segments:
                #                 print "preparing to store day segment stats, min_processed_time = {minpt}  max_processed_time = {maxpt}".format(minpt = thisSegment.min_processed_time, maxpt = thisSegment.max_processed_time)
                #                 print "bounded_transmission_times: {}".format(thisSegment.bounded_processed_times)
                thisSegmentDF = pd.DataFrame(
                    {"AnalysisType": 'Sensor_Coverage_Daily', "SourceData": "Temperature", "Metric": "Coverage",
                     "Segmentation": "Daily",
                     "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": thisSegment.on_time,
                     "Coverage": thisSegment.coverage,
                     "OnTime_OfDelivered": thisSegment.on_time_of_delivered, "WindowStart": thisSegment.min_time,
                     "WindowEnd": thisSegment.max_time,
                     "FirstReadingDate": thisSegment.min_reading_time, "LastReadingDate": thisSegment.max_reading_time,
                     "NumReadings": thisSegment.count,
                     "MinProcessedDate": thisSegment.min_processed_time,
                     "MaxProcessedDate": thisSegment.max_processed_time,
                     "ReportingPeriod": thisSegment.mode_minute_interval, "NumDelays": thisSegment.num_delays,
                     "MaxDelay_Minutes": round(thisSegment.max_delay_time / 60, 1),
                     "NumGaps": thisSegment.num_gaps, "MaxGap_Minutes": round(thisSegment.max_gap_time / 60, 1),
                     "TotalGappage_Minutes": round(thisSegment.total_gap_time / 60, 1), "Online": thisSegment.online,
                     "NumOfflineGaps": thisSegment.num_offline_periods,
                     "MaxOfflineGap_Minutes": round(thisSegment.max_offline_period_time / 60, 1),
                     "TotalOfflineGappage_Minutes": round(thisSegment.total_offline_period_time / 60, 1),
                     "StoreGroupID": thisStoreGroupID, "DeviceID": thisDeviceID, "PortID": thisPortID}, index=[1])
                GapStatsDF = GapStatsDF.append(thisSegmentDF)

                thisDevice_AllDates = thisDevice_AllDates[
                    thisDevice_AllDates != str(datetime.date(thisSegment.min_time))]
            if len(thisDevice_AllDates) > 0:
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenTempCovEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                  thisDeviceID, thisPortID)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                    #     GapStatsDF=pd.merge(GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
                    #                     on='DeviceID',how='left')
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #             print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(temperature_coverage_gapstats_filename, index=False, header=FLAG_HEADER,
                                  mode=WRITE_MODE)
                print GapStatsDF.iloc[0]
                UpsertTempCoverageDFToWarehouse(GapStatsDF)
            device_index = device_index + 1
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        ##  Deal with missing devices
        if len(missing_device_ids) > 0:
            GapStatsDF = pd.DataFrame()
            print "this_group_device_ids: {}".format(this_group_device_ids)
            print "missing_device_ids: {}".format(missing_device_ids)
            for thisDeviceID in missing_device_ids:
                ##  don't fill missing data for gateways
                print "DevicesInfo[DevicesInfo['deviceid']=={thisDeviceID}].hardwarefamily: {Hw}".format(
                    thisDeviceID=thisDeviceID, Hw=DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID].hardwarefamily)
                if DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID].hardwarefamily.values in [2, 3]:
                    continue
                ##  Set up the StoreGroupID
                if len(Monitored_DevicesInfo[
                                   Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                    thisStoreGroupID = None
                else:
                    thisStoreGroupID = Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values[0]
                ##  Create a dummy StrDeviceID
                thisStrDeviceID = "Device:{devid}:?".format(devid=thisDeviceID)
                ##  Create the AllDates Vector
                if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                              basestring):
                    thisDevice_StartDate = max(
                        DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                        AnalysisWindow_StartDate)
                else:
                    thisDevice_StartDate = AnalysisWindow_StartDate
                # if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID'] == thisDeviceID].values[0]) == float:
                #     thisDevice_StartDate = AnalysisWindow_StartDate
                # else:
                #     thisDevice_StartDate = max(datetime.strptime(
                #         DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID'] == thisDeviceID].values[0],
                #         '%Y-%m-%d %H:%M:%S.%f'),
                #                                AnalysisWindow_StartDate)
                thisDevice_EndDate = RunTimestamp
                thisDevice_AllDates = pd.date_range(start=datetime.date(thisDevice_StartDate), end=thisDevice_EndDate,
                                                    freq='1D')
                ##  Iterate through all dates to reflect the missing data
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenTempCovEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                  thisDeviceID, -10)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #         print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(temperature_coverage_gapstats_filename, index=False, header=FLAG_HEADER,
                                  mode=WRITE_MODE)
                UpsertTempCoverageDFToWarehouse(GapStatsDF)

            device_index = device_index + len(missing_device_ids)
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        #         num_rows = len(GapStatsDF)
        # #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
        # #         print "GapStatsDF: {}".format(GapStatsDF)
        #         if num_rows > 0:
        #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
        #             UpsertTempCoverageDFToWarehouse(GapStatsDF)
        #         else:
        #             print "no data for group {GID}".format(GID = this_group_id)

    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    print "Coverage Analysis Time: {TD}".format(TD=TimeDuration)


###
###
###   Temperature Online
###
###
def GenTempOnlineEmptyAssessmentDate(StrDeviceID, Date, StoreGroupID, DeviceID, PortID):
    return pd.DataFrame({"AnalysisType": 'Sensor_Online_Daily', "SourceData": "Temperature", "Metric": "Online",
                         "Segmentation": "Daily",
                         "StrDeviceID": StrDeviceID, "OnTimeDelivery": 0, "Coverage": 0,
                         "OnTime_OfDelivered": 0, "WindowStart": str(Date), "WindowEnd": Date + timedelta(days=1),
                         "FirstReadingDate": None, "LastReadingDate": None, "NumReadings": 0,
                         "ReportingPeriod": 0, "NumDelays": 0, "MaxDelay_Minutes": 0,
                         "NumGaps": 1, "MaxGap_Minutes": 24 * 60,
                         "TotalGappage_Minutes": 24 * 60, "Online": 0,
                         "NumOfflineGaps": 1, "MaxOfflineGap_Minutes": 24 * 60,
                         "TotalOfflineGappage_Minutes": 24 * 60,
                         "StoreGroupID": StoreGroupID, "DeviceID": DeviceID, "PortID": PortID}, index=[1])


def UpsertTempOnlineDFToWarehouse(TempCovDF):
    num_rows = len(TempCovDF)
    print "UpsertTempOnlineDFToWarehouse call with {numrows} rows of data".format(numrows=num_rows)
    insert_statement = 'INSERT INTO daily_temperature_online (deviceid, portid, assessmentdate, coverage, firstreadingdate,'
    insert_statement = insert_statement + 'lastreadingdate, maxdelay_minutes, maxgap_minutes, maxofflinegap_minutes, numdelays, numgaps,'
    insert_statement = insert_statement + 'numofflinegaps, numreadings, ontimedelivery, ontime_ofdelivered, online, reportingperiod,'
    insert_statement = insert_statement + 'storegroupid, strdeviceid, totalgappage_minutes, totalofflinegappage_minutes, windowend, windowstart) VALUES '
    TempCovDF.to_csv(channel_coverage_gapstats_filename)
    for iter in range(0, num_rows):
        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
        insert_statement = insert_statement + "('{deviceid}', {portid}, '{assessmentdate}',{coverage},{firstreadingdate},".format(
            deviceid=TempCovDF['DeviceID'].values[iter],
            portid=TempCovDF['PortID'].values[iter],
            assessmentdate=str(TempCovDF['WindowStart'].values[iter])[0:10],
            coverage=TempCovDF['Coverage'].values[iter],
            firstreadingdate="NULL" if TempCovDF['FirstReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['FirstReadingDate'].values[iter]))
        insert_statement = insert_statement + "{lastreadingdate},{maxdelay_minutes},{maxgap_minutes},{maxofflinegap_minutes},{numdelays},{numgaps},".format(
            lastreadingdate="NULL" if TempCovDF['LastReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['LastReadingDate'].values[iter]),
            maxdelay_minutes=TempCovDF['MaxDelay_Minutes'].values[iter],
            maxgap_minutes=TempCovDF['MaxGap_Minutes'].values[iter],
            maxofflinegap_minutes=TempCovDF['MaxOfflineGap_Minutes'].values[iter],
            numdelays=TempCovDF['NumDelays'].values[iter],
            numgaps=TempCovDF['NumGaps'].values[iter])
        insert_statement = insert_statement + "{numofflinegaps},{numreadings},{ontimedelivery},{ontime_ofdelivered},{online},{reportingperiod},".format(
            numofflinegaps=TempCovDF['NumOfflineGaps'].values[iter],
            numreadings=TempCovDF['NumReadings'].values[iter],
            ontimedelivery=TempCovDF['OnTimeDelivery'].values[iter],
            ontime_ofdelivered=TempCovDF['OnTime_OfDelivered'].values[iter],
            online=TempCovDF['Online'].values[iter],
            reportingperiod=TempCovDF['ReportingPeriod'].values[iter])
        insert_statement = insert_statement + "{storegroupid},'{strdeviceid}',{totalgappage_minutes},{totalofflinegappage_minutes},'{windowend}','{windowstart}')".format(
            storegroupid=TempCovDF['StoreGroupID'].values[iter] if TempCovDF['StoreGroupID'].values[
                                                                       iter] is not None else "NULL",
            strdeviceid=TempCovDF['StrDeviceID'].values[iter],
            totalgappage_minutes=TempCovDF['TotalGappage_Minutes'].values[iter],
            totalofflinegappage_minutes=TempCovDF['TotalOfflineGappage_Minutes'].values[iter],
            windowend=TempCovDF['WindowEnd'].values[iter],
            windowstart=TempCovDF['WindowStart'].values[iter])
        if iter < (num_rows - 1):
            insert_statement = insert_statement + ' , '
        else:
            insert_statement = insert_statement + " ON CONFLICT ON CONSTRAINT daily_temperature_online_pkey DO UPDATE set "
            insert_statement = insert_statement + "deviceid = Excluded.deviceid, portid = EXcluded.portid, assessmentdate = Excluded.assessmentdate , coverage = Excluded.coverage , firstreadingdate = Excluded.firstreadingdate ,"
            insert_statement = insert_statement + " lastreadingdate = Excluded.lastreadingdate, maxdelay_minutes = Excluded.maxdelay_minutes , maxgap_minutes = Excluded.maxgap_minutes , maxofflinegap_minutes = Excluded.maxofflinegap_minutes,"
            insert_statement = insert_statement + " numdelays = Excluded.numdelays, numgaps = Excluded.numgaps, numofflinegaps = Excluded.numofflinegaps, numreadings = Excluded.numreadings,"
            insert_statement = insert_statement + " ontimedelivery = Excluded.ontimedelivery, ontime_ofdelivered = Excluded.ontime_ofdelivered, online = Excluded.online, reportingperiod = Excluded.reportingperiod,"
            insert_statement = insert_statement + " storegroupid = Excluded.storegroupid, strdeviceid = Excluded.strdeviceid, totalgappage_minutes = Excluded.totalgappage_minutes, totalofflinegappage_minutes = Excluded.totalofflinegappage_minutes,"
            insert_statement = insert_statement + " windowend = Excluded.windowend, windowstart = Excluded.windowstart;"
        #     print 'insert_statement:'
        #     print insert_statement
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()
    ret = cursor.execute(insert_statement)
    ret = DATABASE_CONN.commit()
    cursor.close()


if FLAG_run_temp_online is True:
    print "Beginning online analysis using Postgres Temperature at time {now}".format(now=StartTime)
    # TODO: change this value if you'd like to reset the table
    RESET = True

    DEVICE_ID_COL = 'StrDeviceID'
    DATE_COL = 'processedDate'
    READING_COL = 'readingValue'
    # PROCESSED_COL = 'processedDate'

    ###   Get the set of groups being monitored
    AllGroupIDs = Monitored_DevicesInfo['groupid'].unique()
    NumGroups = len(AllGroupIDs)
    num_devices = len(Monitored_DevicesInfo['deviceid'].unique())

    # ####  These worked
    # # database.init('test_ta_utils',)
    # # database.init('Local_DataWarehouse',)
    print "calling database.init"
    database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7',
                  host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
                  port='5432')
    # database.init('test_ta_utils',)


    # database.init('warehouse')
    print "database.init complete"
    WRITE_MODE = 'a'
    if RESET:
        FLAG_HEADER = True
        WRITE_MODE = 'w'
    else:
        if not os.path.exists(temperature_online_gapstats_filename):
            FLAG_HEADER = True
            WRITE_MODE = 'w'
        else:
            FLAG_HEADER = False

    print "WRITE_MODE = {}".format(WRITE_MODE)

    ##  Set up the connection & cursor for querying
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()

    device_index = 1
    for GroupIndex in range(0, NumGroups):
        GapStatsDF = pd.DataFrame()
        this_group_id = AllGroupIDs[GroupIndex]
        print "processing temperature online for group {GID}, number {iter} of {total}".format(GID=this_group_id,
                                                                                               iter=GroupIndex,
                                                                                               total=NumGroups)

        this_group_device_ids = Monitored_DevicesInfo[Monitored_DevicesInfo['groupid'] == this_group_id][
            'deviceid'].unique()
        print "this_group_device_ids: {}".format(this_group_device_ids)
        num_device_ids = len(this_group_device_ids)
        print "found {num} devices in group".format(num=num_device_ids)
        if num_device_ids == 0:
            print "no devices found for group {group}, ".format(group=this_group_id)
            AllStrDeviceIDs = []
        else:
            devices_sql_list = "(" + ",".join(this_group_device_ids) + ")"

            ##  Query for this group's data
            group_data_query = "SELECT * FROM public.sensor_temperature_history WHERE device_id in" + devices_sql_list
            group_data_query = group_data_query + " and processed_date > '{read_date}'".format(
                read_date=AnalysisWindow_StartDate)

            #    ret = cursor.execute(group_data_query)
            #     results = cursor.fetchall()
            print "Submitting at time {time} the query: {query}".format(time=datetime.now(), query=group_data_query)
            results_df = pd.read_sql(group_data_query, conn)
            print "Query returned at time {time} with {rows} rows".format(time=datetime.now(), rows=len(results_df))
            AllStrDeviceIDs = str_deviceid_df[(str_deviceid_df['groupid'] == this_group_id) |
                                              (str_deviceid_df['parentgroupid'] == this_group_id) |
                                               (str_deviceid_df[
                                                   'parentparentgroupid'] == this_group_id)].strdeviceid.values
        NumStrDeviceIDs = len(AllStrDeviceIDs)

        missing_device_ids = this_group_device_ids

        for thisStrDeviceID in AllStrDeviceIDs:
            GapStatsDF = pd.DataFrame()
            print ""
            print "Processing Temperature Online Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(
                DevID=thisStrDeviceID,
                index=device_index,
                total=num_devices)
            thisStrDeviceID_DevOnly = ':'.join(thisStrDeviceID.split(':')[:-1])
            thisDeviceID = thisStrDeviceID_DevOnly.replace('Device:', '', 1)
            thisPortID = thisStrDeviceID.split(':')[-1]
            missing_device_ids = missing_device_ids[missing_device_ids != thisDeviceID]

            if len(Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                thisStoreGroupID = None
            else:
                thisStoreGroupID = int(
                    Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values[0])
                print "thisStoreGroupID: {}".format(thisStoreGroupID)

            ###  Get the data for this device
            sensor_temperature_history = results_df[results_df['str_device_id'] == thisStrDeviceID]
            data_series = pd.Series(data=sensor_temperature_history['temperature'].values,
                                    index=sensor_temperature_history['processed_date'].apply(
                                        lambda x: pd.to_datetime(x)).values)
            this_signal = Signal(data_series,
                                 device_id=thisStrDeviceID,
                                 data_type="Temperature_ProcessedDate")
            this_signal.save()
            print "saved signal; "
            day_segments = this_signal.segment_daily()

            if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],basestring):
                thisDevice_StartDate = max(
                    DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                    AnalysisWindow_StartDate)
            else:
                thisDevice_StartDate = AnalysisWindow_StartDate
            # if type(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0]) == float:
            #     thisDevice_StartDate = AnalysisWindow_StartDate
            # else:
            #     this_install_date = str(DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID]['deviceinstalldate'].iloc[0])
            #     print "this_install_date: {}".format(this_install_date)
            #     thisDevice_StartDate = max(datetime.strptime(
            #         this_install_date,
            #         '%Y-%m-%d %H:%M:%S'),
            #                                AnalysisWindow_StartDate)
            thisDevice_EndDate = RunTimestamp
            print "thisDevice_EndDate: {}".format(thisDevice_EndDate)
            thisDevice_AllDates = pd.date_range(start=datetime.date(thisDevice_StartDate), end=thisDevice_EndDate,
                                                freq='1D')

            for thisSegment in day_segments:
                thisSegmentDF = pd.DataFrame(
                    {"AnalysisType": 'Sensor_Online_Daily', "SourceData": "Temperature", "Metric": "Online",
                     "Segmentation": "Daily",
                     "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": thisSegment.on_time,
                     "Coverage": thisSegment.coverage,
                     "OnTime_OfDelivered": thisSegment.on_time_of_delivered, "WindowStart": thisSegment.min_time,
                     "WindowEnd": thisSegment.max_time,
                     "FirstReadingDate": thisSegment.min_reading_time, "LastReadingDate": thisSegment.max_reading_time,
                     "NumReadings": thisSegment.count,
                     "ReportingPeriod": thisSegment.mode_minute_interval, "NumDelays": thisSegment.num_delays,
                     "MaxDelay_Minutes": round(thisSegment.max_delay_time / 60, 1),
                     "NumGaps": thisSegment.num_gaps, "MaxGap_Minutes": round(thisSegment.max_gap_time / 60, 1),
                     "TotalGappage_Minutes": round(thisSegment.total_gap_time / 60, 1), "Online": thisSegment.online,
                     "NumOfflineGaps": thisSegment.num_offline_periods,
                     "MaxOfflineGap_Minutes": round(thisSegment.max_offline_period_time / 60, 1),
                     "TotalOfflineGappage_Minutes": round(thisSegment.total_offline_period_time / 60, 1),
                     "StoreGroupID": thisStoreGroupID, "DeviceID": thisDeviceID, "PortID": thisPortID}, index=[1])
                GapStatsDF = GapStatsDF.append(thisSegmentDF)

                thisDevice_AllDates = thisDevice_AllDates[
                    thisDevice_AllDates != str(datetime.date(thisSegment.min_time))]
            if len(thisDevice_AllDates) > 0:
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenTempCovEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                  thisDeviceID, thisPortID)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                    #     GapStatsDF=pd.merge(GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
                    #                     on='DeviceID',how='left')
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #         print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(temperature_coverage_gapstats_filename, index=False, header=FLAG_HEADER,
                                  mode=WRITE_MODE)
                UpsertTempOnlineDFToWarehouse(GapStatsDF)
            device_index = device_index + 1
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        ##  Deal with missing devices
        if len(missing_device_ids) > 0:
            GapStatsDF = pd.DataFrame()
            print "this_group_device_ids: {}".format(this_group_device_ids)
            print "missing_device_ids: {}".format(missing_device_ids)
            for thisDeviceID in missing_device_ids:
                ##  don't fill missing data for gateways
                print "DevicesInfo[DevicesInfo['deviceid']=={thisDeviceID}].hardwarefamily: {Hw}".format(
                    thisDeviceID=thisDeviceID, Hw=DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID].hardwarefamily)
                if DevicesInfo[DevicesInfo['deviceid'] == thisDeviceID].hardwarefamily.values in [2, 3]:
                    continue
                ##  Set up the StoreGroupID
                if len(Monitored_DevicesInfo[
                                   Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                    thisStoreGroupID = None
                else:
                    thisStoreGroupID = int(
                        Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values[0])
                ##  Create a dummy StrDeviceID
                thisStrDeviceID = "Device:{devid}:?".format(devid=thisDeviceID)
                ##  Create the AllDates Vector
                if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                              basestring):
                    thisDevice_StartDate = max(
                        DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                        AnalysisWindow_StartDate)
                else:
                    thisDevice_StartDate = AnalysisWindow_StartDate
                # if type(DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID'] == thisDeviceID].values[0]) == float:
                #     thisDevice_StartDate = AnalysisWindow_StartDate
                # else:
                #     thisDevice_StartDate = max(datetime.strptime(
                #         DevicesInfo['DeviceInstallDate'][DevicesInfo['DeviceID'] == thisDeviceID].values[0],
                #         '%Y-%m-%d %H:%M:%S.%f'),
                #                                AnalysisWindow_StartDate)
                thisDevice_EndDate = RunTimestamp
                thisDevice_AllDates = pd.date_range(start=datetime.date(thisDevice_StartDate), end=thisDevice_EndDate,
                                                    freq='1D')
                ##  Iterate through all dates to reflect the missing data
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenTempCovEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                  thisDeviceID, -10)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #         print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(temperature_online_gapstats_filename, index=False, header=FLAG_HEADER,
                                  mode=WRITE_MODE)
                UpsertTempOnlineDFToWarehouse(GapStatsDF)

            device_index = device_index + len(missing_device_ids)
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        #         num_rows = len(GapStatsDF)
        # #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
        # #         print "GapStatsDF: {}".format(GapStatsDF)
        #         if num_rows > 0:
        #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
        #             UpsertTempCoverageDFToWarehouse(GapStatsDF)
        #         else:
        #             print "no data for group {GID}".format(GID = this_group_id)

    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    cursor.close()
    print "Temperature Online Analysis Time: {TD}".format(TD=TimeDuration)


###
###
###   Channel Online
###
###
def GenChanOnlineEmptyAssessmentDate(StrDeviceID, Date, StoreGroupID, DeviceID):
    return pd.DataFrame(
        {"AnalysisType": 'Device_Online_Daily', "SourceData": "Channel", "Metric": "Online", "Segmentation": "Daily",
         "StrDeviceID": StrDeviceID, "OnTimeDelivery": 0, "Coverage": 0,
         "OnTime_OfDelivered": 0, "WindowStart": str(Date), "WindowEnd": Date + timedelta(days=1),
         "FirstReadingDate": None, "LastReadingDate": None, "NumReadings": 0,
         "ReportingPeriod": 0, "NumDelays": 0, "MaxDelay_Minutes": 0,
         "NumGaps": 1, "MaxGap_Minutes": 24 * 60,
         "TotalGappage_Minutes": 24 * 60, "Online": 0,
         "NumOfflineGaps": 1, "MaxOfflineGap_Minutes": 24 * 60,
         "TotalOfflineGappage_Minutes": 24 * 60,
         "StoreGroupID": StoreGroupID, "DeviceID": DeviceID}, index=[1])


def UpsertChanOnlineDFToWarehouse(TempCovDF):
    num_rows = len(TempCovDF)
    print "UpsertChanOnlineDFToWarehouse call with {numrows} rows of data".format(numrows=num_rows)
    insert_statement = 'INSERT INTO daily_channel_online (deviceid, assessmentdate, coverage, firstreadingdate,'
    insert_statement = insert_statement + 'lastreadingdate, maxdelay_minutes, maxgap_minutes, maxofflinegap_minutes, numdelays, numgaps,'
    insert_statement = insert_statement + 'numofflinegaps, numreadings, ontimedelivery, ontime_ofdelivered, online, reportingperiod,'
    insert_statement = insert_statement + 'storegroupid, strdeviceid, totalgappage_minutes, totalofflinegappage_minutes, windowend, windowstart) VALUES '
    TempCovDF.to_csv(channel_coverage_gapstats_filename)
    for iter in range(0, num_rows):
        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
        insert_statement = insert_statement + "('{deviceid}','{assessmentdate}',{coverage},{firstreadingdate},".format(
            deviceid=TempCovDF['DeviceID'].values[iter],
            assessmentdate=str(TempCovDF['WindowStart'].values[iter])[0:10],
            coverage=TempCovDF['Coverage'].values[iter],
            firstreadingdate="NULL" if TempCovDF['FirstReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['FirstReadingDate'].values[iter]))
        insert_statement = insert_statement + "{lastreadingdate},{maxdelay_minutes},{maxgap_minutes},{maxofflinegap_minutes},{numdelays},{numgaps},".format(
            lastreadingdate="NULL" if TempCovDF['LastReadingDate'].values[iter] is None else "'{}'".format(
                TempCovDF['LastReadingDate'].values[iter]),
            maxdelay_minutes=TempCovDF['MaxDelay_Minutes'].values[iter],
            maxgap_minutes=TempCovDF['MaxGap_Minutes'].values[iter],
            maxofflinegap_minutes=TempCovDF['MaxOfflineGap_Minutes'].values[iter],
            numdelays=TempCovDF['NumDelays'].values[iter],
            numgaps=TempCovDF['NumGaps'].values[iter])
        insert_statement = insert_statement + "{numofflinegaps},{numreadings},{ontimedelivery},{ontime_ofdelivered},{online},{reportingperiod},".format(
            numofflinegaps=TempCovDF['NumOfflineGaps'].values[iter],
            numreadings=TempCovDF['NumReadings'].values[iter],
            ontimedelivery=TempCovDF['OnTimeDelivery'].values[iter],
            ontime_ofdelivered=TempCovDF['OnTime_OfDelivered'].values[iter],
            online=TempCovDF['Online'].values[iter],
            reportingperiod=TempCovDF['ReportingPeriod'].values[iter])
        insert_statement = insert_statement + "{storegroupid},'{strdeviceid}',{totalgappage_minutes},{totalofflinegappage_minutes},'{windowend}','{windowstart}')".format(
            storegroupid=TempCovDF['StoreGroupID'].values[iter],
            strdeviceid=TempCovDF['StrDeviceID'].values[iter],
            totalgappage_minutes=TempCovDF['TotalGappage_Minutes'].values[iter],
            totalofflinegappage_minutes=TempCovDF['TotalOfflineGappage_Minutes'].values[iter],
            windowend=TempCovDF['WindowEnd'].values[iter],
            windowstart=TempCovDF['WindowStart'].values[iter])
        if iter < (num_rows - 1):
            insert_statement = insert_statement + ' , '
        else:
            insert_statement = insert_statement + " ON CONFLICT ON CONSTRAINT daily_channel_online_pkey DO UPDATE set "
            insert_statement = insert_statement + "deviceid = Excluded.deviceid, assessmentdate = Excluded.assessmentdate , coverage = Excluded.coverage , firstreadingdate = Excluded.firstreadingdate ,"
            insert_statement = insert_statement + " lastreadingdate = Excluded.lastreadingdate, maxdelay_minutes = Excluded.maxdelay_minutes , maxgap_minutes = Excluded.maxgap_minutes , maxofflinegap_minutes = Excluded.maxofflinegap_minutes,"
            insert_statement = insert_statement + " numdelays = Excluded.numdelays, numgaps = Excluded.numgaps, numofflinegaps = Excluded.numofflinegaps, numreadings = Excluded.numreadings,"
            insert_statement = insert_statement + " ontimedelivery = Excluded.ontimedelivery, ontime_ofdelivered = Excluded.ontime_ofdelivered, online = Excluded.online, reportingperiod = Excluded.reportingperiod,"
            insert_statement = insert_statement + " storegroupid = Excluded.storegroupid, strdeviceid = Excluded.strdeviceid, totalgappage_minutes = Excluded.totalgappage_minutes, totalofflinegappage_minutes = Excluded.totalofflinegappage_minutes,"
            insert_statement = insert_statement + " windowend = Excluded.windowend, windowstart = Excluded.windowstart;"
            # print 'insert_statement:'
            # print insert_statement
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()
    ret = cursor.execute(insert_statement)
    ret = DATABASE_CONN.commit()
    cursor.close()


if FLAG_run_channel_online is True:
    print "Beginning online analysis using Postgres Channel at time {now}".format(now=StartTime)
    # TODO: change this value if you'd like to reset the table

    DEVICE_ID_COL = 'StrDeviceID'
    DATE_COL = 'processedDate'
    READING_COL = 'readingValue'
    # PROCESSED_COL = 'processedDate'

    ###   Get the set of groups being monitored
    AllGroupIDs = Monitored_Gateway_DevicesInfo['groupid'].unique()
    NumGroups = len(AllGroupIDs)
    num_devices = len(Monitored_Gateway_DevicesInfo['deviceid'].unique())

    # ####  These worked
    # # database.init('test_ta_utils',)
    # # database.init('Local_DataWarehouse',)
    print "calling database.init"
    database.init('warehouse', user='jason', password='qvz2LFRzMMVPku7',
                  host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com',
                  port='5432')
    # database.init('test_ta_utils',)


    # database.init('warehouse')
    print "database.init complete"
    WRITE_MODE = 'a'
    RESET = True
    if RESET:
        FLAG_HEADER = True
        WRITE_MODE = 'w'
    else:
        if not os.path.exists(channel_online_gapstats_filename):
            FLAG_HEADER = True
            WRITE_MODE = 'w'
        else:
            FLAG_HEADER = False

    print "WRITE_MODE = {}".format(WRITE_MODE)

    ##  Set up the connection & cursor for querying
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)
    cursor = DATABASE_CONN.cursor()

    device_index = 1
    for GroupIndex in range(0, NumGroups):
        GapStatsDF = pd.DataFrame()
        this_group_id = AllGroupIDs[GroupIndex]
        print "processing channel online for group {GID}, number {iter} of {total}".format(GID=this_group_id,
                                                                                           iter=GroupIndex,
                                                                                           total=NumGroups)

        this_group_device_ids = \
        Monitored_Gateway_DevicesInfo[Monitored_Gateway_DevicesInfo['groupid'] == this_group_id]['deviceid'].unique()
        print "this_group_device_ids: {}".format(this_group_device_ids)
        num_device_ids = len(this_group_device_ids)
        print "found {num} devices in group".format(num=num_device_ids)
        if num_device_ids == 0:
            print "no devices found for group {group}".format(this_group_id)
        devices_sql_list = "(" + ",".join(this_group_device_ids) + ")"

        ##  Query for this group's data
        group_data_query = "SELECT * FROM public.device_channel_history WHERE device_id in" + devices_sql_list
        group_data_query = group_data_query + " and processed_date > '{read_date}'".format(
            read_date=AnalysisWindow_StartDate)

        #    ret = cursor.execute(group_data_query)
        #     results = cursor.fetchall()
        print "Submitting at time {time} the query: {query}".format(time=datetime.now(), query=group_data_query)
        results_df = pd.read_sql(group_data_query, conn)
        print "Query returned at time {time} with {rows} rows".format(time=datetime.now(), rows=len(results_df))
        if len(results_df) == 0:
            print 'No channel data found for group {id}, skipping'.format(id=this_group_id)
            # continue
            #     print 'results_df.head(): {}'.format(results_df.head())
        AllStrDeviceIDs = results_df['str_device_id'].unique()
        NumStrDeviceIDs = len(AllStrDeviceIDs)

        missing_device_ids = this_group_device_ids

        for thisStrDeviceID in AllStrDeviceIDs:
            GapStatsDF = pd.DataFrame()
            print ""
            print "Processing Channel Online Stats for StrDeviceID: {DevID}  Entry {index} of {total}".format(
                DevID=thisStrDeviceID,
                index=device_index,
                total=num_devices)
            thisDeviceID = thisStrDeviceID.replace('Device:', '', 1)
            missing_device_ids = missing_device_ids[missing_device_ids != thisDeviceID]

            if len(Monitored_Gateway_DevicesInfo[
                               Monitored_Gateway_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                thisStoreGroupID = None
            else:
                thisStoreGroupID = int(Monitored_Gateway_DevicesInfo[Monitored_Gateway_DevicesInfo[
                                                                         'deviceid'] == thisDeviceID].storegroupid.values[
                                           0])
                print "thisStoreGroupID: {}".format(thisStoreGroupID)

            ###  Get the data for this device
            device_channel_history = results_df[results_df['str_device_id'] == thisStrDeviceID]
            data_series = pd.Series(data=device_channel_history['channel'].values,
                                    index=device_channel_history['processed_date'].apply(
                                        lambda x: pd.to_datetime(x)).values)
            this_signal = Signal(data_series,
                                 device_id=thisStrDeviceID,
                                 data_type="Channel_ProcessedDate")
            this_signal.save()
            print "saved signal; "
            day_segments = this_signal.segment_daily()

            print "DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0]: {}".format(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0])
            if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],basestring):
                thisDevice_StartDate = max(
                    DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                    AnalysisWindow_StartDate)
            else:
                thisDevice_StartDate = AnalysisWindow_StartDate
            # thisDevice_StartDate = max(
            #     datetime.strptime(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
            #                       '%Y-%m-%d %H:%M:%S.%f'),
            #     AnalysisWindow_StartDate)
            thisDevice_EndDate = RunTimestamp
            print "thisDevice_EndDate: {}".format(thisDevice_EndDate)
            thisDevice_AllDates = pd.date_range(
                start=datetime.date(datetime.strptime(str(thisDevice_StartDate), '%Y-%m-%d %H:%M:%S')),
                end=thisDevice_EndDate, freq='1D')

            for thisSegment in day_segments:
                thisSegmentDF = pd.DataFrame(
                    {"AnalysisType": 'Channel_Online_Daily', "SourceData": "Channel", "Metric": "Online",
                     "Segmentation": "Daily",
                     "StrDeviceID": thisStrDeviceID, "OnTimeDelivery": thisSegment.on_time,
                     "Coverage": thisSegment.coverage,
                     "OnTime_OfDelivered": thisSegment.on_time_of_delivered, "WindowStart": thisSegment.min_time,
                     "WindowEnd": thisSegment.max_time,
                     "FirstReadingDate": thisSegment.min_reading_time, "LastReadingDate": thisSegment.max_reading_time,
                     "NumReadings": thisSegment.count,
                     "ReportingPeriod": thisSegment.mode_minute_interval, "NumDelays": thisSegment.num_delays,
                     "MaxDelay_Minutes": round(thisSegment.max_delay_time / 60, 1),
                     "NumGaps": thisSegment.num_gaps, "MaxGap_Minutes": round(thisSegment.max_gap_time / 60, 1),
                     "TotalGappage_Minutes": round(thisSegment.total_gap_time / 60, 1), "Online": thisSegment.online,
                     "NumOfflineGaps": thisSegment.num_offline_periods,
                     "MaxOfflineGap_Minutes": round(thisSegment.max_offline_period_time / 60, 1),
                     "TotalOfflineGappage_Minutes": round(thisSegment.total_offline_period_time / 60, 1),
                     "StoreGroupID": thisStoreGroupID, "DeviceID": thisDeviceID}, index=[1])
                GapStatsDF = GapStatsDF.append(thisSegmentDF)

                thisDevice_AllDates = thisDevice_AllDates[
                    thisDevice_AllDates != str(datetime.date(thisSegment.min_time))]
            if len(thisDevice_AllDates) > 0:
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenChanOnlineEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                     thisDeviceID)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                    #     GapStatsDF=pd.merge(GapStatsDF,DevicesInfo[GapStats_DeviceInfoFields],
                    #                     on='DeviceID',how='left')
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #         print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(temperature_coverage_gapstats_filename, index=False, header=FLAG_HEADER,
                                  mode=WRITE_MODE)
                UpsertChanOnlineDFToWarehouse(GapStatsDF)
            device_index = device_index + 1
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        ##  Deal with missing devices
        if len(missing_device_ids) > 0:
            GapStatsDF = pd.DataFrame()
            print "this_group_device_ids: {}".format(this_group_device_ids)
            print "missing_device_ids: {}".format(missing_device_ids)
            for thisDeviceID in missing_device_ids:
                ##  Set up the StoreGroupID
                if len(Monitored_DevicesInfo[
                                   Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values) < 1:
                    thisStoreGroupID = None
                else:
                    thisStoreGroupID = int(
                        Monitored_DevicesInfo[Monitored_DevicesInfo['deviceid'] == thisDeviceID].storegroupid.values[0])
                ##  Create a dummy StrDeviceID
                thisStrDeviceID = "Device:{devid}:?".format(devid=thisDeviceID)
                ##  Create the AllDates Vector
                if isinstance(DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                              basestring):
                    thisDevice_StartDate = max(
                        DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                        AnalysisWindow_StartDate)
                else:
                    thisDevice_StartDate = AnalysisWindow_StartDate
                # thisDevice_StartDate = max(datetime.strptime(
                #     DevicesInfo['deviceinstalldate'][DevicesInfo['deviceid'] == thisDeviceID].values[0],
                #     '%Y-%m-%d %H:%M:%S.%f'),
                #                            AnalysisWindow_StartDate)
                thisDevice_EndDate = RunTimestamp
                thisDevice_AllDates = pd.date_range(start=datetime.date(thisDevice_StartDate), end=thisDevice_EndDate,
                                                    freq='1D')
                ##  Iterate through all dates to reflect the missing data
                for thisDate in thisDevice_AllDates:
                    print "adding missing date: {} to device: {}".format(thisDate, thisDeviceID)
                    thisSegmentDF = GenChanOnlineEmptyAssessmentDate(thisStrDeviceID, thisDate, thisStoreGroupID,
                                                                     thisDeviceID)
                    GapStatsDF = GapStatsDF.append(thisSegmentDF)
                #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
            num_rows = len(GapStatsDF)
            #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
            #         print "GapStatsDF: {}".format(GapStatsDF)
            if num_rows > 0:
                GapStatsDF.to_csv(channel_online_gapstats_filename, index=False, header=FLAG_HEADER, mode=WRITE_MODE)
                UpsertChanOnlineDFToWarehouse(GapStatsDF)

            device_index = device_index + len(missing_device_ids)
            FLAG_HEADER = False
            WRITE_MODE = 'a'
        #         num_rows = len(GapStatsDF)
        # #         print "Group {GID} complete, {num_rows} rows of data generated".format(GID = this_group_id, num_rows = num_rows)
        # #         print "GapStatsDF: {}".format(GapStatsDF)
        #         if num_rows > 0:
        #             GapStatsDF.to_csv(temperature_coverage_gapstats_filename,index=False,header=FLAG_HEADER,mode=WRITE_MODE)
        #             UpsertTempCoverageDFToWarehouse(GapStatsDF)
        #         else:
        #             print "no data for group {GID}".format(GID = this_group_id)

    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    print "Channel Online Analysis Time: {TD}".format(TD=TimeDuration)

    ###  close connection to postgres
    cursor.close()

    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    print "Online Analysis Time: {TD}".format(TD=TimeDuration)


# #  Formulate the connection string
# # conn_string = "host='localhost' dbname='Local_DataWarehouse' "
# conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"

# create_table_string = "CREATE TABLE public.device_channel_history(DeviceID int);"
# # DeviceID decimal(20, 0) NOT NULL, ReadingDate timestamp NOT NULL, ProcessedDate timestamp NOT NULL, Channel int NOT NULL, PRIMARY KEY(DeviceID, ReadingDate) );"

# # get a connection, if a connect cannot be made an exception will be raised here
# conn = psycopg2.connect(conn_string)
# # conn.cursor will return a cursor object, you can use this cursor to perform queries
# cursor = conn.cursor()
# # execute our Query
# cursor.execute(create_table_string)
# cursor.close()
# conn.close()

# #####   This is the create statement for channel history
# create table public.device_channel_history (
# device_id decimal(20,0),
# reading_date timestamp not null,
# processed_date timestamp not null,
# channel integer,
# str_device_id varchar(40),
# primary key (device_id, reading_date)
# );

# #####   This is the create statement for temperature history
# create table public.sensor_temperature_history (
# device_id decimal(20,0),
# reading_date timestamp not null,
# processed_date timestamp not null,
# temperature decimal(5,5) not null,
# age int not null,
# port int not null,
# str_device_id varchar(40),
# primary key (device_id, reading_date)
# );


###
###
###  Develop a group-level online metric
### Initial implementation is a basic version that is effective in single-gateway scenarios but could be inaccurate in multi-gateway scenarios
###

def UpsertGroupOnlineDFToWarehouse(GroupOnlineDF):
    num_rows = len(GroupOnlineDF)
    print "UpsertGroupOnlineDFToWarehouse call with {numrows} rows of data".format(numrows=num_rows)
    insert_statement = """INSERT INTO daily_store_gateway_online (
assessmentdate, gw_online, gatewayids, numgateways, storegroupid
) VALUES """
    for iter in range(0, num_rows):
        #             print "building insert_statement, iter={iter}, num_rows={rows}".format(iter=iter,rows=thisupload_rowcount)
        insert_statement = insert_statement + " ('{date}', {gwonline}, '{gids}', {num}, {stid} )".format(
            date=GroupOnlineDF['AssessmentDate'].values[iter], gwonline=GroupOnlineDF['GW_Online'].values[iter],
            gids=GroupOnlineDF['GatewayIDs'].values[iter] if GroupOnlineDF['NumGateways'].values[
                                                                 iter] <= 5 else 'Too Many Gateways to list',
            num=GroupOnlineDF['NumGateways'].values[iter],
            stid=GroupOnlineDF['StoreGroupID'].values[iter] if ~np.isnan(GroupOnlineDF['StoreGroupID'].values[iter]) else -1  )
        if iter < (num_rows - 1):
            insert_statement = insert_statement + ' , '
        else:
            insert_statement = insert_statement + """ON CONFLICT ON CONSTRAINT daily_store_gateway_online_pkey DO UPDATE SET
assessmentdate = EXCLUDED.assessmentdate, gw_online = EXCLUDED.gw_online, gatewayids = EXCLUDED.gatewayids, numgateways = EXCLUDED.numgateways, storegroupid = EXCLUDED.storegroupid;"""
    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)

    cursor = DATABASE_CONN.cursor()
    print "insert_statement: {}".format(insert_statement)
    ret = cursor.execute(insert_statement)
    ret = DATABASE_CONN.commit()
    cursor.close()


if FLAG_run_group_online is True:
    FLAG_HEADER = True
    WRITE_MODE = 'w'

    # conn_string = "host = 'analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname = 'warehouse' user='jason' password='qvz2LFRzMMVPku7'"
    # conn = psycopg2.connect(conn_string)

    StartTime = datetime.now()
    ChanOnlineQuery = " select deviceid, assessmentdate, coverage, storegroupid from public.daily_channel_online;"
    GapStatsDF = pd.read_sql(ChanOnlineQuery, conn)
    AllDates = pd.date_range(start=datetime.date(AnalysisWindow_StartDate), end=RunTimestamp, freq='1D')
    AllStoreGroupIDs = Monitored_DevicesInfo['storegroupid'].unique()
    #     GapStatsDF['WindowStart_DateOnly']=GapStatsDF['WindowStart'].apply(lambda x: x[0:10])


    GroupGWID = 1
    for thisGroupID in AllStoreGroupIDs:
        thisGroup_GatewaysInfo = DevicesInfo[(DevicesInfo['groupid'] == thisGroupID) &
                                             ((DevicesInfo['hardwarefamily'] == 2) | (
                                             DevicesInfo['hardwarefamily'] == 3))]
        thisGroup_NumGateways = len(thisGroup_GatewaysInfo)
        thisGroup_Gateways = thisGroup_GatewaysInfo['deviceid'].values
        this_group_gateways_str = str(thisGroup_Gateways).replace("'", "")
        #         print "thisGroup_Gateways: {}".format(this_group_gateways_str)
        print "iter {iter},  processing group: {groupid} found {numgw} gateways: {gwids}".format(iter=GroupGWID,
                                                                                                 groupid=thisGroupID,
                                                                                                 numgw=thisGroup_NumGateways,
                                                                                                 gwids=this_group_gateways_str)
        thisGroupGatewayGapStats = GapStatsDF[GapStatsDF['deviceid'].isin(thisGroup_Gateways)]
        #     print "thisGroupGatewayGapStats gap stats length: {len}".format(len=len(thisGroupGatewayGapStats))
        thisGroup_GWStatsDF = pd.DataFrame()
        for thisDate in AllDates:
            thisDateGatewayGapStats = thisGroupGatewayGapStats[
                thisGroupGatewayGapStats['assessmentdate'] == str(datetime.date(thisDate))]
            if len(thisDateGatewayGapStats) == 0:
                thisDate_GWOnline = -1
            else:
                print "thisDateGatewayGapStats: {}".format(thisDateGatewayGapStats)
                thisDate_GWOnline = max(thisDateGatewayGapStats['coverage'])
            thisEntry = pd.DataFrame({'StoreGroupID': thisGroupID,
                                      'AssessmentDate': thisDate,
                                      'NumGateways': thisGroup_NumGateways,
                                      'GatewayIDs': this_group_gateways_str,
                                      'GW_Online': thisDate_GWOnline,
                                      'GroupGWID': GroupGWID}, index=[GroupGWID])
            thisGroup_GWStatsDF = thisGroup_GWStatsDF.append(thisEntry)
        thisGroup_GWStatsDF.to_csv(GroupGatewayOnlineStatsFile, index=False, header=FLAG_HEADER, mode=WRITE_MODE)
        UpsertGroupOnlineDFToWarehouse(thisGroup_GWStatsDF)
        GroupGWID = GroupGWID + 1
        FLAG_Header = False
        WRITE_MODE = 'a'

    EndTime = datetime.now()
    TimeDuration = EndTime - StartTime
    print "Group Power Analysis Time: {TD}".format(TD=TimeDuration)

##
##       Final packaging and polishing
##
if FLAG_run_report_processing is True:

    def GenBasicDailyStats(GapStatsDF, NumDays, AnchorDate=None):
        if AnchorDate is None:
            print max(GapStatsDF['WindowStart'])
            AnchorDate = max(GapStatsDF['WindowStart'])
            print "AnchorDate: {}".format(AnchorDate)
        MinAllowedDate = str(datetime.strptime(AnchorDate, '%Y-%m-%d %H:%M:%S') - timedelta(days=NumDays - 1))
        print"GenBasicDailyStats():: called with GapStats having {NumRows} rows and using AnchorDate: {AnchorDate} and cutoff date: {Cutoff}".format(
            NumRows=len(GapStatsDF),
            AnchorDate=AnchorDate,
            Cutoff=MinAllowedDate)
        ##  Filter by date
        RecentGapStatsDF = GapStatsDF[GapStatsDF['WindowStart'] >= MinAllowedDate]
        print "RecentGapStatsDF num rows: {NumRows}".format(NumRows=len(RecentGapStatsDF))
        ##  Means
        GapSummaryStatsDF_Means = RecentGapStatsDF.groupby(['StrDeviceID']).mean()
        GapSummaryStatsDF_Means.columns = GapSummaryStatsDF_Means.columns + "_Mean_{NumDays}Day".format(NumDays=NumDays)
        GapSummaryStatsDF_Means.reset_index(inplace=True, level=0)
        ##  Means
        GapSummaryStatsDF_StDev = RecentGapStatsDF.groupby(['StrDeviceID']).std()
        GapSummaryStatsDF_StDev.columns = GapSummaryStatsDF_StDev.columns + "_StDev_{NumDays}Day".format(
            NumDays=NumDays)
        GapSummaryStatsDF_StDev.reset_index(inplace=True, level=0)
        ##  Sums
        GapSummaryStatsDF_Sums = RecentGapStatsDF.groupby(['StrDeviceID']).sum()
        GapSummaryStatsDF_Sums.columns = GapSummaryStatsDF_Sums.columns + "_Sum_{NumDays}Day".format(NumDays=NumDays)
        GapSummaryStatsDF_Sums.reset_index(inplace=True, level=0)

        CompositeGapSummaryStatsDF = pd.merge(GapSummaryStatsDF_Sums, GapSummaryStatsDF_Means, on=['StrDeviceID'],
                                              how='outer')
        CompositeGapSummaryStatsDF = pd.merge(CompositeGapSummaryStatsDF, GapSummaryStatsDF_StDev, on=['StrDeviceID'],
                                              how='outer')
        return CompositeGapSummaryStatsDF


    def GenLastDayDF(GapStatsDF, AnchorDate=None):
        if AnchorDate is None:
            AnchorDate = max(GapStatsDF.WindowStart)
        LastDayDF = GapStatsDF[GapStatsDF['WindowStart'] == AnchorDate]
        LastDayDF.columns = LastDayDF.columns + "_LastDay"
        LastDayDF = LastDayDF.rename(columns={'StrDeviceID_LastDay': 'StrDeviceID'})
        return LastDayDF


    norm_fields = ["AnalysisType", 'SourceData', 'Metric', 'Segmentation', "StrDeviceID", "DeviceID", "StoreGroupID",
                   "WindowStart", "WindowEnd", "FirstReadingDate", "LastReadingDate", "NumReadings",
                   "ReportingPeriod", "NumDelays", "MaxDelay_Minutes",
                   "NumGaps", "MaxGap_Minutes", "TotalGappage_Minutes",
                   "OnTimeDelivery", "Coverage", "OnTime_OfDelivered"]

    ##  Sensor reading merge of coverage and online
    SensorTransmissionGapStatsDF = pd.read_csv(temperature_online_gapstats_filename)
    sensor_online_gapstats = SensorTransmissionGapStatsDF[norm_fields]
    sensor_online_gapstats['AssessmentDate'] = SensorTransmissionGapStatsDF['WindowStart'].apply(lambda x: x[0:10])
    print "sensor_online_gapstats:  NumRows = {NumRows}".format(NumRows=len(sensor_online_gapstats))

    SensorReadingGapStatsDF = pd.read_csv(temperature_coverage_gapstats_filename)
    sensor_coverage_gapstats = SensorReadingGapStatsDF[norm_fields]
    sensor_coverage_gapstats['AssessmentDate'] = sensor_coverage_gapstats['WindowStart'].apply(lambda x: x[0:10])
    print "sensor_coverage_gapstats:  NumRows = {NumRows}".format(NumRows=len(sensor_coverage_gapstats))
    # FullSensorGapStats=SensorTransmissionGapStatsDF

    sensor_gap_stats_full = sensor_online_gapstats.append(sensor_coverage_gapstats)
    print "sensor_gap_stats_full:  NumRows = {NumRows}".format(NumRows=len(sensor_gap_stats_full))
    #     sensor_gap_stats_full.to_csv(os.path.join(results_dir, 'NetworkMonitoring_{tag}_sensors.csv'.format(tag=DATATAG)),index=False)
    del SensorTransmissionGapStatsDF
    del SensorReadingGapStatsDF

    ##  Device reading merge of coverage and online
    DeviceTransmissionGapStatsDF = pd.read_csv(channel_online_gapstats_filename)
    device_online_gapstats = DeviceTransmissionGapStatsDF[norm_fields]
    device_online_gapstats['AssessmentDate'] = device_online_gapstats['WindowStart'].apply(lambda x: x[0:10])
    print "device_online_gapstats:  NumRows = {NumRows}".format(NumRows=len(device_online_gapstats))

    # DeviceReadingGapStatsDF = pd.read_csv(channel_coverage_gapstats_filename)
    # device_coverage_gapstats = DeviceReadingGapStatsDF[norm_fields]
    # device_coverage_gapstats['AssessmentDate']=device_coverage_gapstats['WindowStart'].apply(lambda x: x[0:10])
    # print "device_coverage_gapstats:  NumRows = {NumRows}".format(NumRows=len(device_coverage_gapstats))
    # FullDeviceGapStats=DeviceTransmissionGapStatsDF

    # device_gap_stats_full = device_online_gapstats.append(device_coverage_gapstats)
    # print "device_gap_stats_full:  NumRows = {NumRows}".format(NumRows=len(device_gap_stats_full))
    # device_gap_stats_full.to_csv('/Users/Jason/Desktop/Results_GapMetrics/NetworkMonitoring_{tag}_devices.csv'.format(tag=DATATAG),index=False)

    # ##  Combine gap info across sensors and devices.  concat used to allow different columns from the sources
    Daily_SensorAndDevice_GapStatsDF = pd.concat([device_online_gapstats, sensor_gap_stats_full], axis=0,
                                                 ignore_index=True)
    del device_online_gapstats
    del sensor_gap_stats_full

    ##  Group-level online status
    GroupGatewayOnlineStatsDF = pd.read_csv(GroupGatewayOnlineStatsFile)
    print "GroupGatewayOnlineStatsDF:  NumRows = {NumRows}".format(NumRows=len(GroupGatewayOnlineStatsDF))

    ##  Augment with normalized online
    Daily_OnlineNormedDF = pd.merge(
        Daily_SensorAndDevice_GapStatsDF[Daily_SensorAndDevice_GapStatsDF['Metric'] == 'Online'],
        GroupGatewayOnlineStatsDF, on=['StoreGroupID', 'AssessmentDate'], how='left')
    Daily_OnlineNormedDF['Metric'] = 'OnlineGroupAdjusted'
    Daily_OnlineNormedDF['Coverage'] = Daily_OnlineNormedDF['Coverage'] / Daily_OnlineNormedDF['GW_Online']
    Daily_OnlineNormedDF['Coverage'] = Daily_OnlineNormedDF['Coverage'].apply(lambda x: min(1, x))
    Daily_OnlineNormedDF['AnalysisType'] = 'Sensor_OnlineGroupAdjusted_Daily'

    Daily_GapStatsDF = pd.concat([Daily_SensorAndDevice_GapStatsDF, Daily_OnlineNormedDF], axis=0, ignore_index=True)
    del Daily_SensorAndDevice_GapStatsDF
    del Daily_OnlineNormedDF
    Daily_GapStatsDF = pd.merge(Daily_GapStatsDF, DevicesInfo[GapStats_DeviceInfoFields],
                                on=['DeviceID'], how='left')
    Daily_GapStatsDF.to_csv(daily_gapstats_filename, index=False)
    print "Daily_GapStatsDF:  wrote to file {file}   NumRows = {NumRows}".format(file=daily_gapstats_filename,
                                                                                 NumRows=len(Daily_GapStatsDF))

    ###
    ###  Generate Store-Level Online Metrics and Results
    ###

    print "starting store-level summary metric generation"
    #     data = pd.read_csv(daily_gapstats_filename, low_memory = False)
    # data = data[data.columns[1:]]
    EvalMetric = 'Sensor_Online_Daily'
    Daily_GapStatsDF = Daily_GapStatsDF[Daily_GapStatsDF['AnalysisType'] == EvalMetric]


    def find_loc(name):
        if re.search('MC', name):
            return 'MC'
        elif re.search('RX', name):
            return 'RX'
        else:
            return 'FS'


    Daily_GapStatsDF['Location'] = Daily_GapStatsDF['DeviceLabel'].apply(find_loc)

    Daily_GapStatsDF = Daily_GapStatsDF.merge(Daily_GapStatsDF[['GroupID', 'StoreNumber']].drop_duplicates(),
                                              left_on='StoreGroupID', right_on='GroupID')
    Daily_GapStatsDF = Daily_GapStatsDF.rename(columns={'StoreNumber_y': 'ParentStoreNumber'})

    #     Daily_GapStatsDF['PostalCode'] = Daily_GapStatsDF['PostalCode'].fillna(-1)
    #     print "Daily_GapStatsDF['PostalCode']: {}".format(Daily_GapStatsDF['PostalCode'])
    #     Daily_GapStatsDF['PostalCode'] = Daily_GapStatsDF['PostalCode'].apply(int)

    Daily_GapStatsDF['FLAG_DeviceMissing'] = Daily_GapStatsDF['Coverage'].apply(lambda x: 1 if x == 0 else 0)

    group_cols = ['StoreGroupID', 'AssessmentDate']
    failed_node_counts = Daily_GapStatsDF[group_cols + ['FLAG_DeviceMissing']].groupby(group_cols).sum()
    failed_node_counts.reset_index(level=0, inplace=True)
    failed_node_counts = failed_node_counts.rename(columns={'FLAG_DeviceMissing': 'NumSilentDevices'})

    cols = ['ParentStoreNumber', 'StoreGroupID', 'Address', 'City', 'StateOrProvince', 'PostalCode']
    cov = Daily_GapStatsDF[cols + ['Coverage']].groupby(cols).mean()

    cols = ['ParentStoreNumber', 'StoreGroupID', 'Address', 'City', 'StateOrProvince', 'PostalCode']
    MCcov = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'MC'][cols + ['Coverage']].groupby(cols).mean()
    RXcov = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'RX'][cols + ['Coverage']].groupby(cols).mean()
    FScov = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'FS'][cols + ['Coverage']].groupby(cols).mean()

    cov[EvalMetric + '_MC_Avg'] = MCcov
    cov[EvalMetric + '_RX_Avg'] = RXcov
    cov[EvalMetric + '_FS_Avg'] = FScov

    cols = ['ParentStoreNumber', 'StoreGroupID', 'Address', 'City', 'StateOrProvince', 'PostalCode', 'AssessmentDate']
    daybyday = Daily_GapStatsDF[cols + ['Coverage']].groupby(cols).mean().sort_index(level='AssessmentDate')
    MCdbd = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'MC'][cols + ['Coverage']].groupby(cols).mean()
    RXdbd = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'RX'][cols + ['Coverage']].groupby(cols).mean()
    FSdbd = Daily_GapStatsDF[Daily_GapStatsDF['Location'] == 'FS'][cols + ['Coverage']].groupby(cols).mean()

    daybyday = daybyday.rename(columns={'Coverage': EvalMetric + '_Avg'})
    daybyday[EvalMetric + '_MC_Avg'] = MCdbd
    daybyday[EvalMetric + '_RX_Avg'] = RXdbd
    daybyday[EvalMetric + '_FS_Avg'] = FSdbd

    daybyday = daybyday.reset_index()

    aug_daybyday = pd.DataFrame()
    for gid in daybyday['StoreGroupID'].unique():
        group = daybyday[daybyday['StoreGroupID'] == gid].sort_values(by='AssessmentDate')
        changes_vec = np.diff(group[EvalMetric + '_Avg'])
        group['OneDayCoverageChange'] = np.append([0], changes_vec)
        aug_daybyday = aug_daybyday.append(group)

    aug_daybyday = aug_daybyday.merge(failed_node_counts, on=['StoreGroupID'], how='outer')

    aug_daybyday.to_csv(summary_store_metrics_filename, index=False)
    print "Writing {numrows} rows of aug_daybyday to file: {file}".format(numrows=len(aug_daybyday),
                                                                          file=summary_store_metrics_filename)
DATABASE_CONN.close()