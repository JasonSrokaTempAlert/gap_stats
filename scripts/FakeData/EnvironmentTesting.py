import os
import sys
import inspect

def dirfind(current_dir):
    # current_dir = os.getcwd()

    # print current_dir
    # for i in range(count):
    # 	exec_base_dir = '/'.join(current_dir.split('/')[:-i])
    # 	print i
    # 	print exec_base_dir
    # print current_dir
    #current_dir= '/Users/Jason/Desktop/Repositories/TA_GapStats/scripts'
    if current_dir.endswith('FakeData'):
        current_dir = current_dir
        scripts_dir = '/'.join(current_dir.split('/')[:-1])
    elif current_dir.endswith('scripts'):
        scripts_dir = current_dir
        scripts_dir = current_dir + '/FakeData'
    elif current_dir.endswith('gap_stats'):
        scripts_dir = current_dir + '/scripts'
        current_dir = scripts_dir + '/FakeData'
    else:
        sys.exit("GO TO FakeData or gapstats or scripts Directory File (cd into that directory)-- how else can you git pull -_-!")
    return [current_dir, scripts_dir]
# TestRun_Environment =''


def gen_env_tag(current_dir):
    all = current_dir.split('/')
    # print "all: {}".format(all)
    who = current_dir.split('/')[2]
    # print "who: {}".format(who)
    if len(all) > 4:
        where = all[-4]
    else:
        where = 'ubuntu'
    # print "where: {}".format(where)
    env_tag = "_" + who + "_" + where
    return env_tag


def lookup_dbname_forenv(env_tag):
    print("lookup_dbname()::  called for env_tag: {tag}".format(tag=env_tag))

    if ('jason' in env_tag) | ('Jason' in env_tag):
        dbname = 'warehouse'
        print(
            "lookup_dbname()::  'jason' in string, setting db name to: {}".format(dbname))
    else:
        dbname = 'warehouse'
    return dbname

    # count=current_dir.count('/')
# print count
# print "lcl_path: {path}\nenv_1: {e1}\nenv_2: {e2}".format(path = lcl_path, e1 = env_1, e2 = env_2)
#
# print "os.getcwd(): {}".format(os.getcwd())
# current_dir = os.getcwd()
# if current_dir.find('/home/'):
# 	TestRun_Environment = 'ubuntu' + '_' + current_dir.split('/')[2] + '_' + current_dir.split('/')[-4]
# else :
# 	TestRun_Environment = 'local_' + current_dir.split('/')[2] + '_' + current_dir.split('/')[-4]
# print TestRun_Environment
