"""
Many Device Signals Module
"""
import warnings
from datetime import datetime

import pandas as pd
import numpy as np

from ta_utils import database
from ta_utils import Signal
from ta_utils import PersistableSignal
from ta_utils import IncrementalSignal


class ManyDeviceSignalsException(BaseException):
    pass


class ManyDeviceSignals(object):
    """
    The Many Device Signals Object represents many devices and many signals.
    """
    def __init__(self, data, device_id_col, reading_date_col,
                 port_col=None, processed_date_col=None, sort=True):
        """
        Initialize the DeviceSignals with either a

        :param data: A Pandas Dataframe or filename of a CSV to load.
        :type data: str | pd.DataFrame
        :param device_id_col: The column name that contains the device IDs.
        :type device_id_col: str
        :param reading_date_col: The column name that contains the reading dates.
        :type reading_date_col: str
        :param port_col: The column name that contains the ports.
        :type port_col: str
        :param processed_date_col: The column name that contains the process times.
        :type processed_date_col: str
        :param sort: Whether or not to sort signals by the date_col
        :type sort: bool
        """
        if isinstance(data, str):
            if data == 'Empty_MDS':
                self.data = None
                return
            else:
                self.data = pd.read_csv(data)
        elif isinstance(data, pd.DataFrame):
            self.data = data
        else:
            raise ManyDeviceSignalsException("""ManyDeviceSignals object has invalid signals:
                                                {0}""".format(data))

        self.device_id_col = device_id_col

        if self.device_id_col not in self.data:
            raise ManyDeviceSignalsException("""ManyDeviceSignals signals has no device id column:
                                                {0}""".format(self.device_id_col))

        if self.data[self.device_id_col].dtype != object:
            raise ManyDeviceSignalsException("""ManyDeviceSignals device id column has invalid type {0},
                                                expected object.""".format(self.data[self.device_id_col].dtype, ))

        self.date_col = reading_date_col

        if self.date_col not in self.data:
            raise ManyDeviceSignalsException("""ManyDeviceSignals signals has no date column:
                                                {0}""".format(self.date_col))

        self.data.loc[:, self.date_col] = pd.to_datetime(self.data[self.date_col])

        self.port_col = port_col

        if self.port_col is not None:
            try:
                self.data.loc[:, self.port_col] = self.data[self.port_col].apply(lambda x: None if x is None else int(x))
            except Exception as e:
                raise ManyDeviceSignalsException("ManyDeviceSignals port column failed to convert to type int | None: {}".format(e))

        self.processed_date_col = processed_date_col

        if self.processed_date_col is not None:
            self.data.loc[:, self.processed_date_col] = pd.to_datetime(self.data[self.processed_date_col])

        if sort and self.data is not None:
            self.data = self.data.sort_values(self.date_col)

    @property
    def devices(self):
        """
        Get a list of all unique device IDs.

        :return: List of unique device IDs
        :rtype: list
        """
        return list(self.data[self.device_id_col].unique())

    @property
    def device_port_pairs(self):
        """
        Get a list of tuples of all unique device and port IDs.

        :return: List of tuples of all unique device and port IDs.
        :rtype: list
        """
        if self.port_col is not None:
            result = [tuple(x) for x in
                        self.data[[self.device_id_col, self.port_col]].drop_duplicates().to_records(index=False)]
            print(result)
            return result

        raise ManyDeviceSignalsException("The device_port_pairs isn't valid if there is no port column specified.")

    def get_all_signals(self, signal_col,
                        min_time_override=None,
                        max_time_override=None,
                        reporting_interval_override=None):
        """
        Gets a list of Signals for a signal column.

        :param signal_col: The column name of the signal we want to get.
        :type signal_col: str
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time
        :type min_time_override: datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time
        :type max_time_override: datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :return: The requested Signals for all devices with this signal column.
        :rtype: list[Signal]
        """
        if self.port_col is not None:
            return [self.get_device_signal(device_id,
                                           signal_col,
                                           port=port,
                                           min_time_override=min_time_override,
                                           max_time_override=max_time_override,
                                           reporting_interval_override=reporting_interval_override)
                    for device_id, port in self.device_port_pairs]

        return [self.get_device_signal(device_id,
                                       signal_col,
                                       min_time_override=min_time_override,
                                       max_time_override=max_time_override,
                                       reporting_interval_override=reporting_interval_override)
                for device_id in self.devices]

    def get_device_signal(self, device_id, signal_col,
                          port=None,
                          min_time_override=None,
                          max_time_override=None,
                          min_processed_time_override=None,
                          max_processed_time_override=None,
                          reporting_interval_override=None):
        """
        Gets a Signal for the given device and signal column.

        :param device_id: The identifier for the device found within the device ID column.
        :type device_id: str
        :param signal_col: The column name of the signal we want to get.
        :type signal_col: str
        :param port: The identifier for the port of the device found within the port column.
        :type port: int
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type min_time_override: datetime | (str, str, str) -> datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type max_time_override: datetime | (str, str, str) -> datetime
        :param min_processed_time_override: An optional processed time used to bound the beginning of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type min_processed_time_override: datetime | (str, str, str) -> datetime
        :param max_processed_time_override: An optional processed time used to bound the end of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type max_processed_time_override: datetime | (str, str, str) -> datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :return: The requested Signal for the device and signal name.
        :rtype: Signal
        """
        if signal_col not in self.data:
            raise ManyDeviceSignalsException("ManyDeviceSignals signals has no column: {0}".format(signal_col))

        if port is not None and self.port_col is not None:
            device_data = self.data[(self.data[self.device_id_col] == device_id)
                                    & (self.data[self.port_col] == port)]
        else:
            device_data = self.data[self.data[self.device_id_col] == device_id]

        if len(device_data) == 0:
            raise ManyDeviceSignalsException("ManyDeviceSignals found no signals for device: {0}".format(device_id))

        device_dates = list(device_data[self.date_col])
        device_signal = list(device_data[signal_col])
        device_series = pd.Series(data=device_signal, index=device_dates)

        if self.processed_date_col is not None:
            device_processed_dates = list(device_data[self.processed_date_col])
            processed_dates = pd.Series(data=device_processed_dates, index=device_dates)
        else:
            processed_dates = None

        return Signal(device_series,
                      device_id=device_id,
                      data_type=signal_col,
                      port=port,
                      processed_dates=processed_dates,
                      min_time_override=min_time_override,
                      max_time_override=max_time_override,
                      min_processed_time_override=min_processed_time_override,
                      max_processed_time_override=max_processed_time_override,
                      reporting_interval_override=reporting_interval_override)

    def update_all_signals(self, signal_col,
                           segment_daily=False,
                           min_time_override=None,
                           max_time_override=None,
                           min_processed_time_override=None,
                           max_processed_time_override=None,
                           reporting_interval_override=None):
        """
        Updates all signals for a signal column.

        :param signal_col: The column name of the signal we want to get.
        :type signal_col: str
        :param segment_daily: Whether or not to run segment_daily on this update.
        :type segment_daily: bool
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type min_time_override: datetime | (str, str, str) -> datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type max_time_override: datetime | (str, str, str) -> datetime
        :param min_processed_time_override: An optional processed time used to bound the beginning of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type min_processed_time_override: datetime | (str, str, str) -> datetime
        :param max_processed_time_override: An optional processed time used to bound the end of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type max_processed_time_override: datetime | (str, str, str) -> datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :return: All updated Signals from this ManyDeviceSignals object.
        :rtype: list[PersistableSignal]
        """
        if self.port_col is not None:
            return [self.update_device_signal(device_id, signal_col,
                                              port=port,
                                              segment_daily=segment_daily,
                                              min_time_override=min_time_override,
                                              max_time_override=max_time_override,
                                              min_processed_time_override=min_processed_time_override,
                                              max_processed_time_override=max_processed_time_override,
                                              reporting_interval_override=reporting_interval_override)
                    for device_id, port in self.device_port_pairs]

        return [self.update_device_signal(device_id, signal_col,
                                          segment_daily=segment_daily,
                                          min_time_override=min_time_override,
                                          max_time_override=max_time_override,
                                          min_processed_time_override=min_processed_time_override,
                                          max_processed_time_override=max_processed_time_override,
                                          reporting_interval_override=reporting_interval_override)
                for device_id in self.devices]

    def update_device_signal(self, device_id, signal_col,
                             port=None,
                             segment_daily=False,
                             min_time_override=None,
                             max_time_override=None,
                             min_processed_time_override=None,
                             max_processed_time_override=None,
                             reporting_interval_override=None):
        """
        Updates an existing Signal for the given device and signal column.

        :param device_id: The identifier for the device found within the device ID column.
        :type device_id: str | int
        :param signal_col: The column name of the signal we want to get.
        :type signal_col: str
        :param port: The identifier for the port of the device found within the port column.
        :type port: str | int
        :param segment_daily: Whether or not to run segment_daily on this update.
        :type segment_daily: bool
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type min_time_override: datetime | (str, str, str) -> datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type max_time_override: datetime | (str, str, str) -> datetime
        :param min_processed_time_override: An optional processed time used to bound the beginning of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type min_processed_time_override: datetime | (str, str, str) -> datetime
        :param max_processed_time_override: An optional processed time used to bound the end of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type max_processed_time_override: datetime | (str, str, str) -> datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :return: The requested Signal for the device and signal name.
        :rtype: PersistableSignal
        """
        if port is not None and not (isinstance(port, int) or isinstance(port, np.int64)):
            raise ManyDeviceSignalsException("Port '{}' is not an integer, it is a '{}'!".format(port, type(port)))

        if callable(min_time_override):
            min_time_override_actual = min_time_override(device_id, signal_col, port)
        else:
            min_time_override_actual = min_time_override

        if callable(max_time_override):
            max_time_override_actual = max_time_override(device_id, signal_col, port)
        else:
            max_time_override_actual = max_time_override

        if callable(min_processed_time_override):
            min_processed_time_override_actual = min_processed_time_override(device_id, signal_col, port)
        else:
            min_processed_time_override_actual = min_processed_time_override

        if callable(max_processed_time_override):
            max_processed_time_override_actual = max_processed_time_override(device_id, signal_col, port)
        else:
            max_processed_time_override_actual = max_processed_time_override

        if callable(reporting_interval_override):
            reporting_interval_override_actual = reporting_interval_override(device_id, signal_col, port)
        else:
            reporting_interval_override_actual = reporting_interval_override

        new_signal = self.get_device_signal(device_id, signal_col,
                                            port=port,
                                            min_time_override=min_time_override_actual,
                                            max_time_override=max_time_override_actual,
                                            min_processed_time_override=min_processed_time_override_actual,
                                            max_processed_time_override=max_processed_time_override_actual,
                                            reporting_interval_override=reporting_interval_override_actual)

        with database.atomic():
            try:
                if port is not None:
                    port_query = PersistableSignal.port == port
                else:
                    port_query = PersistableSignal.port.is_null()

                persisted_signal = (PersistableSignal
                                    .select()
                                    .where((PersistableSignal.device_id == device_id) &
                                           (PersistableSignal.data_type == signal_col) &
                                           port_query)
                                    .get())

                incremental_signal = IncrementalSignal(persisted_signal, new_signal)
                return incremental_signal.save()
            except PersistableSignal.DoesNotExist:
                warning_msg = "No existing signal with device_id='{}', data_type='{}', port='{}', creating a new one!"
                warnings.warn(warning_msg.format(device_id, signal_col, port))

                saved_signal = new_signal.save()

                if segment_daily:
                    new_segments = new_signal.segment_daily()
                    for new_segment in new_segments:
                        new_segment.save()

                return saved_signal

    @staticmethod
    def update_empty_device_signal(device_id, signal_col,
                                   port=None,
                                   segment_daily=False,
                                   min_time_override=None,
                                   max_time_override=None,
                                   min_processed_time_override=None,
                                   max_processed_time_override=None,
                                   reporting_interval_override=None):
        """
        Updates an existing Signal for the given device and signal column.

        :param device_id: The identifier for the device found within the device ID column.
        :type device_id: str
        :param signal_col: The column name of the signal we want to get.
        :type signal_col: str
        :param port: The integer for the port of the device found within the port column.
        :type port: int
        :param segment_daily: Whether or not to run segment_daily on this update.
        :type segment_daily: bool
        :param min_time_override: An optional time used to bound the beginning of this signal instead of using
            the first reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type min_time_override: datetime | (str, str, str) -> datetime
        :param max_time_override: An optional time used to bound the end of this signal instead of using
            the last reading's time. Takes a time or a function that accepts a device_id and signal_col.
        :type max_time_override: datetime | (str, str, str) -> datetime
        :param min_processed_time_override: An optional processed time used to bound the beginning of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type min_processed_time_override: datetime | (str, str, str) -> datetime
        :param max_processed_time_override: An optional processed time used to bound the end of this signal.
            Takes a time or a function that accepts a device_id and signal_col.
        :type max_processed_time_override: datetime | (str, str, str) -> datetime
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :param reporting_interval_override: An optional override for the interval between readings as a number
            of seconds
        :type reporting_interval_override: int
        :return: The requested Signal for the device and signal name.
        :rtype: PersistableSignal
        """
        if port is not None and not (isinstance(port, int) or isinstance(port, np.int64)):
            raise ManyDeviceSignalsException("Port '{}' is not an integer, it is a '{}'!".format(port, type(port)))

        if callable(min_time_override):
            min_time_override_actual = min_time_override(device_id, signal_col, port)
        else:
            min_time_override_actual = min_time_override

        if callable(max_time_override):
            max_time_override_actual = max_time_override(device_id, signal_col, port)
        else:
            max_time_override_actual = max_time_override

        if callable(min_processed_time_override):
            min_processed_time_override_actual = min_processed_time_override(device_id, signal_col, port)
        else:
            min_processed_time_override_actual = min_processed_time_override

        if callable(max_processed_time_override):
            max_processed_time_override_actual = max_processed_time_override(device_id, signal_col, port)
        else:
            max_processed_time_override_actual = max_processed_time_override

        if callable(reporting_interval_override):
            reporting_interval_override_actual = reporting_interval_override(device_id, signal_col, port)
        else:
            reporting_interval_override_actual = reporting_interval_override

        new_signal = Signal(None,
                            device_id=device_id,
                            data_type=signal_col,
                            port=port,
                            min_time_override=min_time_override_actual,
                            max_time_override=max_time_override_actual,
                            min_processed_time_override=min_processed_time_override_actual,
                            max_processed_time_override=max_processed_time_override_actual,
                            reporting_interval_override=reporting_interval_override_actual)

        with database.atomic():
            try:
                if port is not None:
                    port_query = PersistableSignal.port == port
                else:
                    port_query = PersistableSignal.port.is_null()

                persisted_signal = (PersistableSignal
                                    .select()
                                    .where((PersistableSignal.device_id == device_id) &
                                           (PersistableSignal.data_type == signal_col) &
                                           port_query)
                                    .get())
                later_time = new_signal.max_time is not None and new_signal.max_time > persisted_signal.max_time
                later_processed_time = new_signal.max_processed_time is not None and new_signal.max_processed_time > persisted_signal.max_processed_time
                if later_time or later_processed_time:
                    incremental_signal = IncrementalSignal(persisted_signal, new_signal)
                    return incremental_signal.save()
                else:
                    return persisted_signal
            except PersistableSignal.DoesNotExist:
                warning_msg = "No existing signal with device_id='{}', data_type='{}', port='{}', creating a new one!"
                warnings.warn(warning_msg.format(device_id, signal_col, port))

                saved_signal = new_signal.save()

                if segment_daily:
                    new_segments = new_signal.segment_daily()
                    for new_segment in new_segments:
                        new_segment.save()

                return saved_signal
