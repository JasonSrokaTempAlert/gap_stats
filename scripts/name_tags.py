import pandas as pd
import psycopg2
from math import floor

from timing import timing

from config import user, pg_host, pg_user, pg_password


WAREHOUSE_DB_NAME = 'warehouse'
warehouse_conn_string = "host='{host}' dbname='{dbname}' user='{user}' password='{password}'".format(
    host=pg_host, dbname=WAREHOUSE_DB_NAME, user=pg_user, password=pg_password)
print("Warehouse Database: {}".format(warehouse_conn_string))
WAREHOUSE_DATABASE_CONN = psycopg2.connect(warehouse_conn_string)


@timing
def name_tags(devices_info, this_data_tag):
    monitored_devices_info = pd.DataFrame()
    group_ids = None
    device_ids = None
    group_id = None
    device_id = None
    account_id = None

    if this_data_tag == 'GROUPTEST':
        group_ids = [17291, 17283, 17201, 52975, 54290]
    elif this_data_tag == 'CSL':
        account_id = 5223
    elif this_data_tag == 'DEVICETEST':
        device_id = ['11666000000170224003']
    elif this_data_tag == 'LTEGATEWAYS':
        group_ids = devices_info[(devices_info['hardwarefamily']==3) & (devices_info['hardwareversion']==9)].groupid.unique()
    elif this_data_tag == 'CDMAHOMEKITS':
        group_ids = [18561,18564,18568,18579,18581,18582,18591,18594,18738,19082,43000,50338,50833,50853,54342,54428, \
                     54429,55626,55627,55704,55722,55723,55725,55727,55728,55729]
    elif this_data_tag == 'NEWENGLAND':
        devices_info = devices_info[devices_info['accountid'] == 4226]
        print(
            "after CVS account filtering: {count}".format(
                count=len(devices_info)))
        new_england_devices_info = devices_info[(devices_info['state'].isin(
            ['MA', 'ME', 'RI', 'CT', 'VT', 'VA', 'MS', 'NH'])) & ~(devices_info['groupid'].isin([8878, 18524]))]
        group_ids = new_england_devices_info['groupid'].unique()
    elif this_data_tag == 'CVSSTORM':
        devices_info = devices_info[devices_info['accountid'] == 4226]
        print("after CVS account filtering: {count}".format(count=len(
            devices_info)))
        new_england_devices_info = devices_info[(devices_info['state'].isin(
            ['AL', 'GA', 'NC', 'LA', 'MS', 'TX'])) & ~(devices_info['groupid'].isin([8878, 18524]))]
        group_ids = new_england_devices_info['groupid'].unique()
    elif this_data_tag == 'GOLIVEFS':
        devices_info = devices_info[devices_info['accountid'] == 4226]
        print(
            "after CVS account filtering: {count}".format(
                count=len(devices_info)))
        new_england_devices_info = devices_info[((devices_info['state'] == 'RI') & ~(
            devices_info['groupid'].isin([8878, 18524])))]
        group_ids = new_england_devices_info['groupid'].unique()
    elif this_data_tag == 'ALLCVS':
        devices_info = devices_info[devices_info['accountid'] == 4226]
        print(
            "after CVS account filtering: {count}".format(
                count=len(devices_info)))
        all_cvs_devices_info = devices_info[~(devices_info['groupid'].isnull()) & ~(
            devices_info['groupid'].isin([18861, 11886, 12545, 8878, 12088, 8949, 9486, 17460, 11564, 18524]))]
        group_ids = all_cvs_devices_info['groupid'].unique()
    elif this_data_tag == '99':
        account_id = 7376
    elif this_data_tag == 'GOOGLE':
        account_id = 7164
    elif this_data_tag == 'DOMINOS':
        account_id = 7394
    elif this_data_tag == 'WALGREENS':
        account_id = 7033
    elif this_data_tag == 'SAVEALOT':
        account_id = 7548
    elif this_data_tag == 'IPIC':
        account_id = 7489
    elif this_data_tag == 'SHERIDANWINDERMERE':
        account_id = 7490
    elif this_data_tag == 'RACHAELSFOODS':
        account_id = 7564
    elif this_data_tag == 'JENNYCRAIG':
        account_id = 7511
    elif this_data_tag == 'RACETRAC':
        account_id = 7306
    elif this_data_tag == 'VALLEE':
        account_id = 7431
    elif this_data_tag == 'ALBERTSONS':
        account_id = 5169
    elif this_data_tag == 'SMART':
        account_id = 1000026
    elif this_data_tag == 'SAFE':
        account_id = 1000027
    elif this_data_tag == 'SAFEWC':
        account_id = 1000028
    elif this_data_tag == 'CUMBIES':
        account_id = 5767
    elif this_data_tag == 'WEGMANS':
        account_id = 7429
    elif this_data_tag == 'AREAGLE':
        group_id = 41653
    elif this_data_tag == 'CULINART':
        group_id = 1267
    elif this_data_tag.startswith('CVS_'):
        splitstr = this_data_tag.split('_')
        thisset = int(splitstr[1])
        numsets = int(splitstr[2])
        all_group_ids = devices_info[devices_info['accountid'] == 4226]['groupid'].unique()
        all_group_ids.sort()
        num_groups = len(all_group_ids)
        group_ids = all_group_ids[(thisset-1)*floor(num_groups/numsets):thisset*floor(num_groups/numsets)+1]
    else:
        accountid_query = "select * from account where upper(accountname) = upper('{}')".format(
            this_data_tag)
        account_id_data = pd.read_sql(accountid_query, WAREHOUSE_DATABASE_CONN)
        if account_id_data.empty:
            raise RuntimeError(
                "No devices found with account name '{tag}'".format(
                    tag=this_data_tag))
        account_id = account_id_data['accountid'].values[0]
        print(
            "Account name {name} returned account id {acct}".format(
                acct=account_id,
                name=this_data_tag))

    if group_ids is not None:
        monitored_devices_info = devices_info[(devices_info['groupid'].isin(
            group_ids)) | (devices_info['parentgroupid'].isin(group_ids))]
    elif device_id is not None:
        monitored_devices_info = devices_info[devices_info['deviceid'].isin(
            device_id)]
    elif device_ids is not None:
        monitored_devices_info = devices_info[devices_info['deviceid'].isin(
            device_ids)]
    elif group_id is not None:
        monitored_devices_info = devices_info[devices_info['groupid'] == group_id]
    elif account_id is not None:
        monitored_devices_info = devices_info[devices_info['accountid'] == account_id]

    if len(monitored_devices_info) == 0:
        raise RuntimeError(
            "No devices found for data tag '{tag}'".format(
                tag=this_data_tag))

    print(
        "{}_devices_info:\n{}".format(
            this_data_tag,
            monitored_devices_info.head(10)))

    return monitored_devices_info
