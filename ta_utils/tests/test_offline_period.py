"""
Test Gap Module
"""

import unittest
from datetime import datetime, timedelta

from ta_utils import OfflinePeriod

SECONDS_IN_DAY = 60 * 60 * 24

now = datetime.now()
offline_period = OfflinePeriod(now, now + timedelta(days=1), now + timedelta(days=2))
other_offline_period = OfflinePeriod(now + timedelta(days=1), now + timedelta(days=2), now + timedelta(days=3))
same_offline_period = OfflinePeriod(now, now + timedelta(days=1), now + timedelta(days=2))


class OfflinePeriodTestCase(unittest.TestCase):
    def test_offline_period_length(self):
        self.assertEqual(SECONDS_IN_DAY, offline_period.length)

    def test_offline_period_equals(self):
        self.assertFalse(offline_period == None)
        self.assertFalse(offline_period == offline_period.length)
        self.assertFalse (other_offline_period == offline_period)
        self.assertEqual(same_offline_period, offline_period)
