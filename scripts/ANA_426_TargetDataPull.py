import os
from trundle.trundle_client import TrundleClient
import pandas as pd
import numpy as np
import psycopg2
from datetime import datetime, timedelta


def format_datetime(raw_datetime):
    # print "format_datetime()::  called with raw_datetime: {}".format(raw_datetime)
    result = raw_datetime.replace('T', ' ')
    datetime_str = result.split('.')[0]
    return_datetime = datetime.strptime(datetime_str,'%Y-%m-%d %H:%M:%S')
    # print "format_datetime({raw})::  generated result: {conv}".format(raw = raw_datetime, conv = return_datetime)
    return return_datetime


ACCESS_CREDS_DIR = '/Users/Jason/Desktop/Repositories/Prod-Trundle-AnalyticsConfig/'
thisClient = TrundleClient(os.path.join(ACCESS_CREDS_DIR, 'cvs.json'))

results_file = '/Users/Jason/Desktop/tmp/ANA_423_TargetPull/TargetData.csv'

print "Starting ANA_426 Script"

conn_string = "host='analytics-datawarehouse-va.c28weovqa2mc.us-east-1.rds.amazonaws.com' dbname='warehouse' user='jason' password='qvz2LFRzMMVPku7' "
DATABASE_CONN = psycopg2.connect(conn_string)

groups_query = "select groupid, groupname from vu_deviceandstoreinfo where accountid = 4226 and (groupname like '%#16%' or groupname like '%#17%')"
groups_df = pd.read_sql(groups_query,DATABASE_CONN)

print "groups_df: {}".format(groups_df)


FLAG_FIRSTWRITE = True

# num_groups = len(groups_df)
# for index, groupid in enumerate(groups_df['groupid']):
#     print "{time}:     {index} of {len}: processing group: {id}".format(time = datetime.now(), index = index, len = num_groups, id = groupid)
#
#     single_group_query = """select * from
# (select *, raw_times_table.readingdate_utc -
# case when raw_times_table.timezone = 'Pacific Standard Time' then interval '7 hours'
# when raw_times_table.timezone = 'Mountain Standard Time' then interval '6 hours'
# when raw_times_table.timezone = 'Central Standard Time' then interval '5 hours'
# when raw_times_table.timezone = 'Eastern Standard Time' then interval '4 hours'
# end as readingdate_local
# from
# (select 'Device:' || s.deviceid StrDeviceID, t.portid, t.readingdate readingdate_utc, t.originaltemperature raw_sensor_temperature, d.timezone from temperature t
# join sensor s on t.sensorid = s.sensorid
# join device d on s.deviceid = d.deviceid
# where t.readingdate > '2017-06-05 21:00:00' and t.readingdate <= '2017-06-09 04:00:00' and
# d.deviceid in
# (select deviceid from device d where d.datedeleted is null and  d.groupid =
# {groupid})) raw_times_table) raw_and_local_times_table
# where readingdate_local >= '2017-06-05 21:00:00' and readingdate_local <= '2017-06-08 21:00:00';""".format(groupid = groupid)
#     print "formed query:\n{}".format(single_group_query)
#
#     this_group_data = pd.read_sql(single_group_query,DATABASE_CONN)
#     print "query returned {len} rows".format(len = len(this_group_data))
#     this_group_data.to_csv(results_file, mode = 'w' if FLAG_FIRSTWRITE else 'a')
#     print "{time}:    wrote to output file {file}".format(time = datetime.now(), file = results_file)


devices_query = """select deviceid from device d where d.datedeleted is null and  d.groupid in
(select groupid from vu_deviceandstoreinfo where accountid = 4226 and hardwarefamily = 50 and (groupname like '%#16%' or groupname like '%#17%'));"""
devices_df = pd.read_sql(devices_query, DATABASE_CONN)
print "devices_df:  {}".format(devices_df)

num_devices = len(devices_df)
for index, device_id in enumerate(devices_df['deviceid']):
    print "iter {iter} of {tot}: deviceid: {id}".format(iter = index, tot = num_devices, id = device_id)
    base_device_query = "select 'Device:' || d.deviceid strdeviceid, d.deviceid, v.devicelabel, v.groupname, d.timezone from vu_deviceandstoreinfo v join device d on v.deviceid = d.deviceid where d.deviceid = '{}'".format(device_id)
    base_device_df = pd.read_sql(base_device_query, DATABASE_CONN)
    # print "base_device_df: {}".format(base_device_df)

    device_timezone = base_device_df['timezone'].values[0]
    if  device_timezone == 'Eastern Standard Time':
        offset = 4
    elif device_timezone == 'Central Standard Time':
        offset = 5
    elif device_timezone == 'Mountain Standard Time':
        offset = 6
    elif device_timezone == 'Pacific Standard Time':
        offset = 7
    else:
        offset = 0

    start_time_base = datetime.strptime('2017-06-05 21:00:00', '%Y-%m-%d %H:%M:%S')
    start_time = start_time_base + timedelta(hours = offset)
    end_time_base = datetime.strptime('2017-06-08 21:00:00', '%Y-%m-%d %H:%M:%S')
    end_time = end_time_base + timedelta(hours = offset)

    # print "timezone: {tz}  start: {start}   end: {end}".format(tz = device_timezone, start = start_time, end = end_time)

    thisDeviceDataDF = pd.DataFrame(thisClient.get_active_device_by_device_id(device_id = device_id))
    # print "sensor lookup:\n {}".format(thisDeviceDataDF)

    if 'port' in thisDeviceDataDF.columns:
        ports = thisDeviceDataDF['port'].values

        for port in ports:
            print "processing port: {}".format(port)
            thisSensorDataDF = pd.DataFrame(thisClient.get_sensor_readings_by_port_id(device_id, port, verbose=True,
                                                      reading_start_dt = start_time,
                                                      reading_end_dt = end_time))
            if len(thisSensorDataDF) == 0:
                print "No data found, skipping"
                continue
            # print "thisSensorDataDF.head():  {}".format(thisSensorDataDF.head())
            thisSensorDataDF = thisSensorDataDF.drop('alarm',1)
            thisSensorDataDF = thisSensorDataDF.drop('processedDate',1)
            thisSensorDataDF = thisSensorDataDF.drop('unit',1)
            thisSensorDataDF = thisSensorDataDF.drop('readingType',1)
            thisSensorDataDF['readingDate'] = thisSensorDataDF['readingDate'].apply(format_datetime)
            thisSensorDataDF['readingDate_local'] = thisSensorDataDF['readingDate'].apply(lambda x: x - timedelta(hours = offset))
            thisSensorDataDF['temperature_inF'] = thisSensorDataDF['readingValue']*1.8+32
            thisSensorDataDF['deviceid'] = device_id
            thisSensorDataDF['port'] = port
            # print "thisSensorDataDF.head():  {}".format(thisSensorDataDF.head())

            thisSensorDataDF = thisSensorDataDF.merge(base_device_df, on = 'deviceid')
            # print "writing to file {file}, FLAG_FIRSTWRITE: {FLAG_FIRSTWRITE}".format(file = results_file, FLAG_FIRSTWRITE = FLAG_FIRSTWRITE)
            thisSensorDataDF.to_csv(results_file, mode = 'w' if FLAG_FIRSTWRITE else 'a', index=False, header= FLAG_FIRSTWRITE)
            FLAG_FIRSTWRITE = False
