"""
Persistable Models
"""
import peewee
from peewee import fn
import pandas as pd

from ta_utils import database
from ta_utils import Persistable
from ta_utils import logger

DeferredSignal = peewee.DeferredRelation()


class PersistenceException(BaseException):
    pass


class PersistableDelay(Persistable):
    """
    This class represents the persistence gap object.
    """
    reading_date = peewee.DateTimeField(index=True)
    processed_date = peewee.DateTimeField(index=True)
    age = peewee.IntegerField(index=True)
    signal = peewee.ForeignKeyField(DeferredSignal, index=True)

    class Meta:
        db_table = 'delays'
        database = database

    def __init__(self, delay=None, *args, **kwargs):
        """
        Creates a PersistableDelay. If you pass a delay, it will automatically create
        a persistence record from that Delay object.

        :param delay: Optionally a delay to create the record.
        """
        super(PersistableDelay, self).__init__(*args, **kwargs)

        if delay is not None:
            self.reading_date = delay.reading_date
            self.processed_date = delay.processed_date
            self.age = delay.age


class PersistableGap(Persistable):
    """
    This class represents the persistence gap object.
    """
    previous_reading = peewee.DateTimeField(index=True)
    start = peewee.DateTimeField(index=True)
    end = peewee.DateTimeField(index=True)
    length = peewee.IntegerField(index=True)
    signal = peewee.ForeignKeyField(DeferredSignal,
                                    index=True)

    class Meta:
        db_table = 'gaps'
        database = database

    def __init__(self, gap=None, *args, **kwargs):
        """
        Creates a PersistableGap. If you pass a gap, it will automatically create
        a persistence record from that Gap object.

        :param gap: Optionally a gap to create the record.
        """
        super(PersistableGap, self).__init__(*args, **kwargs)

        if gap is not None:
            self.previous_reading = gap.previous_reading
            self.start = gap.start
            self.end = gap.end
            self.length = gap.length


class PersistableOfflinePeriod(Persistable):
    """
    This class represents the persistence offline period object.
    """
    previous_transmission = peewee.DateTimeField(index=True)
    start = peewee.DateTimeField(index=True)
    end = peewee.DateTimeField(index=True)
    length = peewee.IntegerField(index=True)
    signal = peewee.ForeignKeyField(DeferredSignal,
                                    index=True)

    class Meta:
        db_table = 'offline_periods'
        database = database

    def __init__(self, offline_period=None, *args, **kwargs):
        """
        Creates a PersistableOfflinePeriod. If you pass a offline period, it will automatically create
        a persistence record from that offline period object.

        :param offline_period: Optionally a offline_period to create the record.
        """
        super(PersistableOfflinePeriod, self).__init__(*args, **kwargs)

        if offline_period is not None:
            self.previous_transmission = offline_period.previous_transmission
            self.start = offline_period.start
            self.end = offline_period.end
            self.length = offline_period.length


class PersistableSignal(Persistable):
    """
    This class represents the persistence signal object, stripped of the series signals itself.

    Instead the record holds metadata about the signal, such as: count, mean, min_time, num_gaps, coverage, etc.
    """
    device_id = peewee.TextField(index=True, null=True)
    port = peewee.IntegerField(index=True, null=True)
    data_type = peewee.TextField(index=True, null=True)

    count = peewee.IntegerField()
    mean = peewee.FloatField(null=True)
    std = peewee.FloatField(null=True)
    var = peewee.FloatField(null=True)
    min = peewee.FloatField(null=True)
    max = peewee.FloatField(null=True)
    mid = peewee.FloatField(null=True)
    min_time = peewee.DateTimeField(null=True)
    max_time = peewee.DateTimeField(null=True)
    min_reading_time = peewee.DateTimeField(null=True)
    max_reading_time = peewee.DateTimeField(null=True)
    min_processed_time = peewee.DateTimeField(null=True)
    max_processed_time = peewee.DateTimeField(null=True)
    min_time_override = peewee.DateTimeField(null=True)
    max_time_override = peewee.DateTimeField(null=True)
    min_processed_time_override = peewee.DateTimeField(null=True)
    max_processed_time_override = peewee.DateTimeField(null=True)
    mode_minute_interval = peewee.IntegerField(null=True)
    reporting_interval_override = peewee.IntegerField(null=True)

    num_gaps = peewee.IntegerField(null=True)
    total_gap_time = peewee.BigIntegerField(null=True)
    max_gap_time = peewee.IntegerField(null=True)
    min_gap_time = peewee.IntegerField(null=True)

    num_offline_periods = peewee.IntegerField(null=True)
    total_offline_period_time = peewee.BigIntegerField(null=True)
    max_offline_period_time = peewee.IntegerField(null=True)
    min_offline_period_time = peewee.IntegerField(null=True)

    num_delays = peewee.IntegerField(null=True)
    total_delay_time = peewee.BigIntegerField(null=True)
    max_delay_time = peewee.IntegerField(null=True)
    min_delay_time = peewee.IntegerField(null=True)

    coverage = peewee.FloatField(null=True)
    on_time = peewee.FloatField(null=True)
    on_time_of_delivered = peewee.FloatField(null=True)
    online = peewee.FloatField(null=True)

    class Meta:
        db_table = 'signals'
        database = database

        indexes = (
            # create a unique on device_id, port, data_type
            (('device_id', 'port', 'data_type'), True),
        )

    def __init__(self, signal=None, *args, **kwargs):
        """
        Creates a PersistableSignal. If you pass a signal, it will automatically create
        a persistence record from that Signal object.

        :param signal: Optionally a signal to create the record.
        :type signal: Signal
        """
        super(PersistableSignal, self).__init__(*args, **kwargs)

        if signal is not None:
            self.signal = signal

            self.device_id = signal.device_id
            self.port = signal.port
            self.data_type = signal.data_type

            self.count = signal.count
            self.mean = signal.mean
            self.std = signal.std
            self.var = signal.var
            self.min = signal.min
            self.max = signal.max
            self.mid = signal.mid
            self.min_time = signal.min_time
            self.max_time = signal.max_time
            self.min_reading_time = signal.min_reading_time
            self.max_reading_time = signal.max_reading_time
            self.min_processed_time = signal.min_processed_time
            self.max_processed_time = signal.max_processed_time
            self.min_time_override = signal.min_time_override
            self.max_time_override = signal.max_time_override
            self.min_processed_time_override = signal.min_processed_time_override
            self.max_processed_time_override = signal.max_processed_time_override
            self.mode_minute_interval = signal.mode_minute_interval
            self.reporting_interval_override = signal.reporting_interval_override

    def recompute_metrics(self):
        self.num_gaps = self._num_gaps
        self.total_gap_time = self._total_gap_time
        self.max_gap_time = self._max_gap_time
        self.min_gap_time = self._min_gap_time

        self.num_offline_periods = self._num_offline_periods
        self.total_offline_period_time = self._total_offline_period_time
        self.max_offline_period_time = self._max_offline_period_time
        self.min_offline_period_time = self._min_offline_period_time

        self.num_delays = self._num_delays
        self.total_delay_time = self._total_delay_time
        self.max_delay_time = self._max_delay_time
        self.min_delay_time = self._min_delay_time

        logger.info("PersistableSignal.recompute_metrics() calling coverage calc")
        self.coverage = self._coverage
        self.on_time = self._on_time
        self.on_time_of_delivered = self._on_time_of_delivered
        self.online = self._online

    @property
    def time_span(self):
        """
        The difference between minimum and maximum time in this signal.

        :return: The time span of the signal
        :rtype: pd.Timedelta
        """
        time_span = pd.Timedelta(self.max_time - self.min_time)
        return max(time_span, pd.Timedelta(seconds=1))

    @property
    def proxy_gap_filter(self):
        """
        Returns the proxy gap filter used to select only gaps within the signal segment.

        :return: Proxy Gap Filter
        """
        if self.min_time_override is None and self.max_time_override is None:
            return True

        return (((PersistableGap.start >= self.min_time) &
                (PersistableGap.start < self.max_time)) |
                ((PersistableGap.end > self.min_time) &
                 (PersistableGap.end <= self.max_time)) |
                ((PersistableGap.start < self.min_time) &
                 (PersistableGap.end > self.max_time)))

    @property
    def proxy_delay_filter(self):
        """
        Returns the proxy delay filter used to select only delays within the signal segment.

        :return: Proxy Delay Filter
        """
        if self.min_time_override is None and self.max_time_override is None:
            return True

        return ((PersistableDelay.reading_date >= self.min_time) &
                (PersistableDelay.reading_date < self.max_time))

    @property
    def proxy_offline_period_filter(self):
        """
        Returns the proxy delay filter used to select only delays within the signal segment.

        :return: Proxy Delay Filter
        """
        if self.min_processed_time_override is None and self.max_processed_time_override is None:
            return True

        return (((PersistableOfflinePeriod.start >= self.min_processed_time) &
                 (PersistableOfflinePeriod.start < self.max_processed_time)) |
                ((PersistableOfflinePeriod.end > self.min_processed_time) &
                 (PersistableOfflinePeriod.end <= self.max_processed_time)) |
                ((PersistableOfflinePeriod.start < self.min_processed_time) &
                 (PersistableOfflinePeriod.end > self.max_processed_time)))

    @property
    def _num_gaps(self):
        """
        Get the total number of gaps in this Signal.

        :return: Number of gaps in this signal.
        :rtype: int
        """
        return (PersistableGap
                .select()
                .where((PersistableGap.signal == self) &
                       self.proxy_gap_filter)
                .count())

    @property
    def _total_gap_time(self):
        """
        Computes the total gap time of a signal.

        :return: Total gap time, in seconds.
        :rtype: int
        """
        # If there is no proxy, the calculation is trivial. With a proxy, you need to make sure you aren't
        # counting gap times that happen before the day begins or after the day ends.
        if self.min_time_override is None and self.max_time_override is None:
            gap_time = (PersistableGap
                        .select(fn.SUM(PersistableGap.length))
                        .where((PersistableGap.signal == self) &
                               self.proxy_gap_filter)
                        .scalar())
        else:
            gaps = (PersistableGap
                    .select()
                    .where((PersistableGap.signal == self) &
                           self.proxy_gap_filter))
            gap_time = 0
            for gap in gaps:
                start_time = max(self.min_time, gap.start)
                end_time = min(self.max_time, gap.end)
                gap_time += (end_time - start_time).total_seconds()

        if gap_time is None:
            return 0
        else:
            return gap_time

    @property
    def _max_gap_time(self):
        """
        Computes the max gap time of a signal.

        :return: Max gap time, in seconds.
        :rtype: int
        """
        max_gap_time = (PersistableGap
                        .select(fn.MAX(PersistableGap.length))
                        .where((PersistableGap.signal == self) &
                               self.proxy_gap_filter)
                        .scalar())

        if max_gap_time is None:
            return 0
        else:
            return max_gap_time
            
    @property
    def _min_gap_time(self):
        """
        Computes the min gap time of a signal.

        :return: Min gap time, in seconds.
        :rtype: int
        """
        min_gap_time = (PersistableGap
                        .select(fn.MIN(PersistableGap.length))
                        .where((PersistableGap.signal == self) &
                               self.proxy_gap_filter)
                        .scalar())

        if min_gap_time is None:
            return 0
        else:
            return min_gap_time
            
    @property
    def _num_offline_periods(self):
        """
        Get the total number of offline_periods in this Signal.

        :return: Number of offline_periods in this signal.
        :rtype: int
        """
        return (PersistableOfflinePeriod
                .select()
                .where((PersistableOfflinePeriod.signal == self) &
                       self.proxy_offline_period_filter)
                .count())

    @property
    def _total_offline_period_time(self):
        """
        Computes the total offline_period time of a signal.

        :return: Total offline_period time, in seconds.
        :rtype: int
        """
        # If there is no proxy, the calculation is trivial. With a proxy, you need to make sure you aren't
        # counting offline_period times that happen before the day begins or after the day ends.
        if self.min_processed_time_override is None and self.max_processed_time_override is None:
            offline_period_time = (PersistableOfflinePeriod
                                   .select(fn.SUM(PersistableOfflinePeriod.length))
                                   .where((PersistableOfflinePeriod.signal == self) &
                                          self.proxy_offline_period_filter)
                                   .scalar())
        else:
            offline_periods = (PersistableOfflinePeriod
                               .select()
                               .where((PersistableOfflinePeriod.signal == self) &
                                      self.proxy_offline_period_filter))
            offline_period_time = 0
            for offline_period in offline_periods:
                start_time = max(self.min_processed_time, offline_period.start)
                end_time = min(self.max_processed_time, offline_period.end)
                offline_period_time += (end_time - start_time).total_seconds()

        if offline_period_time is None:
            return 0
        else:
            return offline_period_time

    @property
    def _max_offline_period_time(self):
        """
        Computes the max offline_period time of a signal.

        :return: Max offline_period time, in seconds.
        :rtype: int
        """
        max_offline_period_time = (PersistableOfflinePeriod
                                   .select(fn.MAX(PersistableOfflinePeriod.length))
                                   .where((PersistableOfflinePeriod.signal == self) &
                                          self.proxy_offline_period_filter)
                                   .scalar())

        if max_offline_period_time is None:
            return 0
        else:
            return max_offline_period_time

    @property
    def _min_offline_period_time(self):
        """
        Computes the min offline_period time of a signal.

        :return: Min offline_period time, in seconds.
        :rtype: int
        """
        min_offline_period_time = (PersistableOfflinePeriod
                                   .select(fn.MIN(PersistableOfflinePeriod.length))
                                   .where((PersistableOfflinePeriod.signal == self) &
                                          self.proxy_offline_period_filter)
                                   .scalar())

        if min_offline_period_time is None:
            return 0
        else:
            return min_offline_period_time

    @property
    def _num_delays(self):
        """
        Get the total number of delays in this Signal.

        :return: Number of delays in this signal.
        :rtype: int
        """
        return (PersistableDelay
                .select()
                .where((PersistableDelay.signal == self) &
                       self.proxy_delay_filter)
                .count())

    @property
    def _total_delay_time(self):
        """
        Computes the total delay time of a signal.

        :return: Total delay time, in seconds.
        :rtype: int
        """
        total_delay_time = (PersistableDelay
                            .select(fn.SUM(PersistableDelay.age))
                            .where((PersistableDelay.signal == self) &
                                   self.proxy_delay_filter)
                            .scalar())

        if total_delay_time is None:
            return 0
        else:
            return total_delay_time

    @property
    def _max_delay_time(self):
        """
        Computes the max delay time of a signal.

        :return: Max delay time, in seconds.
        :rtype: int
        """
        max_delay_time = (PersistableDelay
                          .select(fn.MAX(PersistableDelay.age))
                          .where((PersistableDelay.signal == self) &
                                 self.proxy_delay_filter)
                          .scalar())

        if max_delay_time is None:
            return 0
        else:
            return max_delay_time

    @property
    def _min_delay_time(self):
        """
        Computes the min delay time of a signal.

        :return: Min delay time, in seconds.
        :rtype: int
        """
        min_delay_time = (PersistableDelay
                          .select(fn.MIN(PersistableDelay.age))
                          .where((PersistableDelay.signal == self) &
                                 self.proxy_delay_filter)
                          .scalar())

        if min_delay_time is None:
            return 0
        else:
            return min_delay_time

    @property
    def _coverage(self):
        """
        Computes the "coverage" of a signal, which is the percentage of time the signal is not in gap.

        :return: Percentage of the time the signal is not in a gap.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        logger.info("PersistableSignal._coverage():: called with terms:\nself._total_gap_time: {gt}\nself.time_span.total_seconds: {tt}\nCoverage: {cov}".format(
            gt = self._total_gap_time, tt = self.time_span.total_seconds(), cov = 1.0 - self._total_gap_time / self.time_span.total_seconds()))
        return 1.0 - self._total_gap_time / self.time_span.total_seconds()

    @property
    def _online(self):
        """
        Computes the ratio of the time that data is online
        
        :return: Ratio of the time the signal is not offline.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - self._total_offline_period_time / self.time_span.total_seconds()

    @property
    def _on_time_of_delivered(self):
        """
        Computes the percentage of time the signal's reporting is "on-time" GIVEN that it was delivered,
        i.e. ignoring any coverage gaps

        :return: The on-time ratio.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - float(self._num_delays) / self.count

    @property
    def _on_time(self):
        """
        Computes the percentage of the time the signal's reporting is "on-time" (doesn't have a delay).

        :return: The on-time percentage.
        :rtype: float | None
        """
        if self.count == 0:
            return 0.0

        return self._coverage * self._on_time_of_delivered


class PersistableSignalSegment(Persistable):
    """
    This class represents the persistence signal segment object, stripped of the series signals itself.

    Instead the record holds metadata about the signal segment, such as: count, mean, min_time, num_gaps, coverage, etc.
    """
    segment_type = peewee.TextField(null=True)
    start_time = peewee.DateTimeField(null=True)
    end_time = peewee.DateTimeField(null=True)
    final_segment = peewee.BooleanField()
    signal = peewee.ForeignKeyField(DeferredSignal, index=True)
    count = peewee.IntegerField()
    mean = peewee.FloatField(null=True)
    std = peewee.FloatField(null=True)
    var = peewee.FloatField(null=True)
    min = peewee.FloatField(null=True)
    max = peewee.FloatField(null=True)
    mid = peewee.FloatField(null=True)
    min_time = peewee.DateTimeField(null=True)
    max_time = peewee.DateTimeField(null=True)
    min_reading_time = peewee.DateTimeField(null=True)
    max_reading_time = peewee.DateTimeField(null=True)
    min_processed_time = peewee.DateTimeField(null=True)
    max_processed_time = peewee.DateTimeField(null=True)
    mode_minute_interval = peewee.IntegerField(null=True)
    reporting_interval_override = peewee.IntegerField(null=True)

    num_gaps = peewee.IntegerField(null=True)
    total_gap_time = peewee.IntegerField(null=True)
    max_gap_time = peewee.IntegerField(null=True)
    min_gap_time = peewee.IntegerField(null=True)

    num_offline_periods = peewee.IntegerField(null=True)
    total_offline_period_time = peewee.IntegerField(null=True)
    max_offline_period_time = peewee.IntegerField(null=True)
    min_offline_period_time = peewee.IntegerField(null=True)

    num_delays = peewee.IntegerField(null=True)
    total_delay_time = peewee.IntegerField(null=True)
    max_delay_time = peewee.IntegerField(null=True)
    min_delay_time = peewee.IntegerField(null=True)

    coverage = peewee.FloatField(null=True)
    on_time = peewee.FloatField(null=True)
    on_time_of_delivered = peewee.FloatField(null=True)
    online = peewee.FloatField(null=True)

    class Meta:
        db_table = 'signal_segments'
        database = database

    def __init__(self, signal_segment=None, *args, **kwargs):
        """
        Creates a PersistableSignalSegment. If you pass a signal segment, it will automatically create
        a persistence record from that SignalSegment object.
`
        :param signal: Optionally a signal to create the record.
        :type signal: Signal
        """
        super(PersistableSignalSegment, self).__init__(*args, **kwargs)

        self._gap_times = None
        self._offline_period_times = None

        if signal_segment is not None:
            self.signal_segment = signal_segment

            self.segment_type = signal_segment.segment_type
            self.start_time = signal_segment.start_time
            self.end_time = signal_segment.end_time
            self.final_segment = signal_segment.final_segment

            signal_filter = True
            if signal_segment.device_id is not None:
                signal_filter &= PersistableSignal.device_id == signal_segment.device_id
            else:
                signal_filter &= PersistableSignal.device_id.is_null()

            if signal_segment.port is not None:
                signal_filter &= PersistableSignal.port == signal_segment.port
            else:
                signal_filter &= PersistableSignal.port.is_null()

            if signal_segment.data_type is not None:
                signal_filter &= PersistableSignal.data_type == signal_segment.data_type
            else:
                signal_filter &= PersistableSignal.data_type.is_null()

            try:
                self.signal = (PersistableSignal.select().where(signal_filter).get())
            except PersistableSignal.DoesNotExist:
                raise PersistenceException("You must save the source signal before saving signal segments!")

            self.count = signal_segment.count
            self.mean = signal_segment.mean
            self.std = signal_segment.std
            self.var = signal_segment.var
            self.min = signal_segment.min
            self.max = signal_segment.max
            self.mid = signal_segment.mid
            self.min_time = signal_segment.min_time
            self.max_time = signal_segment.max_time
            self.min_reading_time = signal_segment.min_reading_time
            self.max_reading_time = signal_segment.max_reading_time
            self.min_processed_time = signal_segment.min_processed_time
            self.max_processed_time = signal_segment.max_processed_time
            self.mode_minute_interval = signal_segment.mode_minute_interval
            self.reporting_interval_override = signal_segment.reporting_interval_override

            self.recompute_metrics()

    def recompute_metrics(self):
        self.num_gaps = self._num_gaps
        self.total_gap_time = self._total_gap_time
        self.max_gap_time = self._max_gap_time
        self.min_gap_time = self._min_gap_time

        self.num_offline_periods = self._num_offline_periods
        self.total_offline_period_time = self._total_offline_period_time
        self.max_offline_period_time = self._max_offline_period_time
        self.min_offline_period_time = self._min_offline_period_time

        self.num_delays = self._num_delays
        self.total_delay_time = self._total_delay_time
        self.max_delay_time = self._max_delay_time
        self.min_delay_time = self._min_delay_time

        self.coverage = self._coverage
        self.on_time = self._on_time
        self.on_time_of_delivered = self._on_time_of_delivered
        self.online = self._online

    @property
    def proxy_start_time(self):
        """
        A proxy for the start time in case there is none.

        :return: Start time (or the minimum reading time)
        :rtype: datetime.datetime
        """
        if self.start_time is None:
            return self.min_time

        return self.start_time

    @property
    def proxy_end_time(self):
        """
        A proxy for the start time in case there is none.

        :return: Start time (or the minimum reading time)
        :rtype: datetime.datetime
        """
        if self.end_time is None:
            return self.max_time

        return self.end_time

    @property
    def time_span(self):
        """
        The difference between minimum and maximum time in this signal.

        If this is 0, return 1 second in order to not break metrics using this in the denominator.

        :return: The time span of the signal
        :rtype: pd.Timedelta
        """
        if self.proxy_end_time is None or self.proxy_start_time is None:
            return pd.Timedelta(seconds=1)

        time_span = pd.Timedelta(self.proxy_end_time - self.proxy_start_time)
        return max(time_span, pd.Timedelta(seconds=1))

    @property
    def proxy_gap_filter(self):
        """
        Returns the proxy gap filter used to select only gaps within the signal segment.

        :return: Proxy Gap Filter
        """
        return (((PersistableGap.start >= self.proxy_start_time) &
                 (PersistableGap.start <= self.proxy_end_time)) |
                ((PersistableGap.end > self.proxy_start_time) &
                 (PersistableGap.end <= self.proxy_end_time)) |
                ((PersistableGap.start < self.proxy_start_time) &
                 (PersistableGap.end > self.proxy_end_time)))

    @property
    def proxy_delay_filter(self):
        """
        Returns the proxy delay filter used to select only delays within the signal segment.

        :return: Proxy Delay Filter
        """
        return ((PersistableDelay.reading_date >= self.proxy_start_time) &
                (PersistableDelay.reading_date < self.proxy_end_time))

    @property
    def proxy_offline_period_filter(self):
        """
        Returns the proxy delay filter used to select only delays within the signal segment.

        :return: Proxy Delay Filter
        """
        return (((PersistableOfflinePeriod.start >= self.proxy_start_time) &
                (PersistableOfflinePeriod.start < self.proxy_end_time)) |
                ((PersistableOfflinePeriod.end >= self.proxy_start_time) &
                 (PersistableOfflinePeriod.end < self.proxy_end_time)) |
                ((PersistableOfflinePeriod.start < self.proxy_start_time) &
                 (PersistableOfflinePeriod.end > self.proxy_end_time)))

    def _get_gap_times(self):
        """
        Get a list of gap times in this Signal.

        :return: A list of gap times in seconds.
        :rtype: list[int]
        """
        if self.count == 0:
            return [self.time_span.total_seconds()]

        if self._gap_times is None:
            gaps = (PersistableGap
                    .select()
                    .where((PersistableGap.signal == self.signal) &
                           self.proxy_gap_filter))

            self._gap_times = []
            for gap in gaps:
                start_time = max(self.proxy_start_time, gap.start)
                end_time = min(self.proxy_end_time, gap.end)
                self._gap_times.append((end_time - start_time).total_seconds())

        return self._gap_times

    @property
    def _num_gaps(self):
        """
        Get the total number of gaps in this Signal.

        :return: Number of gaps in this signal.
        :rtype: int
        """
        if self.count == 0:
            return 1

        return (PersistableGap
                .select()
                .where((PersistableGap.signal == self.signal) &
                       self.proxy_gap_filter)
                .count())

    @property
    def _total_gap_time(self):
        """
        Computes the total gap time of a signal.

        :return: Total gap time, in seconds.
        :rtype: int
        """
        return int(sum(self._get_gap_times() or [0]))

    @property
    def _max_gap_time(self):
        """
        Computes the max gap time of a signal.

        :return: Max gap time, in seconds.
        :rtype: int
        """
        '''
        max_gap_time = (PersistableGap
                        .select(fn.MAX(PersistableGap.length))
                        .where((PersistableGap.signal == self.signal) &
                               self.proxy_gap_filter)
                        .scalar())
        '''
        return int(max(self._get_gap_times() or [0]))

    @property
    def _min_gap_time(self):
        """
        Computes the min gap time of a signal.

        :return: Min gap time, in seconds.
        :rtype: int
        """
        '''
        min_gap_time = (PersistableGap
                        .select(fn.MIN(PersistableGap.length))
                        .where((PersistableGap.signal == self.signal) &
                               self.proxy_gap_filter)
                        .scalar())
        '''
        return int(min(self._get_gap_times() or [0]))

    @property
    def _num_offline_periods(self):
        """
        Get the total number of offline_periods in this Signal.

        :return: Number of offline_periods in this signal.
        :rtype: int
        """
        if self.count == 0:
            return 1

        return (PersistableOfflinePeriod
                .select()
                .where((PersistableOfflinePeriod.signal == self.signal) &
                       self.proxy_offline_period_filter)
                .count())

    def _get_offline_period_times(self):
        """
        Get a list of offline period times in this Signal.

        :return: A list of offline period times in seconds.
        :rtype: list[int]
        """
        if self._offline_period_times is None:
            if self.count == 0:
                return [self.time_span.total_seconds()]

            offline_periods = (PersistableOfflinePeriod
                               .select()
                               .where((PersistableOfflinePeriod.signal == self.signal) &
                                      self.proxy_offline_period_filter))

            self._offline_period_times = []
            for offline_period in offline_periods:
                start_time = max(self.proxy_start_time, offline_period.start)
                end_time = min(self.proxy_end_time, offline_period.end)
                self._offline_period_times.append((end_time - start_time).total_seconds())

        return self._offline_period_times

    @property
    def _total_offline_period_time(self):
        """
        Computes the total offline_period time of a signal.

        :return: Total offline_period time, in seconds.
        :rtype: int
        """
        return int(sum(self._get_offline_period_times() or [0]))

    @property
    def _max_offline_period_time(self):
        """
        Computes the max offline_period time of a signal.

        :return: Max offline_period time, in seconds.
        :rtype: int
        """
        '''
        max_offline_period_time = (PersistableOfflinePeriod
                                   .select(fn.MAX(PersistableOfflinePeriod.length))
                                   .where((PersistableOfflinePeriod.signal == self.signal) &
                                          self.proxy_offline_period_filter)
                                   .scalar())
        '''
        return int(max(self._get_offline_period_times() or [0]))

    @property
    def _min_offline_period_time(self):
        """
        Computes the min offline_period time of a signal.

        :return: Min offline_period time, in seconds.
        :rtype: int
        """
        '''
        min_offline_period_time = (PersistableOfflinePeriod
                                   .select(fn.MIN(PersistableOfflinePeriod.length))
                                   .where((PersistableOfflinePeriod.signal == self.signal) &
                                          self.proxy_offline_period_filter)
                                   .scalar())
        '''
        return int(min(self._get_offline_period_times() or [0]))

    @property
    def _num_delays(self):
        """
        Get the total number of delays in this Signal.

        :return: Number of delays in this signal.
        :rtype: int
        """
        return (PersistableDelay
                .select()
                .where((PersistableDelay.signal == self.signal) &
                       self.proxy_delay_filter)
                .count())

    @property
    def _total_delay_time(self):
        """
        Computes the total delay time of a signal.

        :return: Total delay time, in seconds.
        :rtype: int
        """
        total_delay_time = (PersistableDelay
                            .select(fn.SUM(PersistableDelay.age))
                            .where((PersistableDelay.signal == self.signal) &
                                   self.proxy_delay_filter)
                            .scalar())

        if total_delay_time is None:
            return 0
        else:
            return total_delay_time

    @property
    def _max_delay_time(self):
        """
        Computes the max delay time of a signal.

        :return: Max delay time, in seconds.
        :rtype: int
        """
        max_delay_time = (PersistableDelay
                          .select(fn.MAX(PersistableDelay.age))
                          .where((PersistableDelay.signal == self.signal) &
                                 self.proxy_delay_filter)
                          .scalar())

        if max_delay_time is None:
            return 0
        else:
            return max_delay_time

    @property
    def _min_delay_time(self):
        """
        Computes the min delay time of a signal.

        :return: Min delay time, in seconds.
        :rtype: int
        """
        min_delay_time = (PersistableDelay
                          .select(fn.MIN(PersistableDelay.age))
                          .where((PersistableDelay.signal == self.signal) &
                                 self.proxy_delay_filter)
                          .scalar())

        if min_delay_time is None:
            return 0
        else:
            return min_delay_time

    @property
    def _coverage(self):
        """
        Computes the "coverage" of a signal, which is the percentage of time the signal is not in gap.

        :return: Percentage of the time the signal is not in a gap.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - self._total_gap_time / self.time_span.total_seconds()

    @property
    def _online(self):
        """
        Computes the ratio of the time that data is online

        :return: Ratio of the time the signal is not offline.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - self._total_offline_period_time / self.time_span.total_seconds()

    @property
    def _on_time_of_delivered(self):
        """
        Computes the percentage of time the signal's reporting is "on-time" GIVEN that it was delivered,
        i.e. ignoring any coverage gaps

        :return: The on-time ratio.
        :rtype: float
        """
        if self.count == 0:
            return 0.0

        return 1.0 - float(self._num_delays) / self.count

    @property
    def _on_time(self):
        """
        Computes the percentage of the time the signal's reporting is "on-time" (doesn't have a delay).

        :return: The on-time percentage.
        :rtype: float | None
        """
        if self.count == 0:
            return 0.0

        return self._coverage * self._on_time_of_delivered


DeferredSignal.set_model(PersistableSignal)
