.. gap:

Gap
===

.. autoclass:: ta_utils.Gap
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__

**************
PersistableGap
**************

.. autoclass:: ta_utils.PersistableGap
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__