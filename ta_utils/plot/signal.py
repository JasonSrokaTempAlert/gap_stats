"""
Plot Signal Module
"""
import math

import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FuncFormatter


MIN_WIDTH = 10
MAX_WIDTH = 20
HEIGHT = 5
FONT_SIZE = 16
TICKS_FONT_SIZE = 12
Y_BUFFER = 1


# Blatantly stolen from Stack Overflow
# Makes histogram labels into percents from decimals
def _to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = str(int(100 * y))

    # The percent symbol needs escaping in latex
    if rcParams['text.usetex'] is True:
        return s + r'$\%$'
    else:
        return s + '%'


def plot_signal(signal, degrees=None, filename=None, title=None,
                range_min=None, range_max=None):
    """
    Plots a signal using a default format, and optionally saves that signal as an image.

    :param signal: The Signal to plot
    :type signal: ta_utils.signals.signal.Signal
    :param degrees: The units for this signal, usually either 'C' or 'F' (Celsius or Fahrenheit)
    :type degrees: str
    :param filename: Optionally, a filename for where to save this plot as an image.
    :type filename: str|None
    :param title: Optionally, a name for this signal.
    :type title: str
    :param range_min: Optionally, a lower value for the range of this signal
    :type range_min: float|None
    :param range_max: Optionally, an upper value for the range of this signal
    :type range_max: float|None
    :return: None
    :rtype: None
    """
    if title is None and signal.name is not None:
        title = signal.name

    num_days = signal.days
    width = max(MIN_WIDTH, min(MAX_WIDTH, num_days + 5))
    fig = plt.figure(figsize=(width, HEIGHT))

    plt.suptitle(title, horizontalalignment='center', fontsize=FONT_SIZE)

    # Create the time series plot
    ax = plt.axes()
    ax.set_ylim(ymin=min(0, (signal.min - Y_BUFFER)), ymax=(signal.max + Y_BUFFER))

    # Plot the whole time series
    ax.plot(signal.times, signal.values, 'k')

    if range_max is not None or range_min is not None:
        if range_max is None:
            range_max = signal.max + 0.01
        if range_min is None:
            range_min = signal.min - 0.01

        excursions = signal.excursions(range_min, range_max)
        for excursion in excursions:
            if excursion.mean <= range_min:
                color = '#0033FF'
            elif excursion.mean >= range_max:
                color = 'r'
            else:
                continue

            ax.plot(excursion.times, excursion.values, color, lw=2)


    if degrees is not None:
        ylabel = 'Temperature ($^{\circ}$' + str(degrees) + ')'
    else:
        ylabel = 'Temperature'
    ax.set_ylabel(ylabel, fontsize=FONT_SIZE)

    ax.xaxis.set_major_locator(mdates.DayLocator(interval=2))
    date_format = mdates.DateFormatter('%d %b %y')
    ax.xaxis.set_major_formatter(date_format)
    fig.autofmt_xdate()
    plt.xticks(fontsize=TICKS_FONT_SIZE)
    plt.yticks(fontsize=TICKS_FONT_SIZE)

    if filename is not None:
        plt.savefig(filename)

    plt.close()


def plot_signal_histogram(signal, degrees=None, filename=None, title="Signal",
                          range_min=None, range_max=None):
    """
    Plots the histogram of a signal using a default format, and optionally saves that signal as an image.

    :param signal: The Signal to plot
    :type signal: ta_utils.signals.signal.Signal
    :param degrees: The units for this signal, usually either 'C' or 'F' (Celsius or Fahrenheit)
    :type degrees: str
    :param filename: Optionally, a filename for where to save this plot as an image.
    :type filename: str|None
    :param title: Optionally, a name for this signal.
    :type title: str
    :param range_min: Optionally, a lower value for the range of this signal
    :type range_min: float|None
    :param range_max: Optionally, an upper value for the range of this signal
    :type range_max: float|None
    :return: None
    :rtype: None
    """
    num_days = signal.days
    width = max(MIN_WIDTH, min(MAX_WIDTH, num_days + 5))
    fig = plt.figure(figsize=(width, HEIGHT))

    plt.suptitle(title, horizontalalignment='center', fontsize=FONT_SIZE)

    # Plot horizontal histogram
    ax = plt.axes()

    # Find the ranges of the signals and create histogram bins
    min_int = math.floor(signal.min)
    max_int = math.ceil(signal.max)
    n_bins = (max_int - min_int + 1) * 2
    hist_bins = np.linspace(min_int, max_int, n_bins)

    weights = np.ones(len(signal)) / len(signal)
    ax.hist(signal.values, weights=weights, bins=hist_bins, color='k', alpha=0.5)

    if range_max is not None:
        above_signal = signal.above(range_max)
        weights = np.ones(len(above_signal)) / len(signal)
        ax.hist(above_signal.values, weights=weights,
                bins=hist_bins, color='r', edgecolor='r', alpha=0.5)

    if range_min is not None:
        below_signal = signal.below(range_min)
        weights = np.ones(len(below_signal)) / len(signal)
        ax.hist(below_signal.values, weights=weights,
                 bins=hist_bins, color='#0033FF', edgecolor='#0033FF', alpha=0.5)

    """
    # Add horizontal lines at the ranges of interest
    add_lines(ax2, specs['Range'])
    """

    # Format and label axes
    formatter = FuncFormatter(_to_percent)
    plt.gca().yaxis.set_major_formatter(formatter)
    ax.set_ylabel('% Time', fontsize=FONT_SIZE)

    if degrees is not None:
        xlabel = 'Temperature ($^{\circ}$' + str(degrees) + ')'
    else:
        xlabel = 'Temperature'
    ax.set_xlabel(xlabel, fontsize=FONT_SIZE)

    plt.xticks(fontsize=TICKS_FONT_SIZE)
    plt.yticks(fontsize=TICKS_FONT_SIZE)

    if filename is not None:
        plt.savefig(filename)

    plt.close()
