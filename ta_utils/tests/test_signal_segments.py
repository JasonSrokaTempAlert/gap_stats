"""
Test Signal Segments Module
"""
import unittest
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal, SignalSegmentError

MINUTES_IN_DAY = 60 * 24

now = datetime.now()
BEFORE_TIME = now + timedelta(days=1)
AFTER_TIME = now + timedelta(days=3)

null_signal = Signal(None)
empty_signal = Signal(pd.Series())

dates = [now + timedelta(days=(i ** 2)) for i in range(0, 3)]
trivial_signal = Signal(pd.Series(data=[0., 1., 2.], index=dates))

daily_dates = [now + timedelta(days=i) for i in [0, 1, 3, 4]]
daily_signal = Signal(pd.Series(data=[0., 1., 2., 3.], index=daily_dates))

daily_signal_with_overrides = Signal(pd.Series(data=[0., 1., 2., 3.], index=daily_dates),
                                     min_time_override=daily_dates[0] - timedelta(days=2),
                                     max_time_override=daily_dates[-1] + timedelta(days=2))


class SignalSegmentTestCase(unittest.TestCase):
    def test_invalid_segment(self):
        with self.assertRaises(SignalSegmentError):
            print(trivial_signal.segment_at(segment_type='invalid'))

    def test_segment_daily_null(self):
        daily_segments = null_signal.segment_daily()
        self.assertEqual(0, len(daily_segments))

    def test_segment_daily_empty(self):
        daily_segments = empty_signal.segment_daily()
        self.assertEqual(0, len(daily_segments))

    def test_segment_daily(self):
        daily_segments = daily_signal.segment_daily()
        self.assertEqual(5, len(daily_segments))

        self.assertTrue(daily_signal[:1]._series.equals(daily_segments[0]._series))
        self.assertTrue(daily_signal[1:2]._series.equals(daily_segments[1]._series))
        self.assertTrue(empty_signal._series.equals(daily_segments[2]._series))
        self.assertTrue(daily_signal[2:3]._series.equals(daily_segments[3]._series))
        self.assertTrue(daily_signal[3:]._series.equals(daily_segments[4]._series))

    def test_segment_at(self):
        before_segment = daily_signal.segment_at(end_time=BEFORE_TIME)
        self.assertTrue(daily_signal[:2]._series.equals(before_segment._series))

        after_segment = daily_signal.segment_at(start_time=AFTER_TIME)
        self.assertTrue(daily_signal[2:]._series.equals(after_segment._series))

        between_segment = daily_signal.segment_at(start_time=BEFORE_TIME,
                                                  end_time=AFTER_TIME)
        self.assertTrue(daily_signal[1:3]._series.equals(between_segment._series))

    def test_segment_by(self):
        segment_td = timedelta(days=2)
        # times = 0, 1, 4
        # segments: [0, 2] [2, 4]
        segmented_signals = trivial_signal.segment_by(segment_td)
        self.assertEqual(3, len(segmented_signals))

        self.assertTrue(trivial_signal[:2]._series.equals(segmented_signals[0]._series))
        self.assertTrue(empty_signal._series.equals(segmented_signals[1]._series))
        self.assertTrue(trivial_signal[2:]._series.equals(segmented_signals[2]._series))

        self.assertFalse(segmented_signals[0].final_segment)
        self.assertFalse(segmented_signals[1].final_segment)
        self.assertTrue(segmented_signals[2].final_segment)

    def test_segment_daily_with_out_of_bound_overrides(self):
        daily_segments = daily_signal_with_overrides.segment_daily()
        self.assertEqual(9, len(daily_segments))

        self.assertTrue(empty_signal._series.equals(daily_segments[0]._series))
        self.assertTrue(empty_signal._series.equals(daily_segments[1]._series))
        self.assertTrue(daily_signal[:1]._series.equals(daily_segments[2]._series))
        self.assertTrue(daily_signal[1:2]._series.equals(daily_segments[3]._series))
        self.assertTrue(empty_signal._series.equals(daily_segments[4]._series))
        self.assertTrue(daily_signal[2:3]._series.equals(daily_segments[5]._series))
        self.assertTrue(daily_signal[3:]._series.equals(daily_segments[6]._series))
        self.assertTrue(empty_signal._series.equals(daily_segments[7]._series))
        self.assertTrue(empty_signal._series.equals(daily_segments[8]._series))
