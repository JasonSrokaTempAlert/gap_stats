"""
Test Persistable Signal Module
"""

import unittest
import logging
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal
from ta_utils import PersistableSignal
from ta_utils import database
from ta_utils import query_to_dataframe
from ta_utils import create_tables, drop_tables

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

now = datetime.now()

dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]
dates_no_gaps = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 7, 9, 11, 13, 15]]

empty_signal = Signal(pd.Series(),
                      device_id='empty_signal')

min_date = min(dates)
max_date = max(dates)

null_signal = Signal(None)
signal = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                device_id='test_signal',
                port=0)

signal_no_gaps = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates_no_gaps),
                        device_id='test_signal')

signal_overrided = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                          min_time_override=min_date,
                          max_time_override=max_date,
                          device_id='test_signal_override')

# Transmission dates are crafted so there's still on offline period from 14 - 20
# and there's an offline period from 1 - 5 instead of 5 - 10. Also, there's a delay between 3 and 5.
transmission_dates = [now + timedelta(minutes=i) for i in [0, 1, 7, 5, 10, 12, 14, 20, 22]]
signal_gaps = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                     processed_dates=pd.Series(data=transmission_dates, index=dates),
                     min_time_override=min_date,
                     max_time_override=max_date,
                     device_id='test_signal')

database.init('test_ta_utils')


class PersistableSignalTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_null_signal_save(self):
        persisted_signal = null_signal.save()

        self.assertEqual(null_signal.device_id, persisted_signal.device_id)
        self.assertEqual(null_signal.port, persisted_signal.port)
        self.assertEqual(null_signal.data_type, persisted_signal.data_type)
        self.assertEqual(null_signal.count, persisted_signal.count)
        self.assertEqual(null_signal.mean, persisted_signal.mean)
        self.assertEqual(null_signal.std, persisted_signal.std)
        self.assertEqual(null_signal.var, persisted_signal.var)
        self.assertEqual(null_signal.min, persisted_signal.min)
        self.assertEqual(null_signal.max, persisted_signal.max)
        self.assertEqual(null_signal.min_time, persisted_signal.min_time)
        self.assertEqual(null_signal.max_time, persisted_signal.max_time)
        self.assertEqual(null_signal.min_reading_time, persisted_signal.min_reading_time)
        self.assertEqual(null_signal.max_reading_time, persisted_signal.max_reading_time)
        self.assertEqual(null_signal.mode_minute_interval, persisted_signal.mode_minute_interval)

        self.assertEqual(null_signal.coverage, persisted_signal.coverage)
        self.assertEqual(null_signal.on_time, persisted_signal.on_time)
        self.assertEqual(null_signal.online, persisted_signal.online)
        self.assertEqual(null_signal.on_time_of_delivered, persisted_signal.on_time_of_delivered)

    def test_empty_signal_save(self):
        persisted_signal = empty_signal.save()

        self.assertEqual(empty_signal.device_id, persisted_signal.device_id)
        self.assertEqual(empty_signal.port, persisted_signal.port)
        self.assertEqual(empty_signal.data_type, persisted_signal.data_type)
        self.assertEqual(empty_signal.count, persisted_signal.count)
        self.assertEqual(empty_signal.mean, persisted_signal.mean)
        self.assertEqual(empty_signal.std, persisted_signal.std)
        self.assertEqual(empty_signal.var, persisted_signal.var)
        self.assertEqual(empty_signal.min, persisted_signal.min)
        self.assertEqual(empty_signal.max, persisted_signal.max)
        self.assertEqual(empty_signal.min_time, persisted_signal.min_time)
        self.assertEqual(empty_signal.max_time, persisted_signal.max_time)
        self.assertEqual(empty_signal.min_reading_time, persisted_signal.min_reading_time)
        self.assertEqual(empty_signal.max_reading_time, persisted_signal.max_reading_time)
        self.assertEqual(empty_signal.mode_minute_interval, persisted_signal.mode_minute_interval)

        self.assertEqual(empty_signal.coverage, persisted_signal.coverage)
        self.assertEqual(empty_signal.on_time, persisted_signal.on_time)
        self.assertEqual(empty_signal.online, persisted_signal.online)
        self.assertEqual(empty_signal.on_time_of_delivered, persisted_signal.on_time_of_delivered)

    def test_signal_save(self):
        persisted_signal = signal.save()

        self.assertEqual(signal.device_id, persisted_signal.device_id)
        self.assertEqual(signal.port, persisted_signal.port)
        self.assertEqual(signal.data_type, persisted_signal.data_type)
        self.assertEqual(signal.count, persisted_signal.count)
        self.assertEqual(signal.mean, persisted_signal.mean)
        self.assertEqual(signal.std, persisted_signal.std)
        self.assertEqual(signal.var, persisted_signal.var)
        self.assertEqual(signal.min, persisted_signal.min)
        self.assertEqual(signal.max, persisted_signal.max)
        self.assertEqual(signal.min_time, persisted_signal.min_time)
        self.assertEqual(signal.max_time, persisted_signal.max_time)
        self.assertEqual(signal.min_reading_time, persisted_signal.min_reading_time)
        self.assertEqual(signal.max_reading_time, persisted_signal.max_reading_time)
        self.assertEqual(signal.mode_minute_interval, persisted_signal.mode_minute_interval)

        self.assertEqual(signal.num_gaps, persisted_signal.num_gaps)
        self.assertEqual(signal.total_gap_time, persisted_signal.total_gap_time)
        self.assertEqual(signal.max_gap_time, persisted_signal.max_gap_time)
        self.assertEqual(signal.min_gap_time, persisted_signal.min_gap_time)

        self.assertEqual(signal.num_delays, persisted_signal.num_delays)
        self.assertEqual(signal.total_delay_time, persisted_signal.total_delay_time)
        self.assertEqual(signal.max_delay_time, persisted_signal.max_delay_time)
        self.assertEqual(signal.min_delay_time, persisted_signal.min_delay_time)

        self.assertEqual(signal.num_offline_periods, persisted_signal.num_offline_periods)
        self.assertEqual(signal.total_offline_period_time, persisted_signal.total_offline_period_time)
        self.assertEqual(signal.max_offline_period_time, persisted_signal.max_offline_period_time)
        self.assertEqual(signal.min_offline_period_time, persisted_signal.min_offline_period_time)

        self.assertEqual(signal.coverage, persisted_signal.coverage)
        self.assertEqual(signal.on_time, persisted_signal.on_time)
        self.assertEqual(signal.online, persisted_signal.online)
        self.assertEqual(signal.on_time_of_delivered, persisted_signal.on_time_of_delivered)

    def test_signal_save_overrided(self):
        persisted_signal = signal_overrided.save()

        self.assertEqual(signal_overrided.num_gaps, persisted_signal.num_gaps)
        self.assertEqual(signal_overrided.total_gap_time, persisted_signal.total_gap_time)
        self.assertEqual(signal_overrided.max_gap_time, persisted_signal.max_gap_time)
        self.assertEqual(signal_overrided.min_gap_time, persisted_signal.min_gap_time)

        self.assertEqual(signal_overrided.num_delays, persisted_signal.num_delays)
        self.assertEqual(signal_overrided.total_delay_time, persisted_signal.total_delay_time)
        self.assertEqual(signal_overrided.max_delay_time, persisted_signal.max_delay_time)
        self.assertEqual(signal_overrided.min_delay_time, persisted_signal.min_delay_time)

        self.assertEqual(signal_overrided.num_offline_periods, persisted_signal.num_offline_periods)
        self.assertEqual(signal_overrided.total_offline_period_time, persisted_signal.total_offline_period_time)
        self.assertEqual(signal_overrided.max_offline_period_time, persisted_signal.max_offline_period_time)
        self.assertEqual(signal_overrided.min_offline_period_time, persisted_signal.min_offline_period_time)

    def test_signal_gaps(self):
        persisted_signal = signal_gaps.save()

        self.assertEqual(signal_gaps.num_gaps, persisted_signal.num_gaps)
        self.assertEqual(signal_gaps.total_gap_time, persisted_signal.total_gap_time)
        self.assertEqual(signal_gaps.max_gap_time, persisted_signal.max_gap_time)
        self.assertEqual(signal_gaps.min_gap_time, persisted_signal.min_gap_time)

        self.assertEqual(signal_gaps.num_offline_periods, persisted_signal.num_offline_periods)
        self.assertEqual(signal_gaps.total_offline_period_time, persisted_signal.total_offline_period_time)
        self.assertEqual(signal_gaps.max_offline_period_time, persisted_signal.max_offline_period_time)
        self.assertEqual(signal_gaps.min_offline_period_time, persisted_signal.min_offline_period_time)

    def test_signal_no_gaps(self):
        persisted_signal = signal_no_gaps.save()

        self.assertEqual(signal_no_gaps.num_gaps, persisted_signal.num_gaps)
        self.assertEqual(signal_no_gaps.total_gap_time, persisted_signal.total_gap_time)
        self.assertEqual(signal_no_gaps.max_gap_time, persisted_signal.max_gap_time)
        self.assertEqual(signal_no_gaps.min_gap_time, persisted_signal.min_gap_time)

        self.assertEqual(signal_no_gaps.num_offline_periods, persisted_signal.num_offline_periods)
        self.assertEqual(signal_no_gaps.total_offline_period_time, persisted_signal.total_offline_period_time)
        self.assertEqual(signal_no_gaps.max_offline_period_time, persisted_signal.max_offline_period_time)
        self.assertEqual(signal_no_gaps.min_offline_period_time, persisted_signal.min_offline_period_time)

    def test_query_to_dataframe(self):
        persisted_signal = signal.save()
        persisted_signal_query = (PersistableSignal
                                  .select()
                                  .where(PersistableSignal.id == persisted_signal.id))

        signal_df = query_to_dataframe(persisted_signal_query)
        self.assertEqual(1, len(signal_df))

        signal_df_records = signal_df.to_records()
        signal_df_record = signal_df_records[0]

        self.assertEqual(signal_df_record['id'], persisted_signal.id)
        self.assertEqual(signal_df_record['device_id'], persisted_signal.device_id)
        self.assertEqual(signal_df_record['port'], persisted_signal.port)
        self.assertEqual(signal_df_record['data_type'], persisted_signal.data_type)
