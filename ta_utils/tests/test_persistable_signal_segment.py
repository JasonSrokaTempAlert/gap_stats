"""
Test Persistable Signal Segment Module
"""

import unittest
import logging
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal
from ta_utils import database
from ta_utils import create_tables, drop_tables
from ta_utils import PersistenceException

logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

now = datetime.now()

null_signal = Signal(None)
null_signal_segment = null_signal.segment_at(start_time=now)

empty_signal = Signal(pd.Series(), device_id='empty_signal')
empty_signal_segment = empty_signal.segment_at(start_time=now)

dates = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]
dates_no_gaps = [now + timedelta(minutes=i) for i in [0, 1, 3, 5, 7, 9, 11, 13, 15]]
dates_plus_two_days = dates + [now + timedelta(days=2, minutes=i) for i in [0, 1, 3, 5, 10, 12, 14, 20, 22]]

min_date = min(dates)
max_date = max(dates)
signal = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                device_id='test_signal')
signal_segments = signal.segment_daily()

signal_no_gaps = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates_no_gaps),
                        device_id='test_signal')
signal_no_gaps_segments = signal_no_gaps.segment_daily()

# Transmission dates are crafted so there's still on offline period from 14 - 20
# and there's an offline period from 1 - 5 instead of 5 - 10. Also, there's a delay between 3 and 5.
transmission_dates = [now + timedelta(minutes=i) for i in [0, 1, 7, 5, 10, 12, 14, 20, 22]]
signal_gaps = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.], index=dates),
                     processed_dates=pd.Series(data=transmission_dates, index=dates),
                     min_time_override=min_date,
                     max_time_override=max_date,
                     device_id='test_signal')
signal_gaps_segments = signal_gaps.segment_daily()

signal_gaps_plus_two_days = Signal(pd.Series(data=[0., 1., 2., 3., 4., 3., 2., 1., 0.,
                                                   0., 1., 2., 3., 4., 3., 2., 1., 0.],
                                             index=dates_plus_two_days),
                                   device_id='test_signal')
signal_gaps_plus_two_days_segments = signal_gaps_plus_two_days.segment_daily()

database.init('test_ta_utils')


class PersistableSignalSegmentTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def test_signal_not_saved(self):
        # Commented out on purpose:
        # >>> signal.save()
        for signal_segment in signal_segments:
            with self.assertRaises(PersistenceException):
                signal_segment.save()

    def test_null_signal_save(self):
        null_signal.save()

        persisted_signal_segment = null_signal_segment.save()

        self.assertEqual(null_signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
        self.assertEqual(null_signal_segment.signal_source.port, persisted_signal_segment.signal.port)
        self.assertEqual(null_signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
        self.assertEqual(null_signal_segment.count, persisted_signal_segment.count)
        self.assertEqual(null_signal_segment.mean, persisted_signal_segment.mean)
        self.assertEqual(null_signal_segment.std, persisted_signal_segment.std)
        self.assertEqual(null_signal_segment.var, persisted_signal_segment.var)
        self.assertEqual(null_signal_segment.min, persisted_signal_segment.min)
        self.assertEqual(null_signal_segment.max, persisted_signal_segment.max)
        self.assertEqual(null_signal_segment.min_time, persisted_signal_segment.min_time)
        self.assertEqual(null_signal_segment.max_time, persisted_signal_segment.max_time)
        self.assertEqual(null_signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
        self.assertEqual(null_signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
        self.assertEqual(null_signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

        self.assertEqual(null_signal_segment.num_gaps, persisted_signal_segment.num_gaps)
        self.assertEqual(null_signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
        self.assertEqual(null_signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
        self.assertEqual(null_signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

        self.assertEqual(null_signal_segment.num_delays, persisted_signal_segment.num_delays)
        self.assertEqual(null_signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
        self.assertEqual(null_signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
        self.assertEqual(null_signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

        self.assertEqual(null_signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
        self.assertEqual(null_signal_segment.total_offline_period_time,
                         persisted_signal_segment.total_offline_period_time)
        self.assertEqual(null_signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
        self.assertEqual(null_signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

        self.assertEqual(null_signal_segment.coverage, persisted_signal_segment.coverage)
        self.assertEqual(null_signal_segment.on_time, persisted_signal_segment.on_time)
        self.assertEqual(null_signal_segment.online, persisted_signal_segment.online)
        self.assertEqual(null_signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)

    def test_empty_signal_save(self):
        empty_signal.save()

        persisted_signal_segment = empty_signal_segment.save()

        self.assertEqual(empty_signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
        self.assertEqual(empty_signal_segment.signal_source.port, persisted_signal_segment.signal.port)
        self.assertEqual(empty_signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
        self.assertEqual(empty_signal_segment.count, persisted_signal_segment.count)
        self.assertEqual(empty_signal_segment.mean, persisted_signal_segment.mean)
        self.assertEqual(empty_signal_segment.std, persisted_signal_segment.std)
        self.assertEqual(empty_signal_segment.var, persisted_signal_segment.var)
        self.assertEqual(empty_signal_segment.min, persisted_signal_segment.min)
        self.assertEqual(empty_signal_segment.max, persisted_signal_segment.max)
        self.assertEqual(empty_signal_segment.min_time, persisted_signal_segment.min_time)
        self.assertEqual(empty_signal_segment.max_time, persisted_signal_segment.max_time)
        self.assertEqual(empty_signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
        self.assertEqual(empty_signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
        self.assertEqual(empty_signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

        self.assertEqual(empty_signal_segment.num_gaps, persisted_signal_segment.num_gaps)
        self.assertEqual(empty_signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
        self.assertEqual(empty_signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
        self.assertEqual(empty_signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

        self.assertEqual(empty_signal_segment.num_delays, persisted_signal_segment.num_delays)
        self.assertEqual(empty_signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
        self.assertEqual(empty_signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
        self.assertEqual(empty_signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

        self.assertEqual(empty_signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
        self.assertEqual(empty_signal_segment.total_offline_period_time,
                         persisted_signal_segment.total_offline_period_time)
        self.assertEqual(empty_signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
        self.assertEqual(empty_signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

        self.assertEqual(empty_signal_segment.coverage, persisted_signal_segment.coverage)
        self.assertEqual(empty_signal_segment.on_time, persisted_signal_segment.on_time)
        self.assertEqual(empty_signal_segment.online, persisted_signal_segment.online)
        self.assertEqual(empty_signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)

    def test_persistable_signal_segments_save(self):
        signal.save()

        for signal_segment in signal_segments:
            persisted_signal_segment = signal_segment.save()

            self.assertEqual(signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
            self.assertEqual(signal_segment.signal_source.port, persisted_signal_segment.signal.port)
            self.assertEqual(signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
            self.assertEqual(signal_segment.count, persisted_signal_segment.count)
            self.assertEqual(signal_segment.mean, persisted_signal_segment.mean)
            self.assertEqual(signal_segment.std, persisted_signal_segment.std)
            self.assertEqual(signal_segment.var, persisted_signal_segment.var)
            self.assertEqual(signal_segment.min, persisted_signal_segment.min)
            self.assertEqual(signal_segment.max, persisted_signal_segment.max)
            self.assertEqual(signal_segment.min_time, persisted_signal_segment.min_time)
            self.assertEqual(signal_segment.max_time, persisted_signal_segment.max_time)
            self.assertEqual(signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
            self.assertEqual(signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
            self.assertEqual(signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

            self.assertEqual(signal_segment.num_gaps, persisted_signal_segment.num_gaps)
            self.assertEqual(signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
            self.assertEqual(signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
            self.assertEqual(signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

            self.assertEqual(signal_segment.num_delays, persisted_signal_segment.num_delays)
            self.assertEqual(signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
            self.assertEqual(signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
            self.assertEqual(signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

            self.assertEqual(signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
            self.assertEqual(signal_segment.total_offline_period_time,
                             persisted_signal_segment.total_offline_period_time)
            self.assertEqual(signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
            self.assertEqual(signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

            self.assertEqual(signal_segment.coverage, persisted_signal_segment.coverage)
            self.assertEqual(signal_segment.on_time, persisted_signal_segment.on_time)
            self.assertEqual(signal_segment.online, persisted_signal_segment.online)
            self.assertEqual(signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)

    def test_signal_no_gaps_save(self):
        signal_no_gaps.save()

        for signal_segment in signal_no_gaps_segments:
            persisted_signal_segment = signal_segment.save()

            self.assertEqual(signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
            self.assertEqual(signal_segment.signal_source.port, persisted_signal_segment.signal.port)
            self.assertEqual(signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
            self.assertEqual(signal_segment.count, persisted_signal_segment.count)
            self.assertEqual(signal_segment.mean, persisted_signal_segment.mean)
            self.assertEqual(signal_segment.std, persisted_signal_segment.std)
            self.assertEqual(signal_segment.var, persisted_signal_segment.var)
            self.assertEqual(signal_segment.min, persisted_signal_segment.min)
            self.assertEqual(signal_segment.max, persisted_signal_segment.max)
            self.assertEqual(signal_segment.min_time, persisted_signal_segment.min_time)
            self.assertEqual(signal_segment.max_time, persisted_signal_segment.max_time)
            self.assertEqual(signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
            self.assertEqual(signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
            self.assertEqual(signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

            self.assertEqual(signal_segment.num_gaps, persisted_signal_segment.num_gaps)
            self.assertEqual(signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
            self.assertEqual(signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
            self.assertEqual(signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

            self.assertEqual(signal_segment.num_delays, persisted_signal_segment.num_delays)
            self.assertEqual(signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
            self.assertEqual(signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
            self.assertEqual(signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

            self.assertEqual(signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
            self.assertEqual(signal_segment.total_offline_period_time,
                             persisted_signal_segment.total_offline_period_time)
            self.assertEqual(signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
            self.assertEqual(signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

            self.assertEqual(signal_segment.coverage, persisted_signal_segment.coverage)
            self.assertEqual(signal_segment.on_time, persisted_signal_segment.on_time)
            self.assertEqual(signal_segment.online, persisted_signal_segment.online)
            self.assertEqual(signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)

    def test_signal_gaps_save(self):
        signal_gaps.save()

        for signal_segment in signal_gaps_segments:
            persisted_signal_segment = signal_segment.save()

            self.assertEqual(signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
            self.assertEqual(signal_segment.signal_source.port, persisted_signal_segment.signal.port)
            self.assertEqual(signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
            self.assertEqual(signal_segment.count, persisted_signal_segment.count)
            self.assertEqual(signal_segment.mean, persisted_signal_segment.mean)
            self.assertEqual(signal_segment.std, persisted_signal_segment.std)
            self.assertEqual(signal_segment.var, persisted_signal_segment.var)
            self.assertEqual(signal_segment.min, persisted_signal_segment.min)
            self.assertEqual(signal_segment.max, persisted_signal_segment.max)
            self.assertEqual(signal_segment.min_time, persisted_signal_segment.min_time)
            self.assertEqual(signal_segment.max_time, persisted_signal_segment.max_time)
            self.assertEqual(signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
            self.assertEqual(signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
            self.assertEqual(signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

            self.assertEqual(signal_segment.num_gaps, persisted_signal_segment.num_gaps)
            self.assertEqual(signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
            self.assertEqual(signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
            self.assertEqual(signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

            self.assertEqual(signal_segment.num_delays, persisted_signal_segment.num_delays)
            self.assertEqual(signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
            self.assertEqual(signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
            self.assertEqual(signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

            self.assertEqual(signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
            self.assertEqual(signal_segment.total_offline_period_time,
                             persisted_signal_segment.total_offline_period_time)
            self.assertEqual(signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
            self.assertEqual(signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

            self.assertEqual(signal_segment.coverage, persisted_signal_segment.coverage)
            self.assertEqual(signal_segment.on_time, persisted_signal_segment.on_time)
            self.assertEqual(signal_segment.online, persisted_signal_segment.online)
            self.assertEqual(signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)

    def test_signal_gaps_with_long_gap(self):
        signal_gaps_plus_two_days.save()

        for signal_segment in signal_gaps_plus_two_days_segments:
            persisted_signal_segment = signal_segment.save()

            self.assertEqual(signal_segment.signal_source.device_id, persisted_signal_segment.signal.device_id)
            self.assertEqual(signal_segment.signal_source.port, persisted_signal_segment.signal.port)
            self.assertEqual(signal_segment.signal_source.data_type, persisted_signal_segment.signal.data_type)
            self.assertEqual(signal_segment.count, persisted_signal_segment.count)
            self.assertEqual(signal_segment.mean, persisted_signal_segment.mean)
            self.assertEqual(signal_segment.std, persisted_signal_segment.std)
            self.assertEqual(signal_segment.var, persisted_signal_segment.var)
            self.assertEqual(signal_segment.min, persisted_signal_segment.min)
            self.assertEqual(signal_segment.max, persisted_signal_segment.max)
            self.assertEqual(signal_segment.min_time, persisted_signal_segment.min_time)
            self.assertEqual(signal_segment.max_time, persisted_signal_segment.max_time)
            self.assertEqual(signal_segment.start_time, persisted_signal_segment.start_time)
            self.assertEqual(signal_segment.end_time, persisted_signal_segment.end_time)
            self.assertEqual(signal_segment.min_reading_time, persisted_signal_segment.min_reading_time)
            self.assertEqual(signal_segment.max_reading_time, persisted_signal_segment.max_reading_time)
            self.assertEqual(signal_segment.mode_minute_interval, persisted_signal_segment.mode_minute_interval)

            self.assertEqual(signal_segment.num_gaps, persisted_signal_segment.num_gaps)
            self.assertEqual(signal_segment.total_gap_time, persisted_signal_segment.total_gap_time)
            self.assertEqual(signal_segment.max_gap_time, persisted_signal_segment.max_gap_time)
            self.assertEqual(signal_segment.min_gap_time, persisted_signal_segment.min_gap_time)

            self.assertEqual(signal_segment.num_delays, persisted_signal_segment.num_delays)
            self.assertEqual(signal_segment.total_delay_time, persisted_signal_segment.total_delay_time)
            self.assertEqual(signal_segment.max_delay_time, persisted_signal_segment.max_delay_time)
            self.assertEqual(signal_segment.min_delay_time, persisted_signal_segment.min_delay_time)

            self.assertEqual(signal_segment.num_offline_periods, persisted_signal_segment.num_offline_periods)
            self.assertEqual(signal_segment.total_offline_period_time,
                             persisted_signal_segment.total_offline_period_time)
            self.assertEqual(signal_segment.max_offline_period_time, persisted_signal_segment.max_offline_period_time)
            self.assertEqual(signal_segment.min_offline_period_time, persisted_signal_segment.min_offline_period_time)

            self.assertEqual(signal_segment.coverage, persisted_signal_segment.coverage)
            self.assertEqual(signal_segment.on_time, persisted_signal_segment.on_time)
            self.assertEqual(signal_segment.online, persisted_signal_segment.online)
            self.assertEqual(signal_segment.on_time_of_delivered, persisted_signal_segment.on_time_of_delivered)
