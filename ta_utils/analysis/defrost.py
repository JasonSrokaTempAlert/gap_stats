"""
Defrost Analysis Module
"""


def has_defrost_cycle_naive(signal):
    """
    Naively returns whether or not there is a spike that might be a defrost cycle.

    :param signal: The signal we are analyzing.
    :return: Whether or not there is a defrost cycle.
    :rtype: bool
    """
    signal_above_mid = signal.above(signal.mid)
    fraction_above_mid = 1. * len(signal_above_mid) / len(signal)
    return fraction_above_mid > 0.2
