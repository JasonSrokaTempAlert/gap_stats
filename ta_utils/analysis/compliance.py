"""
Compliance Module
"""
from ta_utils import has_defrost_cycle_naive

YES_COMPLIANCE = 'Yes'
NO_COMPLIANCE = 'No'
CLOSE_COMPLIANCE = 'Close'


def find_compliance(signal, range_min, range_max, max_excursion_length, max_excursion_temp):
    """
    Find if temperature is:
     1. Always within range, then 'Yes'
     2. In range except for an excursion less often than every other (or every)
        day and has a max excursion shorter than a max excursion length, then 'Close'
     3. Or neither, 'No'

    :param signal: The signal to analyze for compliance.
    :param range_min: The minimum value that would not qualify as an excursion.
    :type range_min: float|int
    :param range_max: The maximum value that would not qualify as an excursion.
    :type range_max: float|int
    :param max_excursion_length: The maximum length of excursions (in terms of number of signals points)
    :type max_excursion_length: int
    :param max_excursion_temp: The maximum difference in allowed temperature range.
    :type max_excursion_temp: float|int
    :return: Yes if always in range, Close if it meets the requirement specified above, or No if neither.
    :rtype: str
    """
    min_temp = signal.min
    max_temp = signal.max

    temp_range = max_temp - min_temp
    range_range = range_max - range_min

    if has_defrost_cycle_naive(signal):
        max_excursions_allowed = signal.days
    else:
        max_excursions_allowed = signal.days / 2

    excursion_signals = signal.excursions(range_min, range_max)
    excursion_lengths = [len(excursion) for excursion in excursion_signals]

    if min_temp >= range_min and max_temp <= range_max:
        return YES_COMPLIANCE

    if (temp_range < range_range + max_excursion_temp and
        max(excursion_lengths) <= max_excursion_length and
        len(excursion_lengths) <= max_excursions_allowed):
        return CLOSE_COMPLIANCE

    return NO_COMPLIANCE
