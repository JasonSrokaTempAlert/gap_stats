"""
Test Excursion Analysis
"""
import unittest
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import Signal

now = datetime.now()
dates = [now + timedelta(days=i) for i in range(0, 5)]
processed_dates = pd.Series(dates, index=dates)

above_value = 3.
max_value = 2.
min_value = 1.
below_value = 0.

excursions_over_signal = Signal(pd.Series(data=[above_value, max_value, above_value, above_value, min_value],
                                          index=dates),
                                processed_dates=processed_dates)

excursions_over = [excursions_over_signal[0:1], excursions_over_signal[2:4]]

excursions_below_signal = Signal(pd.Series(data=[below_value, max_value, below_value, below_value, min_value],
                                           index=dates),
                                 processed_dates=processed_dates)

excursions_below = [excursions_below_signal[0:1], excursions_below_signal[2:4]]

no_excursions_signal = Signal(pd.Series(data=[max_value, min_value, max_value, max_value, min_value],
                                        index=dates),
                              processed_dates=processed_dates)


class ExcursionsTestCase(unittest.TestCase):
    def test_excursions_over(self):
        actual_excursions_over =  excursions_over_signal.excursions(min_value, max_value)
        self.assertListEqual(excursions_over, actual_excursions_over)

    def test_excursions_below(self):
        actuals_excursions_below = excursions_below_signal.excursions(min_value, max_value)
        self.assertListEqual(excursions_below, actuals_excursions_below)

    def test_no_excursions(self):
        self.assertListEqual([], no_excursions_signal.excursions(min_value, max_value))
