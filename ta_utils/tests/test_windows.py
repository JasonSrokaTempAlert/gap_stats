"""
Test Windows Module
"""
import unittest
import math
from datetime import datetime, timedelta

from ta_utils import Signal
from ta_utils import Windows
import pandas as pd

FOUR_DAYS = timedelta(days=4)
SIX_DAYS = timedelta(days=6)

now = datetime.now()
dates = [now + timedelta(days=(i ** 2)) for i in range(0, 5)]  # days: 0, 1, 4, 9, 16
min_date = min(dates)
max_date = max(dates)

trivial_signal = Signal(pd.Series(data=[0., 1., 2., 3., 4.], index=dates))
trivial_windows = Windows.sliding_windows_by_index(trivial_signal[:-1], 2, 1)  # [0,1] [1,2] [2,3]


class WindowsTestCase(unittest.TestCase):
    def test_sliding_windows_by_index(self):
        windows = Windows.sliding_windows_by_index(trivial_signal, 2, 1)
        self.assertEqual(len(windows), 4)

        windows = Windows.sliding_windows_by_index(trivial_signal, 2, 2)
        self.assertEqual(len(windows), 2)

        windows = Windows.sliding_windows_by_index(trivial_signal, 3, 1)
        self.assertEqual(len(windows), 3)

    def test_sliding_windows_by_time(self):
        windows = Windows.sliding_windows_by_time(trivial_signal, FOUR_DAYS, FOUR_DAYS)
        self.assertEqual(len(windows), 4)

        windows = Windows.sliding_windows_by_time(trivial_signal, FOUR_DAYS, SIX_DAYS)
        self.assertEqual(len(windows), 3)

        windows = Windows.sliding_windows_by_time(trivial_signal, SIX_DAYS, SIX_DAYS)
        self.assertEqual(len(windows), 2)

    def test_mean_property(self):
        self.assertEqual(trivial_windows.mean(lambda signal: signal.min), 1.)

    def test_std_property(self):
        self.assertEqual(trivial_windows.std(lambda signal: signal.min), math.sqrt(2. / 3.))

    def test_var_property(self):
        self.assertEqual(trivial_windows.var(lambda signal: signal.min), 2. / 3.)

    def test_min_property(self):
        self.assertEqual(trivial_windows.min(lambda signal: signal.min), 0.)

    def test_max_property(self):
        self.assertEqual(trivial_windows.max(lambda signal: signal.min), 2.)

    def test_mid_property(self):
        self.assertEqual(trivial_windows.mid(lambda signal: signal.min), 1.)
