.. pipelines:

Pipelines
=========

Pipelines are helpers that automate common pipeline tasks such as signal updates.
Simply initialize the pipeline with required settings and using ``run()`` to run the pipeline.

.. toctree::
    :maxdepth: 2

    update_pipeline
