"""
Test Many Device Signals Module
"""

import unittest
import logging
from datetime import datetime, timedelta

import pandas as pd

from ta_utils import PersistableSignalSegment, PersistableSignal
from ta_utils import ManyDeviceSignals, ManyDeviceSignalsException

from ta_utils import database
from ta_utils import create_tables, drop_tables


logger = logging.getLogger('peewee')
logger.setLevel(logging.ERROR)

DEVICE_ID = '11666000000134949911'

FILE_NAME = 'Sameday_Incremental_Device.csv'
FILE_NAME_INCREMENT = 'Sameday_Incremental_Device_Part2.csv'
DEVICE_ID_COL = 'DEVICEID'
DATE_COL = 'READINGDATE'
SIGNAL_COL = 'TEMPERATURE'
PORT_COL = 'PORTID'
PROCESSED_DATE_COL = 'CREATEDATE'

#Given it a slightly 
MAX_TIME_OVERRIDE = pd.to_datetime('2018-03-26 21:00:00')

database.init('test_ta_utils')


class ManyDeviceSignalsTestCase(unittest.TestCase):
    def setUp(self):
        drop_tables()
        create_tables()

    def tearDown(self):
        drop_tables()

    def create_initial_signals(self, data):
        many_device_signals = ManyDeviceSignals(data, DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)

        many_device_signals.update_all_signals(SIGNAL_COL, segment_daily=True)

        last_signal_segments = (PersistableSignalSegment
                                .select()
                                .order_by(PersistableSignalSegment.end_time.desc())
                                .limit(2))

        last_signal_segment, second_last_signal_segment = tuple(
            last_signal_segments)

        self.assertEqual(
            second_last_signal_segment.max_time,
            last_signal_segment.min_time)
        self.assertGreater(
            last_signal_segment.min_time,
            second_last_signal_segment.min_time)
        self.assertGreater(
            last_signal_segment.max_time,
            last_signal_segment.min_time)
        self.assertEqual(1, last_signal_segment.coverage)
        self.assertEqual(1, last_signal_segment.on_time)
        # self.assertGreaterEqual(last_signal_segment.coverage, last_signal_segment.online)
        # self.assertNotEqual(last_signal_segment.start_time, second_last_signal_segment.start_time)
        self.assertFalse(second_last_signal_segment.final_segment)
        self.assertTrue(last_signal_segment.final_segment)
        # self.assertNotEqual(second_last_signal_segment.final_segment, True)
        # self.assertNotEqual(last_signal_segment.min_time, second_last_signal_segment.min_time)
        # self.assertNotEqual(0.5, last_signal_segment.coverage)
        # self.assertNotEqual(0.5, last_signal_segment.online)

        return last_signal_segment, second_last_signal_segment

    def increment_signal(self, data, incremental_data, last_signal_segment, second_last_signal_segment):
        many_device_signals = ManyDeviceSignals(incremental_data, DEVICE_ID_COL, DATE_COL,
                                                processed_date_col=PROCESSED_DATE_COL)

        self.assertGreaterEqual(
            incremental_data[PROCESSED_DATE_COL].min(),
            data[PROCESSED_DATE_COL].max())

        many_device_signals.update_all_signals(SIGNAL_COL, segment_daily=True,
                           max_time_override=MAX_TIME_OVERRIDE,
                           max_processed_time_override=MAX_TIME_OVERRIDE)

        last_signal_segments_inc = (PersistableSignalSegment
                                    .select()
                                    .order_by(PersistableSignalSegment.end_time.desc())
                                    .limit(2))

        last_signal_segment_inc, second_last_signal_segment_inc = tuple(
            last_signal_segments_inc)

        self.assertGreaterEqual(
            second_last_signal_segment_inc.start_time,
            second_last_signal_segment.start_time)
        self.assertGreaterEqual(
            second_last_signal_segment_inc.end_time,
            second_last_signal_segment.end_time)


        self.assertEqual(
            second_last_signal_segment_inc.max_time,
            second_last_signal_segment.max_time)
        self.assertEqual(
            second_last_signal_segment_inc.min_time,
            second_last_signal_segment.min_time)
        self.assertEqual(
            last_signal_segment_inc.min_time,
            last_signal_segment.min_time)
        self.assertGreater(
            last_signal_segment_inc.max_time,
            last_signal_segment.max_time)

        self.assertEqual(
            second_last_signal_segment_inc.max_time,
            last_signal_segment_inc.min_time)
        self.assertGreater(
            last_signal_segment_inc.min_time,
            second_last_signal_segment_inc.min_time)
        self.assertGreater(
            last_signal_segment_inc.max_time,
            last_signal_segment_inc.min_time)
        self.assertEqual(1, last_signal_segment_inc.coverage)
        self.assertEqual(1, last_signal_segment_inc.on_time)
        self.assertFalse(second_last_signal_segment_inc.final_segment)
        self.assertTrue(last_signal_segment_inc.final_segment)
        self.assertNotEqual(0, last_signal_segment_inc.coverage)
        self.assertNotEqual(0, last_signal_segment_inc.online)


    def test_update_all_signals_single_increment(self):
        data = pd.read_csv(FILE_NAME)
        data.loc[:, DEVICE_ID_COL] = data[DEVICE_ID_COL].astype(str)
        last_signal_segment, second_last_signal_segment = self.create_initial_signals(data)

        incremental_data = pd.read_csv(FILE_NAME_INCREMENT)
        incremental_data.loc[:, DEVICE_ID_COL] = incremental_data[DEVICE_ID_COL].astype(
            str)

        self.increment_signal(data, incremental_data, last_signal_segment, second_last_signal_segment)

    def test_update_all_signals_two_increments(self):
        data = pd.read_csv(FILE_NAME)
        data.loc[:, DEVICE_ID_COL] = data[DEVICE_ID_COL].astype(str)
        last_signal_segment, second_last_signal_segment = self.create_initial_signals(data)

        incremental_data = pd.read_csv(FILE_NAME_INCREMENT)
        incremental_data.loc[:, DEVICE_ID_COL] = incremental_data[DEVICE_ID_COL].astype(
            str)
        incremental_data.sort_values(PROCESSED_DATE_COL, inplace=True)

        half_index = int(len(incremental_data) / 2)

        incremental_data_first_half = incremental_data[:half_index]
        incremental_data_second_half = incremental_data[half_index:]

        self.increment_signal(data, incremental_data_first_half, last_signal_segment, second_last_signal_segment)
        self.increment_signal(incremental_data_first_half, incremental_data_second_half, last_signal_segment, second_last_signal_segment)
        
