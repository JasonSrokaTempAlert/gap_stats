"""
Cumbie Analysis Script
"""
import pandas as pd

from ta_utils import logger
from ta_utils import ManyDeviceSignals, PersistableSignal, PersistableSignalSegment
from ta_utils import database
from ta_utils import create_tables, drop_tables

# TODO: change this value if you'd like to reset the table
RESET = False

DEVICE_ID_COL = 'SensorID'
DATE_COL = 'ReadingDate'
READING_COL = 'Reading'
PROCESSED_DATE_COL = 'CreateDate'


SAMPLE_DEVICE = 174640

database.init('test_cumbie')

cumbie_df = pd.read_csv('Data/CumbieTemperatureData.csv', usecols=[DEVICE_ID_COL,
                                                                   DATE_COL,
                                                                   READING_COL,
                                                                   PROCESSED_DATE_COL])

cumbie_device_signals = ManyDeviceSignals(cumbie_df, DEVICE_ID_COL, DATE_COL,
                                          processed_date_col=PROCESSED_DATE_COL)

if RESET:
    drop_tables()
    create_tables()
    cumbie_device_signals.update_all_signals(READING_COL)
    # Now check the database 'test_ta_utils' - it will contain 104 different signals and 395 gaps from those signals

# Get a sample signal from the ManyDeviceSignals object
sample_device_readings = cumbie_device_signals.get_device_signal(SAMPLE_DEVICE, READING_COL)

# Check this signal's coverage
logger.info('Total Signal Coverage: {}'.format(sample_device_readings.coverage))

# Print the daily segments in this signal by showing their min_time and max_time
day_segments = sample_device_readings.segment_daily()
for day_signal in day_segments:
    if day_signal.coverage != 1.0:
        logger.info('Daily Signal Coverage: {}'.format(day_signal.coverage))

if RESET:
    for day_segment in day_segments:
        day_segment.save()

persisted_signal = (PersistableSignal
                    .select()
                    .where((PersistableSignal.device_id == SAMPLE_DEVICE) &
                           (PersistableSignal.data_type == READING_COL))
                    .get())

# This coverage is persisted but it should be identical
logger.info('Persisted Signal Coverage: {}'.format(persisted_signal.coverage))

persisted_signal_segments = (PersistableSignalSegment
                             .select()
                             .where((PersistableSignalSegment.signal == persisted_signal) &
                                    (PersistableSignalSegment.segment_type == 'daily')))

for daily_segment in persisted_signal_segments:
    if daily_segment.coverage != 1.0:
        logger.info('Daily Persisted Signal Segment Coverage: {}'.format(daily_segment.coverage))
