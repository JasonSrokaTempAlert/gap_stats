.. temp_alert_utils documentation master file, created by
   sphinx-quickstart on Thu Dec 15 22:47:03 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TempAlert Utilities Documentation
=================================

TempAlert Utilities is a series of python utilities to enable efficient analysis of signals data within TempAlert.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   db
   data
   persistable
   pipelines
   analysis
   plot
