from functools import wraps
from time import time

import numpy as np
import pandas as pd

def printable_arg(arg):
    if isinstance(arg, int) or isinstance(arg, str):
        return arg
    elif isinstance(arg, np.ndarray) or isinstance(arg, list):
        if len(arg) > 3:
            return list(arg[:2]) + ["... {} more".format(len(arg) - 3)]
        else:
            return arg[:2]
    elif isinstance(arg, pd.DataFrame):
        return "DataFrame[{}]".format(arg.shape)
    else:
        return arg

def timing(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start = time()
        result = f(*args, **kwargs)
        end = time()
        printable_args = [printable_arg(arg) for arg in args]
        printable_kwargs = {key: printable_arg(kwarg) for key, kwarg in kwargs.items()}
        print('func:%r args:[%r, %r] took: %2.4f sec' % \
          (f.__name__, printable_args, printable_kwargs, end - start))
        return result

    return wrapper
