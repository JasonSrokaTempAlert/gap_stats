.. delay:

Delay
=====

.. autoclass:: ta_utils.Delay
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__

****************
PersistableDelay
****************

.. autoclass:: ta_utils.PersistableDelay
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__, __eq__